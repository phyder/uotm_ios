//
//  ESOPChildCell.h
//  Universe on the move
//
//  Created by Phyder on 17/11/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESOPChildCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblOptionsVested;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsExercised;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsLapsed;

- (void) setData:(NSDictionary *) data;

@end
