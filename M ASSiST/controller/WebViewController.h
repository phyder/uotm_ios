//
//  WebViewController.h
//  engage
//
//  Created by Suraj on 12/7/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <Speech/Speech.h>

#import "MBProgressHUD.h"

@class Navigation;

@interface WebViewController : UIViewController<UIWebViewDelegate, MBProgressHUDDelegate, UIAlertViewDelegate, NSURLConnectionDelegate, AVSpeechSynthesizerDelegate,SFSpeechRecognizerDelegate,SFSpeechRecognitionTaskDelegate> {
                                                           
    MBProgressHUD *progressHUD;
    
    NSString *webURL;
    UIWebView *webView;
    UILabel *lblTopBarTitle;
    
    NSDictionary *data, *css, *pageCss;
    
    NSString *page, *urlType, *strURLNotification;
    
    NSURL *loadURL;
//    BOOL _Authenticated;
//    NSURLRequest *_FailedRequest;
}

@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;
@property (strong, nonatomic) SFSpeechRecognizer *recognizer;

@property (nonatomic) Navigation *navigation;

@property (strong, nonatomic) IBOutlet NSString *pdfUrl;
@property (strong, nonatomic) IBOutlet NSString *webURL;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet NSDictionary *data;
@property (strong, nonatomic) IBOutlet NSDictionary *css;
@property (strong, nonatomic) IBOutlet NSString *page;
@property (strong, nonatomic) IBOutlet NSString *urlType;

- (void) webViewGoBack;
- (void) refreshWeb;
- (void) openNotification;

@end
