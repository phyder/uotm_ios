//
//  menuViewCtlr.h
//  M ASSiST
//
//  Created by Suraj on 22/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuViewModel;

@interface MenuListViewCtlr : UITableViewController {
    
    MenuViewModel *menuModel;
}

@end
