//
//  ViewController.m
//  Home Offer
//
//  Created by Suraj on 27/02/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "HomeMapViewCtlr.h"

#import "HomeAnnotation.h"

#import "AppConnect.h"
#import "Constant.h"
#import "Holder.h"
#import "Navigation.h"
#import "Util.h"

static NSString *nextPage = @"home-offer";
static NSString *resusePinIdentifier = @"pinView";

@interface HomeMapViewCtlr ()

@end

@implementation HomeMapViewCtlr

- (void) loadView {
    [super loadView];
    
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    self.imgGreenRecent.image   = [Util getImageAtDocDirWithName:@"pinGreen.png"];
    self.imgGreenPrevious.image = [Util getImageAtDocDirWithName:@"pinred.png"];
    
    // Initialize the navigation controller
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
    
    //Remove previous annotation, if any
    NSArray *annotations = [self.mapView annotations];
    for (id annotation in annotations) {
        [[self.mapView viewForAnnotation:annotation] removeFromSuperview];
        [self.mapView removeAnnotation:annotation];
    }
    
    // Fetch offers from server
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(getLocationDataFinished:) onObject:self];
    [conn getJson];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //additional setup before view is loaded
    
    [self.navigation checkHeaderAndFooter];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.delegate = self;
    self.mapView.zoomEnabled = YES;
    self.mapView.scrollEnabled = YES;
    self.mapView.showsUserLocation = YES;
    
    //Center of the India is considered Nagpur
    MKCoordinateRegion center = {{0,0}, {0,0}};
    // Nagpur is set at center of India
    center.center.latitude  = 21.1258071;
    center.center.longitude = 82.7791753;
    //Span or BOX or amount of area visible within map
    center.span.latitudeDelta  = 25.0f;
    center.span.longitudeDelta = 25.0f;
    
    [_mapView setRegion:center animated:YES];
}

- (void)viewDidUnload {
    [self setLblRecentOffer:nil];
    [self setLblOtherOffer:nil];
    [self setNavigation:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Location Update
- (void) getLocationDataFinished:(NSDictionary *) resData {
    
    // Plot the location on map
    //NSLog(@"Map Data: %@", resData);
    if ([Util serverResponseCheckNullAndSession:resData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:resData InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            NSArray *data = resData[@"data"];
            if (data.count <= 0) {
                UIAlertView *alertNoOffer = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                       message:[Util getSettingUsingName:DYNA_LIST_NODATA_MSG]
                                                                      delegate:self
                                                             cancelButtonTitle:ALERT_OPT_OK
                                                             otherButtonTitles:nil];
                [alertNoOffer show];
            } else {
                [self plotGeoData:data forMap:self.mapView];
            }
        }
    }
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_OPT_OK]) {
        [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
    }
}

/**
 * => Selectors
 */
- (void) plotGeoData:(NSArray *) data forMap:(MKMapView *) mapview  {
    
    NSNumber *latitude, *longitude;
    NSString *ID, *title, *subTitle, *recentCount;
    CLLocationCoordinate2D coordinate;
    
    //NSLog(@"Location Data: %@", data);
    for (NSDictionary * row in data) {
        
        ID = row[MAP_RES_ID];
        latitude    = row[MAP_RES_LAT];
        longitude   = row[MAP_RES_LNG];
        title       = [NSString stringWithFormat:@"%@ - %@", row[MAP_RES_CITY], row[MAP_RES_STATE]];
        subTitle    = [Util getSettingUsingName:POPUP_TITLE];
        recentCount = row[MAP_RES_RECENT];
        
        coordinate.latitude = latitude.doubleValue;
        coordinate.longitude = longitude.doubleValue;
        
        HomeAnnotation *annotation = [[HomeAnnotation alloc] initWithViewController:self
                                                                                 ID:ID
                                                                              Title:title
                                                                           subtitle:subTitle
                                                                        recentCount:recentCount
                                                                         coordinate:coordinate];
        [mapview addAnnotation:annotation];
    }
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:resusePinIdentifier];
    
    // Does not modify the user location annotation.
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
//        NSLog(@"User location annotation is not modified.");
        pinView.annotation = annotation;
        return pinView;
    }
    
    if (!pinView) {
        
        HomeAnnotation *annotationObj = (HomeAnnotation *) annotation;
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:resusePinIdentifier];
        
        if ([annotationObj getRecentOfferCount] > 0) {
            pinView.pinColor = MKPinAnnotationColorGreen;
            //pinView.image = self.imgGreenRecent.image;
        } else {
            pinView.pinColor = MKPinAnnotationColorRed;
            //pinView.image = self.imgGreenPrevious.image;
        }
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        
        pinView.rightCalloutAccessoryView = annotationObj.detailButton;
    } else {
        pinView.annotation = annotation;
    }
    return pinView;
}

//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
//    NSLog(@"ok");
//}

#pragma mark - Notification Handler
- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

@end
