//
//  OtpViewController.h
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OTPModel;

@interface OtpViewController : UIViewController<UITextFieldDelegate> {
    
    BOOL slide;
    
    OTPModel *otpPage;
}

@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgOtpBackground;
@property (strong, nonatomic) IBOutlet UITextField *txtOtp;

@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;

- (IBAction)btnClearPressed:(id)sender;

@end
