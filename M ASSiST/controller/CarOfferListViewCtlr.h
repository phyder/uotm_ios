//
//  OfferListViewCtlr.h
//  Universe on the move
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Navigation;

@interface CarOfferListViewCtlr : UITableViewController<UITableViewDataSource, UITableViewDelegate> {
    
    Navigation *navigation;
    NSString *notificationURL;
    NSDictionary *listCSS;
}

@property (strong, nonatomic) NSString *brandName;
@property (strong, nonatomic) NSArray *data;

- (void) openNotification;

@end
