//
//  DetailPageViewCtlr.h
//  Home Offer
//
//  Created by Suraj on 05/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

#define OFFER_HOME  1
#define OFFER_CAR   2
#define OFFER_OTHER 3

@class Navigation;
@class OtherOfferDetail;

@interface OtherDetailPageViewCtlr : UITableViewController {
    
    NSString *notificationURL;
}

@property (nonatomic) Navigation *navigation;
@property (nonatomic) OtherOfferDetail *modelOtherOffer;

@property (nonatomic) IBOutlet NSDictionary *data;
@property (nonatomic) int offerType;
@property (nonatomic) IBOutlet UILabel *lblSubTitle;

- (void) openNotification;
- (NSString *) getOfferId;

@end
