//
//  VerifyMPinViewCtlr.h
//  Universe on the move
//
//  Created by Phyder on 22/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Navigation;

@interface VerifyMPinViewCtlr : UIViewController <UITextFieldDelegate,UIGestureRecognizerDelegate> {
    
    BOOL slide;
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button
}

@property (nonatomic) UIViewController *controller;
@property (nonatomic) NSObject *model;
@property (nonatomic) NSInteger index;

@property (strong, nonatomic) IBOutlet UITextField *txtMPin1;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin2;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin3;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin4;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIView *viewPopup;

- (IBAction)txtFieldMPin1Didchange:(id)sender;
- (IBAction)txtFieldMPin2Didchange:(id)sender;
- (IBAction)txtFieldMPin3Didchange:(id)sender;
- (IBAction)txtFieldMPin4Didchange:(id)sender;

- (IBAction)btnVerifyClick:(id)sender;

@end
