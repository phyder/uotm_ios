//
//  GridViewCtlr.h
//  Universe on the move
//
//  Created by Suraj on 29/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PSTCollectionView.h"

@class GridViewCell;
@class GridViewModel;
@class Navigation;
@interface GridViewCtlr : PSUICollectionViewController<PSTCollectionViewDataSource, PSTCollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate> {
    
    GridViewCell *cell;
    GridViewModel *gridViewModel;
}

@property (nonatomic) Navigation *navigation;
@property (nonatomic, strong) IBOutlet UIImageView *imgBgImage;

@property (nonatomic, strong) IBOutlet NSString *nextPage;
@property (nonatomic, strong) IBOutlet NSString *nextTitle;


@property (nonatomic, strong) IBOutlet NSMutableDictionary *content;

- (void) openNotification;

@end
