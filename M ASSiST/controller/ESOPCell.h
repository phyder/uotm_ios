//
//  ESOPCell.h
//  Universe on the move
//
//  Created by Phyder on 28/10/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESOPCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDateGrant;
@property (weak, nonatomic) IBOutlet UILabel *lblExercisePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalOptions;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsOutstanding;


@property (nonatomic) BOOL isExpanded;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

- (void)setData:(NSDictionary *) data;
- (void)setState:(BOOL)state;

@end
