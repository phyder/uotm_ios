//
//  ListViewCtlr.h
//  M ASSiST
//
//  Created by Suraj on 24/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ListViewModel;

@interface ListViewCtlr : UITableViewController {
    
    NSMutableArray *data;
    NSString *listType;
    
    ListViewModel *listModel;
    UIRefreshControl *refreshControl;
    
    NSArray *footer;
    UIBarButtonItem *btnBarEdit;
}

@property (strong, nonatomic) IBOutlet NSString *page;
@property (strong, nonatomic) IBOutlet NSString *jsonURL;
@property (strong, nonatomic) IBOutlet NSDictionary *jsonData;
@property (strong, nonatomic) IBOutlet NSMutableDictionary *content;

- (void) openNotification;

- (void) refreshList;
- (void) reloadTable;

- (void) editButtonSelected: (id) sender;
- (UIBarButtonItem *) editButtonWithState:(BOOL)isSelected;

- (void) responseMultipleApproveReject:(NSDictionary *)response;

// Generate instance for holiday calendar
- (UIBarButtonItem *)holidayButton;

@end
