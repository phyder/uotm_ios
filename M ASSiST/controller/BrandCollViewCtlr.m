//
//  BrandCollViewCtlr.m
//  Universe on the move
//
//  Created by Suraj on 16/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "BrandCollViewCtlr.h"

#import "AppConnect.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Navigation.h"
#import "Util.h"

#import "BrandCollViewCell.h"
#import "CarOfferListViewCtlr.h"

static NSString *CellIdentifier = @"brandcell";

@interface BrandCollViewCtlr ()

@end

@implementation BrandCollViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    
    // Custom initialization
    NSString *nextPage = @"car-offer";
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    //NSLog(@"page-name: %@, data: %@", nextPage, nextPageContent);
    
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getCarOfferBrandAfterFinishCall:@selector(offerBrandListFinished:) onObject:self];
    self.imgBackground.image = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    // Initialize the navigation controller
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

- (void) offerBrandListFinished: (NSDictionary *) resData {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    if ([Util serverResponseCheckNullAndSession:resData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:resData InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            brandData = resData[@"data"];
            if (brandData.count <= 0) {
                UIAlertView *alertNoOffer = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                       message:[Util getSettingUsingName:DYNA_LIST_NODATA_MSG]
                                                                      delegate:self
                                                             cancelButtonTitle:ALERT_OPT_OK
                                                             otherButtonTitles:nil];
                [alertNoOffer show];
            } else {
                [self.collectionView reloadData];
            }
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PSTCollectionView
- (NSString *)formatIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"{%ld,%ld}", (long)indexPath.row, (long)indexPath.section];
}

- (NSInteger)numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [brandData count];
}

- (PSUICollectionViewCell *)collectionView:(PSUICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"Data: %@", [brandData objectAtIndex:indexPath.row]);
    BrandCollViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell initWithData:brandData[indexPath.row]];
    
    return cell;
}

- (void)collectionView:(PSTCollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BrandCollViewCell *selCell  = (BrandCollViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    selCell.backgroundColor     = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
}

- (void)collectionView:(PSTCollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BrandCollViewCell *selCell  = (BrandCollViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    selCell.backgroundColor     = [UIColor clearColor];
}

- (void)collectionView:(PSTCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"Brand data: %@", [brandData objectAtIndex:indexPath.row]);
//    [conn getCarOfferListForOfferId:[[brandData objectAtIndex:indexPath.row] objectForKey:CAR_OFFER_BRAND_ID]
//                    AfterFinishCall:@selector(offerCarListFinished:) onObject:self];
    NSString *unsafeString  = brandData[indexPath.row][CAR_OFFER_BRAND_NAME];
    NSString *safeString    = [unsafeString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getCarOfferListForBrandId:brandData[indexPath.row][CAR_OFFER_BRAND_ID]
                          BrandName:safeString
                    AfterFinishCall:@selector(offerCarListFinished:)
                           onObject:self];
//    NSLog(@"Delegate cell %@ : SELECTED offer id: %@", [self formatIndexPath:indexPath], [[brandData objectAtIndex:indexPath.row] objectForKey:@"id"]);
}

- (void) offerCarListFinished: (NSDictionary *) resListData {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    
    if ([Util serverResponseCheckNullAndSession:resListData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:resListData InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            
            @try {
                carListData = resListData[@"data"];
                //NSLog(@"Cars list Response data: %@", resListData);//
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                CarOfferListViewCtlr *offerListViewCtlr = (CarOfferListViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carofferlist"];
                offerListViewCtlr.brandName = resListData[@"brand"];
                offerListViewCtlr.data = resListData[@"data"];
                //[self.navigationController pushViewController:offerListViewCtlr animated:YES];
                [Util pushWithFadeEffectOnViewController:self ViewController:offerListViewCtlr];
            } @catch (NSException *exception) {
                NSString *strLogMessage = @"Problem opening Car List View";
#if DEBUG_ERROR_LOG
                NSLog(@"%@", strLogMessage);
#endif
                [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
            }
        }
    }
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_OPT_OK]) {
        [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
    }
}

@end
