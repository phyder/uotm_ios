//
//  ToolsViewCtlr.h
//  Universe on the move
//
//  Created by Phyder on 21/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceListCell.h"

@class Navigation;

@interface ToolsViewCtlr : UIViewController <UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, DeregisterDeviceDelegate> {
    
    BOOL slide;
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button

}

- (void) resDeregister:(NSMutableDictionary *) resDeregister;
@property (weak, nonatomic) IBOutlet UIButton *btnResetMPin;
@property (weak, nonatomic) IBOutlet UIButton *btnDeRegister;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (nonatomic) Navigation *navigation;
@property (weak, nonatomic) IBOutlet UIImageView *imgResetMPin;
@property (weak, nonatomic) IBOutlet UIImageView *imgDeregister;
@property (weak, nonatomic) IBOutlet UILabel *lblResetMPin;
@property (weak, nonatomic) IBOutlet UILabel *lblDeregister;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UITableView *tableDeviceList;

- (IBAction)btnResetMPinClick:(id)sender;
- (IBAction)btnDeregisterClick:(id)sender;
@end
