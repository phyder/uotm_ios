//
//  DeviceListCell.h
//  Universe on the move
//
//  Created by Phyder on 27/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DeregisterDeviceDelegate <NSObject>

- (void) deregisterDeviceWithInfo:(NSDictionary *)info;

@end

@interface DeviceListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDeviceName;
@property (weak, nonatomic) IBOutlet UIButton *btnDeregister;
@property (weak, nonatomic) id <DeregisterDeviceDelegate> delegate;

- (IBAction)btnDeregisterClick:(id)sender;
- (void) setCellData:(NSDictionary *)data;
@end
