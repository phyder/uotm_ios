//
//  ListViewCtlr.h
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeListData.h"
#import "HomeListViewCell.h"


@class MBProgressHUD;
@class Navigation;
@class HomeListData;

@interface HomeListViewCtlr : UITableViewController {
    
    NSString *notificationURL;
    
    HomeListData *list;
    MBProgressHUD *progressBar;
    NSDictionary *offerDetail;
    NSDictionary *listCSS;
}

@property (nonatomic) Navigation *navigation;

//@property (weak, nonatomic) NSThread *backgroundThread;
@property (weak, nonatomic) IBOutlet NSString *title;
@property (weak, nonatomic) IBOutlet NSArray *listData;

- (void) openNotification;;

@end
