//
//  ListViewCtlr.m
//  M ASSiST
//
//  Created by Suraj on 24/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "ListViewCtlr.h"

#import "ListViewModel.h"

#import "Constant.h"
#import "Holder.h"
#import "Navigation.h"
#import "Util.h"

#import "ListViewCell.h"

@interface ListViewCtlr ()

@end

@implementation ListViewCtlr

- (id)initWithStyle:(UITableViewStyle)style {
    
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    [Holder sharedHolder].testingNoti = false;
    // custom initialzation during view load
//    if (![_page isEqualToString:@"emergency-number"]) {
//        self.tableView.backgroundColor      = [UIColor colorWithPatternImage:[Util getImageAtDocDirWithName:@"login-view-bg.png"]];
//    }
   
    
    listModel = [[ListViewModel alloc] initDataForView:self WithPage:_page AndWithData:_content];
    if ([_content[LIST_TYPE] isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        //
        [listModel initDynaListWithURL:_jsonURL Data:_jsonData];
    }
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount:) name:@"updateNotificationCountList" object:nil];
//    self.refreshControl = [[UIRefreshControl alloc] init];
//    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
//    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Refreshing"]];
//    [self.tableView addSubview:self.refreshControl];
}

- (void)updateNotificationCount:(NSNotification*)notification {
    [listModel updateNotificationCount];
}

- (void)refreshView:(UIRefreshControl *)sender {
    //For demonstration purposes just add one row
    //set the title while refreshing
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing the TableView"];
    //set the date and time of refreshing
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, h:mm:ss a"];
    NSString *lastupdated = [NSString stringWithFormat:@"Last Updated on %@",[dateFormatter stringFromDate:[NSDate date]]];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:lastupdated];
    //end the refreshing
    [sender endRefreshing];
    //and of course reload
    [self.tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /**
     * This code is resposible to refresh the dyna-list when user comes back from web view
     */
    
    
    if ([[Holder sharedHolder].backFlag isEqualToString:PAGE_TRUE]) {
        
        [listModel viewDidAppear];
        [Holder sharedHolder].backFlag = PAGE_FALSE;
    }
    [self.tableView flashScrollIndicators];
    
    [listModel updatePendingApprovalCount];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {

    // Make sure you call super first
    [super setEditing:editing animated:animated];
    
    if (editing) {
//        footer = self.toolbarItems;
//        SEL arr[] = {@selector(footerButtons:)};
//        self.toolbarItems = [Navigation dynalistFooterWithController:self AndSelector:arr];
//        listModel.isSelected = NO;
    } else {
//        self.toolbarItems = footer;
    }
}

- (void) editButtonSelected: (id) sender {
    
    if (self.tableView.editing) {
        [self setEditing:NO animated:YES];
        self.toolbarItems = footer;
    } else {
        [self setEditing:YES animated:YES];
        footer = self.toolbarItems;
        SEL arr[] = {@selector(footerButtons:)};
        self.toolbarItems = [Navigation dynalistFooterWithController:self AndSelector:arr];
        listModel.isSelected = NO;
    }
}

- (void) footerButtons:(id)sender {
    
    UIBarButtonItem *btnFooter = (UIBarButtonItem *) sender;
    if (btnFooter.tag == 101) {
        [listModel selectAll:sender];
    } else if (btnFooter.tag == 102) {
        [listModel approve:sender];
    } else if (btnFooter.tag == 103) {
        [listModel reject:sender];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [listModel getRowCount];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return [listModel heightForHeader];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return [listModel loadSearchBar];
}

- (void) reloadTable {
    
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [listModel getHeightOfCellAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"listviewcell";
    ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[ListViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    [listModel loadCell:cell AtIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return [listModel heightForFooter];
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return [listModel loadLegendBar];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Editing: %@", (self.isEditing)?@"YES":@"NO");
    if (!self.isEditing) {
        // Navigation logic may go here. Create and push another view controller.
        [listModel tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void) finishedLoadingDynalistData: (NSDictionary *) resDynalist {
    
    //This function is not called on List, called fron grid
    [listModel dynalistDataLoad:resDynalist];
}

- (void) refreshedDynalistData:(NSDictionary *) resDynalist {
    
    if ([Util serverResponseCheckNullAndSession:resDynalist InFunction:__FUNCTION__ Silent:NO]) {
        NSDate *start = [NSDate date];
        
        [listModel dynalistDataRefresh:resDynalist];
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
        NSString *msg = [NSString stringWithFormat:@"Rendering Execution Time: %f", executionTime];
        NSLog(@"%@", msg);
        
        if (SHOW_EXECUTION_TIME) {
            UIAlertView *exeTimeAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:MOBILE4U_URL]
                                                                   message:msg
                                                                  delegate:nil
                                                         cancelButtonTitle:ALERT_OPT_OK
                                                         otherButtonTitles:nil, nil];
            [exeTimeAlert show];
        }
    }
    
    // Updates the Pending Approval Count if list count changes
    [listModel updatePendingApprovalCount];
}

- (void) openNotification {
    
    [listModel openNotification];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [listModel finishedLoadingNotificationData:resNotificationlist];
}

- (void) refreshList {
    
    [listModel refreshDynalist];
}

- (UIBarButtonItem *) editButtonWithState:(BOOL)isSelected {
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.tableView.allowsSelectionDuringEditing = NO;
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    UIImage *imgNormal = [Util getImageAtDocDirWithName:@"tool_edit.png"];
    UIImage *imgSelected = [Util getImageAtDocDirWithName:@"tool_done.png"];
    UIButton *btnNavEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    if (self.isEditing) {
        btnNavEdit.frame = CGRectMake(0, 0, imgSelected.size.width, imgSelected.size.height);
    } else {
        btnNavEdit.frame = CGRectMake(0, 0, imgNormal.size.width, imgNormal.size.height);
    }
    [btnNavEdit setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [btnNavEdit setBackgroundImage:imgSelected forState:UIControlStateHighlighted];
    [btnNavEdit setBackgroundImage:imgSelected forState:UIControlStateSelected];
    [btnNavEdit addTarget:self action:@selector(editButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    if (isSelected) {
        [btnNavEdit setSelected:YES];
    }
    btnBarEdit = [[UIBarButtonItem alloc] initWithCustomView:btnNavEdit];
    
    return btnBarEdit;
}

- (void) responseMultipleApproveReject:(NSDictionary *)response {
    
    [listModel responseMultipleApproveReject:response];
}

- (UIBarButtonItem *)holidayButton {
    
    UIImage *imgOtherCalendar = [Util getImageAtDocDirWithName:@"othercalendar.png"];
    UIButton *btnBottombarOtherCalendar = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBottombarOtherCalendar setImage:imgOtherCalendar forState:UIControlStateNormal];
    btnBottombarOtherCalendar.frame = CGRectMake(0, 0, imgOtherCalendar.size.width, imgOtherCalendar.size.height);
    [btnBottombarOtherCalendar addTarget:self action:@selector(otherHolidayCalendarPressed) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:btnBottombarOtherCalendar];
}

- (void)otherHolidayCalendarPressed {
    
    NSString *strPageName = @"other-holiday-web";
    NSDictionary *dicNextPageData = [Util getPage:strPageName];
    [Util OpenWebWithNextPage:strPageName AndParam:dicNextPageData[PAGE_PARAMS] OnView:self];
}

@end
