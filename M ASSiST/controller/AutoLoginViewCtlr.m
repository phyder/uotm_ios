//
//  AutoLoginViewCtlr.m
//  Universe on the move
//
//  Created by Phyder on 20/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "AutoLoginViewCtlr.h"
#import "Util.h"
#import "AutoLoginModel.h"
#import "Constant.h"
#import "MenuListViewCtlr.h"
#import "IIViewDeckController.h"
#import "Holder.h"
#import "ResetMPinViewCtlr.h"

@interface AutoLoginViewCtlr ()

@end

@implementation AutoLoginViewCtlr

- (void) loadView {
    
    [super loadView];
     //[Holder sharedHolder].notificationId = [Util getKeyChainNotificationId];
    _imgLoginBackground.image   = [Util getImageAtDocDirWithName:@"loginbox.png"];
    _imgBackground.image        = [Util getImageAtDocDirWithName:@"login-view-bg.png"];

    autologinPage                   = [[AutoLoginModel alloc] init];
    [autologinPage initDataWithView:self];
    
    NSArray *lists              = [autologinPage getPageData][PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list      = lists[i];
        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
            controlData         = list;
        }
    }
    
    _logo.image = [Util getImageAtDocDirWithName:controlData[@"img"]];
    
    [self initUsername];
    [self initPassword];
    
    self.txtUserName.text = [Util getKeyChainUserName];
    NSString *mpin = [Util getKeyChainMPin];
    
//    NSString *ch = [mpin substringWithRange:NSMakeRange(0, 1)];
//    self.txtMPin1.text = ch;
//    ch = [mpin substringWithRange:NSMakeRange(1, 1)];
//    self.txtMPin2.text = ch;
//    ch = [mpin substringWithRange:NSMakeRange(2, 1)];
//    self.txtMPin3.text = ch;
//    ch = [mpin substringWithRange:NSMakeRange(3, 1)];
//    self.txtMPin4.text = ch;
    
    //[loginPage initNavControlWithUser:_txtUserName MPin1:_txtMPin1 MPin2:_txtMPin2 MPin3:_txtMPin3 MPin4:_txtMPin4 Emergency:_btnEmergency SignIn:_btnSignIn ResetMPin:_btnResetMPin];
    
    //[self signIn];
    
    //    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
    //    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
    //                                                     message:[[NSUUID UUID] UUIDString]
    //                                                    delegate:nil
    //                                           cancelButtonTitle:@"Ok"
    //                                           otherButtonTitles:nil];
    //    [device show];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
   
    NSLog(@"This is [Holder sharedHolder] notification Id %@",[Holder sharedHolder].notificationId);
    //Custom Initialization
    [autologinPage initNavControlWithUser:_txtUserName MPin1:_txtMPin1 MPin2:_txtMPin2 MPin3:_txtMPin3 MPin4:_txtMPin4 Emergency:_btnEmergency SignIn:_btnSignIn ResetMPin:_btnResetMPin];
    
    //[self signIn];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"Scale: %f", [UIScreen mainScreen].scale);
    NSLog(@"Bounds: %@", NSStringFromCGRect([UIScreen mainScreen].bounds));
    //    self.txtUserName.text = @"250282";
    //    self.txtPassword.text = @"march@2015";
    
    /*NSString *appLocalVersion = [Util getSettingUsingName:APP_LOCAL_VERSION];
     NSLog(@"App local version: %@", appLocalVersion);
     
     if (appLocalVersion == (id) [NSNull null] || [appLocalVersion length] == 0 || [appLocalVersion isEqualToString:@""]) {
     [_txtUserName resignFirstResponder];
     [Util clearCacheWithViewController:self];
     [ResourceCheck copyAssets];
     } else {
     float fltAppLocalVersion = [appLocalVersion floatValue];
     float fltAppVersion  = [[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] floatValue];
     if (fltAppVersion != fltAppLocalVersion) {
     [_txtUserName resignFirstResponder];
     [Util clearCacheWithViewController:self];
     [ResourceCheck copyAssets];
     }
     }*/
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) clearLoginCredentials {
    [autologinPage clearLoginCredentials];
}

- (void) initUsername {
    
    NSString *strLastLoginUserId = [Util getSettingUsingName:LAST_LOGIN_USER_ID];
    NSLog(@"Last Login User: %@", strLastLoginUserId);
    
    if (strLastLoginUserId != nil && ![strLastLoginUserId isEqualToString:@""]) {
        _txtUserName.text = strLastLoginUserId;
    }
    
    //_txtUserName.textColor = [Util colorWithHexString:@"E9B472"];
    [_txtUserName setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"user.png"];
    [paddingView addSubview:userimage];
    _txtUserName.leftView               = paddingView;
    _txtUserName.leftViewMode           = UITextFieldViewModeAlways;
    _txtUserName.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtUserName.borderStyle            = UITextBorderStyleNone;
    _txtUserName.layer.cornerRadius     = 4.0f;
    _txtUserName.layer.masksToBounds    = YES;
    _txtUserName.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtUserName.layer.borderWidth      = 1.0f;
    _txtUserName.delegate               = self;
    // Custom place holder color txtUserName.placeholder
    //[_txtUserName drawPlaceholderInRect:_txtUserName.frame];
//    if ([_txtUserName.text isEqualToString:@""]) {
//        [_txtUserName becomeFirstResponder];
//    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
//    if (textField == _txtUserName) {
//        [_txtPassword becomeFirstResponder];
//    } else if (textField == _txtPassword) {
//        [_txtPassword resignFirstResponder];
//        [self signIn];
//        
//    }
    return YES;
}

- (void) initPassword {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtMPin1.delegate               = self;
    _txtMPin2.delegate               = self;
    _txtMPin3.delegate               = self;
    _txtMPin4.delegate               = self;
    
    [_txtMPin1 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_txtMPin2 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_txtMPin3 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_txtMPin4 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *leftPaddingView             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"pass.png"];
    [leftPaddingView addSubview:userimage];
    
    //Single TextField
    _txtMPin1.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtMPin2.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtMPin3.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtMPin4.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    
    _txtMPin1.layer.borderWidth      = 1.0f;
    _txtMPin2.layer.borderWidth      = 1.0f;
    _txtMPin3.layer.borderWidth      = 1.0f;
    _txtMPin4.layer.borderWidth      = 1.0f;
    
    _txtMPin1.layer.cornerRadius     = 4.0f;
    _txtMPin2.layer.cornerRadius     = 4.0f;
    _txtMPin3.layer.cornerRadius     = 4.0f;
    _txtMPin4.layer.cornerRadius     = 4.0f;
    
    _txtMPin1.returnKeyType = UIReturnKeyNext;
    _txtMPin2.returnKeyType = UIReturnKeyNext;
    _txtMPin3.returnKeyType = UIReturnKeyNext;
    _txtMPin4.returnKeyType = UIReturnKeyNext;
    
    _txtMPin1.keyboardType = UIKeyboardTypeNumberPad;
    _txtMPin2.keyboardType = UIKeyboardTypeNumberPad;
    _txtMPin3.keyboardType = UIKeyboardTypeNumberPad;
    _txtMPin4.keyboardType = UIKeyboardTypeNumberPad;
    
    _txtMPin1.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtMPin2.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtMPin3.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtMPin4.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
}

#pragma mark - UITextFieldDelegate
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    // Transform the views to the normal view
    [self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self resetViewToIdentityTransform];
}

#pragma mark - Keyboard Slider
- (void) maintainVisibityOfControl: (UITextField *) textField {
    
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    //double visibleHeight = bounds.size.height-216;
    
    double fieldYPosition = textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}

- (IBAction)btnClearPressed:(id)sender {
    [self hideKeyboard];
}

- (void)clearPassword {

}

- (IBAction)txtFieldMPin1Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldMPin2Didchange:(id)sender {
    
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldMPin3Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldMPin4Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (void) textFieldDidChange: (UITextField *) textfield {
    NSString *text = textfield.text;
    /* get first char */
    NSString *firstChar = [[NSString alloc] init];
    if ([text length]>0) {
        firstChar = [text substringToIndex:1];
    }
    
    if ([textfield isEqual:self.txtMPin1]) {
        
        if ([text length] >= 1) {
            self.txtMPin1.text = firstChar;
            [self.txtMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtMPin2]) {
        
        if ([text length] >= 1) {
            self.txtMPin2.text = firstChar;
            [self.txtMPin3 becomeFirstResponder];
        } else {
            [self.txtMPin1 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtMPin3]) {
        if ([text length] >= 1) {
            self.txtMPin3.text = firstChar;
            [self.txtMPin4 becomeFirstResponder];
        } else {
            [self.txtMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtMPin4]) {
        if ([text length] >= 1) {
            self.txtMPin4.text = firstChar;
            [self.txtMPin4 resignFirstResponder];
            if ((![self.txtMPin3.text isEqualToString:@""]  && ![self.txtMPin2.text isEqualToString:@""] && ![self.txtMPin1.text isEqualToString:@""])  ) {
                [autologinPage signIn];
            }
            //[self.view endEditing:YES];
            
        } else {
            [self.txtMPin3 becomeFirstResponder];
        }
    }
}

- (void) hideKeyboard {
    [_txtUserName resignFirstResponder];
    [_txtMPin1 resignFirstResponder];
    [_txtMPin2 resignFirstResponder];
    [_txtMPin3 resignFirstResponder];
    [_txtMPin4 resignFirstResponder];
}

#pragma mark - Navigation Slider
- (void) initPanMenuListController {
    
    NSLog(@"%s", __FUNCTION__);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MenuListViewCtlr *menuListViewCtlr = [storyboard instantiateViewControllerWithIdentifier:@"menuview"];
    
    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    //deckController.panningMode = IIViewDeckFullViewPanning;
    deckController.leftController = menuListViewCtlr;
}

- (void) initRegisterViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginview"];
    
    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    
    deckController.centerController = initViewController;
}

- (void) initResetViewController {
    
    NSLog(@"%s", __FUNCTION__);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    ResetMPinViewCtlr *resetMPinViewController = [storyboard instantiateViewControllerWithIdentifier:@"resetmpin"];
    
    [self.navigationController pushViewController:resetMPinViewController animated:YES];
    
    //    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    //    //deckController.panningMode = IIViewDeckFullViewPanning;
    //    deckController.leftController = otpViewController;
}

#pragma mark - Sign In
- (void) signIn {
    
    [autologinPage signIn];
}

- (void) resSignIn:(NSMutableDictionary *) resSignIn {

    [autologinPage postAuthenticationValidationWithResData:resSignIn];
}

@end
