//
//  ToolsViewCtlr.m
//  Universe on the move
//
//  Created by Phyder on 21/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "ToolsViewCtlr.h"
#import "Constant.h"
#import "Util.h"
#import "Navigation.h"
#import "ResetMPinViewCtlr.h"
#import "Authentication.h"
#import "Holder.h"
#import "IIViewDeckController.h"

@interface ToolsViewCtlr () {
    NSMutableArray *deviceList;
    DeviceListCell *cell;
    NSDictionary *deregisterData;
}

@end

@implementation ToolsViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    data        = [Util getPage:PAGE_TOOLS];
    css         = [Util getPageCSS:data[PAGE_CSS]];//[Holder sharedHolder].css;
    
    _imgBackground.image        = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    _imgResetMPin.image         = [Util getImageAtDocDirWithName:@"reset-mpin.png"];
    _imgDeregister.image        = [Util getImageAtDocDirWithName:@"deregister.png"];
    
    
    NSArray *lists              = data[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list      = lists[i];
        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
            controlData         = list;
        }
    }
    
    [self initButton];
    self.viewBackground.hidden = YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopup)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [tapRecognizer setDelegate:self];
    [self.viewBackground addGestureRecognizer:tapRecognizer];
    self.tableDeviceList.delegate = self;
    self.tableDeviceList.dataSource = self;
    
    self.viewPopup.layer.borderWidth = 1;
    self.viewPopup.layer.cornerRadius = 5;
    self.viewPopup.layer.borderColor = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    
    self.viewPopup.hidden = YES;
    CGRect frame = self.viewPopup.frame;
    CGFloat bviewHeight = self.view.frame.size.height;
    CGFloat viewHeight = frame.size.height;
    frame.origin.y = (bviewHeight - viewHeight)/2 - 50;
    self.viewPopup.frame = frame;
    self.tableDeviceList.layer.cornerRadius = 5;
    //    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
    //    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
    //                                                     message:[[NSUUID UUID] UUIDString]
    //                                                    delegate:nil
    //                                           cancelButtonTitle:@"Ok"
    //                                           otherButtonTitles:nil];
    //    [device show];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //Custom Initialization
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:data];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initButton {
    
    NSString *strLastLoginUserId = [Util getSettingUsingName:LAST_LOGIN_USER_ID];
    NSLog(@"Last Login User: %@", strLastLoginUserId);
    
    //_txtUserName.textColor = [Util colorWithHexString:@"E9B472"];
    [_btnDeRegister.titleLabel setFont:[UIFont fontWithName:APP_FONT_BOLD size:17]];
    [_btnResetMPin.titleLabel setFont:[UIFont fontWithName:APP_FONT_BOLD size:17]];
    
    [_lblDeregister setFont:[UIFont fontWithName:APP_FONT_BOLD size:15]];
    [_lblResetMPin setFont:[UIFont fontWithName:APP_FONT_BOLD size:15]];
    
//    [_btnResetMPin setTitle:@"Reset MPin" forState:UIControlStateNormal];
//    [_btnDeRegister setTitle:@"Deregister" forState:UIControlStateNormal];
    
    _lblResetMPin.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _lblDeregister.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];

}

#pragma mark - Navigation Slider
- (void) initResetViewController {
    
    NSLog(@"%s", __FUNCTION__);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    ResetMPinViewCtlr *resetMPinViewController = [storyboard instantiateViewControllerWithIdentifier:@"resetmpin"];
    
    [self.navigationController pushViewController:resetMPinViewController animated:YES];
    
    //    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    //    //deckController.panningMode = IIViewDeckFullViewPanning;
    //    deckController.leftController = otpViewController;
}

- (void) initRegisterViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginview"];
    
    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    
    deckController.centerController = initViewController;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [deviceList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"list";
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.delegate = self;
    [cell setCellData:[deviceList objectAtIndex:indexPath.row]];
    
    return cell;
    
}

#pragma mark DeregisterDeviceDelegate
- (void) deregisterDeviceWithInfo:(NSDictionary *)info {
    NSString *message = [NSString stringWithFormat:@"Are you sure? Do you want to Deregister Device %@?",info[@"DVCE_MDL"]];
    
    deregisterData = info;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                      message:message
                                                     delegate:self
                                            cancelButtonTitle:ALERT_OPT_OK
                                            otherButtonTitles:ALERT_OPT_CANCEL, nil];
    [alert show];
}

- (void) resDeregister:(NSMutableDictionary *) resDeregister {
    
    NSLog(@"response: %@", resDeregister);
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                      message:resDeregister[SERVER_RES_MEG]
                                                     delegate:nil
                                            cancelButtonTitle:ALERT_OPT_OK
                                            otherButtonTitles:nil];
    [message show];
    [[Holder sharedHolder].auth postDeregisterAuthenticationWithData:resDeregister];
}

- (void) resGetRegisterList:(NSMutableDictionary *) resGetRegisterList {
    
    NSLog(@"response: %@", resGetRegisterList);
    deviceList = resGetRegisterList[@"list"];
    [[Holder sharedHolder].auth postGetRegisterListAuthenticationWithData:resGetRegisterList];
}

- (void) showRegisterDeviceList {
    [self.tableDeviceList reloadData];
    self.tableDeviceList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.viewBackground.hidden = NO;
    self.viewPopup.hidden = NO;
}

- (void) hidePopup {
    self.viewBackground.hidden = YES;
    self.viewPopup.hidden = YES;
}

- (void) dealloc {
    
    self.navigation = nil;
}

- (IBAction)btnResetMPinClick:(id)sender {
    [self initResetViewController];
}

- (IBAction)btnDeregisterClick:(id)sender {
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self AndModel:nil];
    NSString *userName = [Util getKeyChainUserName];
    [[Holder sharedHolder].auth requestGetRegisterListWithUsername:userName];    
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_OPT_OK]) {
        [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self AndModel:nil];
        NSString *userName = [Util getKeyChainUserName];
        [[Holder sharedHolder].auth requestDeregisterWithUsername:userName deviceId:deregisterData[@"DVCE_ID"]];
    }
}
@end
