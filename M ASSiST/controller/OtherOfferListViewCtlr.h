//
//  OfferListViewCtlr.h
//  Other Offer
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Navigation.h"

@interface OtherOfferListViewCtlr : UITableViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    
    NSString *notificationURL;
    
    NSArray *data;
    NSDictionary *listCSS;
}

@property (nonatomic) Navigation *navigation;

- (void) openNotification;

@end
