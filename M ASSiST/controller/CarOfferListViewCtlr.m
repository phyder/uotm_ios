//
//  OfferListViewCtlr.m
//  Universe on the move
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "CarOfferListViewCtlr.h"

#import "AppConnect.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Navigation.h"
#import "Util.h"

#import "CarDetailPageViewCtlr.h"
#import "OtherDetailPageViewCtlr.h"
#import "CarOfferListCell.h"

@interface CarOfferListViewCtlr ()

@end

@implementation CarOfferListViewCtlr

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    
    // init View
    //[self initControl];
    NSString *nextPage = @"car-offer-list";
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    listCSS = nextPageContent[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    //NSLog(@"page-name: %@, data: %@", nextPage, nextPageContent);
    /**
     * Initialize the navigation controller
     */
    navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
    [navigation setHeaderTitle:self.brandName];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;   // Return the number of sections.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_data count];   // Return the number of rows in the section.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height = [CarOfferListCell getHeightForCellAt:indexPath
                                               withData:_data[indexPath.row]
                                                 AndCSS:listCSS];
    //NSLog(@"height: %f", height);
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"offer-list-cell";
    CarOfferListCell *cell = (CarOfferListCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    [cell loadCellWithData:_data[indexPath.row]
                    AndCSS:listCSS];
    //NSLog(@"Cell %@ called for cell at indexpath: %ld", cell, (long)indexPath.row);
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getOfferDetailsForOfferId:_data[indexPath.row][CAR_OFFER_LIST_ID] AfterFinishCall:@selector(offerDeatilsFinishedWithData:) onObject:self];
}

#pragma mark - Offer Finish
- (void) offerDeatilsFinishedWithData:(NSDictionary *) resData {
    
    if ([Util serverResponseCheckNullAndSession:resData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:resData InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            
            @try {
                //NSLog(@"response received with data: %@", resData);
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                OtherDetailPageViewCtlr *detailViewCtlr = (OtherDetailPageViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"otherofferdetailpage"];
                detailViewCtlr.data = resData[@"data"];
                detailViewCtlr.offerType = OFFER_CAR;
                [Util pushWithFadeEffectOnViewController:self ViewController:detailViewCtlr];
                
//                CarDetailPageViewCtlr *detailViewCtlr = (CarDetailPageViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carofferdetailpage"];
//                detailViewCtlr.data = resData[@"data"];
//                //[self.navigationController pushViewController:detailViewCtlr animated:YES];
//                [Util pushWithFadeEffectOnViewController:self ViewController:detailViewCtlr];
            } @catch (NSException *exception) {
                NSString *strLogMessage = @"";
#if DEBUG_ERROR_LOG
                NSLog(@"%@", strLogMessage);
#endif
                [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
                [Util showTechnicalError];
            }
        }
    }
}

@end
