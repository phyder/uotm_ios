//
//  CarouselViewCtlr.m
//  M ASSiST
//
//  Created by Suraj on 27/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "CarouselViewCtlr.h"

#import "ReflectionView.h"

#import "AppConnect.h"
#import "Constant.h"
#import "Navigation.h"
#import "Holder.h"
#import "Util.h"
#import "ToolsViewCtlr.h"
#import "IIViewDeckController.h"
#import "ESOPViewController.h"

@interface CarouselViewCtlr ()

@end

@implementation CarouselViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    
    
//    void* sbServices = dlopen("/System/Library/PrivateFrameworks/SpringBoardServices.framework/SpringBoardServices", RTLD_LAZY);
//    int (*SBSLaunchApplicationWithIdentifier)(CFStringRef identifier, Boolean suspended) = dlsym(sbServices, "SBSLaunchApplicationWithIdentifier");
//    int result = SBSLaunchApplicationWithIdentifier((CFStringRef)bundleId, false);
//    dlclose(sbServices);
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:_content];
    [Holder sharedHolder].currentPageName = _content[@"name"];    
    [Holder sharedHolder].title = _content[@"title"];
    
    self.imgBgImage.image   = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    NSDictionary *content   = _content[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    data                    = [content mutableCopy];
    data                    = [Util menuAuthorization:data];
    css                     = [Util getPageCSS:_content[PAGE_CSS]];//[Holder sharedHolder].css;
    
    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    
    indexOld = 0;
    
    int btnEmergencyYPos = 322;
    if ([UIScreen mainScreen].bounds.size.height==568) {
        btnEmergencyYPos    = 364;
    }
    
    UIButton *btnEmergency = [[UIButton alloc] initWithFrame:CGRectMake(67, btnEmergencyYPos, 192, 44)];
    [btnEmergency setImage:[Util getImageAtDocDirWithName:@"emergency-number.png"] forState:UIControlStateNormal];
    [btnEmergency addTarget:self action:@selector(btnEmergencyCallPressed) forControlEvents:UIControlEventTouchUpInside];
    [_carousel addSubview:btnEmergency];
    
    //[self getPendingCount];
}

//- (void) getPendingCount {
//    
//    NSString *strURL = nil, *strMAC = nil;
//    BOOL isPendingCountRequired = FALSE;
//    
//    // Below iteration check wheater Grid contains the pending approvals and accrodingly server is request
//    NSArray *arrMacCode = [[Holder sharedHolder].loginInfo[@"menu_ar"] allKeys];
//    NSLog(@"Codes: %@", arrMacCode);
//    if ([arrMacCode containsObject:@"LV_APP"] || [strMAC isEqualToString:@"MT_APP"] || [strMAC isEqualToString:@"EXP_APP"]) {
//        isPendingCountRequired = TRUE;
//    }
//    if (isPendingCountRequired) {
//        strURL = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_LEAVE_PENDING_COUNT], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
//        NSLog(@"Pending Count: %@", strURL);
//        AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(countReceived:)
//                                                              onObject:self];
//        [conn requestPendingApprovalsCount:strURL];
//        
//        strURL = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_MUSTER_PENDING_COUNT], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
//        NSLog(@"Pending Count: %@", strURL);
//        conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(countReceived:)
//                                                  onObject:self];
//        [conn requestPendingApprovalsCount:strURL];
//        
//        strURL = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_CONVEYANCE_PENDING_COUNT], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
//        NSLog(@"Pending Count: %@", strURL);
//        conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(countReceived:)
//                                                  onObject:self];
//        [conn requestPendingApprovalsCount:strURL];
//    }
//}
//
//- (void) countReceived:(NSMutableDictionary *) resCount {
//    //LV_P_CON_TOT, MS_P_CON_TOT
//    NSLog(@"Pending approval count response: %@", resCount);
//    NSArray *resData = [resCount[@"data"] allKeys];
//    if ([resData containsObject:PEN_LV_APP_CNT]) {
//        [Holder sharedHolder].pendingCountLeave = resCount[PEN_CNT_DATA][PEN_LV_APP_CNT];
//    } else if ([resData containsObject:PEN_MS_APP_CNT]) {
//        [Holder sharedHolder].pendingCountMuster = resCount[PEN_CNT_DATA][PEN_MS_APP_CNT];
//    } else if ([resData containsObject:PEN_EXP_APP_CNT]) {
//        [Holder sharedHolder].pendingCountMuster = resCount[PEN_CNT_DATA][PEN_EXP_APP_CNT];
//    }
//}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [Holder sharedHolder].currentPageName = _content[@"name"];
    //[self initControl];

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount:) name:@"updateNotificationCount" object:nil];
    
    //configure carousel
    _carousel.type = iCarouselTypeRotary;
    
    // Sets the vertical offset of the carousel
    _carousel.viewpointOffset = CGSizeMake(0, 50);
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
        _carousel.viewpointOffset = CGSizeMake(0, 100);
        _carousel.frame = CGRectMake(_carousel.frame.origin.x, _carousel.frame.origin.y+44, _carousel.frame.size.width, _carousel.frame.size.height);
    }
    
    //NSLog(@"offset %@", _carousel);
    
    // Disable the feature where, when item selected is automatically centered
    _carousel.centerItemWhenSelected = NO;
    
}

- (void) viewDidUnload {
    
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateNotificationCount" object:nil];
    
    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    
    self.strEmergencyNumber = nil;
    self.page = nil;
    self.content = nil;
    self.imgBgImage = nil;
    self.carousel = nil;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) btnEmergencyCallPressed {
    
//    NSString *str = self.strEmergencyNumber;
    
    if ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", self.strEmergencyNumber]]];
    } else {
        [Util featureNotSupported];
    }
}

- (void)updateNotificationCount:(NSNotification*)notification {
    NSLog(@"This is Carousel view Notification method");
    [Holder sharedHolder].testingNoti = false;
    [self.navigation initTopRightButton];
    
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

#pragma mark - iCarousel methods
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    
    return [data count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(ReflectionView *)view {
    
    UIImageView *imageView  = nil;
    NSString *imgName   = nil;
    
    //create new view if no view is available for recycling
    //if (view == nil) {
        view = [[ReflectionView alloc] initWithFrame:CGRectMake(0, 0, 150.0f, 150.0f)];
        
        if (carousel==_carousel) {
            imgName = data[index][@"img-icon"];
            if (index == carousel.currentItemIndex) {
                // view for current item
                imgName = data[index][@"img-icon-sel"];
//                _lblFunElement.text = dataFunctional[index][@"title"];
//                [_lblEngElement bringSubviewToFront:view];
            }
        }
        
        imageView = [[UIImageView alloc] initWithFrame:view.bounds];
        imageView.image = [Util getImageAtDocDirWithName:imgName];
        //imageView.contentMode = UIViewContentModeCenter; // maintains the aspect ratio of the image
        [view addSubview:imageView];
    
        /*if ([data[index][@"name"] isEqualToString:@"my-approvals"]) {
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(120, 0, 31, 31)];
            UIImage *imgBack = [Util getImageAtDocDirWithName:@"pending-count.png"];
            [button setBackgroundImage:imgBack forState:UIControlStateNormal];
            [button setTag:TAG_CAROUSEL_APPROVAL];
            [view addSubview:button];
            
            Holder *holder = [Holder sharedHolder];
            if (![holder.pendingCountLeave isEqualToString:@""] && ![holder.pendingCountMuster isEqualToString:@""] && ![holder.pendingCountConveyance isEqualToString:@""]) {
                
                int intPendingCount = [holder.pendingCountLeave floatValue] + [holder.pendingCountMuster floatValue] + [holder.pendingCountConveyance floatValue];
                [button setTitle:[NSString stringWithFormat:@"%d", intPendingCount] forState:UIControlStateNormal];
            } else {
                UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                spinner.frame = button.frame;
                [spinner setTag:TAG_PAC_CAROUSEL];
                [spinner startAnimating];
                [view addSubview:spinner];
            }
        }*/
    /*} else {
        imageView = [[view subviews] lastObject];
    }*/
    
    // update reflection this step is expensive, so if you don't need unique reflections
    // for each item, don't do this and you'll get much smoother peformance
    //[view update];
    
    return view;
}

//- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
//    
//    // you need to reload carousel for update view of current index
//    if (carousel==_carousel) {
//        [_carousel reloadData];
//    }
//}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel {
    
    if (carousel==_carousel) {
        
        indexCurrent = (int) carousel.currentItemIndex;
        //NSLog(@"Current index: %ld old index: %ld", (long)indexCurrent, (long)indexOld);
        
        [carousel reloadItemAtIndex:indexOld animated:NO];
        [carousel reloadItemAtIndex:indexCurrent animated:NO];
        indexOld = indexCurrent;
        
        //[_carousel reloadData];
    }
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
    
    if (carousel==_carousel) {
        if (carousel.currentItemIndex==index) {
            NSDictionary *dict = [Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_MPIN_MENU];
            if([dict isEqual:[NSNull null]]) {
                [self didSelectItemAtIndex:index];
            } else {
                NSString *mac = data[index][@"mac"];
                if ([[dict allKeys] containsObject:mac]) {
                    // contains key
                    NSString *value = [NSString stringWithFormat:@"%@",[dict objectForKey:mac]];
                    if([value isEqualToString:@"1"]) {
                        Util *util = [[Util alloc] init];
                        [util showVerifyViewControllerOnController:viewCtlr.centerController Model:nil Index:index];
                    } else {
                        [self didSelectItemAtIndex:index];
                    }
                } else {
                    [self didSelectItemAtIndex:index];
                }
            }
        }
    }
}

- (void)didSelectItemAtIndex:(NSInteger)index {
    
    NSString *nextPage = data[index][PAGE_NAME];
    NSDictionary *nextPageData = [Util getPage:nextPage];
    //NSLog(@"next page: %@ data: %@", nextPage, nextPageData);
    if ([nextPageData[PAGE_TYPE] isEqualToString:PAGE_TYPE_MENULIST]) {
        [Util openMenuAsGridWithNextPage:nextPage Data:[Util getPage:nextPage] OnView:self];
    } else if ([nextPageData[PAGE_TYPE] isEqualToString:PAGE_TYPE_MENULIST_DB]) {
        [Util openMenuWithNextPage:nextPage OnView:self];
    } else if ([nextPageData[PAGE_TYPE] isEqualToString:PAGE_TYPE_WEB]) {
        [Util OpenWebWithNextPage:nextPage AndParam:nextPageData[PAGE_PARAMS] OnView:self];
    } else if ([nextPageData[PAGE_TYPE] isEqualToString:PAGE_TYPE_APP]) {
        if ([nextPageData[PAGE_NAME] isEqualToString:@"pms"]){
            [self openpmstms:0];
        }else if ([nextPageData[PAGE_NAME] isEqualToString:@"tms"])
        {
            [self openpmstms:1];
        }
    }
    else if ([nextPageData[PAGE_NAME] isEqualToString:@"esop"]) {
        UINavigationController *nav = (UINavigationController *) self.navigationController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        ESOPViewController *esop = (ESOPViewController *) [storyboard instantiateViewControllerWithIdentifier:@"esop"];
        esop.nextPage       = nextPage;
        esop.content        = [Util getPage:nextPage].mutableCopy;
        
        [Util pushWithFadeEffectOnNavigationController:nav ViewController:esop];
    }else if ([nextPageData[PAGE_TYPE] isEqualToString:PAGE_TYPE_APP]) {
        [Util OpenWebWithNextPage:nextPage AndParam:nextPageData[PAGE_PARAMS] OnView:self];
    }
    
}

-(void)openpmstms:(NSInteger)apptype{
    AppConnect *conn = [[AppConnect alloc] init];
    NSString *token = [conn fetchPmsToken];
   
    NSData *data = [token dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //token = @"test";
    //NSString *dummytoken = @"eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIxMTY2ODgiLCJpYXQiOjE0ODMxNzAwMjcsImlzcyI6IlBNU19UTVNfU0VSVkVSIiwiZXhwIjoxNDgzMTcwMzI3LCJ1c2VyIjoiMTE2Njg4In0.c72ZjZ7YEihEWFXmoSjBvuSq2PCZ2aw26tUHThCnYrtPubNi5IxCCr_qvU1gFNJqoYsnd4OH8pYzVes2vsGoLZumIaDqlOBb7viRUn-9vacnuEnp_g7CUUSl6StVhunT7_PeUjCBzCEpMT1J33HZw9cnKCW9PRzJ4yKsZ2dEv5YORgs47W5TIERn6cJrZdLvsD2qwX-miDLctmtz3KMxjuPvbrQ61ej8slkLWTgvTdPomlQizxCzwhxfNERwdTcr7jSmeksPqwSiaKBvkO_DnS2SPptYeAJHeePUBrK1EIktQh1Y18qQo5_fMC9-bdkaq9x10j4P69xDgbFG34Jbmg";

    if(![token isEqualToString:@""] && ![json[@"sts"] isEqualToString:@"500"]){
        NSData *jsonData = [token dataUsingEncoding:NSUTF8StringEncoding];
        NSError * error=nil;
        NSDictionary * parsedJson = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        NSDictionary * parsedData = [parsedJson objectForKey:@"data"];
        NSString *tkn = [parsedData objectForKey:@"tkn"];
        NSString *appToken = tkn;
        //NSString *appToken = dummytoken;
        NSString *appTypeVal = @"pms";
        if(apptype==1){
            appTypeVal = @"tms";
        }
        NSString *customURL = @"icicipms://?token=";
        NSString *finalURL = [NSString stringWithFormat:@"%@%@%@%@", customURL, appToken, @"&type=",appTypeVal];
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        NSLog(@"This is %f ",currSysVer.floatValue);
        if (currSysVer.floatValue >= 10.0) {
            NSLog(@"This is ios 10 block");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL] options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    //NSLog(@"Opened %@",scheme);
                    
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App not installed"
                                                                    message:[NSString stringWithFormat:
                                                                             @"Please install PMS/TMS app."]
                                                                   delegate:self cancelButtonTitle:@"No"
                                                          otherButtonTitles:@"Yes", nil];
                    [alert show];
                    
                }
            }];
//        }else{
//            NSLog(@"This is responce %hhd",[[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:finalURL]]);
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL]];
//            NSLog(@"This is called means method ");
//        }
        }else {
           
        
            if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL]]) {
                NSLog(@"yes this is version lower than 10");
                NSLog(@"This is responce app not installed %hhd",[[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]]);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App not installed"
                                                                message:[NSString stringWithFormat:
                                                                         @"Please install PMS/TMS app."]
                                                               delegate:self cancelButtonTitle:@"No"
                                                      otherButtonTitles:@"Yes",nil];
               
                [alert show];
            }
        }

    }else{
        if ([json[@"sts"] isEqualToString:@"500"]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Technical Error "
                                                            message:json[@"msg"]
                                                           delegate:nil cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Technical Error Occurred"
                                                        message:[NSString stringWithFormat:
                                                                 @"Unable to connect to PMS/TMS. Please try again later."]
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        }
    }
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    
    //customize carousel display
    switch (option) {
        case iCarouselOptionSpacing: {
            if (carousel == _carousel) {
                //add a bit of spacing between the item views
                return value * 1.5f;
            }
        }
        default: {
            return value;
        }
    }
}
#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    NSLog(@"Alert title: %@", title);
    if ([title isEqualToString:@"Yes"]) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"https://universeonthemove.icicibank.com/engage/downloadptms.htm"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://universeonthemove.icicibank.com/engage/downloadptms.htm"]];
        }
    }
}

@end
