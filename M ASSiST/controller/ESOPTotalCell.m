//
//  ESOPTotalCell.m
//  Universe on the move
//
//  Created by Phyder on 17/11/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "ESOPTotalCell.h"

#define NULL_VALUE           @"---"

@implementation ESOPTotalCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setData:(NSDictionary *) data {

    self.lblTotalOptions.text = data[@"total_options"];
    self.lblOptionsAvailable.text = data[@"options_available"];
    self.lblOptionsOutstanding.text = data[@"outstanding_options"];
    
    if (data[@"total_options"] == nil || data[@"total_options"] == [NSNull null] || [data[@"total_options"] isEqualToString:@""]) {
        self.lblTotalOptions.text = NULL_VALUE;
    } else {
        self.lblTotalOptions.text = [data valueForKey:@"total_options"];
    }
    
    
    if (data[@"options_available"] == nil || data[@"options_available"] == [NSNull null] || [data[@"options_available"] isEqualToString:@""]) {
        self.lblOptionsAvailable.text = NULL_VALUE;
    } else {
        self.lblOptionsAvailable.text = [data valueForKey:@"options_available"];
    }
    
    if (data[@"outstanding_options"] == nil || data[@"outstanding_options"] == [NSNull null] || [data[@"outstanding_options"] isEqualToString:@""]) {
        self.lblOptionsOutstanding.text = NULL_VALUE;
    } else {
        self.lblOptionsOutstanding.text = [data valueForKey:@"outstanding_options"];
    }
    
}

@end
