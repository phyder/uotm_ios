//
//  SettingsViewCtlr.m
//  engage
//
//  Created by Suraj on 11/30/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "SettingsViewCtlr.h"

#import "Constant.h"
#import "Util.h"

@interface SettingsViewCtlr ()

@end

@implementation SettingsViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _imgBackground.image = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    _txtAppURL.delegate = self;
    
    _data = [Util getPage:PAGE_SETTINGS];
    _css = [Util getPageCSS:_data[PAGE_CSS]];
    
    self.view.backgroundColor = [Util colorWithHexString:_css[CSS_PAGE_BGCOLOR] alpha:@"1.0f"];
    
    if (![_data[PAGE_HEADER] isEqualToString:PAGE_TRUE]) {
        self.navigationController.navigationBarHidden = YES;
    } else {
        self.navigationController.navigationBarHidden = NO;
        self.navigationController.navigationBar.tintColor = [Util colorWithHexString:_css[CSS_PAGE_TINTCOLOR] alpha:@"1.0f"];
        
        // Create image for navigation background - portrait
        UIImage *NavigationPortraitBackground = [[Util getImageAtDocDirWithName:@"nav-bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        // Create image for navigation background - landscape
        //UIImage *NavigationLandscapeBackground = [[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, @"nav-bar.png"]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        // Set the background image all UINavigationBars
        [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
        
        [[UIBarButtonItem appearance] setTintColor:[Util colorWithHexString:_css[CSS_PAGE_TINTCOLOR] alpha:@"1.0f"]];
        
        if ([_data[PAGE_TOP_LEFT_BUTTON] isEqual:PAGE_TRUE]) {
            
            _buttonCss = [Util getButtonCSS:_data[PAGE_TOP_LEFT_BUTTON_CSS]];
            //NSLog(@"page css %@", pageCss);
            UIImage *imgTopbarLeft = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, _buttonCss[@"img"]]];
            //create the button and assign the image
            UIButton *btnTopbarLeft = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnTopbarLeft setImage:imgTopbarLeft forState:UIControlStateNormal];
            //sets the frame of the button to the size of the image
            btnTopbarLeft.frame = CGRectMake(0, 0, imgTopbarLeft.size.width, imgTopbarLeft.size.height);
            //defining the action
            [btnTopbarLeft addTarget:self action:@selector(btnTopBarLeftSettingsPressed) forControlEvents:UIControlEventTouchUpInside];
            //creates a UIBarButtonItem with the button as a custom view
            UIBarButtonItem *btnTopbarLeftBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnTopbarLeft];
            self.navigationItem.leftBarButtonItem = btnTopbarLeftBarButton;
        }
        
        if ([_data[PAGE_TOP_RIGHT_BUTTON] isEqual:PAGE_TRUE]) {
            
            _buttonCss = [Util getButtonCSS:_data[PAGE_TOP_RIGHT_BUTTON_CSS]];
            //NSLog(@"page css %@", pageCss);
            UIImage *imgTopbarRight = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, _buttonCss[@"img"]]];
            UIButton *btnTopbarRight = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnTopbarRight setImage:imgTopbarRight forState:UIControlStateNormal];
            btnTopbarRight.frame = CGRectMake(0, 0, imgTopbarRight.size.width, imgTopbarRight.size.height);
            [btnTopbarRight addTarget:self action:@selector(btnTopBarRightSettingsPressed) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *btnTopbarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnTopbarRight];
            self.navigationItem.rightBarButtonItem = btnTopbarRightBarButton;
        }
        
        UILabel *lblTopBarTitle = (UILabel *) self.navigationItem.titleView;
        if (!lblTopBarTitle) {
            lblTopBarTitle = [[UILabel alloc] initWithFrame:CGRectZero];
            lblTopBarTitle.backgroundColor = [UIColor clearColor];
            lblTopBarTitle.font = [UIFont fontWithName:APP_FONT_BOLD size:20];
            lblTopBarTitle.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
            lblTopBarTitle.textColor = [UIColor whiteColor];
            self.navigationItem.titleView = lblTopBarTitle;
        }
        lblTopBarTitle.text = _data[PAGE_HEADER_TITLE];
        [lblTopBarTitle sizeToFit];
    }// if (! [[data objectForKey:PAGE_HEADER] isEqualToString:PAGE_TRUE])
    
    if (![_data[PAGE_FOOTER] isEqualToString:PAGE_TRUE]) {
        [self.navigationController setToolbarHidden:YES];
    } else {
        [self.navigationController setToolbarHidden:NO];
        self.navigationController.toolbar.tintColor = [Util colorWithHexString:_css[CSS_PAGE_TINTCOLOR] alpha:@"1.0f"];
        
        UIImage *NavigationPortraitBackground = [[Util getImageAtDocDirWithName:@"tool-bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [[UIToolbar appearance] setBackgroundImage:NavigationPortraitBackground forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
        [self.navigationController.toolbar setBackgroundImage:NavigationPortraitBackground forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
        
        NSMutableArray *items = [[NSMutableArray alloc] init];
        
        if ([_data[PAGE_BOTTOM_LEFT_BUTTON1] isEqualToString:PAGE_TRUE]) {
            //Load the image
            _buttonCss = [Util getButtonCSS:_data[PAGE_BOTTOM_LEFT_BUTTON1_CSS]];
            UIImage *imgBottombarLeft = [Util getImageAtDocDirWithName:_buttonCss[@"img"]];
            //create the button and assign the image
            
            UIButton *btnBottombarLeft = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnBottombarLeft setImage:imgBottombarLeft forState:UIControlStateNormal];
            //sets the frame of the button to the size of the image
            btnBottombarLeft.frame = CGRectMake(0, 0, imgBottombarLeft.size.width, imgBottombarLeft.size.height);
            //defining the action
            [btnBottombarLeft addTarget:self action:@selector(btnBottombarLeftBarButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            //creates a UIBarButtonItem with the button as a custom view
            UIBarButtonItem *btnBottombarLeftBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarLeft];
            
            [items addObject:btnBottombarLeftBarButton];
        }
        
        if ([_data[PAGE_BOTTOM_LEFT_BUTTON2] isEqualToString:PAGE_TRUE]) {
            //Load the image
            _buttonCss = [Util getButtonCSS:_data[PAGE_BOTTOM_LEFT_BUTTON2_CSS]];
            UIImage *imgBottombarLeft = [Util getImageAtDocDirWithName:_buttonCss[@"img"]];
            //create the button and assign the image
            
            UIButton *btnBottombarLeft = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnBottombarLeft setImage:imgBottombarLeft forState:UIControlStateNormal];
            //sets the frame of the button to the size of the image
            btnBottombarLeft.frame = CGRectMake(0, 0, imgBottombarLeft.size.width, imgBottombarLeft.size.height);
            //defining the action
            [btnBottombarLeft addTarget:self action:@selector(btnBottombarLeftBarButtonPressed2) forControlEvents:UIControlEventTouchUpInside];
            //creates a UIBarButtonItem with the button as a custom view
            UIBarButtonItem *btnBottombarLeftBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarLeft];
            
            [items addObject:btnBottombarLeftBarButton];
        }
        
        if ([_data[PAGE_BOTTOM_RIGHT_BUTTON] isEqualToString:PAGE_TRUE]) {
            UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            [items addObject:flexible];
            
            _buttonCss = [Util getButtonCSS:_data[PAGE_BOTTOM_RIGHT_BUTTON_CSS]];
            UIImage *imgBottombarRight = [Util getImageAtDocDirWithName:_buttonCss[@"img"]];
            UIButton *btnBottombarRight = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnBottombarRight setImage:imgBottombarRight forState:UIControlStateNormal];
            btnBottombarRight.frame = CGRectMake(0, 0, imgBottombarRight.size.width, imgBottombarRight.size.height);
            [btnBottombarRight addTarget:self action:@selector(btnBottombarRightBarButton) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *btnBottombarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarRight];
            
            [items addObject:btnBottombarRightBarButton];
        }
        
        self.toolbarItems = items;
        NSLog(@"URL: %@", [Util getSettingUsingName:MOBILE4U_URL]);
        _lblAppURL.text = [Util getSettingUsingName:MOBILE4U_URL_TEXT];
        _txtAppURL.text = [Util getSettingUsingName:MOBILE4U_URL];
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [self setBtnTopBarLeftBack:nil];
    [self setBtnTopBarRightSave:nil];
    [self setLblBottomBarTitle:nil];
    [self setTxtAppURL:nil];
    [self setLblAppURL:nil];
    [super viewDidUnload];
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    [self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self resetViewToIdentityTransform];
}

- (void) maintainVisibityOfControl: (UITextField *) textField {
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    double fieldYPosition = textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}


#pragma UITextFieldDelegate implementation for resignFirstResponder

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_txtAppURL resignFirstResponder];
    return YES;
}

//#pragma UITextFieldDelegate implementation for resignFirstResponder

- (void) btnTopBarLeftSettingsPressed {
    
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) btnTopBarRightSettingsPressed {
    NSLog(@"%s", __FUNCTION__);
    UIAlertView *message = nil;
    if ([_txtAppURL.text isEqualToString:EMPTY]) {
        message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                             message:[NSString stringWithFormat:@"%@ can not be empty", [Util getSettingUsingName:MOBILE4U_URL_TEXT]]
                                            delegate:self
                                   cancelButtonTitle:ALERT_OPT_OK
                                   otherButtonTitles:nil];
    } else {
        [Util setSettingWithName:MOBILE4U_URL ByValue:_txtAppURL.text];
        message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                             message:[Util getSettingUsingName:SETTING_PAGE_SAVE_MSG]
                                            delegate:self
                                   cancelButtonTitle:ALERT_OPT_OK
                                   otherButtonTitles:nil];
    }
    [message show];
}

- (void) btnBottombarLeftBarButtonPressed {
#ifdef DEBUG
    NSLog(@"%s help button", __FUNCTION__);
#endif
    
    [Util openHelp:self];
}

- (void) btnBottombarLeftBarButtonPressed2 {
    NSLog(@"%s", __FUNCTION__);
}

- (void) btnBottombarRightBarButton {
    NSLog(@"%s", __FUNCTION__);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", POWERED_BY_ENGAGE_URL]]];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:ALERT_OPT_OK]) {
        NSLog(@"%s OK Button was clicked.", __FUNCTION__);
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)hideKeyboard:(id)sender {
    [_txtAppURL resignFirstResponder];
}

@end
