//
//  RegisterViewCtlr.m
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "RegisterViewCtlr.h"
#import "Util.h"
#import "RegisterModel.h"
#import "Constant.h"
#import "OtpViewController.h"
#import "Holder.h"
#import "IIViewDeckController.h"
#import "MPinViewCtlr.h"

@interface RegisterViewCtlr ()

@end

@implementation RegisterViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    _imgRegisterBackground.image   = [Util getImageAtDocDirWithName:@"loginbox.png"];
    _imgBackground.image        = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    registerPage                   = [[RegisterModel alloc] init];
    [registerPage initDataWithView:self];
    
    NSArray *lists              = [registerPage getPageData][PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list      = lists[i];
        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
            controlData         = list;
        }
    }
    
    _logo.image = [Util getImageAtDocDirWithName:controlData[@"img"]];
    
    [self initEmployeeId];
    [self initPassword];
    
    NSLog(@"Log %@ %@",[Util getSettingUsingName:REGISTER_LOADER_MSG],[Util getSettingUsingName:POPUP_TITLE]);
    
    //    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
    //    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
    //                                                     message:[[NSUUID UUID] UUIDString]
    //                                                    delegate:nil
    //                                           cancelButtonTitle:@"Ok"
    //                                           otherButtonTitles:nil];
    //    [device show];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //Custom Initialization
    [registerPage initNavControlWithEmployeeId:_txtEmployeeId Password:_txtPassword Next:_btnNext];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initEmployeeId {
    
    NSString *strLastLoginUserId = [Util getSettingUsingName:LAST_LOGIN_USER_ID];
    NSLog(@"Last Login User: %@", strLastLoginUserId);
    
    if (strLastLoginUserId != nil && ![strLastLoginUserId isEqualToString:@""]) {
        _txtEmployeeId.text = strLastLoginUserId;
    }
    
    //_txtUserName.textColor = [Util colorWithHexString:@"E9B472"];
    [_txtEmployeeId setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"user.png"];
    [paddingView addSubview:userimage];
    _txtEmployeeId.leftView               = paddingView;
    _txtEmployeeId.leftViewMode           = UITextFieldViewModeAlways;
    _txtEmployeeId.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtEmployeeId.borderStyle            = UITextBorderStyleNone;
    _txtEmployeeId.layer.cornerRadius     = 4.0f;
    _txtEmployeeId.layer.masksToBounds    = YES;
    _txtEmployeeId.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtEmployeeId.layer.borderWidth      = 1.0f;
    _txtEmployeeId.delegate               = self;
    // Custom place holder color txtUserName.placeholder
    //[_txtUserName drawPlaceholderInRect:_txtUserName.frame];
    if ([_txtEmployeeId.text isEqualToString:@""]) {
        [_txtEmployeeId becomeFirstResponder];
    }
}

- (void) initPassword {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtPassword.delegate               = self;
    [_txtPassword setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *leftPaddingView             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"pass.png"];
    [leftPaddingView addSubview:userimage];
    _txtPassword.leftView               = leftPaddingView;
    _txtPassword.leftViewMode           = UITextFieldViewModeAlways;
    _txtPassword.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtPassword.borderStyle            = UITextBorderStyleNone;
    _txtPassword.layer.cornerRadius     = 4.0f;
    _txtPassword.layer.masksToBounds    = YES;
    _txtPassword.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtPassword.layer.borderWidth      = 1.0f;
    _txtPassword.returnKeyType = UIReturnKeyGo;
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _txtEmployeeId) {
        [_txtPassword becomeFirstResponder];
    } else if (textField == _txtPassword) {
        [_txtPassword resignFirstResponder];
        [self next];
        
    }
    return YES;
}

#pragma mark - UITextFieldDelegate
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    // Transform the views to the normal view
    [self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self resetViewToIdentityTransform];
}

#pragma mark - Keyboard Slider
- (void) maintainVisibityOfControl: (UITextField *) textField {
    
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    //double visibleHeight = bounds.size.height-216;
    
    double fieldYPosition = textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}

- (void) hideKeyboard {
    [_txtEmployeeId resignFirstResponder];
    [_txtPassword resignFirstResponder];
}

#pragma mark - Navigation Slider
- (void) initOtpViewController {
    
    NSLog(@"%s", __FUNCTION__);
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    OtpViewController *otpViewController = [storyboard instantiateViewControllerWithIdentifier:@"otppage"];
//    
//    [self.navigationController pushViewController:otpViewController animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MPinViewCtlr *mPinViewCtlr = [storyboard instantiateViewControllerWithIdentifier:@"mpin"];
    
    [self.navigationController pushViewController:mPinViewCtlr animated:YES];
    
//    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
//    //deckController.panningMode = IIViewDeckFullViewPanning;
//    deckController.leftController = otpViewController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) next {
    
    [registerPage next];
}

- (void) resRegister:(NSMutableDictionary *) resRegister {
    
    [registerPage postAuthenticationValidationWithResData:resRegister];
}

- (IBAction)btnClearPressed:(id)sender {
    [self hideKeyboard];
}
@end
