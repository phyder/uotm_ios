//
//  ListViewCell.m
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "HomeListViewCell.h"

#import "AppConnect.h"
#import "Constant.h"
#import "Util.h"

@implementation HomeListViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier  {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    UIView *bgColorView = [[UIView alloc] init];
    NSArray *color = [[Util getSettingUsingName:LIST_TOUCHUP_COLOR] componentsSeparatedByString:@"|"];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [self.height floatValue]);
    gradient.colors = @[(id)[[Util colorWithHexString:color[0] alpha:@"0.75f"] CGColor], (id)[[Util colorWithHexString:color[1] alpha:@"0.75f"] CGColor]];
    [bgColorView.layer insertSublayer:gradient atIndex:0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void) initCellWithData:(NSDictionary *) data AndCSS:(NSDictionary *) css {
    
    // Responsible for showing loader image while image is downloaded form internet
//    self.leftImage.animationImages = [NSArray arrayWithObjects:
//                                  [Util getImageAtDocDirWithName:@"loader1.png"],
//                                  [Util getImageAtDocDirWithName:@"loader2.png"],
//                                  [Util getImageAtDocDirWithName:@"loader2.png"],
//                                  [Util getImageAtDocDirWithName:@"loader4.png"], nil];
//    self.leftImage.animationDuration = 0.8;
//    [self.leftImage startAnimating];
//    
//    [self getImageForImageName:[data objectForKey:LIST_RES_IMAGE]];
    self.leftImage.image        = [Util getImageAtDocDirWithName:@"home-sample.png"];
    
    //self.leftImage.image    = [AppConnect getListImageWithName:[data objectForKey:LIST_RES_IMAGE]];
    self.leftImage.frame        = CGRectMake(self.leftImage.frame.origin.x, self.frame.size.height/2-self.leftImage.frame.size.height/2, self.leftImage.frame.size.width, self.leftImage.frame.size.height);
    
    NSDictionary *lblCSS        = [Util getLabelCSS:css[@"title-css"]];
    self.title.text             = data[LIST_RES_TITLE];
    self.title.textColor        = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    self.title.font             = [Util fontWithName:lblCSS[@"font-name"]
                                           style:lblCSS[@"font-style"]
                                            size:lblCSS[@"font-size"]];
    [self.title setNumberOfLines:0];
    [self.title sizeToFit];
    
    
    lblCSS                      = [Util getLabelCSS:css[@"sub-title-css"]];
    self.subTitle.text          = data[LIST_RES_SUB_TITLE];
    self.subTitle.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                 alpha:@"1.0f"];
    self.subTitle.font          = [Util fontWithName:lblCSS[@"font-name"]
                                           style:lblCSS[@"font-style"]
                                            size:lblCSS[@"font-size"]];
    [self.subTitle setNumberOfLines:0];
    [self.subTitle sizeToFit];
    self.subTitle.frame  = CGRectMake(self.subTitle.frame.origin.x, self.title.frame.origin.y+self.title.frame.size.height, self.subTitle.frame.size.width, self.subTitle.frame.size.height);
    
    lblCSS                      = [Util getLabelCSS:css[@"offer-css"]];
    self.lblOffer.text          = data[LIST_RES_OFFER];
    self.lblOffer.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                     alpha:@"1.0f"];
    self.lblOffer.font          = [Util fontWithName:lblCSS[@"font-name"]
                                               style:lblCSS[@"font-style"]
                                                size:lblCSS[@"font-size"]];
    [self.lblOffer setNumberOfLines:0];
    [self.lblOffer sizeToFit];
    self.lblOffer.frame  = CGRectMake(self.lblOffer.frame.origin.x, self.subTitle.frame.origin.y+self.subTitle.frame.size.height, self.lblOffer.frame.size.width, self.lblOffer.frame.size.height);
    
    lblCSS                      = [Util getLabelCSS:css[@"offer-css"]];
    self.lblLocation.text       = data[LIST_RES_LOCATION];
    self.lblLocation.textColor  = [Util colorWithHexString:lblCSS[@"font-color"]
                                                     alpha:@"1.0f"];
    self.lblLocation.font       = [Util fontWithName:lblCSS[@"font-name"]
                                               style:lblCSS[@"font-style"]
                                                size:lblCSS[@"font-size"]];
    [self.lblLocation setNumberOfLines:0];
    [self.lblLocation sizeToFit];
    self.lblLocation.frame  = CGRectMake(self.lblLocation.frame.origin.x, self.lblOffer.frame.origin.y+self.lblOffer.frame.size.height, self.lblLocation.frame.size.width, self.lblLocation.frame.size.height);
    
    self.lblValidity.text       = @"";//[data objectForKey:LIST_RES_LOCATION];
    self.lblValidity.textColor  = [Util colorWithHexString:lblCSS[@"font-color"]
                                                     alpha:@"1.0f"];
    self.lblValidity.font       = [Util fontWithName:lblCSS[@"font-name"]
                                               style:lblCSS[@"font-style"]
                                                size:lblCSS[@"font-size"]];
    [self.lblValidity setNumberOfLines:0];
    [self.lblValidity sizeToFit];
    self.lblValidity.frame  = CGRectMake(self.lblValidity.frame.origin.x, self.lblOffer.frame.origin.y+self.lblOffer.frame.size.height, self.lblValidity.frame.size.width, self.lblValidity.frame.size.height);
    
    if ([data[LIST_RES_RECENT] intValue]==1) {
        self.imgNewOffer.hidden = NO;
    } else if ([data[LIST_RES_RECENT] intValue]==0) {
        self.imgNewOffer.hidden = YES;
        self.imgNewOffer.image = [Util getImageAtDocDirWithName:@"star.png"];
    }
    
    self.accessoryView = [[UIImageView alloc] initWithImage:[Util getImageAtDocDirWithName:@"right-arrow.png"]];
}

- (void) getImageForImageName:(NSString *) imageName {
    
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getListImageWithName:imageName AfterFinishCall:@selector(getImageFinishedWithData:) onObject:self];
}

- (void) getImageFinishedWithData:(NSData *) data {
    
    [self.leftImage stopAnimating];
    self.leftImage.image         = [UIImage imageWithData:data];//[[UIImage alloc] initWithData:data];
    //NSLog(@"image holder: %@", data);
}

@end
