//
//  SettingsViewCtlr.h
//  engage
//
//  Created by Suraj on 11/30/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface SettingsViewCtlr : UIViewController<UIAlertViewDelegate, UITextFieldDelegate> {
    
    BOOL slide;
}

//@property (retain, nonatomic) IBOutlet UILabel *lblTopBarTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnTopBarLeftBack;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnTopBarRightSave;

@property (strong, nonatomic) IBOutlet UILabel *lblBottomBarTitle;

@property (strong, nonatomic) IBOutlet UILabel *lblAppURL;
@property (strong, nonatomic) IBOutlet UITextField *txtAppURL;

@property (strong, nonatomic) IBOutlet NSDictionary *data;
@property (strong, nonatomic) IBOutlet NSDictionary *css;
@property (strong, nonatomic) IBOutlet NSDictionary *buttonCss;

- (IBAction)hideKeyboard:(id)sender;

@end
