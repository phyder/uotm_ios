//
//  ESOPViewController.h
//  Universe on the move
//
//  Created by Phyder on 27/10/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ESOPModel;

@interface ESOPViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate> {
    ESOPModel *esopModel;
}

@property (nonatomic, strong) IBOutlet UIImageView *imgBgImage;
@property (weak, nonatomic) IBOutlet UIView *esopHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgBgHeader;

@property (nonatomic, strong) IBOutlet NSString *nextPage;
@property (nonatomic, strong) IBOutlet NSString *nextTitle;

@property (nonatomic, strong) IBOutlet NSMutableDictionary *content;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
