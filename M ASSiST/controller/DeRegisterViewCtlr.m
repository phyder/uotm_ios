//
//  DeRegisterViewCtlr.m
//  Universe on the move
//
//  Created by Phyder on 30/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "DeRegisterViewCtlr.h"
#import "Holder.h"
#import "Constant.h"
#import "Authentication.h"
#import "Util.h"
#import "DeviceListCell.h"
#import "IIViewDeckController.h"
#import "Navigation.h"

@interface DeRegisterViewCtlr () {
    NSMutableArray *deviceList;
    DeviceListCell *cell;
    NSDictionary *deregisterData;
}

@end

@implementation DeRegisterViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    data        = [Util getPage:PAGE_DEREGISTER];
    css         = [Util getPageCSS:data[PAGE_CSS]];//[Holder sharedHolder].css;
    
//    NSArray *lists              = data[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
//    NSDictionary *controlData = nil;
//    for (int i=0; i<[lists count]; i++) {
//        NSDictionary *list      = lists[i];
//        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
//            controlData         = list;
//        }
//    }

    [self getDeviceList];
    self.tableDeviceList.delegate = self;
    self.tableDeviceList.dataSource = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //Custom Initialization
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:data];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{.
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) getDeviceList {
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self AndModel:nil];
    NSString *userName = [Util getKeyChainUserName];
    [[Holder sharedHolder].auth requestGetRegisterListWithUsername:userName];
}

- (void) resDeregister:(NSMutableDictionary *) resDeregister {
    
    NSLog(@"response: %@", resDeregister);
    
    if(resDeregister[@"data"] == nil) {
        
    } else {
        resDeregister = resDeregister[@"data"];
    }
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                      message:resDeregister[SERVER_RES_MEG]
                                                     delegate:nil
                                            cancelButtonTitle:ALERT_OPT_OK
                                            otherButtonTitles:nil];
    [message show];
    [[Holder sharedHolder].auth postDeregisterAuthenticationWithData:resDeregister];
}

- (void) resGetRegisterList:(NSMutableDictionary *) resGetRegisterList {
    
    NSLog(@"response: %@", resGetRegisterList);
    deviceList = resGetRegisterList[@"list"];
    [[Holder sharedHolder].auth postGetRegisterListAuthenticationWithData:resGetRegisterList];
}

- (void) showRegisterDeviceList {
    [self.tableDeviceList reloadData];
    self.tableDeviceList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void) initRegisterViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginview"];
    
    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    
    deckController.centerController = initViewController;
}

#pragma mark UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [deviceList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"list";
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //cell.delegate = self;
    [cell setCellData:[deviceList objectAtIndex:indexPath.row]];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    deregisterData = [deviceList objectAtIndex:indexPath.row];
    NSString *deviceModel = deregisterData[@"DVCE_MDL"];
    if([deviceModel isEqual:[NSNull null]]) {
        deviceModel = @"";
    }
    
    NSString *message = [NSString stringWithFormat:@"Are you sure? Do you want to Deregister Device %@?",deviceModel];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:ALERT_OPT_OK
                                          otherButtonTitles:ALERT_OPT_CANCEL, nil];
    [alert show];
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_OPT_OK]) {
        [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self AndModel:nil];
        NSString *userName = [Util getKeyChainUserName];
        [[Holder sharedHolder].auth requestDeregisterWithUsername:userName deviceId:deregisterData[@"DVCE_ID"]];
    }
}

- (void) dealloc {
    
    self.navigation = nil;
}

@end
