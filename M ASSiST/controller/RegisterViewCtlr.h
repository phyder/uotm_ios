//
//  RegisterViewCtlr.h
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RegisterModel;

@interface RegisterViewCtlr : UIViewController <UITextFieldDelegate> {
    
    BOOL slide;
    
    RegisterModel *registerPage;
}

@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgRegisterBackground;
@property (strong, nonatomic) IBOutlet UITextField *txtEmployeeId;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnNext;

- (IBAction)btnClearPressed:(id)sender;

- (void) next;
- (void) resRegister:(NSMutableDictionary *) resRegister;

@end
