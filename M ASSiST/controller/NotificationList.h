//
//  NotificationList.h
//  engage
//
//  Created by Suraj on 22/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class Navigation;

@interface NotificationList : UITableViewController {
    
    Navigation *navigation;
    
    NSDictionary *cssPage;
    NSMutableArray *data;
    
    BOOL isRefreshing;
}

@property (strong, nonatomic) IBOutlet NSDictionary *pageData;
@property (strong, nonatomic) IBOutlet NSArray *listData;
@property (strong, nonatomic) IBOutlet NSString *url;

- (void) refreshNotificationList;

@end
