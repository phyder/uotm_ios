//
//  menuViewCtlr.m
//  M ASSiST
//
//  Created by Suraj on 22/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "MenuListViewCtlr.h"

#import "MenuViewModel.h"

#import "Constant.h"
#import "Util.h"

#import "ListViewCell.h"

@interface MenuListViewCtlr ()

@end

static int menuHeaderHeight     = 44.0f;
static int menuRowHeight        = 60.0f;
static NSString *CellIdentifier = @"menuviewcell";

@implementation MenuListViewCtlr

- (id)initWithStyle:(UITableViewStyle)style {
    
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f) {
        self.view.bounds = CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    // Custom initalization
    menuModel = [[MenuViewModel alloc] init];
    [menuModel initDataWithView:self];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[Util getImageAtDocDirWithName:@"login-view-bg.png"]];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 20;
    self.view.frame=frame;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [menuModel getMenuItemCount];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return menuHeaderHeight;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return [menuModel getSignOutButtonInMenuHeaderWithHeight:menuHeaderHeight];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return menuRowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    [cell initCellWithData:[menuModel getDataForIndexPath:(int)indexPath.row]
                  OfHeight:menuRowHeight
                       CSS:nil
                       For:PAGE_MAIN_MENU_LIST
                  PageName:nil];
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    [menuModel tableView:tableView didSelectRowAtIndexPath:indexPath];
}

@end
