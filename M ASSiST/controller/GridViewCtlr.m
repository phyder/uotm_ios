//
//  GridViewCtlr.m
//  Universe on the move
//
//  Created by Suraj on 29/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "GridViewCtlr.h"

#import "GridViewModel.h"
#import "Navigation.h"
#import "Constant.h"
#import "Util.h"
#import "Holder.h"

#import "GridViewCell.h"


static NSString *cellId = @"gridviewcell";
static NSString *identifier = nil;
static NSString *headerViewIdentifier = @"gridviewheader";
static NSString *footerViewIdentifier = @"gridviewfooter";

@implementation GridViewCtlr

- (void) loadView {
    
    [super loadView];
    
    gridViewModel = [[GridViewModel alloc] initDataForView:self WithPage:_nextPage WithTitle:_nextTitle AndWithData:_content];
    _imgBgImage.image = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount:) name:@"updateNotificationCountGrid" object:nil];
    [Holder sharedHolder].testingNoti = true;
   
}
-(void) viewWillAppear:(BOOL)animated{
    NSLog(@"this is view will appear");
    if ([Holder sharedHolder].testingNoti == false){
        [self loadView];
    }
    
}
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [gridViewModel updatePendingApprovalCount];
   
}
- (void)updateNotificationCount:(NSNotification*)notification {
   [gridViewModel updateNotificationCount];
}
- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [gridViewModel finishedLoadingNotificationData:resNotificationlist];
}

#pragma mark - PSTCollectionView
- (NSString *)formatIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"{%ld,%ld}", (long)indexPath.row, (long)indexPath.section];
}

- (NSInteger)numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section; {
    return [gridViewModel getRowCount];
}

- (PSUICollectionViewCell *)collectionView:(PSUICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"Data: %@", [brandData objectAtIndex:indexPath.row]);
    cell = [cv dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    NSLog(@"this is index path %ld",(long)indexPath.row);
    [gridViewModel loadCell:cell AtIndexPath:indexPath];
    
    return cell;
}

- (PSUICollectionReusableView *)collectionView:(PSUICollectionView *)collectionView
             viewForSupplementaryElementOfKind:(NSString *)kind
                                   atIndexPath:(NSIndexPath *)indexPath {
	
    if ([kind isEqualToString:PSTCollectionElementKindSectionHeader]) {
		identifier = headerViewIdentifier;
	} else if ([kind isEqualToString:PSTCollectionElementKindSectionFooter]) {
		identifier = footerViewIdentifier;
	}
    PSUICollectionReusableView *supplementaryView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                       withReuseIdentifier:identifier
                                                                                              forIndexPath:indexPath];
    
    if ([kind isEqualToString:PSTCollectionElementKindSectionHeader]) {
		// No header
	} else if ([kind isEqualToString:PSTCollectionElementKindSectionFooter]) {
		// Footer for expense
        /**
         * Gives section footer a top partition of Black colour
         */
        /*CALayer *topBorder = [CALayer layer];
         topBorder.frame = CGRectMake(0.0f, 0.0f, 320.0f, 1.0f);
         topBorder.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f].CGColor;
         
         [supplementaryView.layer addSublayer:topBorder];*/
        [supplementaryView addSubview:[gridViewModel getFooterForExpense]];
	}
    
    return supplementaryView;
}

- (void)collectionView:(PSTCollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    cell = (GridViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell didHighlightItemForGridViewCell:cell];
}

- (void)collectionView:(PSTCollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    cell = (GridViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell didUnHighlightItemForGridViewCell:cell];
}

- (void)collectionView:(PSTCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    cell = (GridViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [gridViewModel didSelectItemAtIndexPath:indexPath];
}

- (void) openNotification {
    
    [gridViewModel openNotification];
}

- (void) finishedLoadingDynalistData: (NSDictionary *) resDynalist {
    
    if ([Util serverResponseCheckNullAndSession:resDynalist InFunction:__FUNCTION__ Silent:NO]) {
        
        NSDate *start = [NSDate date];
        [gridViewModel dynalistDataLoad:resDynalist];
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
        NSString *msg = [NSString stringWithFormat:@"Rendering Execution Time: %f", executionTime];
        NSLog(@"%@", msg);
        
        if (SHOW_EXECUTION_TIME) {
            UIAlertView *exeTimeAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:MOBILE4U_URL]
                                                                   message:msg
                                                                  delegate:nil
                                                         cancelButtonTitle:ALERT_OPT_OK
                                                         otherButtonTitles:nil, nil];
            [exeTimeAlert show];
        }
    }
}

- (void) webPageDataReceived:(NSDictionary *) resData {
    
    [gridViewModel webPageDataReceived:resData];
}

@end
