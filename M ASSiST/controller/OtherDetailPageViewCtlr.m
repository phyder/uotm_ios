//
//  DetailPageViewCtlr.m
//  Home Offer
//
//  Created by Suraj on 05/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "OtherDetailPageViewCtlr.h"

#import "Constant.h"
#import "Navigation.h"
#import "Util.h"

#import "OtherOfferDetail.h"

static NSString *nextPage = @"other-offer-details";

@interface OtherDetailPageViewCtlr ()

@end

@implementation OtherDetailPageViewCtlr

- (id)initWithStyle:(UITableViewStyle)style {
    
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modelOtherOffer = [[OtherOfferDetail alloc] initWithData:(NSMutableDictionary *) self.data
                                                 UIViewController:self];
    
    //[self initControl];
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    //NSLog(@"page-name: %@, data: %@", nextPage, nextPageContent);
    
    // Initialize the navigation controller
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
    [self.navigation setHeaderTitle:(self.data)[DETAIL_OFFER_TITLE]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

- (NSString *) getOfferId {
    return [self.modelOtherOffer getOfferId];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;   // Return the number of sections.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;   // Return the number of rows in the section.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int height = 0;
//    float cellHeight = 0;
    switch (indexPath.row) {
        case 0:
            //cellHeight = [self.modelOtherOffer getCellOneHeight];
            height = [self.modelOtherOffer getCellOneHeight];
            break;
        case 1:
            //cellHeight = [self.modelOtherOffer getCellTwoHeight];
            height = [self.modelOtherOffer getCellTwoHeight];
            break;
        case 2:
            //cellHeight = [self.modelOtherOffer getCellThreeHeight];
            height = [self.modelOtherOffer getCellThreeHeight];
            break;
        case 3:
            //cellHeight = [self.modelOtherOffer getCellFourHeight];
            height = [self.modelOtherOffer getCellFourHeight];
            break;
        case 4:
            //cellHeight = [self.modelOtherOffer getCellFourHeight];
            height = [self.modelOtherOffer getCellFiveHeight];
            break;
            
        default:
            break;
    }
    //NSLog(@"cell one height: %f", cellHeight);
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellOneIdentifier      = @"row1";
    static NSString *CellTwoIdentifier      = @"row2";
    static NSString *CellThreeIdentifier    = @"row3";
    static NSString *CellFourIdentifier     = @"row4";
    static NSString *CellFiveIdentifier     = @"row5";
    
    OtherOfferDetail *cell = nil;
    //NSLog(@"Row: %ld", (long)indexPath.row);
    // Init the cell with data
    
    switch (indexPath.row) {
        case 0:
            cell = (OtherOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellOneIdentifier];
            [self.modelOtherOffer getCellOneWithCell:cell];
            break;
        case 1:
            cell = (OtherOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellTwoIdentifier];
            [self.modelOtherOffer getCellTwoWithCell:cell];
            break;
        case 2:
            cell = (OtherOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellThreeIdentifier];
            [self.modelOtherOffer getCellThreeWithCell:cell];
            break;
        case 3:
            cell = (OtherOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellFourIdentifier];
            [self.modelOtherOffer getCellFourWithCell:cell];
            break;
        case 4:
            cell = (OtherOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellFiveIdentifier];
            [self.modelOtherOffer getCellFiveWithCell:cell];
            break;
            
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

@end
