//
//  ViewController.h
//  Home Offer
//
//  Created by Suraj on 27/02/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "MBProgressHUD.h"

@class Navigation;

@interface HomeMapViewCtlr : UIViewController <MBProgressHUDDelegate, MKMapViewDelegate, UIAlertViewDelegate> {
    
    NSString *notificationURL;
}

@property (nonatomic) Navigation *navigation;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIImageView *imgGreenRecent;
@property (strong, nonatomic) IBOutlet UIImageView *imgGreenPrevious;
//@property (nonatomic, strong) NSThread *backgroundThread;

@property (strong, nonatomic) IBOutlet UILabel *lblRecentOffer;
@property (strong, nonatomic) IBOutlet UILabel *lblOtherOffer;

- (void) openNotification;

@end
