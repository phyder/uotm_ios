//
//  WebViewController.m
//  engage
//
//  Created by Suraj on 12/7/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "WebViewController.h"

#import "AppConnect.h"
#import "Authentication.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Holder.h"
#import "Navigation.h"
#import "Util.h"
#import "IIViewDeckController.h"
#import "UIImage+animatedGIF.h"

#define KB 1024
#define MB KB*KB

@interface WebViewController (){
   
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    SFSpeechRecognitionTask *recognitionTask;
    AVAudioEngine *audioEngine;
    
    // Record speech using audio Engine
    AVAudioInputNode *inputNode;
    NSString *str;
    
    UIImageView *gifView;
    UIButton *recBtn;
    
    BOOL testPdf;
}

@end

@implementation WebViewController

@synthesize webURL=webURL, webView=webView;
@synthesize data=data, css=css, page=page, urlType=urlType;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [Holder sharedHolder].isConnectionAbort = PAGE_FALSE;
    }
    return self;
}

- (void) loadView {
    [super loadView];
    _synthesizer  = [[AVSpeechSynthesizer alloc] init];
    //Additional Setup
    [Holder sharedHolder].refresh = PAGE_FALSE;
}

//commented for not using speech recognition

//- (void) viewDidAppear:(BOOL)animated {
//    
//    _recognizer = [[SFSpeechRecognizer alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
//    [_recognizer setDelegate:self];
//    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus authStatus) {
//        switch (authStatus) {
//            case SFSpeechRecognizerAuthorizationStatusAuthorized:
//                //User gave access to speech recognition
//                NSLog(@"Authorized");
//                break;
//                
//            case SFSpeechRecognizerAuthorizationStatusDenied:
//                //User denied access to speech recognition
//                NSLog(@"SFSpeechRecognizerAuthorizationStatusDenied");
//                break;
//                
//            case SFSpeechRecognizerAuthorizationStatusRestricted:
//                //Speech recognition restricted on this device
//                NSLog(@"SFSpeechRecognizerAuthorizationStatusRestricted");
//                break;
//                
//            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
//                //Speech recognition not yet authorized
//                
//                break;
//                
//            default:
//                NSLog(@"Default");
//                break;
//        }
//    }];
//    
//    audioEngine = [[AVAudioEngine alloc] init];
//    _synthesizer  = [[AVSpeechSynthesizer alloc] init];
//    [_synthesizer setDelegate:self];
//}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     testPdf = NO;
     
    //test for notification 
    
//    
//    webView.scrollView.delegate = self; // set delegate method of UISrollView
//    webView.scrollView.maximumZoomScale = 500; // set as you want.
//    webView.scrollView.minimumZoomScale = 1; // set as you want.
//    
//    //// Below two line is for iOS 6, If your app only supported iOS 7 then no need to write this.
//    webView.scrollView.zoomScale = 2;
//    webView.scrollView.zoomScale = 1;
//    
    
    if (![data[PAGE_FOOTER] isEqualToString:PAGE_TRUE]) {
        [self.navigationController setToolbarHidden:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.delegate = self;

//    NSLog(@"url check: %@\n%@", self.urlType, self.webURL);
    
    if ([self.urlType isEqualToString:@"e:/"]) {
        self.webURL = [self.webURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        loadURL = [NSURL URLWithString:self.webURL];
    } else if ([self.urlType isEqualToString:@"l:/"]) {
        NSURL *baseURL =[NSURL fileURLWithPath:self.webURL];

//  loadURL = [NSURL URLWithString:[NSString stringWithFormat:@"#/%@/Ajay%20Maurya",([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]] relativeToURL:baseURL];
      NSLog(@" web URL is %@",self.webURL);
        
    NSString *urlEncodedString = [([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_EMPLOYEE_NAME] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
       // NSLOG(@"Name is %@",urlEncodedString);
     loadURL = [NSURL URLWithString:[NSString stringWithFormat:@"#/%@/%@",([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID], urlEncodedString] relativeToURL:baseURL];
      //  ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]
//        loadURL = [NSURL URLWithString:[NSString stringWithFormat:@"#/shailratnani/%@",urlEncodedString] relativeToURL:baseURL];
        NSLog(@" Load URL is %@",loadURL);

    } else {
        loadURL = [NSURL URLWithString:self.webURL];
    }
    
    int memoryCapacity  = 0.1*MB;
    int discCapacity    = 5*MB;
    NSURLCache *cache   = [[NSURLCache alloc] init];
    [cache setMemoryCapacity:memoryCapacity];
    [cache setDiskCapacity:discCapacity];
    [NSURLCache setSharedURLCache:cache];
    
    int timeoutInterval;
    
    //    NSURL *bbudle = [[NSBundle mainBundle] bundleURL];
    //NSLog(@"ok %@", loadURL);
    if ([loadURL.absoluteString rangeOfString:@"expense/apply_form.htm"].location!=NSNotFound) {
        //This fucntionality is currently disabled, request timeout for all pages is now 1000 secs
//        NSLog(@"Loading apply expense with session timeout 60s NOT defoult value");
        timeoutInterval = [[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT_EXPENSE_APPLY] intValue];
    } else if ([loadURL.absoluteString rangeOfString:@"team_calendar"].location!=NSNotFound) {
        //This fucntionality is currently disabled, request timeout for all pages is now 1000 secs
//        NSLog(@"Loading team calendar with session timeout 30s NOT defoult value");
        timeoutInterval = [[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT_TEAMCALENDAR] intValue];
    } else {
        timeoutInterval = [[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:loadURL
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:timeoutInterval];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:nil];
    
    if (connection) {
        [self.webView loadRequest:request];
    }
    
//    NSLog(@"web loaded %@ with url: ||%@||", data[@"name"], loadURL);
    
    // Initialize the navigation controller
    self.navigation = [[Navigation alloc] initWithViewController:self PageData:data];
    [Holder sharedHolder].currentPageName = page;
    
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}

- (void) signoutWithTitle:(NSString *) alertTitle AndMessage:(NSString *) alertMessage {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                    message:alertMessage
                                                   delegate:self
                                          cancelButtonTitle:ALERT_OPT_OK
                                          otherButtonTitles:nil];
    alert.tag = 1;
    
    [alert show];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:ALERT_OPT_OK]) {
        [self checkHeaderAndFooter];
        
        Holder *singleton       = [Holder sharedHolder];
        
        NSString *strSignOutURL = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_LOGOUT_URL], J_SESSION_ID, (singleton.loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
//        NSLog(@"Sign Out URL: %@", strSignOutURL);
        AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(responseSignOut)
                                                              onObject:self];
        [conn requestSignOutWithURL:strSignOutURL];
        
        singleton.isLoggedIn    = PAGE_FALSE;
        singleton.loginInfo     = nil;
        [Util setSettingWithName:BACKGROUND_IS_LOGGED_IN ByValue:PAGE_FALSE];
        [Util setSettingWithName:BACKGROUND_LOGGED_INFO ByValue:EMPTY];
        
        if(alertView.tag == 1) {
            
                
                NSLog(@"%s", __FUNCTION__);
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:@"navautologin"];
                //
                IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
                //    //deckController.panningMode = IIViewDeckFullViewPanning;
                deckController.centerController = initViewController;
        } else {
            [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:YES];
        }
    }
}

- (void) responseSignOut {
    
    NSLog(@"Sign out response");
}

#pragma mark - MBProgressHUDDelegate Methods
- (void) loaderCancelPressed {
#ifdef DEBUG
    NSLog(@"%s", __FUNCTION__);
#endif
    [Holder sharedHolder].isConnectionAbort = PAGE_TRUE;
    [self checkHeaderAndFooter];
    [webView stopLoading];
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    
//    NSLog(@"refresh check: %@", self.webView.request.URL.absoluteString);
    if([self.webView.request.URL.absoluteString isEqualToString:EMPTY]) {
        [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
    }
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    
	// Remove HUD from screen when the HUD was hidded
	[progressHUD removeFromSuperview];
	progressHUD = nil;
}

//#pragma mark - NSURLConnectionDelegate
//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
//    
//    NSLog(@"\n\nwill cache response called\n\n");
//    
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)[cachedResponse response];
//    
//    // Look up the cache policy used in our request
//    if([connection currentRequest].cachePolicy == NSURLRequestUseProtocolCachePolicy) {
//        NSDictionary *headers = [httpResponse allHeaderFields];
//        NSString *cacheControl = [headers valueForKey:@"Cache-Control"];
//        NSString *expires = [headers valueForKey:@"Expires"];
//        if((cacheControl == nil) && (expires == nil)) {
//            NSLog(@"server does not provide expiration information and we are using NSURLRequestUseProtocolCachePolicy");
//            return nil; // don't cache this
//        }
//    }
//    return cachedResponse;
//}
//
//-(void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
//        if ([challenge.protectionSpace.host isEqualToString:loadURL.host]) {
//            NSLog(@"trusting connection to host %@", challenge.protectionSpace.host);
//            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
//        } else
//            NSLog(@"Not trusting connection to host %@", challenge.protectionSpace.host);
//    }
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
//}
//
//-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)pResponse {
//    _Authenticated = YES;
//    [connection cancel];
//    [self.webView loadRequest:_FailedRequest];
//}

#pragma mark - UIWebViewDelegate Methods
- (void) webViewDidStartLoad:(UIWebView *)uiWebView {
    
    /*NSString *uiwebViewURL = uiWebView.request.URL.absoluteString;
    NSLog(@"web url on start: %@", uiwebViewURL);
    
    if ([uiwebViewURL rangeOfString:@"refresh=true"].location != NSNotFound) {
        NSLog(@"refresh found");
        [uiWebView stopLoading];
        //self.webURL = [uiwebViewURL substringToIndex:[uiwebViewURL rangeOfString:@"&refresh=true"].location];
        //NSLog(@"New URL: %@", self.webURL);
        if ([uiWebView canGoBack]) {
            NSLog(@"go back");
            [uiWebView goBack];
        } else {
            NSLog(@"no go back");
        }
    } else {
        NSLog(@"refresh not found");
        [self webViewGoBack];
        [Holder sharedHolder].refresh = PAGE_FALSE;
    }
    else {*/
    
    if (![_pdfUrl hasSuffix:@"pdf"]) {
        if (testPdf) {
           
            webView.scrollView.maximumZoomScale = 50; // set as you want.
            webView.scrollView.minimumZoomScale = 1;
        }
        
        
        
    progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:progressHUD];
    
    progressHUD.mode                = MBProgressHUDModeCustomView;
    progressHUD.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    progressHUD.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    progressHUD.delegate            = self;
    progressHUD.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    progressHUD.dimBackground       = YES;
    progressHUD.detailsLabelText    = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"web-loader"]];
    [progressHUD show:YES];
    //}
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    if ([[Holder sharedHolder].isConnectionAbort isEqualToString:PAGE_TRUE]) {
        [Holder sharedHolder].isConnectionAbort = PAGE_FALSE;
    } else {
        NSLog(@"Error: %@", error);
        NSLog(@"Error: occured during the page loading: %@", [Holder sharedHolder].refresh);
        [progressHUD hide:YES];
        if ([[Holder sharedHolder].refresh isEqualToString:PAGE_FALSE]) {
            [self checkHeaderAndFooter];
            [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
        }
        [ErrorLog logError:&error WithCode:ERR_CODE_NSERROR Message:ERR_MSG_NSERROR InFunction:__FUNCTION__];
        [Util showTechnicalError];
    }
}
#pragma mark -
#pragma mark - UIScrollView Delegate Methods

//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
//{
//    self.webView.scrollView.maximumZoomScale = 20; // set similar to previous.
//}

- (void) webViewDidFinishLoad:(UIWebView *) uiwebView {
    
    if (testPdf) {
        testPdf = NO;
        webView.scrollView.maximumZoomScale = 50; // set as you want.
        webView.scrollView.minimumZoomScale = 1;
    }
    
    @try {
        
//        [webView stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 5.0;"];
        
        
        [progressHUD hide:YES];
        
        [Holder sharedHolder].refresh = PAGE_FALSE;
        
        NSLog(@"Request Header: %@",[[webView request] valueForHTTPHeaderField:@"Cache-Control"]);
        
        NSCachedURLResponse *resp = [[NSURLCache sharedURLCache] cachedResponseForRequest:uiwebView.request];
        NSLog(@"Response Header: %@",[(NSHTTPURLResponse*)resp.response allHeaderFields]);
        
        NSString *htmlContent = [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.textContent"];
//        NSString *htmlContent = [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
//        NSLog(@"html content:\n %@", htmlContent);
        NSError* error = nil;
        NSMutableDictionary *jsonContent = [NSJSONSerialization JSONObjectWithData:[htmlContent dataUsingEncoding:NSUTF8StringEncoding]
                                                                           options:NSJSONReadingMutableLeaves
                                                                             error:&error];
        if (error!=nil) {
            // This is checked whether the server is sending JSON response
//            NSLog(@"%s Error: while parsing html response", __FUNCTION__);
        } else {
            
//            NSLog(@"Server Response parsed: \n%@", jsonContent);
            if ([Util isKey:SERVER_RES_STATUS ExistForDictionary:jsonContent]) {
                
                if ([jsonContent[SERVER_RES_STATUS] intValue] == ERR_CODE_500) {
                    // show msg in alertbox & sign out
                    [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML=''"];
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:@""];
                    [self signoutWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                AndMessage:jsonContent[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Runtime";
#if DEBUG_ERROR_LOG
        NSLog(@"%s %@", __FUNCTION__, strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    } @finally {
        
    }
}

// This selector is called when something is loaded in our webview, By something I don't mean anything but just "some" :
//  - main html document, - sub iframes document
// But all images, xmlhttprequest, css, ... files/requests doesn't generate such events :/
- (BOOL) webView:(UIWebView *)webView2
shouldStartLoadWithRequest:(NSURLRequest *)request
  navigationType:(UIWebViewNavigationType)navigationType {
    
    BOOL result = YES;
    @try {
//        result = _Authenticated;
//        if (!_Authenticated) {
//            _FailedRequest = request;
//            NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//            if (connection) {
//                // Create the NSMutableData to hold the received data.
//                // receivedData is an instance variable declared elsewhere.
//                NSLog(@"Connection initiated.");
//            } else {
//                Inform the user that the connection failed from the connection:didFailWithError method
//                NSLog(@"Connection NOT initiated");
//            }
//            NSLog(@"Authenticating connection: %@", connection);
//        }
        NSString *requestString = [[request URL] absoluteString];
        NSDictionary *params = [self getParamsFromURL:requestString];
        NSLog(@"URL fired is: %@", requestString);
        
        //Opens the request in the Web browser
        if ([requestString rangeOfString:@"open=new"].location != NSNotFound) {
//            NSLog(@"URL contains open=new so the URL will open in browser.");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:requestString]];
            result = NO;
        }
        
        if ([requestString rangeOfString:@"tel"].location != NSNotFound) {
            //            NSLog(@"URL contains tel so the URL will open in browser.");
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:requestString]];
            
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                
                result = YES;
            } else {
                result = NO;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Universe on the move"
                                                                message:@"Your device doesn't support this feature."
                                                               delegate:nil
                                                      cancelButtonTitle:ALERT_OPT_OK
                                                      otherButtonTitles:nil];
                [alert show];
                
               // [notPermitted release];
            }
            
        }
        
        if ([requestString hasPrefix:@"m://engage?"]) {
            
            
            if ([requestString hasPrefix:@"URL_NM"] && [requestString hasPrefix:@"state"] ) {
                
                NSString * parameter =@"";
                NSString * urlName =@"";
                NSString * state =@"";
                NSString * typeDetail =@"";
                NSString * year =@"";
                
                if ([params[@"action"] isEqualToString:@"url"]) {
                    
                    urlName = params[@"URL_NM"];
                    state =  params[@"state"];
                    typeDetail = params[@"type"];
                    year = params[@"year"];
                }
                
                urlName = [urlName substringWithRange:NSMakeRange(0, [urlName length]-1)];
                
                state = [@"?state=" stringByAppendingString:state];
                typeDetail = [@"&type=" stringByAppendingString:typeDetail];
                year = [@"&year=" stringByAppendingString:year];
                parameter = [NSString stringWithFormat: state, typeDetail, year];
                
                NSLog(@"Parameter in Url %@", parameter);
                
                result = NO;
            } else {
                // NSLog(@"request : %@ & params: %@", requestString, params);
                
                if ([params[@"action"] isEqualToString:@"url"]) {
                    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
                    NSString *strUrl = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], params[@"URL_NM"], J_SESSION_ID, strSessionID];
                    NSMutableString *URLParams = [NSMutableString string];
                    [params enumerateKeysAndObjectsWithOptions:NSEnumerationConcurrent
                                                    usingBlock:^(id key, id object, BOOL *stop) {
                                                        // NSLog(@"%@ = %@", key, object);
                                                        if ([key isEqualToString:@"action"] || [key isEqualToString:@"URL_NM"]) {
                                                            return;
                                                        }
                                                        [URLParams appendFormat:@"%@=%@&", key, object];
                                                    }];
                    NSString *strParams = [URLParams substringToIndex:[URLParams length]-1];
                    
                    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(responseOtherCalendar:) onObject:self];
                    [conn requestOtherCalendar:strUrl Params:strParams];
                }
                
                if ([params[@"action"] isEqualToString:@"alert"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:params[@"msgHead"]
                                                                    message:params[SERVER_RES_MEG]
                                                                   delegate:nil
                                                          cancelButtonTitle:ALERT_OPT_OK
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                
                if ([params[@"action"] isEqualToString:@"back"]) {
                    [[Holder sharedHolder].auth startBackgroundNotificationCheck];
                    [self checkHeaderAndFooter];
                    [Holder sharedHolder].backFlag = PAGE_TRUE;
                    [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
                    
                }
                
                if ([params[@"action"] isEqualToString:@"open"]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:params[@"url"]]];
                }
                
                if ([params[@"action"] isEqualToString:@"signout"]) {
                    [self signoutWithTitle:params[@"msgHead"]
                                AndMessage:params[SERVER_RES_MEG]];
                }
                
                result = NO;
            }
            
        } else {
            
            
//            _pdfUrl = requestString;
//            if ([requestString containsString:@"pdf"]) {
//                UIApplication *app = [UIApplication sharedApplication];
//                NSURL *request = [NSURL URLWithString:requestString];
//                if ([app canOpenURL:request]) {
//                    [app openURL:request]; //this is for testing
//                }
//                
//                NSLog(@"yes");
//               
////                 [webView stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 5.0;"];
//            }            else{
            self.webURL = requestString;
            if ([params[@"action"] isEqualToString:@"omin"]) {
                testPdf = YES;
//                webView.scrollView.maximumZoomScale = 50; // set as you want.
//                webView.scrollView.minimumZoomScale = 1;
//                 [webView stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 50.0;"];
            }
            
            if ([requestString isEqualToString:@"ios:converttotext"]) {
              NSLog(@"Call back received");
               [self initRecording];
                result = NO;
            }
            
            if ([requestString isEqualToString:@"ios:webToNativeSessionExpiredCall"]) {
                [self performSelector:@selector(sessionExpired)];
                // Cancel the location change
                return NO;
            }

        
        }
        return result;
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Runtime";
#if DEBUG_ERROR_LOG
        NSLog(@"%s %@", __FUNCTION__, strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    } @finally {
        
    }
}



-(NSString *)urlDecodeUsingEncoding:(NSString *) string  {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                                 (__bridge CFStringRef)string,
                                                                                                 CFSTR(""), 
                                                                                                 kCFStringEncodingUTF8));
}
- (void)responseOtherCalendar:(NSDictionary *)response {
    NSLog(@"Other Calendar response: %@", response);
    if ([Util serverResponseCheckNullAndSession:response InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:PAGE_TYPE_LIST In:response InFunction:__FUNCTION__ Message:[NSString stringWithFormat:@"%@ parameter", PAGE_TYPE_LIST] Silent:NO]) {
            unsigned long dataCount = [response[PAGE_TYPE_LIST] count];
            //            NSLog(@"response count %d page: %@", dataCount, nextPage);
            
            //Checks the data count in list
            if (dataCount==0) {
                
                UIAlertView *noData =[[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                message:[Util getSettingUsingName:DYNA_LIST_NODATA_MSG]
                                                               delegate:nil
                                                      cancelButtonTitle:ALERT_OPT_OK
                                                      otherButtonTitles:nil];
                [noData show];
            } else {
                NSString *strNextPage = @"other-holiday";
                [Util finishLoadingDynaListWithata:response NextPage:strNextPage JSONURL:@"" OnView:self];
            }
        }
    }
}

- (void) webViewGoBack {
    
    if ([data[@"check-web-stack"] isEqualToString:PAGE_FALSE]) {
        
        [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
    } else {
        //NSString *uiwebViewURL = webView.request.URL.absoluteString;
        //NSLog(@"URL on BACK: %@", uiwebViewURL);
        //if ([uiwebViewURL rangeOfString:@"refresh=true"].location != NSNotFound) {
        //    [Holder sharedHolder].refresh = PAGE_TRUE;
        //}
        
        if ([webView canGoBack]) {
            [webView goBack];
        } else if (![webView canGoBack]) {
            [self checkHeaderAndFooter];
            //[Holder sharedHolder].backFlag = PAGE_TRUE;
            [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
            //NSLog(@"POP - Header: %@ Footer: %@", [data objectForKey:PAGE_HEADER], [data objectForKey:PAGE_FOOTER]);
        }
    }
}

#pragma mark - Custom Implementation
- (NSDictionary *) getParamsFromURL: (NSString *) url {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([url rangeOfString:@"?"].location != NSNotFound) {
        url = [url substringFromIndex:[url rangeOfString:@"?"].location+1];
        NSArray *components = [url componentsSeparatedByString:@"&"];
        
        for (int i=0; i<[components count]; i++) {
            if([components[i] rangeOfString:@"="].location != NSNotFound) {
            NSArray *tmp = [components[i] componentsSeparatedByString:@"="];
            [params setValue:tmp[1] forKey:tmp[0]];
            }
        }
    }
    return params;
}

- (void) checkHeaderAndFooter {
    
    if ([data[PAGE_HEADER] isEqualToString:PAGE_FALSE]) {
        self.navigationController.navigationBarHidden = YES;
    }
    if ([data[PAGE_FOOTER] isEqualToString:PAGE_FALSE]) {
        [self.navigationController setToolbarHidden:NO];
    }
}

- (void) btnBottombarLeftBarButton1Pressed {
#ifdef DEBUG
    NSLog(@"%s help button", __FUNCTION__);
#endif
    
    [Util openHelp:self];
}

- (void) refreshWeb {
    NSLog(@"refresh called: %@", (self.data)[@"server-data"]);
//    NSString *url = self.webURL;
//    if ([url rangeOfString:@"refresh=true"].location == NSNotFound) {
//        url = [NSString stringWithFormat:@"%@%@", url, @"&refresh=true"];
//        if ([self.webURL rangeOfString:@"refresh=true"].location!=NSNotFound) {
//            [self.webView goBack];
//        }
//        self.webURL = url;
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webURL]
//                                                   cachePolicy:NSURLCacheStorageAllowed
//                                               timeoutInterval:[[Util getSettingUsingName:@"server-request-timeout"] intValue]]];
//    } else {
//        [self.webView reload];
//    }
    [Holder sharedHolder].refresh = PAGE_TRUE;
    NSLog(@"current page URL: %@ %@", self.webURL, [Holder sharedHolder].refresh);
    if ([(self.data)[@"server-data"] isEqualToString:PAGE_TRUE]) {
        // TODO get JSON data again
        //NSLog(@"URL: %@\n data: %@", self.page, self.data);
        NSString *strRefreshDataURL = [(self.data)[@"url"] substringFromIndex:2];
        AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(dataRefreshCompleteWithData:) onObject:self];
        [conn refreshWebPageDataForURL:strRefreshDataURL];
    } else {
        [self.webView reload];
    }
    //NSLog(@"Refresh URL: %@", self.webURL);
}

- (void) dataRefreshCompleteWithData:(NSMutableDictionary *) resData {
    
    //NSLog(@"Data received is: %@", resData);
    if ([Util isNull:resData]) {
        
        [ErrorLog logNullResponseInFunciton:__FUNCTION__
                                WithMessage:@"server response"];
        [Util showTechnicalError];
    } else {
        
        if ([Util isNull:resData[SERVER_RES_STATUS]]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__
                                    WithMessage:@"status parameter"];
            if ([resData[SERVER_RES_STATUS] intValue] == ERR_CODE_500) {
                [self signoutWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                            AndMessage:[Util getSettingUsingName:SERVER_SESSION_EXPIRED]];
            }
        } else {
            [Util writeDataInJS:resData[@"data"]];
            [self.webView reload];
        }
    }
}

#pragma mark - Notification Request
- (void) openNotification {
    
    strURLNotification = [Util getNotificationURL];
    [Util openNotificationWithURL:strURLNotification AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:strURLNotification];
}

#pragma mark - Speech Recognition


- (void) initRecording {
   
    if (audioEngine.isRunning) {
        NSLog(@"running");
        gifView.hidden = YES;
        recBtn.hidden = YES;
        //        [_recBtn setImage:[UIImage imageNamed:@"contest_recording"] forState:UIControlStateNormal];
        recognitionTask = [_recognizer recognitionTaskWithRequest:recognitionRequest
                                                         delegate:self];
        gifView.hidden = YES;
        recBtn.hidden  = YES;
        [inputNode removeTapOnBus:0];
        [audioEngine stop];
        [recognitionRequest endAudio];
        
    }
    else {
        NSLog(@"starting");
        gifView =[[UIImageView alloc] initWithFrame:CGRectMake(100,100,150,150)];
        //gifView.image=[UIImage imageNamed:@"draw.png"];
        [self.view addSubview:gifView];
       // gifView.hidden = YES;
        
        recBtn = [[UIButton alloc] initWithFrame: CGRectMake(100, 100, 125 , 125)];
        // recBtn.frame = CGRectMake(10, 10, 100 , 100);
        [recBtn setTitle:@"Show View" forState:UIControlStateNormal];
        [recBtn setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
        [recBtn addTarget:self action:@selector(initRecording) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:recBtn];
       // recBtn.hidden = YES;

        [self startRecording];
    }

}
// recording
- (void)startRecording {
    
//    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"recording_animate" withExtension:@"gif"];
    gifView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
   // [recBtn setImage:[UIImage imageNamed:@"voice_contest_recording"] forState:UIControlStateNormal];

//    gifView.hidden = NO;
//    recBtn.hidden = NO;
   
    
    if (recognitionTask) {
        [recognitionTask cancel];
        recognitionTask = nil;
    }
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryRecord mode:AVAudioSessionModeMeasurement options:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    [session setActive:TRUE withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    
    inputNode = audioEngine.inputNode;
    
    recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    recognitionRequest.shouldReportPartialResults = YES;
    
    
    AVAudioFormat *format = [inputNode outputFormatForBus:0];
    
    [inputNode installTapOnBus:0 bufferSize:1024 format:format block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        [recognitionRequest appendAudioPCMBuffer:buffer];
    }];
    [audioEngine prepare];
    NSError *error1;
    [audioEngine startAndReturnError:&error1];
    NSLog(@"This is error %@", error1.description);
    
}


- (void)speechRecognitionTask:(SFSpeechRecognitionTask *)task didFinishRecognition:(SFSpeechRecognitionResult *)result {
    
    NSLog(@"speechRecognitionTask:(SFSpeechRecognitionTask *)task didFinishRecognition");
    NSString * translatedString = [[[result bestTranscription] formattedString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //[self log:translatedString];
  //  self.textToSpeechView.text = translatedString;
    
    if ([result isFinal]) {
        [audioEngine stop];
        [inputNode removeTapOnBus:0];
        recognitionTask = nil;
        recognitionRequest = nil;
        
        NSString * jsCallBack = [NSString stringWithFormat:@"testEcho('%@')",translatedString];
        [webView stringByEvaluatingJavaScriptFromString:jsCallBack];
        NSLog(@"this is background thread this is ");
        
    }
}

//- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
//{
//      NSLog(@"this is background thread");
//}

- (void)speakNextUtterance
{
    AVSpeechUtterance *nextUtterance = [[AVSpeechUtterance alloc]
                                        initWithString:@"my name is siri"];
    [nextUtterance setVolume:1.0];
    [self.synthesizer speakUtterance:nextUtterance];
}



- (void)startSpeaking
{
    if (!self.synthesizer) {
        self.synthesizer = [[AVSpeechSynthesizer alloc] init];
    }
    
    [self speakNextUtterance];
}

- (void) sessionExpired {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@ Session Expired.", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    // [[Holder sharedHolder].auth requestSignOutWithMenuToggleCheck:YES];
    [self signoutWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                AndMessage:@"Your session has been expired. Please login again"];
}
@end
