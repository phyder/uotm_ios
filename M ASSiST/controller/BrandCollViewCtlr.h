//
//  BrandCollViewCtlr.h
//  Universe on the move
//
//  Created by Suraj on 16/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "PSTCollectionView.h"

@class AppConnect;
@class Navigation;

@interface BrandCollViewCtlr : PSUICollectionViewController<PSTCollectionViewDataSource, PSTCollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate> {
    
    NSString *notificationURL;
    NSArray *brandData, *carListData;
}

@property (nonatomic) Navigation *navigation;

@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;

- (void) openNotification;

@end
