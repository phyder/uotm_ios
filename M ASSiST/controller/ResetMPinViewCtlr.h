//
//  ResetMPinViewCtlr.h
//  Universe on the move
//
//  Created by Phyder on 21/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ResetMPINModel;

@interface ResetMPinViewCtlr : UIViewController <UITextFieldDelegate> {
    
    BOOL slide;
    
    ResetMPINModel *mPinPage;
}

@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgMPinBackground;

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;

@property (strong, nonatomic) IBOutlet UITextField *txtNewMPin1;
@property (strong, nonatomic) IBOutlet UITextField *txtNewMPin2;
@property (strong, nonatomic) IBOutlet UITextField *txtNewMPin3;
@property (strong, nonatomic) IBOutlet UITextField *txtNewMPin4;

@property (strong, nonatomic) IBOutlet UITextField *txtRetypeMPin1;
@property (strong, nonatomic) IBOutlet UITextField *txtRetypeMPin2;
@property (strong, nonatomic) IBOutlet UITextField *txtRetypeMPin3;
@property (strong, nonatomic) IBOutlet UITextField *txtRetypeMPin4;

@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

- (IBAction)btnClearPressed:(id)sender;

- (IBAction)txtFieldNewMPin1Didchange:(id)sender;
- (IBAction)txtFieldNewMPin2Didchange:(id)sender;
- (IBAction)txtFieldNewMPin3Didchange:(id)sender;
- (IBAction)txtFieldNewMPin4Didchange:(id)sender;

- (IBAction)txtFieldRetypeMPin1Didchange:(id)sender;
- (IBAction)txtFieldRetypeMPin2Didchange:(id)sender;
- (IBAction)txtFieldRetypeMPin3Didchange:(id)sender;
- (IBAction)txtFieldRetypeMPin4Didchange:(id)sender;

@end
