//
//  VerifyMPinViewCtlr.m
//  Universe on the move
//
//  Created by Phyder on 22/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "VerifyMPinViewCtlr.h"
#import "Util.h"
#import "Constant.h"
#import "Holder.h"
#import "Authentication.h"
#import "CarouselViewCtlr.h"
#import "GridViewModel.h"
#import "MenuViewModel.h"

@interface VerifyMPinViewCtlr ()

@end

@implementation VerifyMPinViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    _viewPopup.layer.borderColor = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _viewPopup.layer.borderWidth = 1;
    _viewPopup.layer.cornerRadius = 5;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideVerifyCtlr)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [tapRecognizer setDelegate:self];
    [self.viewBackground addGestureRecognizer:tapRecognizer];
    
    [self initMPin];
    [self initButton];
    
    
    //    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
    //    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
    //                                                     message:[[NSUUID UUID] UUIDString]
    //                                                    delegate:nil
    //                                           cancelButtonTitle:@"Ok"
    //                                           otherButtonTitles:nil];
    //    [device show];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initMPin {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtMPin1.delegate               = self;
    _txtMPin2.delegate               = self;
    _txtMPin3.delegate               = self;
    _txtMPin4.delegate               = self;
    
    [_txtMPin1 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_txtMPin2 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_txtMPin3 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_txtMPin4 setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    
    //Single TextField
    _txtMPin1.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtMPin2.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtMPin3.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtMPin4.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    
    _txtMPin1.layer.borderWidth      = 1.0f;
    _txtMPin2.layer.borderWidth      = 1.0f;
    _txtMPin3.layer.borderWidth      = 1.0f;
    _txtMPin4.layer.borderWidth      = 1.0f;
    
    _txtMPin1.layer.cornerRadius     = 4.0f;
    _txtMPin2.layer.cornerRadius     = 4.0f;
    _txtMPin3.layer.cornerRadius     = 4.0f;
    _txtMPin4.layer.cornerRadius     = 4.0f;
    
    _txtMPin1.keyboardType = UIKeyboardTypeNumberPad;
    _txtMPin2.keyboardType = UIKeyboardTypeNumberPad;
    _txtMPin3.keyboardType = UIKeyboardTypeNumberPad;
    _txtMPin4.keyboardType = UIKeyboardTypeNumberPad;
    
    _txtMPin1.returnKeyType = UIReturnKeyNext;
    _txtMPin2.returnKeyType = UIReturnKeyNext;
    _txtMPin3.returnKeyType = UIReturnKeyNext;
    _txtMPin4.returnKeyType = UIReturnKeyNext;
    
    _txtMPin1.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtMPin2.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtMPin3.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtMPin4.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
}

- (void) initButton {
    
    //_txtUserName.textColor = [Util colorWithHexString:@"E9B472"];
    //[_btnVerify.titleLabel setFont:[UIFont fontWithName:APP_FONT_BOLD size:17]];
    
    //[_btnVerify setTitle:@"Verify" forState:UIControlStateNormal];
    
    //_btnVerify.titleLabel.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    [_lblTitle setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    [_btnVerify setImage:[Util getImageAtDocDirWithName:@"submit.png"] forState:UIControlStateNormal];
}

#pragma mark - Navigation Slider
- (void) initSalarySlip {
    
    NSLog(@"%s %@", __FUNCTION__, [_controller class]);
    
    if([self.controller isKindOfClass:[CarouselViewCtlr class]]) {
        CarouselViewCtlr *ctlr = (CarouselViewCtlr *) self.controller;
        [ctlr didSelectItemAtIndex:self.index];
    } else {
        if([self.model isKindOfClass:[GridViewModel class]]) {
            GridViewModel *mdl = (GridViewModel *) self.model;
            [mdl didSelectItemAtIndex:self.index];
        } else if([self.model isKindOfClass:[MenuViewModel class]]){
            MenuViewModel *mdl = (MenuViewModel *) self.model;
            [mdl didSelectItemAtIndex:self.index];
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void) hideVerifyCtlr {
    Util *util = [[Util alloc] init];
    [util hideVerifyCtlrWithView:self];
}

#pragma mark - UITextFieldDelegate
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    // Transform the views to the normal view
    //[self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

#pragma mark - Keyboard Slider
- (void) maintainVisibityOfControl: (UITextField *) textField {
    
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    //double visibleHeight = bounds.size.height-216;
    
    double fieldYPosition = textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}

- (void) resVerifyMPin:(NSMutableDictionary *) resVerifyMPin {
    
    NSLog(@"Verify MPin response: %@", resVerifyMPin);
    [[Holder sharedHolder].auth postVerifyMPinAuthenticationWithData:resVerifyMPin];
}

- (IBAction)btnVerifyClick:(id)sender {
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self AndModel:nil];
    NSString *userName = [Util getKeyChainUserName];
    NSString *mpin = [NSString stringWithFormat:@"%@%@%@%@",_txtMPin1.text,_txtMPin2.text,_txtMPin3.text,_txtMPin4.text];
    [[Holder sharedHolder].auth requestVerifyMPinWithUserName:userName MPin:mpin];
}

- (IBAction)txtFieldMPin1Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldMPin2Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldMPin3Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldMPin4Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (void) textFieldDidChange: (UITextField *) textfield {
    NSString *text = textfield.text;
    /* get first char */
    NSString *firstChar = [[NSString alloc] init];
    if ([text length]>0) {
        firstChar = [text substringToIndex:1];
    }
    
    if ([textfield isEqual:self.txtMPin1]) {
        
        if ([text length] >= 1) {
            self.txtMPin1.text = firstChar;
            [self.txtMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtMPin2]) {
        
        if ([text length] >= 1) {
            self.txtMPin2.text = firstChar;
            [self.txtMPin3 becomeFirstResponder];
        } else {
            [self.txtMPin1 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtMPin3]) {
        if ([text length] >= 1) {
            self.txtMPin3.text = firstChar;
            [self.txtMPin4 becomeFirstResponder];
        } else {
            [self.txtMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtMPin4]) {
        if ([text length] >= 1) {
            self.txtMPin4.text = firstChar;
            [self.txtMPin4 resignFirstResponder];
            [self resetViewToIdentityTransform];
        } else {
            [self.txtMPin3 becomeFirstResponder];
        }
    }
}

@end
