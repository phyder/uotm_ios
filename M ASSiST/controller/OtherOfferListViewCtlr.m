//
//  OfferListViewCtlr.m
//  Other Offer
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "OtherOfferListViewCtlr.h"

#import "AppConnect.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Util.h"

#import "OtherOfferListCell.h"
#import "OtherDetailPageViewCtlr.h"

static NSString *nextPage       = @"other-offer";
static NSString *CellIdentifier = @"offer-list-cell";

@interface OtherOfferListViewCtlr ()

@end

@implementation OtherOfferListViewCtlr

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

- (void) loadView {
    [super loadView];
    
    // Custom Setup after loadView
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getOtherOfferListAfterFinishCall:@selector(offerListFinished:) onObject:self];
    
    data = [[NSArray alloc] init];
    
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    //NSLog(@"page-name: %@, data: %@", nextPage, nextPageContent);
    listCSS = nextPageContent[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    
    // Initialize the navigation controller
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_OPT_OK]) {
        [Util popWithFadeEffectOnViewController:self ShouldPopToRoot:NO];
    }
}

- (void) offerListFinished: (NSDictionary *) responseData {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    if ([Util serverResponseCheckNullAndSession:responseData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:responseData InFunction:__FUNCTION__ Message:@"data" Silent:NO]) {
            data = responseData[@"data"];
            // NSLog(@"Response data: %@", data);
            if (data.count <= 0) {
                UIAlertView *alertNoOffer = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                       message:[Util getSettingUsingName:DYNA_LIST_NODATA_MSG]
                                                                      delegate:self
                                                             cancelButtonTitle:ALERT_OPT_OK
                                                             otherButtonTitles:nil];
                [alertNoOffer show];
            } else {
                [self.tableView reloadData];
            }
        }
    }
//    data = responseData[@"data"];
//    //NSLog(@"Response data: %@", data);
//    [self.tableView reloadData];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1; // Return the number of sections.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [data count];    // Return the number of rows in the section.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height = [OtherOfferListCell getHeightForCellAt:indexPath
                                                 withData:data[indexPath.row]
                                                   AndCSS:[listCSS copy]];
    //NSLog(@"height: %f", height);
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OtherOfferListCell *cell = (OtherOfferListCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                                                      forIndexPath:indexPath];
    
    // Configure the cell...
    [cell loadWithData: data[indexPath.row] AndCSS: [listCSS copy]];
    //NSLog(@"Cell %@ called for cell at indexpath: %ld", cell, (long)indexPath.row);
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getOtherOfferDetailsForOfferId:data[indexPath.row][OTHER_OFFER_LIST_ID]
                         AfterFinishCall:@selector(offerDeatilsFinishedWithData:)
                                onObject:self];
}

- (void) offerDeatilsFinishedWithData:(NSDictionary *) responseData {
    
    if ([Util serverResponseCheckNullAndSession:responseData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:responseData InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            
            @try {
                //NSLog(@"response received with data: %@", responseData);
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                OtherDetailPageViewCtlr *detailViewCtlr = (OtherDetailPageViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"otherofferdetailpage"];
                detailViewCtlr.data = responseData[@"data"];
                detailViewCtlr.offerType = OFFER_OTHER;
                //[self.navigationController pushViewController:detailViewCtlr animated:YES];
                [Util pushWithFadeEffectOnViewController:self ViewController:detailViewCtlr];
            } @catch (NSException *exception) {
                NSString *strExcepMsg = @"Not able to start Other offer Detail View";
#if DEBUG_ERROR_LOG
                NSLog(@"%@", strExcepMsg);
#endif
                [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strExcepMsg];
                [Util showTechnicalError];
            }
        }
    }
    
//    //NSLog(@"response received with data: %@", resData);
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    OtherDetailPageViewCtlr *detailViewCtlr = (OtherDetailPageViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"otherofferdetailpage"];
//    detailViewCtlr.data = responseData[@"data"];
//    //[self.navigationController pushViewController:detailViewCtlr animated:YES];
//    [Util pushWithFadeEffectOnViewController:self ViewController:detailViewCtlr];
}

@end
