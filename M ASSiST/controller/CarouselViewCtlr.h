//
//  CarouselViewCtlr.h
//  M ASSiST
//
//  Created by Suraj on 27/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@class Navigation;

@interface CarouselViewCtlr : UIViewController <iCarouselDataSource, iCarouselDelegate> {
    
    NSMutableArray *data;                      // Current page data
    NSMutableArray *dataFunctional;     // Current page functional data
    NSMutableArray *dataEngagement;     // Current page enagegment data
    NSDictionary *css;                  // Current page css data
    NSDictionary *cssPage;
    NSString *notificationURL;
    int indexOld, indexCurrent;
}

@property (nonatomic) Navigation *navigation;
@property (weak, nonatomic) NSString *strEmergencyNumber;
@property (weak, nonatomic) NSString *page;
@property (weak, nonatomic) NSMutableDictionary *content;

@property (weak, nonatomic) IBOutlet UIImageView *imgBgImage;
@property (weak, nonatomic) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UIView *viewPopup;

- (void) openNotification;
- (void)didSelectItemAtIndex:(NSInteger)index;
-(void)openpmstms:(NSInteger)apptype;
@end
