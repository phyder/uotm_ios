//
//  DeviceListCell.m
//  Universe on the move
//
//  Created by Phyder on 27/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "DeviceListCell.h"
#import "Util.h"
#import "Constant.h"
#import <UIKit/UIKit.h>

@interface DeviceListCell() {
    NSDictionary *deviceData;
}

@end

@implementation DeviceListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setCellData:(NSDictionary *)data {
    deviceData = data;
    
    self.lblDeviceName.font = [UIFont fontWithName:APP_FONT_ROMAN size:16];
    NSString *deviceModel = data[@"DVCE_MDL"];
    if([deviceModel isEqual:[NSNull null]]) {
        deviceModel = @"";
    }
    
    self.lblDeviceName.text = deviceModel;
    NSString *deviceType = data[@"DVCE_TYP"];
    if([deviceType isEqual:[NSNull null]]) {
        deviceType = @"";
    }
    UIImage *image = [Util getImageAtDocDirWithName:@"de-register.png"];
    
    [self.btnDeregister setFrame:CGRectMake(191, 5, 80, 30)];
    //[self.imgIcon setFrame:CGRectMake(5, 5, 5, 5)];
    
    if([deviceType isEqualToString:@"A"]) {
        self.imgIcon.image = [Util getImageAtDocDirWithName:@"android.png"];
    } else {
        self.imgIcon.image = [Util getImageAtDocDirWithName:@"ios.png"];
    }
    [self.btnDeregister setBackgroundImage:image forState:UIControlStateNormal];
}

- (IBAction)btnDeregisterClick:(id)sender {
    if([self.delegate respondsToSelector:@selector(deregisterDeviceWithInfo:)]) {
        [self.delegate deregisterDeviceWithInfo:deviceData];
    }
}
@end
