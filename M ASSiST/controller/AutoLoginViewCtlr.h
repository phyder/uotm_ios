//
//  AutoLoginViewCtlr.h
//  Universe on the move
//
//  Created by Phyder on 20/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AutoLoginModel;

@interface AutoLoginViewCtlr : UIViewController <UITextFieldDelegate> {
    
    BOOL slide;
    
    AutoLoginModel *autologinPage;
}

@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgLoginBackground;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin1;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin2;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin3;
@property (strong, nonatomic) IBOutlet UITextField *txtMPin4;

@property (strong, nonatomic) IBOutlet UIButton *btnEmergency;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIn;
@property (strong, nonatomic) IBOutlet UIButton *btnResetMPin;

- (IBAction)btnClearPressed:(id)sender;
- (IBAction)txtFieldMPin1Didchange:(id)sender;
- (IBAction)txtFieldMPin2Didchange:(id)sender;
- (IBAction)txtFieldMPin3Didchange:(id)sender;
- (IBAction)txtFieldMPin4Didchange:(id)sender;
- (void)clearLoginCredentials;

- (void)signIn;

- (void)clearPassword;
- (void) initResetViewController;


@end
