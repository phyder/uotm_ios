//
//  ESOPViewController.m
//  Universe on the move
//
//  Created by Phyder on 27/10/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "ESOPViewController.h"
#import "ESOPModel.h"
#import "Util.h"
#import "ESOPCell.h"
#import "Holder.h"
#import "Authentication.h"
#import "ESOPChildCell.h"
#import "ESOPTotalCell.h"

#define Cell_TYPE @"type"
#define Cell_PARENT @"parent"
#define Cell_TOTAL @"total"
#define Cell_CHILD @"child"
static NSString *cellParentIdentifier = @"parent";
static NSString *cellChildIdentifier = @"child";
static NSString *cellTotalIdentifier = @"total";

@interface ESOPViewController () {
    NSMutableArray *esopData;
//    ESOPCell *cell;
}

@end

@implementation ESOPViewController

- (void) loadView {
    
    [super loadView];
    
    esopModel = [[ESOPModel alloc] initDataForView:self WithPage:_nextPage WithTitle:_nextTitle AndWithData:_content];
    _imgBgImage.image = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    self.imgBgHeader.image = [Util getImageAtDocDirWithName:@"esop-header.png"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self AndModel:esopModel];
    [[Holder sharedHolder].auth requestGetESOP];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return esopData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self tableView:self.tableView cellAtIndexPath:indexPath data:esopData[indexPath.row]];
    
//    cell = [self.tableView dequeueReusableCellWithIdentifier:cellParentIdentifier forIndexPath:indexPath];
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellAtIndexPath:(NSIndexPath *)indexPath data:(NSDictionary *)currentData {
    UITableViewCell *cell = nil;
    
    if ([currentData[Cell_TYPE] isEqualToString:Cell_PARENT] ) {
        ESOPCell *parentCell = (ESOPCell *) [tableView dequeueReusableCellWithIdentifier:cellParentIdentifier forIndexPath:indexPath];
        [parentCell setData:currentData];
        cell = parentCell;
    } else if ([currentData[Cell_TYPE] isEqualToString:Cell_CHILD]) {
        ESOPChildCell *childCell = (ESOPChildCell *) [tableView dequeueReusableCellWithIdentifier:cellChildIdentifier forIndexPath:indexPath];
        cell = childCell;
        [childCell setData:currentData];
    } else if ([currentData[Cell_TYPE] isEqualToString:Cell_TOTAL]) {
        ESOPTotalCell *totalCell = (ESOPTotalCell *) [tableView dequeueReusableCellWithIdentifier:cellTotalIdentifier forIndexPath:indexPath];
        cell = totalCell;
        [totalCell setData:currentData];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = esopData[indexPath.row];
    if ([data[Cell_TYPE] isEqualToString:Cell_TOTAL]) {
    
    } else {
        [self tableView:tableView didApplyAccordionAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didApplyAccordionAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *parentIndexPath;
    // Check is Cell Open
    if ([self tableView:tableView isCellExpandedForIndexPath:&parentIndexPath]) {
        
        // Collapse expanded cell
        [self tableView:tableView removeAtIndexPath:parentIndexPath];
        
        if (indexPath.row > parentIndexPath.row) {
            // After existing cell
            indexPath = [NSIndexPath indexPathForRow:(indexPath.row - 1) inSection:0];
            [self tableView:tableView addChildAtIndex:indexPath];
        } else if (indexPath.row < parentIndexPath.row) {
            // After existing cell
            [self tableView:tableView addChildAtIndex:indexPath];
        } else {
            // Same cell clicked
        }
        
    } else {
        // Open clicked index
        [self tableView:tableView addChildAtIndex:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView isCellExpandedForIndexPath:(NSIndexPath **)indexPath {
    // Check open
    int section = 0;
    for (int inc = 0; inc < [tableView numberOfRowsInSection:section]; inc++) {
        NSIndexPath *index = [NSIndexPath indexPathForRow:inc inSection:section];
        ESOPCell *newCell = (ESOPCell *) [tableView cellForRowAtIndexPath:index];
        if (newCell.isExpanded) {
            newCell.isExpanded = NO;
            *indexPath = index;
            return YES;
        }
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView addChildAtIndex:(NSIndexPath *)indexPath {
    
    NSDictionary *child = [self getChildForParentAtIndexPath:indexPath];
    
    NSInteger row = indexPath.row;
    [esopData insertObject:child atIndex:row+1];
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:row+1 inSection:0];
    [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    // set expanded flag
    ESOPCell *newCell = (ESOPCell *) [tableView cellForRowAtIndexPath:indexPath];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [newCell setState:YES];
}

- (void)tableView:(UITableView *)tableView removeAtIndexPath:(NSIndexPath *)indexPath {
    // Get Parent index
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0];
    // Remove/Close open index & adjust the index
    [esopData removeObjectAtIndex:index.row];
    [tableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
    
    ESOPCell *newCell = (ESOPCell *) [tableView cellForRowAtIndexPath:indexPath];
    [newCell setState:NO];
}

- (NSDictionary *)getChildForParentAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *child = [esopData[indexPath.row] mutableCopy];
    [child setValue:Cell_CHILD forKey:Cell_TYPE];
    return [child copy];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    NSIndexPath *parentIndexPath;
    if ([self tableView:self.tableView isCellExpandedForIndexPath:&parentIndexPath]) {
        // Collapse expanded cell
        [self tableView:self.tableView removeAtIndexPath:parentIndexPath];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

- (void) resESOP:(NSMutableDictionary *) resESOP {
    
    int total_total_options = 0, total_options_available = 0, total_options_outstandings = 0;
    if ([Util serverResponseCheckNullAndSession:resESOP InFunction:__FUNCTION__ Silent:NO]) {
        NSLog(@"ESOP response: %@", resESOP);
        esopData = [NSMutableArray array];
        for(NSDictionary *dict in resESOP[@"data"]) {
            
            NSString *total_options, *options_available, *options_outstandings;
            
            total_options = dict[@"total_options"];
            options_available = dict[@"options_available"];
            options_outstandings = dict[@"outstanding_options"];
            
            total_total_options = total_total_options + [total_options intValue];
            total_options_available = total_options_available + [options_available intValue];
            total_options_outstandings = total_options_outstandings + [options_outstandings intValue];
            
            NSMutableDictionary *sanData = dict.mutableCopy;
            [sanData setValue:Cell_PARENT forKey:Cell_TYPE];
            
            [esopData addObject:sanData];
        }
        NSMutableDictionary *sanData = [[NSMutableDictionary alloc] init];
        [sanData setValue:Cell_TOTAL forKey:Cell_TYPE];
        [sanData setValue:[NSString stringWithFormat:@"%i",total_total_options] forKey:@"total_options"];
        [sanData setValue:[NSString stringWithFormat:@"%i",total_options_available] forKey:@"options_available"];
        [sanData setValue:[NSString stringWithFormat:@"%i",total_options_outstandings] forKey:@"outstanding_options"];
        [esopData addObject:sanData];
        
        [self.tableView reloadData];
    }
}

@end
