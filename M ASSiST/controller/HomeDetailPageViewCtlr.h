//
//  DetailPageViewCtlr.h
//  Home Offer
//
//  Created by Suraj on 05/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Navigation;
@class HomeOfferDetail;

@interface HomeDetailPageViewCtlr : UITableViewController {
    
    NSString *notificationURL;
    
    HomeOfferDetail *offerDetailData;
}

@property (nonatomic) Navigation *navigation;

@property (nonatomic) IBOutlet NSDictionary *data;
@property (nonatomic) IBOutlet UILabel *lblSubTitle;

- (NSString *) getOfferId;
- (void) openNotification;

@end
