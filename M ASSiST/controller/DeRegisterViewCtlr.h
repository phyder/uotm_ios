//
//  DeRegisterViewCtlr.h
//  Universe on the move
//
//  Created by Phyder on 30/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Navigation;

@interface DeRegisterViewCtlr : UIViewController <UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate> {
    
    BOOL slide;
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableDeviceList;
@property (nonatomic) Navigation *navigation;

@end
