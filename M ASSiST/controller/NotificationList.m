//
//  NotificationList.m
//  engage
//
//  Created by Suraj on 22/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "NotificationList.h"

#import "AppConnect.h"
#import "Constant.h"
#import "Holder.h"
#import "Navigation.h"
#import "Util.h"

#import "NotificationCell.h"
#import "WebViewController.h"

static NSString *CellIdentifier = @"notificationcell";

@interface NotificationList ()

@end

@implementation NotificationList

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    
    //NSLog(@"data count: %d data:\n%@", [_listData count], _listData);
    data = [[NSMutableArray alloc] init];
    for (int i=0; i<[_listData count]; ++i) {
        [data addObject:[[NSMutableArray alloc] init]];
        
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
//    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[Util getImageAtDocDirWithName:@"login-view-bg.png"]];
    
    //NSLog(@"notification page data: %@", _pageData);
    // Initialize the navigation controller
    navigation  = [[Navigation alloc] initWithViewController:self PageData:_pageData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [_listData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //NSLog(@"row count: %u in section with index %d", [[[_listData objectAtIndex:section] objectForKey:@"DATA"] count], section);
    return [_listData[section][@"DATA"] count];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSDictionary *secData = (NSDictionary *) _listData[section];
    NSDictionary *cssData = [Util getLabelCSS:secData[@"css"]];
    NSArray *bounds = nil;
    //NSLog(@"header css: %@", cssData);
    
	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 44.0)];
    
    //add background
    UIImage *backgroundImage= [[Util getImageAtDocDirWithName:@"noti-header.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    UIImageView *background = [[UIImageView alloc] initWithFrame:header.frame];
    background.image        = backgroundImage;
    [header addSubview:background];
    [header sendSubviewToBack:background];
    header.contentMode      = UIViewContentModeScaleAspectFit;
    
    
    bounds                  = [cssData[CSS_LABEL_NOTIFICATION_ICONS_BOUNDS] componentsSeparatedByString:@"|"];
    UIImage *image          = [Util getImageAtDocDirWithName:secData[@"icon"]];
    UIImageView *imgLogo    = [[UIImageView alloc] initWithFrame:CGRectMake([bounds[0] floatValue], [bounds[1] floatValue], [bounds[2] floatValue], [bounds[3] floatValue])];
    [imgLogo setImage:image];
    
    
    bounds                  = [cssData[CSS_LABEL_TEXT_BOUNDS] componentsSeparatedByString:@"|"];
	UILabel *label          = [[UILabel alloc] initWithFrame:CGRectMake([bounds[0] floatValue], [bounds[1] floatValue], [bounds[2] floatValue], [bounds[3] floatValue])];
	label.backgroundColor   = [UIColor clearColor];
	label.textColor         = [Util colorWithHexString:cssData[@"font-color"] alpha:@"1.0"];
	label.shadowColor       = [UIColor blackColor];
	label.shadowOffset      = CGSizeMake(0, 1);
	label.font              = [Util fontWithName:cssData[@"font-name"]
                                           style:cssData[@"font-style"]
                                            size:cssData[@"font-size"]];
	label.text = secData[@"title"];
    
    [header addSubview:imgLogo];
	[header addSubview:label];
    
	return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *secData = (NSDictionary *) _listData[indexPath.section];
    
    NSString *text  = secData[@"DATA"][indexPath.row][@"title"];
    text            = [Util handleNewLine:text];
//    text            = [text stringByReplacingOccurrencesOfString:@"<BR>" withString:@"\\n"];
    NSDictionary *titleCSS = [Util getLabelCSS:_pageData[@"lists"][@"list"][@"title-css"]];
    CGSize constraint = CGSizeMake(250, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:[titleCSS[@"font-size"] intValue]]
                   constrainedToSize:constraint
                       lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat titleHeight = MAX(size.height+42, 72.0f);
    CGFloat subTitleHeight;
    CGFloat descHeight;
    CGFloat height = titleHeight;
    
    NSString *subText   = secData[@"DATA"][indexPath.row][@"sub-title"];
    subText             = [Util handleNewLine:subText];
//    subText             = [subText stringByReplacingOccurrencesOfString:@"<BR>" withString:@"\\n"];
    NSDictionary *subTitleCSS = [Util getLabelCSS:_pageData[@"lists"][@"list"][@"sub-title-css"]];
    if (![subText isEqualToString:EMPTY]) {
        size = [subText sizeWithFont:[UIFont systemFontOfSize:[subTitleCSS[@"font-size"] intValue]]
                   constrainedToSize:constraint
                       lineBreakMode:NSLineBreakByWordWrapping];
        subTitleHeight = MAX(size.height, 20.0f);
        height += subTitleHeight;
    }
    
    NSString *descText  = secData[@"DATA"][indexPath.row][@"desc"];
    descText            = [Util handleNewLine:descText];
//    descText            = [descText stringByReplacingOccurrencesOfString:@"<BR>" withString:@"\\n"];
    NSDictionary *descCSS = [Util getLabelCSS:_pageData[@"lists"][@"list"][@"desc-css"]];
    if (![descText isEqualToString:EMPTY]) {
        size = [descText sizeWithFont:[UIFont systemFontOfSize:[descCSS[@"font-size"] intValue]]
                    constrainedToSize:constraint
                        lineBreakMode:NSLineBreakByWordWrapping];
        descHeight = MAX(size.height+21, 10.0f);
        height += descHeight;
    }
    
    if (height==titleHeight && height>100) {
        height += 42;
    }
    
    //NSLog(@"row section: %d row: %d size:%.2f title: %.2f sub-title: %.2f desc: %.2f", indexPath.section, indexPath.row, height, titleHeight, subTitleHeight, descHeight);
    //NSLog(@"height: %.2f of cell at section %d row %d", height, indexPath.section, indexPath.row);
    
    [data[indexPath.section] insertObject:[NSString stringWithFormat:@"%.0f", height] atIndex:indexPath.row];
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *listCSS;
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    NSDictionary *secData = (NSDictionary *) _listData[indexPath.section];
    listCSS = _pageData[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    //NSLog(@"list data %@", listCSS);
    
    float height = [data[indexPath.section][indexPath.row] floatValue];
    //NSLog(@"height: %.2f", height);
    
    [cell cellForRowAtIndexPath:indexPath
                       WithData:secData[@"DATA"][indexPath.row]
                            CSS:listCSS
                         Height:height];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NotificationCell *cell = (NotificationCell *) [tableView cellForRowAtIndexPath:indexPath];
    NSLog(@"cell page name: %@ url: %@", cell.pageName, cell.url);
    [self openWebView:indexPath];
}

- (void) openWebView:(NSIndexPath *) indexPath {
    
    NotificationCell *cell = (NotificationCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.details isEqualToString:PAGE_TRUE]) {
        UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        WebViewController *webview  = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"webview"];
        
        webview.page = cell.pageName;
        webview.data = [Util getPage:cell.pageName];
        NSLog(@"webview data: %@", webview.page);
        webview.css = [Util getPageCSS:(webview.data)[PAGE_CSS]];
        NSString *url = (webview.data)[@"url"];
        
        if ([url hasPrefix:@"e:/"]) {
            url = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [url substringFromIndex:2], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
            webview.urlType = @"e:/";
        } else if ([url hasPrefix:@"l:/"]) {
            url = [url substringFromIndex:3];
            NSString *path = [NSString stringWithFormat:@"%@/%@", [Util getDocumentsDirectory], url];
            // Need to be double-slashes to work correctly with UIWebView, so change all "/" to "//"
            //path = [path stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
            // Also need to replace all spaces with "%20"
            //path = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            //And make a proper URL
            //url = [NSURL URLWithString:[NSString stringWithFormat:@"file:/%@",path]];
            url = path;
            webview.urlType = @"l:/";
        }
        
        NSString *params = cell.params;
        if ((params != (id)[NSNull null] && params.length!=0)) {
            url = [NSString stringWithFormat:@"%@%@%@", url , @"?", params];
        }
        NSLog(@"url test: %@", url);
        webview.webURL = url;
        [Util pushWithFadeEffectOnViewController:self ViewController:webview];
    }
}

//Refresh Notification list called Navigation class
- (void) refreshNotificationList {
    
    NSString *url = [NSString stringWithFormat:@"%@?refresh=true", _url];
//    NSLog(@"Notification refresh request: %@", url);
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(refreshNotificationListResponseReceived:) onObject:self];
    [conn requestDynalistDataWithURL:url];
}


- (void) refreshNotificationListResponseReceived:(NSDictionary *) responseDict {
    
//    NSLog(@"Response: %@", responseDict);
    if (isRefreshing==YES) {
        
        if ([Util serverResponseCheckNullAndSession:responseDict InFunction:__FUNCTION__ Silent:NO]) {
            
            if ([Util serverResponseForField:PAGE_TYPE_LIST In:responseDict InFunction:__FUNCTION__ Message:[NSString stringWithFormat:@"%@ parameter", PAGE_TYPE_LIST] Silent:NO]) {
                
                NSLog(@"\n\nRefresh is called.");
                data = responseDict[PAGE_TYPE_LIST];
                //NSLog(@"Table Refresh start");
                [self.tableView reloadData];
                //NSLog(@"Table Refresh end");
                isRefreshing=NO;
            }
        }
    }
}

@end
