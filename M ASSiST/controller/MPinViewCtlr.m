//
//  MPinViewCtlr.m
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "MPinViewCtlr.h"
#import "Util.h"
#import "MPinModel.h"
#import "Constant.h"
#import "IIViewDeckController.h"
#import "MenuListViewCtlr.h"
#import "Holder.h"



@interface MPinViewCtlr ()

@end

@implementation MPinViewCtlr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    if (isiPhone4s) {
        NSLog(@"this is iphone 4s");
        self.logo.frame   = CGRectMake(60.0f, 0.0f, 200.0f, 90.0f);
        self.label1.frame = CGRectMake(22.0f, 75.0f, 276.0f, 35.0f);
        self.label2.frame = CGRectMake(22.0f, 330.0f, 276.0f, 35.0f);
        self.viewBackground.frame = CGRectMake(54.0f, 125.0f, 211.0f, 154.0f);
    }
    
    _imgMPinBackground.image   = [Util getImageAtDocDirWithName:@"loginbox.png"];
    _imgBackground.image        = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    mPinPage                   = [[MPinModel alloc] init];
    [mPinPage initDataWithView:self];
    
    NSArray *lists              = [mPinPage getPageData][PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list      = lists[i];
        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
            controlData         = list;
        }
    }
    
    _logo.image = [Util getImageAtDocDirWithName:controlData[@"img"]];
    
    [self initNewMPin];
    [self initRetypeMPin];
    
    //    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
    //    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
    //                                                     message:[[NSUUID UUID] UUIDString]
    //                                                    delegate:nil
    //                                           cancelButtonTitle:@"Ok"
    //                                           otherButtonTitles:nil];
    //    [device show];
}

- (void) viewWillAppear:(BOOL)animated {
    
    NSLog(@"This is SessionID %@",([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]);
    [super viewWillAppear:animated];
    //Custom Initialization
    [mPinPage initNavControlWithNewMPin1:_txtNewMPin1 newMPin2:_txtNewMPin2 newMPin3:_txtNewMPin3 newMPin4:_txtNewMPin4 retypeMPin1:_txtRetypeMPin1 retypeMPin2:_txtRetypeMPin2 retypeMPin3:_txtRetypeMPin3 retypeMPin4:_txtRetypeMPin4 Submit:_btnSubmit];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initNewMPin {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtNewMPin.delegate               = self;
    [_txtNewMPin setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *leftPaddingView             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"pass.png"];
    [leftPaddingView addSubview:userimage];
    _txtNewMPin.leftView               = leftPaddingView;
    _txtNewMPin.leftViewMode           = UITextFieldViewModeAlways;
    _txtNewMPin.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtNewMPin.borderStyle            = UITextBorderStyleNone;
    _txtNewMPin.layer.cornerRadius     = 4.0f;
    _txtNewMPin.layer.masksToBounds    = YES;
    _txtNewMPin.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtNewMPin.layer.borderWidth      = 1.0f;
    _txtNewMPin.returnKeyType = UIReturnKeyNext;
    
    //Single TextField
    _txtNewMPin1.delegate               = self;
    _txtNewMPin2.delegate               = self;
    _txtNewMPin3.delegate               = self;
    _txtNewMPin4.delegate               = self;
    
    _txtNewMPin1.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtNewMPin2.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtNewMPin3.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtNewMPin4.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    
    _txtNewMPin1.layer.borderWidth      = 1.0f;
    _txtNewMPin2.layer.borderWidth      = 1.0f;
    _txtNewMPin3.layer.borderWidth      = 1.0f;
    _txtNewMPin4.layer.borderWidth      = 1.0f;
    
    _txtNewMPin1.layer.cornerRadius     = 4.0f;
    _txtNewMPin2.layer.cornerRadius     = 4.0f;
    _txtNewMPin3.layer.cornerRadius     = 4.0f;
    _txtNewMPin4.layer.cornerRadius     = 4.0f;
    
    _txtNewMPin1.keyboardType = UIKeyboardTypeNumberPad;
    _txtNewMPin2.keyboardType = UIKeyboardTypeNumberPad;
    _txtNewMPin3.keyboardType = UIKeyboardTypeNumberPad;
    _txtNewMPin4.keyboardType = UIKeyboardTypeNumberPad;
    
    _txtNewMPin1.returnKeyType = UIReturnKeyNext;
    _txtNewMPin2.returnKeyType = UIReturnKeyNext;
    _txtNewMPin3.returnKeyType = UIReturnKeyNext;
    _txtNewMPin4.returnKeyType = UIReturnKeyNext;
    
    _txtNewMPin1.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtNewMPin2.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtNewMPin3.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtNewMPin4.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
}

- (void) initRetypeMPin {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtRetypeMPin.delegate               = self;
    [_txtRetypeMPin setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *leftPaddingView             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"pass.png"];
    [leftPaddingView addSubview:userimage];
    _txtRetypeMPin.leftView               = leftPaddingView;
    _txtRetypeMPin.leftViewMode           = UITextFieldViewModeAlways;
    _txtRetypeMPin.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtRetypeMPin.borderStyle            = UITextBorderStyleNone;
    _txtRetypeMPin.layer.cornerRadius     = 4.0f;
    _txtRetypeMPin.layer.masksToBounds    = YES;
    _txtRetypeMPin.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtRetypeMPin.layer.borderWidth      = 1.0f;
    _txtRetypeMPin.returnKeyType = UIReturnKeyGo;
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
    
    
    //Single TextField
    _txtRetypeMPin1.delegate               = self;
    _txtRetypeMPin2.delegate               = self;
    _txtRetypeMPin3.delegate               = self;
    _txtRetypeMPin4.delegate               = self;
    
    _txtRetypeMPin1.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtRetypeMPin2.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtRetypeMPin3.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtRetypeMPin4.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    
    _txtRetypeMPin1.layer.borderWidth      = 1.0f;
    _txtRetypeMPin2.layer.borderWidth      = 1.0f;
    _txtRetypeMPin3.layer.borderWidth      = 1.0f;
    _txtRetypeMPin4.layer.borderWidth      = 1.0f;
    
    _txtRetypeMPin1.layer.cornerRadius     = 4.0f;
    _txtRetypeMPin2.layer.cornerRadius     = 4.0f;
    _txtRetypeMPin3.layer.cornerRadius     = 4.0f;
    _txtRetypeMPin4.layer.cornerRadius     = 4.0f;
    
    _txtRetypeMPin1.keyboardType = UIKeyboardTypeNumberPad;
    _txtRetypeMPin2.keyboardType = UIKeyboardTypeNumberPad;
    _txtRetypeMPin3.keyboardType = UIKeyboardTypeNumberPad;
    _txtRetypeMPin4.keyboardType = UIKeyboardTypeNumberPad;
    
    _txtRetypeMPin1.returnKeyType = UIReturnKeyNext;
    _txtRetypeMPin2.returnKeyType = UIReturnKeyNext;
    _txtRetypeMPin3.returnKeyType = UIReturnKeyNext;
    _txtRetypeMPin4.returnKeyType = UIReturnKeyGo;
    
    _txtRetypeMPin1.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtRetypeMPin2.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtRetypeMPin3.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtRetypeMPin4.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _txtNewMPin) {
        [_txtNewMPin becomeFirstResponder];
    } else if (textField == _txtRetypeMPin) {
        [_txtRetypeMPin resignFirstResponder];
        [self setMPin];
    }
    return YES;
}

#pragma mark - UITextFieldDelegate
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    // Transform the views to the normal view
    [self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self resetViewToIdentityTransform];
}

#pragma mark - Keyboard Slider
- (void) maintainVisibityOfControl: (UITextField *) textField {
    
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    //double visibleHeight = bounds.size.height-216;
    
    double fieldYPosition = self.viewBackground.frame.origin.y+textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}

- (void) hideKeyboard {
    [_txtNewMPin resignFirstResponder];
    [_txtRetypeMPin resignFirstResponder];
    
    [self.view endEditing:YES];
}

#pragma mark - Navigation Slider
- (void) initPanMenuListController {
    
    NSLog(@"%s", __FUNCTION__);
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    MenuListViewCtlr *menuListViewCtlr = [storyboard instantiateViewControllerWithIdentifier:@"menuview"];
//    
//    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
//    //deckController.panningMode = IIViewDeckFullViewPanning;
//    deckController.leftController = menuListViewCtlr;
    
    NSLog(@"%s", __FUNCTION__);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:@"navautologin"];
    //
    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    //    //deckController.panningMode = IIViewDeckFullViewPanning;
    deckController.centerController = initViewController;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void) setMPin {
    
    [mPinPage setMPin];
}

- (void) resSetMPin:(NSMutableDictionary *) resSetMPin {
    
    [mPinPage postAuthenticationValidationWithResData:resSetMPin];
}


- (IBAction)btnClearPressed:(id)sender {
    [self hideKeyboard];
}

- (IBAction)txtFieldNewMPin1Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldNewMPin2Didchange:(id)sender {
    
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldNewMPin3Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldNewMPin4Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldRetypeMPin1Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldRetypeMPin2Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldRetypeMPin3Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (IBAction)txtFieldRetypeMPin4Didchange:(id)sender {
    [self textFieldDidChange:(UITextField *) sender];
}

- (void) textFieldDidChange: (UITextField *) textfield {
    NSString *text = textfield.text;
    /* get first char */
    NSString *firstChar = [[NSString alloc] init];
    if ([text length]>0) {
        firstChar = [text substringToIndex:1];
    }
    
    if ([textfield isEqual:self.txtNewMPin1]) {
        
        if ([text length] >= 1) {
            self.txtNewMPin1.text = firstChar;
            [self.txtNewMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtNewMPin2]) {
        
        if ([text length] >= 1) {
            self.txtNewMPin2.text = firstChar;
            [self.txtNewMPin3 becomeFirstResponder];
        } else {
            [self.txtNewMPin1 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtNewMPin3]) {
        if ([text length] >= 1) {
            self.txtNewMPin3.text = firstChar;
            [self.txtNewMPin4 becomeFirstResponder];
        } else {
            [self.txtNewMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtNewMPin4]) {
        if ([text length] >= 1) {
            self.txtNewMPin4.text = firstChar;
            [self.txtRetypeMPin1 becomeFirstResponder];
        } else {
            [self.txtNewMPin3 becomeFirstResponder];
        }
    }
    
    //    Re-Enter Pin
    
    if ([textfield isEqual:self.txtRetypeMPin1]) {
        if ([text length] >= 1) {
            self.txtRetypeMPin1.text = firstChar;
            [self.txtRetypeMPin2 becomeFirstResponder];
        } else {
            [self.txtNewMPin4 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtRetypeMPin2]) {
        if ([text length] >= 1) {
            self.txtRetypeMPin2.text = firstChar;
            [self.txtRetypeMPin3 becomeFirstResponder];
        } else {
            [self.txtRetypeMPin1 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtRetypeMPin3]) {
        if ([text length] >= 1) {
            self.txtRetypeMPin3.text = firstChar;
            [self.txtRetypeMPin4 becomeFirstResponder];
        } else {
            [self.txtRetypeMPin2 becomeFirstResponder];
        }
    } else if ([textfield isEqual:self.txtRetypeMPin4]) {
        if ([text length] >= 1) {
            self.txtRetypeMPin4.text = firstChar;
            [self.txtRetypeMPin4 resignFirstResponder];
        } else {
            [self.txtRetypeMPin3 becomeFirstResponder];
        }
    }
}

@end
