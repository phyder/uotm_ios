//
//  ESOPCell.m
//  Universe on the move
//
//  Created by Phyder on 28/10/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "ESOPCell.h"
#import "Util.h"

#define NULL_VALUE           @"---"

@implementation ESOPCell

- (void)awakeFromNib {
    // Initialization code
    self.isExpanded = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setData:(NSDictionary *) data {
    self.lblDateGrant.text = data[@"date_of_grant"];
    self.lblExercisePrice.text = data[@"exercise_price"];
    self.lblTotalOptions.text = data[@"total_options"];
    self.lblOptionsAvailable.text = data[@"options_available"];
    self.lblOptionsOutstanding.text = data[@"outstanding_options"];
    
    if (data[@"date_of_grant"] == nil || data[@"date_of_grant"] == [NSNull null] || [data[@"date_of_grant"] isEqualToString:@""]) {
        self.lblDateGrant.text = NULL_VALUE;
    } else {
        self.lblDateGrant.text = [data valueForKey:@"date_of_grant"];
    }
    
    if (data[@"exercise_price"] == nil || data[@"exercise_price"] == [NSNull null] || [data[@"exercise_price"] isEqualToString:@""]) {
        self.lblExercisePrice.text = @"";
    } else {
        self.lblExercisePrice.text = [data valueForKey:@"exercise_price"];
    }
    
    if (data[@"total_options"] == nil || data[@"total_options"] == [NSNull null] || [data[@"total_options"] isEqualToString:@""]) {
        self.lblTotalOptions.text = NULL_VALUE;
    } else {
        self.lblTotalOptions.text = [data valueForKey:@"total_options"];
    }
    
    
    if (data[@"options_available"] == nil || data[@"options_available"] == [NSNull null] || [data[@"options_available"] isEqualToString:@""]) {
        self.lblOptionsAvailable.text = NULL_VALUE;
    } else {
        self.lblOptionsAvailable.text = [data valueForKey:@"options_available"];
    }
    
    if (data[@"outstanding_options"] == nil || data[@"outstanding_options"] == [NSNull null] || [data[@"outstanding_options"] isEqualToString:@""]) {
        self.lblOptionsOutstanding.text = NULL_VALUE;
    } else {
        self.lblOptionsOutstanding.text = [data valueForKey:@"outstanding_options"];
    }
    
    self.imgView.image = [Util getImageAtDocDirWithName:@"DownErrow.png"];
}

- (void)setState:(BOOL)state {
    
    self.isExpanded = state;
    
    [UIView animateWithDuration:0.50f animations:^{
        //Move the image view to 100, 100 over 10 seconds.
        self.imgView.transform = CGAffineTransformRotate(self.imgView.transform, M_PI);
    }];
    
}

@end
