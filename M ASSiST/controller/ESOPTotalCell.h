//
//  ESOPTotalCell.h
//  Universe on the move
//
//  Created by Phyder on 17/11/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESOPTotalCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTotalOptions;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsOutstanding;
@property (nonatomic) BOOL isExpanded;


- (void)setData:(NSDictionary *) data;

@end
