//
//  OtpViewController.m
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "OtpViewController.h"
#import "Util.h"
#import "OTPModel.h"
#import "Constant.h"
#import "MPinViewCtlr.h"
#import "ResetMPinViewCtlr.h"

@interface OtpViewController ()

@end

@implementation OtpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    
    [super loadView];
    
    _imgOtpBackground.image   = [Util getImageAtDocDirWithName:@"loginbox.png"];
    _imgBackground.image        = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    otpPage                   = [[OTPModel alloc] init];
    [otpPage initDataWithView:self];
    
    NSArray *lists              = [otpPage getPageData][PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list      = lists[i];
        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
            controlData         = list;
        }
    }
    
    _logo.image = [Util getImageAtDocDirWithName:controlData[@"img"]];
    [self.lblNote setFont:[UIFont fontWithName:APP_FONT_ROMAN size:15]];
    CGRect frame = self.lblNote.frame;
    frame.origin.y = self.logo.frame.origin.y + self.logo.frame.size.height;
    [self.lblNote setFrame:frame];
    
    [self initOtp];
    
    //    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
    //    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
    //                                                     message:[[NSUUID UUID] UUIDString]
    //                                                    delegate:nil
    //                                           cancelButtonTitle:@"Ok"
    //                                           otherButtonTitles:nil];
    //    [device show];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //Custom Initialization
    [otpPage initNavControlWithOtp:_txtOtp Confirm:_btnConfirm];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initOtp {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtOtp.delegate               = self;
    [_txtOtp setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *leftPaddingView             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"pass.png"];
    [leftPaddingView addSubview:userimage];
    _txtOtp.leftView               = leftPaddingView;
    _txtOtp.leftViewMode           = UITextFieldViewModeAlways;
    _txtOtp.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtOtp.borderStyle            = UITextBorderStyleNone;
    _txtOtp.layer.cornerRadius     = 4.0f;
    _txtOtp.layer.masksToBounds    = YES;
    _txtOtp.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtOtp.layer.borderWidth      = 1.0f;
    _txtOtp.keyboardType = UIKeyboardTypeNumberPad;
    _txtOtp.returnKeyType = UIReturnKeyGo;
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [_txtOtp resignFirstResponder];
    [self confirmOtp];
    
    return YES;
}

#pragma mark - UITextFieldDelegate
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    // Transform the views to the normal view
    [self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self resetViewToIdentityTransform];
}

#pragma mark - Keyboard Slider
- (void) maintainVisibityOfControl: (UITextField *) textField {
    
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    //double visibleHeight = bounds.size.height-216;
    
    double fieldYPosition = textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}

- (void) hideKeyboard {
    [_txtOtp resignFirstResponder];
}

#pragma mark - Navigation Slider
- (void) initMPinViewController {
    
    NSLog(@"%s", __FUNCTION__);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MPinViewCtlr *mPinViewCtlr = [storyboard instantiateViewControllerWithIdentifier:@"mpin"];
    //ResetMPinViewCtlr *mPinViewCtlr = [storyboard instantiateViewControllerWithIdentifier:@"resetmpin"];
    
    [self.navigationController pushViewController:mPinViewCtlr animated:YES];
    
//    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
//    //deckController.panningMode = IIViewDeckFullViewPanning;
//    deckController.leftController = menuListViewCtlr;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void) confirmOtp {
    
    [otpPage confirmOtp];
}

- (void) resOtp:(NSMutableDictionary *) resOtp {
    
    [otpPage postAuthenticationValidationWithResData:resOtp];
}

- (IBAction)btnClearPressed:(id)sender {
    [self hideKeyboard];
}

@end
