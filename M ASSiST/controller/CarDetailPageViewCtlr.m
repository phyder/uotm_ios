//
//  DetailPageViewCtlr.m
//  Universe on the move
//
//  Created by Suraj on 05/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "CarDetailPageViewCtlr.h"

#import "CarOfferDetail.h"

#import "Constant.h"
#import "Navigation.h"
#import "Util.h"

@interface CarDetailPageViewCtlr ()

@end

@implementation CarDetailPageViewCtlr

- (id)initWithStyle:(UITableViewStyle)style {
    
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSLog(@"Data: %@", self.data);
    self.modelCarOfferDetail = [[CarOfferDetail alloc] initWithData:(NSMutableDictionary *) _data
                                                   UIViewController:self];
    
    //[self initControl];
    NSString *nextPage = @"car-offer-details";
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    //NSLog(@"page-name: %@, data: %@", nextPage, nextPageContent);
    
    // Initialize the navigation controller
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
    [self.navigation setHeaderTitle:(self.data)[CAR_OFFER_DETAIL_OFFER_TITLE]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

- (NSString *) getOfferId {
    return [self.modelCarOfferDetail getOfferId];
}

//- (void) btnIMInterestedPressed {
//    
//    [offerDetailData btnIMInterestedPressed];
//}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;   // Return the number of sections.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;   // Return the number of rows in the section.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int height = 0;
//    float cellHeight = 0;
    switch (indexPath.row) {
        case 0:
            //cellHeight = [self.modelCarOfferDetail getCellOneHeight];
            height = [self.modelCarOfferDetail getCellOneHeight];
            break;
        case 1:
            //cellHeight = [self.modelCarOfferDetail getCellTwoHeight];
            height = [self.modelCarOfferDetail getCellTwoHeight];
            break;
        case 2:
            //cellHeight = [self.modelCarOfferDetail getCellThreeHeight];
            height = [self.modelCarOfferDetail getCellThreeHeight];
            break;
        case 3:
            //cellHeight = [self.modelCarOfferDetail getCellFourHeight];
            height = [self.modelCarOfferDetail getCellFourHeight];
            break;
        case 4:
            //cellHeight = [self.modelCarOfferDetail getCellFourHeight];
            height = [self.modelCarOfferDetail getCellFiveHeight];
            break;
            
        default:
            height = 40;
            break;
    }
    //NSLog(@"cell three height: %f", cellHeight);
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellOneIdentifier      = @"row1";
    static NSString *CellTwoIdentifier      = @"row2";
    static NSString *CellThreeIdentifier    = @"row3";
    static NSString *CellFourIdentifier     = @"row4";
    static NSString *CellFiveIdentifier     = @"row5";
    
    //NSLog(@"Row: %ld", (long)indexPath.row);
    CarOfferDetail *cell = nil;
    
    // Init the cell with data
    switch (indexPath.row) {
        case 0:
            cell = (CarOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellOneIdentifier];
            [self.modelCarOfferDetail getCellOneWithCell:cell];
            break;
        case 1:
            cell = (CarOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellTwoIdentifier];
            [self.modelCarOfferDetail getCellTwoWithCell:cell];
            break;
        case 2:
            cell = (CarOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellThreeIdentifier];
            [self.modelCarOfferDetail getCellThreeWithCell:cell];
            break;
        case 3:
            cell = (CarOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellFourIdentifier];
            [self.modelCarOfferDetail getCellFourWithCell:cell];
            break;
        case 4:
            cell = (CarOfferDetail *) [tableView dequeueReusableCellWithIdentifier:CellFiveIdentifier];
            [self.modelCarOfferDetail getCellFiveWithCell:cell];
            break;
            
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

@end
