//
//  ListViewCtlr.m
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "HomeListViewCtlr.h"

#import "Constant.h"
#import "Navigation.h"
#import "Util.h"

@interface HomeListViewCtlr ()

@end

@implementation HomeListViewCtlr

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    
    list = [[HomeListData alloc] initWithData:_listData ViewController:self];
    //NSLog(@"data: %@", list.data);
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *nextPage = @"home-offer-list";
    NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
    listCSS     = nextPageContent[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    //NSLog(@"page-name: %@, data: %@", nextPage, nextPageContent);
    
    // Initialize the navigation controller
    self.navigation  = [[Navigation alloc] initWithViewController:self PageData:nextPageContent];
    [self.navigation setHeaderTitle:self.title];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) openNotification {
    
    notificationURL = [Util getNotificationURL];
    [Util openNotificationWithURL:notificationURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:notificationURL];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    [list initTable:tableView WithSection:section
   WithNumberOfRows:[_listData count]
             AndCSS:listCSS];
    // Return the number of rows in the section.
    return [_listData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height = [list getHeightforindexpath:indexPath
                                        AndCSS:listCSS];
    //NSLog(@"height: %f", height);
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeListViewCell *cell = [list cellAtRow:indexPath.row];
    return cell;
}

#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"Row clicked: %ld", (long)indexPath.row);
    [list getOfferDataWithIndexPath:indexPath];
}

//- (void) loaderCancelPressed {
//#ifdef DEBUG
//    NSLog(@"%s", __FUNCTION__);
//#endif
//    
//    [self.backgroundThread cancel];
//    [progressBar hide:YES afterDelay:0.1];
//}

- (void) responseReceivedHomeDetails:(NSDictionary *) responseDict {
    
    //NSLog(@"Home Details: %@", responseDict);
    [list openOfferDetailsWithData:responseDict AndNavigation:self.navigationController];
}

@end
