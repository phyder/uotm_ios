//
//  ESOPChildCell.m
//  Universe on the move
//
//  Created by Phyder on 17/11/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "ESOPChildCell.h"

#define NULL_VALUE           @"---"

@implementation ESOPChildCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData:(NSDictionary *) data {
    if (data[@"options_vested"] == nil || data[@"options_vested"] == [NSNull null] || [data[@"options_vested"] isEqualToString:@""]) {
        self.lblOptionsVested.text = NULL_VALUE;
    } else {
        self.lblOptionsVested.text = [data valueForKey:@"options_vested"];
    }
    
    if (data[@"options_exercised"] == nil || data[@"options_exercised"] == [NSNull null] || [data[@"options_exercised"] isEqualToString:@""]) {
        self.lblOptionsExercised.text = NULL_VALUE;
    } else {
        self.lblOptionsExercised.text = [data valueForKey:@"options_exercised"];
    }
    
    if (data[@"options_lapsed"] == nil || data[@"options_lapsed"] == [NSNull null] || [data[@"options_lapsed"] isEqualToString:@""]) {
        self.lblOptionsLapsed.text = NULL_VALUE;
    } else {
        self.lblOptionsLapsed.text = [data valueForKey:@"options_lapsed"];
    }
}

@end
