/* global malarkey:false, moment:false */
(function () {
  'use strict';

  var constants = {
    // POC | Local | UAT | LIVE
    build: 'LIVE',
    uotmName: 'Zeno'
  };

  angular
    .module('uotmChatBot')
    .constant('UOTMConstants', constants);

})();
