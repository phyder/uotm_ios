(function () {
  'use strict';

  angular
    .module(
    'uotmChatBot',
    ['ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ngRoute',
      'ui.bootstrap',
      'toastr',
      'ksSwiper',
      'luegg.directives',
      'directives.autoscroll',
      'ngMaterial',
      'mdPickers'
    ]);

})();
