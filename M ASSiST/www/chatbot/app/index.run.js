(function() {
  'use strict';

  angular
    .module('uotmChatBot')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
