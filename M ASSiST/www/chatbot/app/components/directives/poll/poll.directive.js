(function () {
    'use strict';

    angular.module('uotmChatBot')
        .directive('chatpoll', ChatPoll);

    /**@nginject*/
    function ChatPoll() {
        return {
            restrict: 'E',
            templateUrl: 'app/components/directives/poll/poll.directive.html',
            scope: {
                say: '=',
                data: '=',
                config: '='
            },
            link: link,
        }
        // link function
        function link(scope, element, attr) {

        }
    }
})();