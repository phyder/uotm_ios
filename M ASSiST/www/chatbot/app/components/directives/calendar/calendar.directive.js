(function () {
    'use strict';

    angular.module('uotmChatBot')
        .directive('calendar', Calendar);

    /**@nginject*/
    function Calendar($timeout, $mdpTimePicker) {
        return {
            restrict: 'E',
            templateUrl: 'app/components/directives/calendar/calendar.directive.html',
            scope: {
                pickertype: '=',
                config: '='
            },
            link: link,
        }
        //link function
        function link(scope, element, attr) {
            scope.isOpen = false;
            scope.isOpenEnd = false;
            scope.isDateOpen = false;
            scope.currentTime = new Date();
            scope.startDate = new Date();

            scope.startMinMaxDate = new Date();
            scope.startMinDate = new Date(
                scope.startMinMaxDate.getFullYear(),
                scope.startMinMaxDate.getMonth() ,
                scope.startMinMaxDate.getDate() - 0,
                scope.startMinMaxDate.getDay() 
            );

            scope.startMaxDate = new Date(
                scope.startMinMaxDate.getFullYear(),
                scope.startMinMaxDate.getMonth() + 12,
                scope.startMinMaxDate.getDate()
            );
            
             scope.endMinDateFromStartDate = function(endMinMaxDate){
                scope.endMinDate = new Date(
                    endMinMaxDate.getFullYear(),
                    endMinMaxDate.getMonth() ,
                    endMinMaxDate.getDate(),
                    endMinMaxDate.getDay() 
                )

                scope.endMaxDate = new Date(
                    endMinMaxDate.getFullYear(),
                    endMinMaxDate.getMonth() + 12,
                    endMinMaxDate.getDate()
                );
                scope.enableEndDates = function(endMinMaxDate){
                    var day = endMinMaxDate.getDay();
                    return day === 0 || day === 1 || day === 2 || day === 3 || day === 4 || day === 5 || day === 6;
                }
             }

            scope.showTimePicker = function (ev) {
                $mdpTimePicker(scope.currentTime, {
                    targetEvent: ev
                }).then(function (selectedDate) {
                    scope.currentTime = selectedDate;
                    scope.config.click(scope.currentTime, 'time');
                });
            }

        }
    }
})()
