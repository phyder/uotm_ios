(function () {
  'use strict';

  angular.module('uotmChatBot')
    .directive('chatquickreply', ChatQuickReply);

  /**@nginject*/
  function ChatQuickReply($timeout, $filter) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/directives/quickreply/quickreply.directive.html',
      scope: {
        replies: '=',
        config: '='
      },
      link: link,
    }
    //link function
    function link(scope, element, attr) {
     
     }
  }
})()
