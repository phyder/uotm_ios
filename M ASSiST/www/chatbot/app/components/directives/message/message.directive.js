(function () {
    'use strict';

    angular.module('uotmChatBot')
        .directive('chatmessage', ChatMessage);

    /**@nginject*/
    function ChatMessage() {
        return {
            restrict: 'E',
            templateUrl: 'app/components/directives/message/message.directive.html',
            scope: {
                say: '=',
                source: '=',
                name: '=',
                cdate: '=',
                source1: '=',
                data: '=',
                index: '=',
                bg: '='
            },
            link: link,
        }
        //link function
        function link(scope, element, attr) {
            
            // *********** Below code refere to alter nate color so {{DONT' DELETE}} ***************

            // scope.colors = ['#36B0E4', '#29AAE2', '#FDB813', '#a9cf24', '#ED3E96', '#6951A2'];
            // scope.bgcolors=["bg-blue","bg-blue1","bg-yellow","bg-green","bg-pink","bg-purple"];
            // scope.borders = ['10px solid #36B0E4', ' 10px solid #29AAE2', '10px solid #FDB813', '10px solid #a9cf24', '10px solid #ED3E96', '10px solid #6951A2'];
            
            
            // alert(scope.bgcolors);
            // if (scope.bgcolors === "29AAE2") {
            //     alert();
            // }
            // var styleElem = document.head.appendChild(document.createElement("style"));

            // styleElem.innerHTML = "user::before {border-top: black;}";
            // var user = document.getElementById("user");
            // // alert(JSON.stringify(user))
            // user.user("before", "border-top", scope.borders[scope.index]);
            // user.user("after", "border-top", scope.borders[scope.index]);

            // var bot = document.getElementById("bot");
            // bot.pseudoStyle("before", "border-top", scope.borders[scope.index]);
            // bot.pseudoStyle("after", "border-top", scope.borders[scope.index]);

        }
    }
})();