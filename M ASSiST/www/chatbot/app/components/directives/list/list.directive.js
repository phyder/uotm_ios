(function () {
    'use strict';

    angular.module('uotmChatBot')
        .directive('chatlist', ChatList);

    /**@nginject*/
    function ChatList() {
        return {
            restrict: 'E',
            templateUrl: 'app/components/directives/list/list.directive.html',
            scope: {
                say: '=',
                data: '='
            },
            link: link,
        }
        //link function
        function link(scope, element, attr) {

        }
    }
})();