(function () {
    'use strict';

    angular
        .module('uotmChatBot')
        .service('ChatService', ChatService);

    /** @ngInject */
    function ChatService($http, UOTMConstants) {
        var vm = this;

        vm.sendChat = sendChat;

        function sendChat(msg, id) {
            if (UOTMConstants.build == 'Local')
                return sendChatLocal(msg, id);
            else if (UOTMConstants.build == "UAT")
                return sendChatUAT(msg, id);
            else if (UOTMConstants.build == "POC")
                return sendChatPOC(msg, id);
            else if (UOTMConstants.build == "LIVE")
                return sendChatLIVE(msg, id);
        }

        function sendChatLocal(msg, id) {
            return new Promise(function (resolve, reject) {

                $http({
                    method: 'POST',
                    url: 'http://192.168.0.80:9082/EngageChatbot/restapichannel.htm',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    timeout: 30 * 1000,

                    data: {
                        senderid: '231797',
                        data: {

                            sessionid: "12345678912345678912345679812345",
                            message: msg
                        }
                    }
                }).then(function (response) {
                    response = {
                        data: JSON.stringify(response.data)
                    };
                    resolve(response);
                }, function (error) {
                    reject();
                });
            });
        }

        function sendChatUAT(msg, id) {
            return new Promise(function (resolve, reject) {

                /* This is audi_chatbot service and only meant for testing it should be commented after testing*/

//                $http({
//                    method: 'POST',
//                    url: 'https://api.api.ai/v1/query?v=20150910',
//                    headers: {
//                        'Content-Type': 'application/json; charset=utf-8',
//                        'Authorization': 'Bearer 6cd741e002a14063b2c8a590c22772ef'
//                    },
//                    data: {
//                        query: [
//                            msg
//                        ],
//                        lang: "en",
//                        sessionId: id,
//                    }
//                }).then(function (response) {
//                    var speech = response.data.result.fulfillment.speech;
//                    if (speech == "") {
//                        // var payload = response.data.result.fulfillment.messages[0].payload;
//                        var payload = {
//                            say: "What time is in your clok?",
//                            type: "calendar",
//                            data: "daterange"
//                        }
//                        //  payload.img = vm.imageUrl;
//                        //  payload.video = vm.videoUrl;
//                        // console.log('response.data.type', response.data.result.fulfillment.messages[0].payload.type);
//                        resolve(payload);
//                    } else {
//                        var sayRes = {
//                            "say": response.data.result.fulfillment.speech,
//
//                        }
//                        resolve(sayRes);
//                    }
//                }, function (error) {
//                    reject();
//                });




                /* code to be uncommentted after testing */

                  $http({
                      method: 'POST',
                      url: 'http://203.189.92.144:9025/engage/chatbot/chat/chatbot_message.htm;jsessionid=' + id,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      timeout: 30 * 1000,
                      data: 'data=' + JSON.stringify({
                          sessionid: id,
                          message: msg,
                          channel: 'uotm',
                          os: 'android'
                      })

                  }).then(function (response) {
                      if (response.data.sts == 500 || response.data.STS == "500") {
                          HtmlViewer.chatbotSession();
                      }
                      var data = response.data.data;
                      data = JSON.parse(data);
                      data = data.message;
                      data = JSON.parse(data);
                      resolve(data);
                  }, function (error) {
                      reject();
                  });


            });
        }


        function sendChatPOC(msg, id) {
            return new Promise(function (resolve, reject) {

                $http({
                    method: 'POST',
                    url: 'http://pice.cloud.tyk.io/chatbot-hrmspoc/',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    timeout: 90 * 1000,
                    data: 'data=' + JSON.stringify({
                        empid: '231797',
                        sessionid: id,
                        msg: msg
                    })
                }).then(function (response) {
                    resolve(response.data);
                }, function (error) {
                    reject();
                });
            });
        }

        function sendChatLIVE(msg, id) {
            return new Promise(function (resolve, reject) {
                // https://universeonthemove.icicibank.com/engage"
                $http({
                    method: 'POST',
                    url: 'https://universeonthemove.icicibank.com/engage/chatbot/chat/chatbot_message.htm;jsessionid=' + id,
                    //                     url: 'https://universeonthemove.icicibank.com/engage/' + id,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    timeout: 30 * 1000,
                    data: 'data=' + JSON.stringify({
                        sessionid: id,
                        message: msg,
                        channel: 'uotm',
                        os: 'android'
                    })

                }).then(function (response) {
                    if (response.data.sts == 500 || response.data.STS == "500") {
                        HtmlViewer.chatbotSession();
                    }
                    var data = response.data.data;
                    data = JSON.parse(data);
                    data = data.message;
                    data = JSON.parse(data);
                    resolve(data);
                }, function (error) {
                    reject();
                });
            });
        }



    }
})();
