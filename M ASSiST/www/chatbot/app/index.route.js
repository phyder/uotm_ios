(function () {
  'use strict';

  angular
    .module('uotmChatBot')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($routeProvider) {
    $routeProvider

      .when('/:id/:name', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      /*
      .when('/', {
        templateUrl: 'app/test/test.html',
        controller: 'TestController',
        controllerAs: 'vm'
      })
      */
      ;
  }

})();
