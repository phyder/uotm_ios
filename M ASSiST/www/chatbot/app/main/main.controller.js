(function () {
  'use strict';

  angular
    .module('uotmChatBot')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, $http, ChatService, $routeParams, $filter, $scope, $log, UOTMConstants) {
    var vm = this;
    vm.id = $routeParams.id;
    vm.shouldRead = false;
    vm.name = $routeParams.name;
    vm.sendButtonDisable = false;
    vm.message = "";
    vm.chats = [];
    vm.lastReply = {};
    vm.msgtyping = false;
    vm.UOTMName = UOTMConstants.uotmName;
    vm.qckrepData = "";
    vm.qckrepConfig = {
      click: function (qckrep) {
        vm.sendMessage(qckrep);
      }
    };

    $(".input-message").focus();

    vm.strDate = '';
    vm.endDate = '';

    vm.calendarDateFormat = {
      click: function (qckrep, qckDateType, dateType) {        
        if (qckDateType == "date") {
          vm.qckrepData = $filter('date')(qckrep, 'MMMM dd yyyy');
          vm.message = vm.qckrepData;
        } else if (qckDateType == "time") {
          vm.qckrepData = $filter('date')(qckrep, 'h:mm a');
          vm.message = vm.qckrepData;
        } else if (qckDateType == "daterange") {
          vm.qckrepData = $filter('date')(qckrep, 'MMMM dd yyyy');

          if(dateType == "startdate"){
             vm.strDate = vm.qckrepData;
             vm.endDate = '';
             vm.message = vm.strDate; 
          }else{
            vm.endDate = vm.qckrepData;
            var res = vm.strDate.concat(' to ' + vm.endDate);
            vm.message = res;
          }

          // if (dateType == "startdate" && vm.strDate =="" && vm.endDate == "" ) {
          //   vm.strDate = vm.qckrepData;
          //   vm.message = vm.strDate.concat(' ' + vm.endDate);
          // } else if (dateType == "enddate" && vm.strDate !== "" ) {
          //   vm.endDate = ' to ' + vm.qckrepData;
          //   var res = vm.strDate.concat(' ' + vm.endDate);
          //   vm.message = res;
          // } 
          // else if (dateType == "enddate" && vm.strDate == "") {
          //   vm.endDate = vm.qckrepData;
          //   var res = vm.strDate.concat(' ' + vm.endDate);
          //   vm.message = res;
          // } 
          // else if (dateType == "startdate" && vm.endDate !== "") {
          //   vm.strDate = vm.qckrepData;
          //   // vm.endDate = '';
          //   var res = vm.strDate.concat(' ' + vm.endDate);
          //   vm.message = res;
          // }
          // else{
          //   alert('Select first Start Date & then End Date !');
          // }



        } else{
          vm.qckrepData = qckrep;
          vm.message = vm.qckrepData;
          // vm.sendMessage(vm.qckrepData);
        }
        // vm.sendMessage(vm.qckrepData);
      }
    };

    vm.selectedDateRange = function (test) {
      alert('this test ' + test);
    }


    // vm.ctrlFn = function () {
    //   console.log("test");
    // }

    vm.pollConfig = {
      click: function (rep) {
        vm.sendMessage(rep);
      }
    };

    vm.jsMethod = function () {
      HtmlViewer.convertToText();
    }
    vm.textToSpeech = function (text) {
      HtmlViewer.convertToSpeech(text);
    }




    $scope.$on('speechToText', function (event, text) {
      $timeout(function () {
        vm.message = text;
        vm.shouldRead = true;
        vm.sendMessage(text);
      })
    })

    function processResult(result) {

      $timeout(function () {
        // vm.msgtypeing = false;
        result.msgtypeing = false;
        vm.currentDate = $filter('date')(new Date(), 'h:mm:ss a');
        result.cdate = vm.currentDate;
        if (result.type == "error") {
          result = {
            type: "text",
            say: "Sorry i was facing some problem fetching your records. \n"
            + "Please try again after some time."
          };
        }
        result.source = 'bot';
        result.name = UOTMConstants.uotmName;
        vm.msgtyping = false;
        vm.lastReply = result;
        // console.log('vm.lastReply', vm.lastReply);

        vm.chats.push(result);
        var target = $('#footer');
        $("html, body").animate({ scrollTop: target.offset().top }, "slow");
        vm.sendButtonDisable = false;
        if (vm.shouldRead) {
          vm.textToSpeech(result.say);
        }
        vm.shouldRead = false;
      });
    }
    vm.sendMessage = function (msg) {
      if (!msg)
        msg = vm.message;
      if (msg && msg != "") {
        //        vm.shouldRead = true;
        vm.sendButtonDisable = true;
        $(document.activeElement).filter(':input:focus').blur();
        vm.message = "";
        vm.currentDate = $filter('date')(new Date(), 'h:mm:ss a');
        vm.msgtyping = true;
        var chat = { type: "text", say: msg, source: 'user', name: vm.name, cdate: vm.currentDate, msgtypeing: vm.msgtyping };
        vm.lastReply = chat;
        // chat.bg="bg-blue";
        vm.chats.push(chat);
        $timeout(function () {
          var target = $('#footer');
          $("html, body").animate({ scrollTop: target.offset().top }, "slow");
        });

        // processResult(msg);

        ChatService.sendChat(msg, vm.id)
          .then(function success(data) {
            // console.log('data ',data);
            processResult(data);
            $(".input-message").focus();
          }, function error() {
            processResult({
              type: "text",
              say: "Sorry I was facing some problem fetching your records. \n"
              + "Please try again after some time."
            });
            $(".input-message").focus();

          });
      }
    };

  }
})();
