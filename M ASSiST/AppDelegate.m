    //
//  AppDelegate.m
//  M ASSiST
//
//  Created by Suraj on 19/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "AppDelegate.h"

#import "Constant.h"
#import "ErrorLog.h"
#import "Holder.h"
#import "ResourceCheck.h"
#import "Authentication.h"
#import "ECrypto.h"

#import "IIViewDeckController.h"
#import "LoginViewCtlr.h"

#import "EzTokenSDK.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

//#import "BWQuincyManager.h"

//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

// setSubmissionURL for self hosted server
static NSString *CRASH_URL = @"https://cogmat.in/mobile_app/server/crash_v500.php";
NSString *const SubscriptionTopic = @"/topics/global";

/*!
 @category Universe on the move (AppDelegate)
 @class AppDelegate
 @superclass UIResponder
 @discussion This class instantiate the app.
 @encoding utf8
 @author Suraj Singh
 */

@interface AppDelegate()<FIRMessagingDelegate,UNUserNotificationCenterDelegate>  {
    NSString *regToken;
}
@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;
//@property (nonatomic, strong) NSString *strDeviceToken;
@end

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    @try {
        // Custom initialzation
       // [Fabric with:@[[Crashlytics class]]];
        
        
        [EzTokenSDK initSDK];
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [Holder sharedHolder].notificationId = [Util getKeyChainNotificationId];
        if ([Holder sharedHolder].notificationId) {
           
            [defaults setObject:[Holder sharedHolder].notificationId forKey:@"notification"];
            [defaults synchronize];
        }
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"notification id"
//                                                        message:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"notification"]]
//                                                       delegate:nil cancelButtonTitle:@"No"
//                                              otherButtonTitles:@"Yes", nil];
//        [alert show];
        
        NSLog(@"Home: %@ done", NSHomeDirectory());
        NSLog(@"PATH: %@ done", [Util getDocumentsDirectory]);
        
        // Check existance of www directory handles file update functionality
        [ResourceCheck checkResourceCopy];
        
        
        // [START_EXCLUDE]
        _registrationKey = @"onRegistrationCompleted";
        _messageKey = @"onMessageReceived";
        
    
        // Register for Remote Notification
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
        #if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]
             requestAuthorizationWithOptions:authOptions
             completionHandler:^(BOOL granted, NSError * _Nullable error) {
             }
             ];
            
            // For iOS 10 display notification (sent via APNS)
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            // For iOS 10 data message (sent via FCM)
            [[FIRMessaging messaging] setRemoteMessageDelegate:self];
        #endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        // [START configure_firebase]
        [FIRApp configure];
        // [END configure_firebase]
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                     name:kFIRInstanceIDTokenRefreshNotification object:nil];
        
        // Implements third party crash logging, using Qunicy Kit
        _application = application;
        
//        // Check existance of www directory handles file update functionality
//        [ResourceCheck checkResourceCopy];
        
        //Clear device id on first run
        if ([[Util getSettingUsingName:FIRST_RUN] isEqualToString:PAGE_TRUE]) {
            [Util removeUserSettings];
        }
        
        CRASH_URL = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_CRASH_REPORT]];
//        [[BWQuincyManager sharedQuincyManager] setSubmissionURL:CRASH_URL];
//        [[BWQuincyManager sharedQuincyManager] setDelegate:self];
//        [[BWQuincyManager sharedQuincyManager] setLoggingEnabled:NO];
//        [[BWQuincyManager sharedQuincyManager] startManager];
        
        
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        
        
        IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:nil
                                                                                        leftViewController:[[UIViewController alloc] init]
                                                                                       rightViewController:nil];
        
        NSString *userName = [Util getKeyChainUserName];
        NSString *mPin = [Util getKeyChainMPin];
        
        
//        NSLog(@"mPin %@",mPin);
//        userName = @"";
//        mPin = @"";
        
        if([userName isEqual:[NSNull null]] || [mPin isEqual:[NSNull null]] || userName == nil || mPin == nil) {
            // Override point for customization after application launch.
            UIViewController *initViewController = [storyBoard instantiateInitialViewController];
            deckController.centerController = initViewController;
        } else {
            UIViewController *initViewController = [storyBoard instantiateViewControllerWithIdentifier:@"navautologin"];
            deckController.centerController = initViewController;
        }
        
        deckController.leftSize = 80.0f;
        deckController.panningMode = IIViewDeckNoPanning;
        [Holder sharedHolder].viewController = deckController;
        
        /* To adjust speed of open/close animations, set either of these two properties. */
        deckController.openSlideAnimationDuration = 0.15f;
        deckController.closeSlideAnimationDuration = 0.25f;
        
        //[Util removeAllKeyChain];
        
        [self.window setRootViewController:deckController];
        
        
        // Download the resource need once from the server
//        AppUpdate *app = [[AppUpdate alloc] init];
//        [app downloadUpdated];
        
//        [Util sendErrorLog];
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"App not initialized";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
    //[Fabric with:@[[Crashlytics class]]];
    

    return YES;
}
-(void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"didReceive"
//                                                    message:[NSString stringWithFormat:@"%@",remoteMessage]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
    
}

- (void) postAutoLoginAuthenticationWithData:(NSMutableDictionary *) responseData {
   
    [[Holder sharedHolder].auth postAutoLoginAuthenticationWithData:responseData];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs token here.
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    NSLog(@"Received push notification: %@, identifier: %@", userInfo, identifier); // iOS 8
    completionHandler();
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (NSString *)setKeyChainWithDeviceId:(NSString *)deviceToken NotificationId:(NSString *)notificationId {
    NSString *deviceId = [Util setKeyChainWithDeviceId:deviceToken];
    [Util setKeyChainWithNotificationId:notificationId];
    return deviceId;
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
   
    NSLog(@"Unable to register for remote notifications: %@", error);
}

//This function is called when app recieve notification(s)
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"didReceive"
//                                                    message:[NSString stringWithFormat:@"%@",userInfo]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
    // Print message ID.
    if ([userInfo objectForKey:kGCMMessageIDKey]) {
        NSLog(@"User Info is: %@ \nMessage ID: %@",userInfo, userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"Full Message is %@", userInfo);
 }
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//       willPresentNotification:(UNNotification *)notification
//         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
//{
//    NSLog( @"Handle push from foreground" );
//    // custom code to handle push while app is in the foreground
//    NSLog(@"%@", notification.request.content.userInfo);
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"didReceive"
//                                                    message:[NSString stringWithFormat:@"%@",notification.request.content.userInfo]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
//}
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//didReceiveNotificationResponse:(UNNotificationResponse *)response
//         withCompletionHandler:(void (^)())completionHandler
//{
//    
//    NSLog( @"Handle push from background or closed" );
//    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
//    NSLog(@"%@", response.notification.request.content.userInfo);
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"didReceivejhcbasd"
//                                                    message:[NSString stringWithFormat:@"%@",response.notification.request.content.userInfo]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
//}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler
{
  //  [self receiveNotification:userInfo application:application];
    handler(UIBackgroundFetchResultNoData);
    
    
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"didReceiveRemoteNo"
//                                                    message:[NSString stringWithFormat:@"%@",userInfo]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
    
    // Print message ID.
    if ([userInfo objectForKey:kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"Notification received is %@", userInfo);
    NSLog(@"anuj 1");
        @try {
            NSLog(@"Notification received: %@", userInfo);
            // This works only if the app started the GCM service
           // [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
            // Handle the received message
            // [START_EXCLUDE]
            [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                                object:nil
                                                              userInfo:userInfo];
    
            // [END_EXCLUDE]
    
            //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification"
            //                                                        message:@"You have received notification"
            //                                                       delegate:nil
            //                                              cancelButtonTitle:ALERT_OPT_OK
            //                                              otherButtonTitles:nil];
            NSLog(@"anuj 2");
            NSLog(@"Notification received: %@", userInfo);
            NSArray *keys = [userInfo allKeys];
            NSLog(@"this is keys %@",keys);
            NSLog(@"this is keys userInfo %@",userInfo[@"aps"]);
//            if ([keys containsObject:@"aps"]) { // Keys Exist
//                if(userInfo[@"aps"]) { // Value Exist
//                    NSError *jsonError;
//                    NSData *objectData = [userInfo[@"aps"] dataUsingEncoding:NSUTF8StringEncoding];
//                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
//                                                                         options:NSJSONReadingMutableContainers
//                                                                           error:&jsonError];
//                    NSNumber *badge = nil;
//                    if (!jsonError) { // Error Check
//                        badge = json[@"badge"];
//                    }
                    NSString *badge = userInfo[@"aps"][@"badge"];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:badge forKey:@"badge"];
                    [defaults synchronize];
    
                    [Holder sharedHolder].pendingBadgeCount = [badge intValue];
                    NSLog(@"set bagdecount not active %@",[defaults valueForKey:@"badge"]);
                    NSLog(@"anuj 3");
                    [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
                    [Holder sharedHolder].pendingCountLeave  = userInfo[@"gcm.notification.NTFN_LV"];
                    [Holder sharedHolder].pendingCountMuster = userInfo[@"gcm.notification.NTFN_MU"];
                    [Holder sharedHolder].pendingCountLatePunch  = userInfo[@"gcm.notification.NTFN_LTP"];
                    [Holder sharedHolder].pendingCountConveyance = userInfo[@"gcm.notification.NTFN_EXP"];
            
                    NSLog(@"this is 1 data %@",userInfo[@"gcm.notification.NTFN_LV"]);
                    NSLog(@"this is 2 data %@",userInfo[@"gcm.notification.NTFN_EXP"]);
                    NSLog(@"this is 3 data %@",userInfo[@"gcm.notification.NTFN_MU"]);
                    NSLog(@"this is 4 data %@",userInfo[@"gcm.notification.NTFN_LTP"]);
               // }
//            } else {
//                NSLog(@"anuj 4");
//                NSLog(@"this is 11 data %@",userInfo[@"gcm.notification.NTFN_LV"]);
//                NSLog(@"this is 22 data %@",userInfo[@"gcm.notification.NTFN_EXP"]);
//                NSLog(@"this is 33 data %@",userInfo[@"gcm.notification.NTFN_MU"]);
//                NSLog(@"this is 44 data %@",userInfo[@"gcm.notification.NTFN_LTP"]);
//                [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue];
//            }
    
            NSLog(@"Notification received: %@", userInfo);
    
            //        if (application.applicationState == UIApplicationStateActive ) {
            //
            //            NSLog(@"Notification received: Active");
            //
            //            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            //            localNotification.userInfo = userInfo;
            //            localNotification.soundName = UILocalNotificationDefaultSoundName;
            //            localNotification.alertBody = userInfo[@"aps"][@"alert"][@"body"];
            //            localNotification.alertTitle = userInfo[@"aps"][@"alert"][@"title"];
            //            localNotification.fireDate = [NSDate date];
            //            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            //
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCount" object:self userInfo:nil];
            //        }
    
            if ( application.applicationState == UIApplicationStateActive ) {
                // app was already in the foreground
                NSString *title = userInfo[@"aps"][@"alert"][@"title"];
                NSString *message = userInfo[@"aps"][@"alert"][@"body"];
    
                NSString *badge = userInfo[@"aps"][@"badge"];
    
                [Holder sharedHolder].pendingBadgeCount = [badge intValue];
    
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:badge forKey:@"badge"];
                [defaults synchronize];
                 NSLog(@"anuj");
                 NSLog(@"set bagdecount %@",[defaults valueForKey:@"badge"]);
    
                [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
                NSLog(@"Receive Notification: APP in Foreground %@",message);
                if(message != nil) {
                    if([message isEqualToString:@""]) {
    
                    } else {
                        NSLog(@"anuj alert");
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                                        message:message
                                                                       delegate:nil
                                                              cancelButtonTitle:ALERT_OPT_OK
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCount" object:self userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCountGrid" object:self userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCountList" object:self userInfo:nil];
    
            } //else {
            //            // app was just brought from background to foreground
            //            //[self updateNotificationCenter];
            //            NSLog(@"Receive Notification: APP brought from background to foreground");
            //        }
        } @catch (NSException *exception) {
            NSString *strLogMessage = @"Can not show notification";
    #if DEBUG_ERROR_LOG
            NSLog(@" strlog %@", strLogMessage);
    #endif
            [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        }

}

//-(void) receiveNotification:(NSDictionary *)userInfo application:(UIApplication *)application {
//    @try {
//        NSLog(@"Notification received: %@", userInfo);
//        // This works only if the app started the GCM service
//       // [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
//        // Handle the received message
//        // [START_EXCLUDE]
//        [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
//                                                            object:nil
//                                                          userInfo:userInfo];
//        
//        // [END_EXCLUDE]
//        
//        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification"
//        //                                                        message:@"You have received notification"
//        //                                                       delegate:nil
//        //                                              cancelButtonTitle:ALERT_OPT_OK
//        //                                              otherButtonTitles:nil];
//

//        NSArray *keys = [userInfo allKeys];
//        if ([keys containsObject:@"gcm.notification.aps"]) { // Keys Exist
//            if(userInfo[@"gcm.notification.aps"]) { // Value Exist
//                NSError *jsonError;
//                NSData *objectData = [userInfo[@"gcm.notification.aps"] dataUsingEncoding:NSUTF8StringEncoding];
//                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
//                                                                     options:NSJSONReadingMutableContainers
//                                                                       error:&jsonError];
//                NSNumber *badge = nil;
//                if (!jsonError) { // Error Check
//                    badge = json[@"badge"];
//                }
//                
//                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                [defaults setObject:badge forKey:@"badge"];
//                [defaults synchronize];
//                
//                [Holder sharedHolder].pendingBadgeCount = [badge intValue];
//                
//                [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
//            }
//        } else {
//            [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue];
//        }
//        
//        NSLog(@"Notification received: %@", userInfo);
//        
//        //        if (application.applicationState == UIApplicationStateActive ) {
//        //
//        //            NSLog(@"Notification received: Active");
//        //
//        //            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//        //            localNotification.userInfo = userInfo;
//        //            localNotification.soundName = UILocalNotificationDefaultSoundName;
//        //            localNotification.alertBody = userInfo[@"aps"][@"alert"][@"body"];
//        //            localNotification.alertTitle = userInfo[@"aps"][@"alert"][@"title"];
//        //            localNotification.fireDate = [NSDate date];
//        //            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//        //
//        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCount" object:self userInfo:nil];
//        //        }
//        
//        if ( application.applicationState == UIApplicationStateActive ) {
//            // app was already in the foreground
//            NSString *title = userInfo[@"aps"][@"alert"][@"title"];
//            NSString *message = userInfo[@"aps"][@"alert"][@"body"];
//            
//            NSString *badge = userInfo[@"aps"][@"badge"];
//            
//            [Holder sharedHolder].pendingBadgeCount = [badge intValue];
//            
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setObject:badge forKey:@"badge"];
//            [defaults synchronize];
//            
//            
//            [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
//            NSLog(@"Receive Notification: APP in Foreground %@",message);
//            if(message != nil) {
//                if([message isEqualToString:@""]) {
//                    
//                } else {
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                                    message:message
//                                                                   delegate:nil
//                                                          cancelButtonTitle:ALERT_OPT_OK
//                                                          otherButtonTitles:nil];
//                    [alert show];
//                }
//            }
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCount" object:self userInfo:nil];
//            
//        } //else {
//        //            // app was just brought from background to foreground
//        //            //[self updateNotificationCenter];
//        //            NSLog(@"Receive Notification: APP brought from background to foreground");
//        //        }
//    } @catch (NSException *exception) {
//        NSString *strLogMessage = @"Can not show notification";
//#if DEBUG_ERROR_LOG
//        NSLog(@"%@", strLogMessage);
//#endif
//        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
//    }
//}


- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
   
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    NSString *strUuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            NSString *strDeviceId = [self setKeyChainWithDeviceId:strUuid
                                                   NotificationId:refreshedToken];
    
            [Util setSettingWithName:APP_DEVICE_ID ByValue:strDeviceId];
                NSLog(@"Device ID from Keychain  %@", strDeviceId);
    
            [Util setSettingWithName:APP_NOTIFICATION_ID ByValue:refreshedToken];
            NSLog(@"Registration Token : %@", refreshedToken);
    
            [Holder sharedHolder].notificationId = refreshedToken;
//           NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//           [defaults setObject:refreshedToken forKey:@"notification"];
//           [defaults synchronize];
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

// [START connect_to_fcm]
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}
// [END connect_to_fcm]


- (NSString *)modeString
{
#if DEBUG
    return @"Development (sandbox)";
#else
    return @"Production";
#endif
}

- (void)updateNotificationCenter {
    long badgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeCount];
}
							
- (void)applicationWillResignActive:(UIApplication *)application {
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
//    int badgeCount = [[Holder sharedHolder].pendingCountLeave intValue] + [[Holder sharedHolder].pendingCountMuster intValue] + [[Holder sharedHolder].pendingCountLatePunch intValue] + [[Holder sharedHolder].pendingCountConveyance intValue];
//    
//    [Holder sharedHolder].pendingBadgeCount = badgeCount;
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [Holder sharedHolder].pendingBadgeCount = [[defaults valueForKey:@"badge"] integerValue];
//    NSLog(@"this is bagdecount active %d",([Holder sharedHolder].pendingBadgeCount));
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [Holder sharedHolder].pendingBadgeCount;
}



- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    @try {
        NSString *loginData = [Util getSettingUsingName:BACKGROUND_LOGGED_INFO];
        
        NSError *error;
        NSDictionary *loginInfo = [NSJSONSerialization JSONObjectWithData:[loginData dataUsingEncoding:NSUTF8StringEncoding]
                                                                  options:NSJSONReadingMutableLeaves
                                                                    error:&error];
        
        [Holder sharedHolder].pendingBadgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
        
        if (!error) {
//            NSLog(@"Login Data: %@", [Holder sharedHolder].loginInfo);
            [Holder sharedHolder].loginInfo = loginInfo;
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Exception while entering foreground";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Clear App Notification Badge
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [Holder sharedHolder].pendingBadgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
    
    // Connect to the GCM server to receive non-APNS notifications
//    [[GCMService sharedInstance] connectWithHandler:^(NSError *error) {
//        if (error) {
//            NSLog(@"Could not connect to GCM: %@", error.localizedDescription);
//        } else {
//            _connectedToGCM = true;
//            NSLog(@"Connected to GCM");
//            // [START_EXCLUDE]
//            [self subscribeToTopic];
//            // [END_EXCLUDE]
//        }
//    }];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - CoreData
//Explicitly write Core Data accessors
- (NSManagedObjectContext *) managedObjectContext {
    
    @try {
        if (managedObjectContext != nil) {
            return managedObjectContext;
        }
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (coordinator != nil) {
            managedObjectContext = [[NSManagedObjectContext alloc] init];
            [managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Core Data Managed context not initialized";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    
    @try {
        if (managedObjectModel != nil) {
            return managedObjectModel;
        }
        managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Core Data Managed Object Model not initialized";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    @try {
        if (persistentStoreCoordinator != nil) {
            return persistentStoreCoordinator;
        }
        
        NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"ErrorLog.sqlite"]];
        NSError *error = nil;
        persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                     configuration:nil
                                                               URL:storeUrl
                                                           options:nil
                                                             error:&error]) {
            /*Error for store creation should be handled in here*/
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Core Data persistant store can not be initialized";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }
    
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

@end
