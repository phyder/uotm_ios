//
//  AppConnect.h
//  M ASSiST
//
//  Created by Suraj on 22/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PSTCollectionView.h"
#import "MBProgressHUD.h"


@class BrandCollViewCell;
@class CarOfferListCell;

@interface AppConnect : NSObject <NSURLConnectionDelegate, MBProgressHUDDelegate> {
    
    int intReqFor;
    SEL callAfterfinish;
    UIViewController *callAfterOnObject;
    NSObject *callAfterOnNSObject;
    PSUICollectionViewController *callAfterOnGridObject;
    
//    NSURLResponse *response;
//    NSMutableData *responseData;
    
    NSDate *startDate, *endDate;
}

@property (nonatomic, weak) MBProgressHUD *progressBar;

@property (nonatomic) NSURLConnection *connection;
@property (nonatomic) NSURLResponse *response;
@property (nonatomic) NSMutableData *responseData;

- (id)initWithAfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view;
- (id)initWithAfterFinishCall:(SEL) offerListFinished onGridObject:(PSUICollectionViewController *) view;
- (id)initWithAfterFinishCall:(SEL) offerListFinished onNSObject:(NSObject *) view;


+ (Boolean) checkInternetConnectivity;

- (NSString *) fetchPmsToken;
- (void) requestWebPageDataForURL:(NSString *) url;             //Webpage Data Request
- (void) refreshWebPageDataForURL:(NSString *) url;             //Webpage Data Refresh

- (void)requestRegisterWithURL:(NSString *)url username:(NSString *) username password:(NSString *)password key:(NSString *)key;
- (void)requestOTPWithURL:(NSString *)url username:(NSString *) username otp:(NSString *)otp key:(NSString *)key;
- (void)requestSetMPinWithURL:(NSString *)url username:(NSString *)username newMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin key:(NSString *)key;
- (void)requestResetMPinWithURL:(NSString *)url username:(NSString *)username password:(NSString *)password newMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin key:(NSString *)key;
- (void)requestAutoLoginWithURL:(NSString *)url username:(NSString *)username mPin:(NSString *)mPin key:(NSString *)key bio:(NSString *) bio;
- (void)requestVerifyMPinWithURL:(NSString *)url username:(NSString *)username mPin:(NSString *)mPin key:(NSString *)key;
- (void)requestSignInWithURL:(NSString *)url username:(NSString *) username password:(NSString *)password key:(NSString *)key;  // SignIn resquest
- (void)requestDeregisterWithURL:(NSString *)url username:(NSString *)username deviceId:(NSString *)deviceId;
- (void)requestGetRegisterListWithURL:(NSString *)url username:(NSString *)username;
- (void) requestSignOutWithURL:(NSString *) url;                // SignOut request
- (void) requestDynalistDataWithURL:(NSString *) url;           // Dynalist request
- (void)requestESOPWithURL:(NSString *)url;                     // ESOP request

- (void)multipleApproveRejectWithType:(int)type For:(int)requestType Param:(NSString *)param;    // Multiple Approve/Reject

- (void) requestPendingApprovalsCount:(NSString *) strURL;      // Pending Approval Request Count

// Home Offer
- (void) getJson;
- (void) getOfferListWithCityId: (NSString *) cityId AnsCityName:(NSString *) cityName OnNSObject:(NSObject *) obj;
- (void) getOfferDetails: (NSString *) offerId;

// Car Offer
- (void) getCarOfferBrandAfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view;
- (void) getCarOfferListForBrandId:(NSString *) brandId BrandName:(NSString *) brandName AfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view;
- (void) getOfferDetailsForOfferId:(NSString *) offerId AfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view;

- (void) getBrandImageWithName:(NSString *) name AfterFinishCall:(SEL) offerListFinished onObject:(BrandCollViewCell *) view;
- (void) getListImageWithName:(NSString *) name AfterFinishCall:(SEL) offerListFinished onObject:(UITableViewCell *) view;

// Other Offer
- (void) getOtherOfferListAfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view;
- (void) getOtherOfferDetailsForOfferId:(NSString *) offerId AfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view;

// Offer I'm Interested
//- (void) btnIMInterestedPressedForUserId: (NSString *)userId offerId:(NSString *) offerId;
- (void) btnIMInterestedPressedForUserId: (NSString *)userId offerId:(NSString *) offerId WithNSObject:(NSObject *) obj;

// Other Calendar
- (void)requestOtherCalendar:(NSString *)url Params:(NSString *)params;

@end
