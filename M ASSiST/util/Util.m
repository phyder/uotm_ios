
//
//  Util.m
//  engage
//
//  Created by Suraj on 12/3/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "Util.h"

#import <sys/sysctl.h>

#import "AppConnect.h"
#import "Authentication.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Holder.h"

#import "XMLReader.h"
#import "XMLWriter.h"

#import "BrandCollViewCtlr.h"
#import "HomeMapViewCtlr.h"
#import "LoginViewCtlr.h"
#import "CarouselViewCtlr.h"
#import "IIViewDeckController.h"
#import "OtherOfferListViewCtlr.h"
#import "GridViewCtlr.h"
#import "ListViewCtlr.h"
#import "MenuListViewCtlr.h"
#import "WebViewController.h"
#import "SSKeychain.h"
#import "VerifyMPinViewCtlr.h"

@implementation Util

+ (void) pushWithFadeEffectOnViewController:(UIViewController *) sourceVC ViewController:(UIViewController *) destinationVC {
    
    /*[CATransaction begin];
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFade;
    transition.duration = 1.0f;
    transition.fillMode = kCAFillModeForwards;
    transition.removedOnCompletion = YES;
    
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [CATransaction setCompletionBlock: ^ {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^ {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        });
    }];
    
    [sourceVC.navigationController pushViewController:destinationVC animated:NO];
    
    [CATransaction commit];*/
    
    [UIView beginAnimations:@"Page Curl Up" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    if ([sourceVC isKindOfClass:[LoginViewCtlr class]] || [sourceVC isKindOfClass:[CarouselViewCtlr class]] || [sourceVC isKindOfClass:[UINavigationController class]]) {
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                               forView:sourceVC.navigationController.view cache:NO];
    } else {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:sourceVC.navigationController.view cache:NO];
    }
    
    
    [sourceVC.navigationController pushViewController:destinationVC animated:YES];
    [UIView commitAnimations];
    
    //sourceVC.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    //[sourceVC.navigationController presentViewController:destinationVC animated:YES completion:nil];
    //[sourceVC.navigationController pushViewController:destinationVC animated:NO];
}

+ (void) pushWithFadeEffectOnNavigationController:(UINavigationController *) sourceNC ViewController:(UIViewController *) destinationVC {
    
    /*[CATransaction begin];
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFade;
    transition.duration = 1.0f;
    transition.fillMode = kCAFillModeForwards;
    transition.removedOnCompletion = YES;
    
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [CATransaction setCompletionBlock: ^ {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^ {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        });
    }];
    
    [sourceNC pushViewController:destinationVC animated:NO];
    [CATransaction commit];*/
    
    [UIView beginAnimations:@"Page Curl Up" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    if ([sourceNC isKindOfClass:[LoginViewCtlr class]]
        || [sourceNC isKindOfClass:[CarouselViewCtlr class]]
        || ([sourceNC isKindOfClass:[UINavigationController class]] && [destinationVC isKindOfClass:[GridViewCtlr class]])
        || ([sourceNC isKindOfClass:[UINavigationController class]] && [destinationVC isKindOfClass:[ListViewCtlr class]])) {
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                               forView:sourceNC.view
                                 cache:NO];
    } else {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:sourceNC.view
                                 cache:NO];
    }
    
    [sourceNC pushViewController:destinationVC animated:YES];
    [UIView commitAnimations];
    
    //[sourceNC pushViewController:destinationVC animated:YES];
}

+ (void) popWithFadeEffectOnViewController:(UIViewController *) sourceVC ShouldPopToRoot:(BOOL) option {
    
    /*[CATransaction begin];
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFade;
    transition.duration = 1.0f;
    transition.fillMode = kCAFillModeForwards;
    transition.removedOnCompletion = YES;
    
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [CATransaction setCompletionBlock: ^ {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^ {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        });
    }];
    
    if (option==YES) {
        [sourceVC.navigationController popToRootViewControllerAnimated:NO];
    } else {
        [sourceVC.navigationController popViewControllerAnimated:NO];
    }
    [CATransaction commit];*/
    
    [UIView beginAnimations:@"Page Curl Down" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    if ([sourceVC isKindOfClass:[CarouselViewCtlr class]] || [sourceVC isKindOfClass:[GridViewCtlr class]]) {
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
                               forView:sourceVC.navigationController.view cache:NO];
    } else {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                               forView:sourceVC.navigationController.view cache:NO];
    }
    
    if (option==YES) {
        [Holder sharedHolder].isLoggedIn = PAGE_FALSE;
        [sourceVC.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [sourceVC.navigationController popViewControllerAnimated:YES];
    }
    [UIView commitAnimations];
}

+ (void) popWithFadeEffectOnNavigationController:(UINavigationController *) sourceNC ShouldPopToRoot:(BOOL) option {
    
    /*[CATransaction begin];
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFade;
    transition.duration = 1.0f;
    transition.fillMode = kCAFillModeForwards;
    transition.removedOnCompletion = YES;
    
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [CATransaction setCompletionBlock: ^ {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^ {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        });
    }];
    
    if (option==YES) {
        [sourceNC popToRootViewControllerAnimated:NO];
    } else {
        [sourceNC popViewControllerAnimated:NO];
    }
    
    [CATransaction commit];*/
    
    [UIView beginAnimations:@"Page Curl Down" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    if ([sourceNC isKindOfClass:[CarouselViewCtlr class]] || [sourceNC isKindOfClass:[GridViewCtlr class]]) {
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
                               forView:sourceNC.view cache:NO];
    } else {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                               forView:sourceNC.view cache:NO];
    }
    
    if (option==YES) {
        [Holder sharedHolder].isLoggedIn = PAGE_FALSE;
        [sourceNC popToRootViewControllerAnimated:YES];
    } else {
        [sourceNC popViewControllerAnimated:YES];
    }
    [UIView commitAnimations];
}

+ (NSString *)platformRawString {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = @(machine);
    free(machine);
    return platform;
}

+ (NSString *)platformNiceString {
    
    NSString *platform = [[self class] platformRawString];
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"] || [platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad 1";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (4G,2)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (4G,3)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

+ (void) featureNotSupported {
    UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE] // Title in the Alert view
                                                         message:@"Your device doesn't support this feature."
                                                        delegate:nil
                                               cancelButtonTitle:ALERT_OPT_OK
                                               otherButtonTitles:nil];
    [Notpermitted show];
}

+ (NSString *) getDocumentsDirectory {
    
    return [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

+ (void) clearCacheWithViewController:(UIViewController *) viewController {
    
    MBProgressHUD *progressBar      = [MBProgressHUD showHUDAddedTo:viewController.navigationController.view animated:YES];
    progressBar.mode                = MBProgressHUDModeCustomView;
    progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    progressBar.delegate            = nil;
    progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    progressBar.dimBackground       = YES;
    progressBar.detailsLabelText    = [NSString stringWithFormat:@"Clearing cache"];
    
    [[self class] clearCache];
    int64_t delayInSeconds = 5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [progressBar hide:YES];
    });
}

+ (void) clearCache {
    
    if ([[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [Util getDocumentsDirectory], @"www"] error:nil]) {
        NSLog(@"Cache cleared");
    } else {
        NSLog(@"Cache NOT cleared");
    }
}


+ (NSDictionary *) getMenu {
    
    return [[Holder sharedHolder].menu copy];
}

+ (void) initSettingFromXML {
    
    NSString *settingXMLPath = [[Util getDocumentsDirectory] stringByAppendingPathComponent:SETTINGS_FILE];
    //NSLog(@"xml path: %@", settingXMLPath);
    
    NSString *xmlData = [NSString stringWithContentsOfFile:settingXMLPath encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *settings;
    
    settings = [Util parseXML:xmlData];
    //NSLog(@"Hashmap: %@", settings);
    
    NSArray *tempData = settings[@"settings"][@"setting"];
    NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] init];
    for (int i=0; i<[tempData count]; i++) {
        //NSLog(@"%@=%@", [[tempData objectAtIndex:i] objectForKey:@"name"], [[tempData objectAtIndex:i] objectForKey:@"value"]);
        [settingsDictionary setValue:tempData[i][@"value"] forKey:tempData[i][@"name"]];
    }
    
    [Holder sharedHolder].settings = settingsDictionary;
}

+ (void) initXML {
    
    NSString *pathXML, *xmlData;
    NSError *error = nil;
    pathXML = [[Util getDocumentsDirectory] stringByAppendingPathComponent:MENU_FILE];
    xmlData = [NSString stringWithContentsOfFile:pathXML encoding:NSUTF8StringEncoding error:&error];
    if (error==nil) {
        [Holder sharedHolder].menu = [Util parseXML:xmlData];
    } else {
        NSLog(@"Error: Util.initXML\n%@", error);
        error=nil;
    }
    
    
    pathXML = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:CSS_FILE];
    //NSLog(@"xml path: %@", menuXMLPath);
    xmlData = [NSString stringWithContentsOfFile:pathXML encoding:NSUTF8StringEncoding error:nil];
    if (error==nil) {
        [Holder sharedHolder].css = [Util parseXML:xmlData];
    } else {
        NSLog(@"Error: Util.initXML\n%@", error);
        error=nil;
    }
    
    pathXML = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:FILE_VERSION_LIST];
    //NSLog(@"xml path: %@", menuXMLPath);
    xmlData = [NSString stringWithContentsOfFile:pathXML encoding:NSUTF8StringEncoding error:nil];
    if (error==nil) {
        [Holder sharedHolder].file = [Util parseXML:xmlData];
    } else {
        NSLog(@"Error: Util.initXML\n%@", error);
        error=nil;
    }
}

+ (NSDictionary *) parseXML: (NSString *) xmlData {
    
    xmlData = [xmlData stringByReplacingOccurrencesOfString:@"    " withString:@""];
    //xmlData = [xmlData stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    xmlData = [xmlData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //NSLog(@"XML Data: %@", xmlData);
    
    // Parse the XML into a dictionary
    NSError *parseError = nil;
    NSDictionary *settings = [XMLReader dictionaryForXMLString:xmlData error:&parseError];
    // Print the dictionary
    //NSLog(@"\n\n\n\n\nData: \n%@", settings);
    
    return settings;
}

+ (BOOL)setSettingWithName:(NSString *)settingName ByValue:(NSString *) settingValue {
    
    Holder *holder = [Holder sharedHolder];
    NSMutableDictionary *settings = (NSMutableDictionary *) holder.settings;
    
    if (settings[settingName]!=nil) {
        [settings setValue:settingValue forKey:settingName];
        holder.settings = settings;
        [[self class] writeSettings];
        /*if ([settingName isEqualToString:MOBILE4U_URL]) {
            [[self class] writeURLInJS:settingValue];
        }*/
    } else {
        // TODO new value is to be created.
    }
    
    return YES;
}

+ (void) writeDataInJS:(NSDictionary *) data {
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *writeData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        writeData = [[self class] encodingStringJavaScriptEscaped:writeData];
        writeData = [NSString stringWithFormat:@"var data=\"%@\";", writeData];
        NSLog(@"Data to be write: %@", writeData);
        
        NSString *filePath  = [[[self class] getDocumentsDirectory] stringByAppendingPathComponent:JSPATH_WEBPAGE_DATA];
        //NSError *error = nil;
        [writeData writeToFile:filePath
                    atomically:YES
                      encoding:NSUTF8StringEncoding
                         error:&error];
    }
    
//    SBJsonWriter *jsonWriter = [SBJsonWriter new];
//    NSString *writeData = [jsonWriter stringWithObject:data];
//    //NSLog(@"Data to be write: %@", writeData);
//    writeData           = [NSString stringWithFormat:@"var data=\"%@\";", [[jsonWriter stringWithObject:data] stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
}

+ (NSString *) encodingStringJavaScriptEscaped:(NSString *) unescapedString {
    
    return [unescapedString stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
}

/*+ (void) writeURLInJS: (NSString *) url {
    NSError *error;
    NSString *filePath = [[[self class] getDocumentsDirectory] stringByAppendingPathComponent:JS_WITH_URL];
    [[NSString stringWithFormat:@"var url=\"%@\";", url] writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
}*/

+ (NSString *) getSettingUsingName: (NSString *) settingName {
    NSString* settingValue = nil;
    Holder *holder = [Holder sharedHolder];
    NSDictionary *settings = holder.settings;
    
    if (settings[settingName]!=nil) {
        settingValue = settings[settingName];
    } else {
        NSLog(@"%s Error: No setting exist with name - %@", __FUNCTION__, settingName);
    }
    return settingValue;
}

+ (NSString *) setKeyChainWithDeviceId:(NSString *) deviceId {
    NSError *error;
    NSString *notyId = [SSKeychain passwordForService:KEY_CHAIN_DEVICE_ID_KEY account:KEY_CHAIN_ACCOUNT];
    if([notyId isEqual:[NSNull null]] || notyId == nil) {
        [SSKeychain setPassword:deviceId forService:KEY_CHAIN_DEVICE_ID_KEY account:KEY_CHAIN_ACCOUNT error:&error];
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:deviceId                                                                   message:@"This is IF block"
//                                                           delegate:nil
//                                                  cancelButtonTitle: @"test"
//                                                  otherButtonTitles:nil, nil];
//        [alertView show];
        return deviceId;
    } else {
        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[self getKeyChainDeviceId]                                                                    message:@"This is else block"
//                                                           delegate:nil
//                                                  cancelButtonTitle: @"test"
//                                                  otherButtonTitles:nil, nil];
//        [alertView show];
        return [self getKeyChainDeviceId];
    }
}

+ (NSString *) getKeyChainDeviceId {
    return [SSKeychain passwordForService:KEY_CHAIN_DEVICE_ID_KEY account:KEY_CHAIN_ACCOUNT];
}

+ (BOOL) setKeyChainWithNotificationId:(NSString *) deviceId {
    NSError *error;
    NSString *notyId = [SSKeychain passwordForService:KEY_CHAIN_NOTIFICATION_ID_KEY account:KEY_CHAIN_ACCOUNT];
    
    if([notyId isEqual:[NSNull null]] || notyId == nil) {
        
    } else {
        [SSKeychain deletePasswordForService:KEY_CHAIN_NOTIFICATION_ID_KEY account:KEY_CHAIN_ACCOUNT];
    }
    return [SSKeychain setPassword:deviceId forService:KEY_CHAIN_NOTIFICATION_ID_KEY account:KEY_CHAIN_ACCOUNT error:&error];
}

+ (NSString *) getKeyChainNotificationId {
    NSLog(@"this method is called");
    return [SSKeychain passwordForService:KEY_CHAIN_NOTIFICATION_ID_KEY account:KEY_CHAIN_ACCOUNT];
}

+ (void) setKeyChainWithUserName:(NSString *) userName {
    
    NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    [defaul setObject:userName forKey:KEY_CHAIN_USER_NAME_KEY];
    [defaul synchronize];
}

+ (NSString *) getKeyChainUserName {
    NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    return [defaul stringForKey:KEY_CHAIN_USER_NAME_KEY];
}

+ (void) setKeyChainWithMPin:(NSString *) mPin {
    
    NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    [defaul setObject:mPin forKey:KEY_CHAIN_MPIN_KEY];
    [defaul synchronize];
}

+ (NSString *) getKeyChainMPin {
    NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    return [defaul stringForKey:KEY_CHAIN_MPIN_KEY];
}

+ (BOOL) removeUserSettings {
    
    NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    
    //Reset Badge Count
    [defaul setObject:0 forKey:@"badge"];
    [defaul setObject:@"" forKey:LOCAL_DB_IS_FINGER_ACTIVE];
    [defaul synchronize];
    [Holder sharedHolder].pendingBadgeCount = 0;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [defaul removeObjectForKey:KEY_CHAIN_USER_NAME_KEY];
    [defaul removeObjectForKey:KEY_CHAIN_MPIN_KEY];
    return YES;
}

+ (BOOL) removeAllKeyChain {
    [Util removeUserSettings];
    [SSKeychain deletePasswordForService:KEY_CHAIN_DEVICE_ID_KEY account:KEY_CHAIN_ACCOUNT];
    [SSKeychain deletePasswordForService:KEY_CHAIN_NOTIFICATION_ID_KEY account:KEY_CHAIN_ACCOUNT];
    return YES;
}

+ (BOOL) writeSettings {
    Holder *holder = [Holder sharedHolder];
    NSDictionary *settings = holder.settings;
    //NSLog(@"settings count: %u", [settings count]);
    XMLWriter *xmlWriter = [[XMLWriter alloc] init];
    
    [xmlWriter writeStartDocumentWithEncodingAndVersion:@"UTF-8" version:@"1.0"]; // add document header
    [xmlWriter writeStartElement:@"settings"];  // add root element
    // add element with an attribute and some some text
    NSArray *keys = [settings allKeys];
    NSArray *values = [settings allValues];
    for (int i=0; i<[settings count]; i++) {
        [xmlWriter writeStartElement:@"setting"];
        //NSLog(@"%@-%@",[keys objectAtIndex:i], [values objectAtIndex:i]);
        [xmlWriter writeAttribute:@"name" value:keys[i]];
        [xmlWriter writeAttribute:@"value" value:values[i]];
        [xmlWriter writeEndElement];
    }
    
    [xmlWriter writeEndElement]; // close root element
    [xmlWriter writeEndDocument]; // end document
    
    //NSLog(@"XML data:\n%@", [xmlWriter toString]);
    
    NSError *error;
    NSString *filePath = [[[self class] getDocumentsDirectory] stringByAppendingPathComponent:SETTINGS_FILE];
    [[xmlWriter toString] writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    return YES;
}

+ (BOOL) writeFileList: (NSDictionary *) list {
    //NSLog(@"file upate data: %@", list);
    
    XMLWriter *xmlWriter = [[XMLWriter alloc] init];
    
    [xmlWriter writeStartDocumentWithEncodingAndVersion:@"UTF-8" version:@"1.0"]; // add document header
    [xmlWriter writeStartElement:@"files"];  // add root element
    [xmlWriter writeAttribute:@"ver" value:list[@"ver"]];
    // add element with an attribute and some some text
    NSArray *fileList = list[@"data"];
    for (int i=0; i<[fileList count]; i++) {
        [xmlWriter writeStartElement:@"file"];
        //NSLog(@"%@-%@",[keys objectAtIndex:i], [values objectAtIndex:i]);
        [xmlWriter writeAttribute:@"name" value:fileList[i][@"name"]];
        [xmlWriter writeAttribute:@"ver" value:fileList[i][@"ver"]];
        [xmlWriter writeEndElement];
    }
    
    [xmlWriter writeEndElement]; // close root element
    [xmlWriter writeEndDocument]; // end document
    
    //NSLog(@"XML data:\n%@", [xmlWriter toString]);
    
    NSError *error;
    NSString *filePath = [[[self class] getDocumentsDirectory] stringByAppendingPathComponent:FILE_VERSION_LIST];
    [[xmlWriter toString] writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    return YES;
}

+ (NSDictionary *) getPage: (NSString *) pageName {
    NSDictionary *menuList = [self getMenu];
    NSDictionary *data = nil;
    
    //NSLog(@"%s - Page Name: %@", __FUNCTION__, pageName);
    
    NSMutableArray *pages = menuList[@"pages"][@"page"];
    NSDictionary *page;
    
    for (int i=0; i<[pages count]; i++) {
        page = pages[i];
        if ([page[@"name"] isEqual:pageName]) {
            data = page;
            break;
        }
    }
    
    return data;
}

+ (NSDictionary *) getCSS {
    
    return [Holder sharedHolder].css;
}

+ (NSDictionary *) getPageCSS: (NSString *) pageName {
    NSDictionary *css = [self getCSS];
    NSDictionary *data = nil;
    
    //NSLog(@"%s - Page Name: %@\n", __FUNCTION__, pageName);
    
    NSMutableArray *pages = css[@"css"][@"pages"][@"page"];
    NSDictionary *page;
    
    for (int i=0; i<[pages count]; i++) {
        page = pages[i];
        if ([page[@"name"] isEqual:pageName]) {
            data = page;
            break;
        }
    }
    
    return data;
}

+ (NSDictionary *) getButtonCSS: (NSString *) buttonName {
    NSDictionary *css = [self getCSS];
    NSDictionary *data = nil;
    
    //NSLog(@"%s - Page Name: %@\n", __FUNCTION__, pageName);
    
    NSMutableArray *pages = css[@"css"][@"buttons"][@"button"];
    NSDictionary *page;
    
    for (int i=0; i<[pages count]; i++) {
        page = pages[i];
        if ([page[@"name"] isEqual:buttonName]) {
            data = page;
            break;
        }
    }
    
    return data;
}

+ (NSDictionary *) getLabelCSS: (NSString *) labelName {
    NSDictionary *css = [self getCSS];
    NSDictionary *data = nil;
    
    //NSLog(@"%s - Page Name: %@\n", __FUNCTION__, pageName);
    
    NSMutableArray *pages = css[@"css"][@"labels"][@"label"];
    NSDictionary *page;
    
    for (int i=0; i<[pages count]; i++) {
        page = pages[i];
        if ([page[@"name"] isEqual:labelName]) {
            data = page;
            break;
        }
    }
    
    return data;
}

+ (UIFont *) fontWithName: (NSString *)name style: (NSString *)style size: (NSString *) size {
    NSMutableString *font = [[NSMutableString alloc] init];
    [font appendString: name];
    
    if ([style isEqualToString:@"B"]) {
        [font appendString:@"-Bold"];
    } else if ([style isEqualToString:@"I"]) {
        [font appendString:@"-Italic"];
    } else if ([style isEqualToString:@"B|I"]) {
        [font appendString:@"-BoldItalic"];
    } else {
        [font appendString:@"-Roman"];
    }
    //NSLog(@"Font details: name: %@ style: %@ size: %@ - font: %@", name, style, size, font);
    return [UIFont fontWithName:font size:[size floatValue]];
}

+ (UIColor*)colorWithHexString:(NSString*)hex alpha:(NSString *)alpha {
    
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+ (void) initLabel: (UILabel *) label withTitle: (NSString *) title AndCSS: (NSDictionary *) css {
    
    NSString *fontName = css[@"font-name"];
    NSString *fontStyle = css[@"font-style"];
    NSString *fontSize = css[@"font-size"];
    NSString *fontColor = css[@"font-color"];
    NSString *bounds = css[CSS_LABEL_TEXT_BOUNDS];
    NSArray *bound = [bounds componentsSeparatedByString:@"|"];
    
    [label setFont:[Util fontWithName:fontName style:fontStyle size:fontSize]];
    label.textColor = [Util colorWithHexString:fontColor alpha:@"1.0f"];
    label.frame     = CGRectMake([bound[0] floatValue], [bound[1] floatValue], [bound[2] floatValue], [bound[3] floatValue]);
    title           = [title stringByReplacingOccurrencesOfString:@"<BR>" withString:@"\n"];
    label.text      = title;
    [label setNumberOfLines:0];
    [label sizeToFit];
}

+ (void) openHelp: (UIViewController *) view {
    
    @try {
        NSLog(@"Help Button pressed");
        NSString *nextPage = @"help";
        
        UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        WebViewController *webViewCtlr  = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"webview"];
        webViewCtlr.page    = nextPage;
        webViewCtlr.data    = [Util getPage:nextPage];
        webViewCtlr.css     = [Util getPageCSS:nextPage];
        
        NSString *url = (webViewCtlr.data)[@"url"];
        if ([url hasPrefix:@"e:/"]) {
            url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [url substringFromIndex:2]];
            webViewCtlr.urlType = @"e:/";
        } else if ([url hasPrefix:@"l:/"]) {
            url = [url substringFromIndex:3];
            NSString *path = [NSString stringWithFormat:@"%@/%@", [Util getDocumentsDirectory], url];
            url = path;
            webViewCtlr.urlType = @"l:/";
        }
        NSString *params = (webViewCtlr.data)[@"params"];
        //NSLog(@"params: %@", params);
        if (params != (id)[NSNull null] && params.length!=0) {
            url = [NSString stringWithFormat:@"%@%@%@", url , @"?", params];
        }
        //NSLog(@"url test: %@", url);
        webViewCtlr.webURL = url;
        //[view.navigationController pushViewController:webViewCtlr animated:YES];
        [Util pushWithFadeEffectOnViewController:view ViewController:webViewCtlr];
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem opening help page.";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
}

+ (UIImageView *) getLoaderImageWithTarget: (id)target Action:(SEL)method {
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:target action:method];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    UIImage *loaderImage = [[UIImage alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, @"loader1.png"]];
    UIImageView *loader = [[UIImageView alloc] initWithImage:loaderImage];
    loader.animationImages = @[[Util getImageAtDocDirWithName:@"loader1.png"],
                              [Util getImageAtDocDirWithName:@"loader2.png"],
                              [Util getImageAtDocDirWithName:@"loader3.png"],
                              [Util getImageAtDocDirWithName:@"loader4.png"]];
    loader.animationDuration = 0.8;
    [loader startAnimating];
    [loader setUserInteractionEnabled:YES];
    [loader addGestureRecognizer:singleTap];
    return loader;
}

+ (UIImage *) getImageAtDocDirWithName:(NSString *) name {
    NSArray *component = [name componentsSeparatedByString: @"."];
    if ([UIScreen mainScreen].scale == 2.0) {
        name = [NSString stringWithFormat:@"%@@2x.%@", component[0], component[1]];
    }
    //NSLog(@"Path: %@", [NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, name]);
    return [[UIImage alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, name]];
}

+ (NSMutableArray *) menuAuthorization:(NSMutableArray *) menuList {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    //NSLog(@"Menu Authorization Checking.");
    NSDictionary *menuAccessCode = ([Holder sharedHolder].loginInfo)[@"menu_ar"];
    
    //NSLog(@"menu auth code: %@", menuAccessCode);
    NSString *mac = nil;
    //NSLog(@"Data count %u", [data count]);
    for (int i=0; i<[menuList count]; i++) {
        mac = menuList[i][@"mac"];
//        if([mac isEqualToString:@"ESOP"]) {
//            
//        } else {
            if ([menuAccessCode valueForKey:mac] == nil) {
                [menuList removeObjectAtIndex:i];
                i--;
                //NSLog(@"Not Found: %@", mac);
            }
        /*} else {
              NSLog(@"data mac: %@, access code: %@", mac, [menuAccessCode objectForKey:mac]);
              }*/
    }
    //NSLog(@"Menu Authorization checked");
    return menuList;
}

+ (void) openMenuWithNextPage:(NSString *) nextPage OnView:(UIViewController *) view {
    
    @try {
        //NSLog(@"name: %@", nextPage);
        NSMutableDictionary *nextPageContent = (NSMutableDictionary *) [Util getPage:nextPage];
        //NSLog(@"content: %@", nextPageContent);
        
        UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        ListViewCtlr *listViewCtlr  = (ListViewCtlr *) [storyboard instantiateViewControllerWithIdentifier:@"listview"];
        listViewCtlr.page           = nextPage;
        listViewCtlr.content        = nextPageContent;
        
        if ([view isKindOfClass:[ListViewCtlr class]] || [view isKindOfClass:[CarouselViewCtlr class]]) {
            //[view.navigationController pushViewController:listViewCtlr animated:YES];
            [Util pushWithFadeEffectOnViewController:view ViewController:listViewCtlr];
        } else if ([view isKindOfClass:[MenuListViewCtlr class]]) {
            IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
            [viewCtlr toggleLeftView];
            //viewCtlr.panningMode = IIViewDeckFullViewPanning;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            //[nav pushViewController:listViewCtlr animated:YES];
            [Util pushWithFadeEffectOnNavigationController:nav ViewController:listViewCtlr];
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem Opening List from local data";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
}

+ (void) openMenuAsGridWithNextPage:(NSString *) nextPage Data:(NSMutableDictionary *) nextPageData OnView:(UIViewController *) view {
    
    @try {
        UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        GridViewCtlr *gridViewCtlr  = (GridViewCtlr *) [storyboard instantiateViewControllerWithIdentifier:@"gridview"];
        gridViewCtlr.nextPage       = nextPage;
        gridViewCtlr.content        = nextPageData;
        
        if ([view isKindOfClass:[ListViewCtlr class]] || [view isKindOfClass:[CarouselViewCtlr class]]) {
            //[view.navigationController pushViewController:gridViewCtlr animated:YES];
            [Util pushWithFadeEffectOnViewController:view ViewController:gridViewCtlr];
        } else if ([view isKindOfClass:[MenuListViewCtlr class]]) {
            IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
            [viewCtlr toggleLeftView];
            //viewCtlr.panningMode = IIViewDeckFullViewPanning;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            //[nav pushViewController:gridViewCtlr animated:YES];
            [Util pushWithFadeEffectOnNavigationController:nav ViewController:gridViewCtlr];
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem Opening Grid view";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
    
}

+ (NSString *) openDynaListWithNextPage:(NSString *) nextPage AfterFinishCall:(SEL) callAfterFinish OnView:(UIViewController *) view {
    
    NSString *jsonURL;
    NSDictionary *nextPageData = [Util getPage:nextPage];
    
    jsonURL = nextPageData[@"url"];
    if ([jsonURL hasPrefix:@"e:/"]) {
        jsonURL = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [jsonURL substringFromIndex:2], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
        //NSLog(@"url test: %@", url);
    } else if ([jsonURL hasPrefix:@"l:/"]) {
        jsonURL = [jsonURL substringFromIndex:3];
        NSString *path = [NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], @"www/", jsonURL];
        // Need to be double-slashes to work correctly with UIWebView, so change all "/" to "//"
        path = [path stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
        // Also need to replace all spaces with "%20"
        path = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        //And make a proper URL
        jsonURL = [NSURL URLWithString:[NSString stringWithFormat:@"file:/%@",path]].absoluteString;
    }
    
    NSLog(@"URL: %@", jsonURL);
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:callAfterFinish onObject:view];
    [conn requestDynalistDataWithURL:jsonURL];
    
    return jsonURL;
}

+ (void) refreshDynaListWithURL:(NSString *)url AfterFinishCall:(SEL)callAfterFinish OnView:(UIViewController *)view {
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:callAfterFinish onObject:view];
    [conn requestDynalistDataWithURL:url];
}

+ (NSString *) getNotificationURL {
    
    NSString *notificationPage = @"notification";
    NSDictionary *nextContent = [Util getPage:notificationPage];
    
    NSString *url = nextContent[@"url"];
    if ([url hasPrefix:@"e:/"]) {
        url = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [url substringFromIndex:2], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
        NSLog(@"url test: %@", url);
    } else if ([url hasPrefix:@"l:/"]) {
        url = [url substringFromIndex:3];
        NSString *path = [NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], @"www/", url];
        // Need to be double-slashes to work correctly with UIWebView, so change all "/" to "//"
        path = [path stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
        // Also need to replace all spaces with "%20"
        path = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        //And make a proper URL
        url = [NSURL URLWithString:[NSString stringWithFormat:@"file:/%@",path]].absoluteString;
    }
    return url;
}

+ (void) openNotificationWithURL:(NSString *)url AfterFinishCall:(SEL)callAfterFinish OnView:(UIViewController *)view {
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:callAfterFinish onObject:view];
    [conn requestDynalistDataWithURL:url];
}

+ (void) finishLoadingDynaListWithata:(NSDictionary *) resData
                             NextPage:(NSString *) nextPage
                              JSONURL:(NSString *) url
                               OnView:(UIViewController *) view {
    @try {
        //NSLog(@"Response data: %@", resData);
        NSDictionary *nextPageContent = [Util getPage:nextPage];
        
        UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        ListViewCtlr *listViewCtlr  = (ListViewCtlr *) [storyboard instantiateViewControllerWithIdentifier:@"listview"];
        
        listViewCtlr.page = nextPage;
        listViewCtlr.jsonURL = url;
        //NSLog(@"page type: %@ content: %@", nextPage, nextPageContent);
        listViewCtlr.content = (NSMutableDictionary *) nextPageContent;
        listViewCtlr.jsonData = resData;
        
        //[view.navigationController pushViewController:listViewCtlr animated:YES];
        [Util pushWithFadeEffectOnViewController:view ViewController:listViewCtlr];
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem Opening list with Dynamic data";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
}

+ (void) OpenWebWithNextPage:(NSString *) nextPage AndParam:(NSString *) params OnView:(UIViewController *) view {
    
    @try {
        //NSLog(@"%s nextPage: %@", __FUNCTION__, nextPage);
        NSDictionary *nextPageData = [Util getPage:nextPage];
        UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        WebViewController *webViewCtlr  = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"webview"];
        webViewCtlr.page    = nextPage;
        webViewCtlr.data    = nextPageData;
        webViewCtlr.css     = [Util getPageCSS:nextPage];
        
        NSString *url = (webViewCtlr.data)[@"url"];
        if ([url hasPrefix:@"e:/"]) {
            url = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [url substringFromIndex:2], J_SESSION_ID, @"12356"];
            webViewCtlr.urlType = @"e:/";
        } else if ([url hasPrefix:@"l:/"]) {
            url = [url substringFromIndex:3];
            NSString *path = [NSString stringWithFormat:@"%@/%@", [Util getDocumentsDirectory], url];
            // Need to be double-slashes to work correctly with UIWebView, so change all "/" to "//"
            //path = [path stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
            // Also need to replace all spaces with "%20"
            //path = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            //And make a proper URL
            //url = [NSURL URLWithString:[NSString stringWithFormat:@"file:/%@",path]];
            url = path;
            webViewCtlr.urlType = @"l:/";
        }
        //NSString *params = [[data objectAtIndex:indexPath.row] objectForKey:@"params"];
        //NSLog(@"params: %@ \n null check: %@ length: %lu", params, (params != (id)[NSNull null]) ? @"true": @"false", (unsigned long)params.length);
        if (params != (id)[NSNull null] && params.length!=0) {
            url = [NSString stringWithFormat:@"%@%@%@", url , @"?", params];
            NSLog(@"url test: %@", url);
        }
        webViewCtlr.webURL = url;
        if ([view isKindOfClass:[ListViewCtlr class]] || [view isKindOfClass:[CarouselViewCtlr class]] || [view isKindOfClass:[GridViewCtlr class]]) {
            //[view.navigationController pushViewController:webViewCtlr animated:YES];
            [Util pushWithFadeEffectOnViewController:view ViewController:webViewCtlr];
        } else if ([view isKindOfClass:[MenuListViewCtlr class]]) {
            IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
            [viewCtlr toggleLeftView];
            //viewCtlr.panningMode = IIViewDeckFullViewPanning;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            //[nav pushViewController:webViewCtlr animated:YES];
            [Util pushWithFadeEffectOnNavigationController:nav ViewController:webViewCtlr];
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem Opening webpage";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
}

+ (void) OpenWebPageAfterDataLoadWithNextPage:(NSString *) nextPage AndParam:(NSString *) params OnView:(UIViewController *) view {
    
    NSDictionary *nextPageData = [Util getPage:nextPage];
    UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WebViewController *webViewCtlr  = (WebViewController *) [storyboard instantiateViewControllerWithIdentifier:@"webview"];
    webViewCtlr.page    = nextPage;
    webViewCtlr.data    = nextPageData;
    webViewCtlr.css     = [Util getPageCSS:nextPage];
    
    NSString *url = (webViewCtlr.data)[@"url"];
    if ([url hasPrefix:@"e:/"]) {
        url = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [url substringFromIndex:2], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
        webViewCtlr.urlType = @"e:/";
    } else if ([url hasPrefix:@"l:/"]) {
        url = [url substringFromIndex:3];
        NSString *path = [NSString stringWithFormat:@"%@/www/%@", [Util getDocumentsDirectory], url];
        // Need to be double-slashes to work correctly with UIWebView, so change all "/" to "//"
        //path = [path stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
        // Also need to replace all spaces with "%20"
        //path = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        //And make a proper URL
        //url = [NSURL URLWithString:[NSString stringWithFormat:@"file:/%@",path]];
        url = path;
        webViewCtlr.urlType = @"l:/";
    }
    //NSString *params = [[data objectAtIndex:indexPath.row] objectForKey:@"params"];
    //NSLog(@"params: %@", params);
    if ((params != (id)[NSNull null] && params.length!=0)) {
        url = [NSString stringWithFormat:@"%@%@%@", url , @"?", params];
    }
    //NSLog(@"url test: %@", url);
    webViewCtlr.webURL = url;
    if ([view isKindOfClass:[ListViewCtlr class]] || [view isKindOfClass:[CarouselViewCtlr class]] || [view isKindOfClass:[GridViewCtlr class]]) {
        //[view.navigationController pushViewController:webViewCtlr animated:YES];
        [Util pushWithFadeEffectOnViewController:view ViewController:webViewCtlr];
    } else if ([view isKindOfClass:[MenuListViewCtlr class]]) {
        IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
        [viewCtlr toggleLeftView];
        //viewCtlr.panningMode = IIViewDeckFullViewPanning;
        UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
        //[nav pushViewController:webViewCtlr animated:YES];
        [Util pushWithFadeEffectOnNavigationController:nav ViewController:webViewCtlr];
    }
}

+ (void) getWebPageDataForWebPage:(NSString *) pageName AfterfinishCall:(SEL) callAfterFinish OnViewController:(UIViewController *) controller {
    
    NSDictionary *pageData = [Util getPage:pageName];
    if ([pageData[@"server-data"] isEqualToString:PAGE_TRUE]) {
        
        NSString *url = pageData[@"url"];
        url = [url substringFromIndex:3];
        AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:callAfterFinish onObject:controller];
        [conn requestWebPageDataForURL:url];
    }
}

+ (void) openCarouselSelectionOnView:(UINavigationController *) navCtlr {
    
    @try {
        NSMutableDictionary *content = (NSMutableDictionary *) [Util getPage:PAGE_MAIN_MENU];
        UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CarouselViewCtlr *carouselCtlr  = (CarouselViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carouselview"];
        carouselCtlr.page               = PAGE_MAIN_MENU;
        carouselCtlr.content            = content;
        
        [Util pushWithFadeEffectOnNavigationController:navCtlr ViewController:carouselCtlr];
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem Opening Carousel Selection";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
}

+ (void) openEngagementSelection:(NSString *) nextPage OnView:(UINavigationController *) navCtlr {
    
    @try {
        if ([nextPage isEqualToString:@"home-offer"]) {
//            if ([Holder sharedHolder].offerHome!=nil) {
//                NSLog(@"Home Offer instance already exits");
//                [Util pushWithFadeEffectOnNavigationController:navCtlr ViewController:[Holder sharedHolder].offerHome];
//            } else {
//                NSLog(@"Home Offer does NOT already exits");
                UIStoryboard *storyboard                = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                HomeMapViewCtlr *mapViewCtlr            = (HomeMapViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"HomeMapView"];
                //[navCtlr pushViewController:mapViewCtlr animated:YES];
                [Holder sharedHolder].offerHome         = mapViewCtlr;
                [Util pushWithFadeEffectOnNavigationController:navCtlr ViewController:mapViewCtlr];
//            }
        } else if ([nextPage isEqualToString:@"car-offer"]) {
            UIStoryboard *storyboard                = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            BrandCollViewCtlr *brandViewCtlr        = (BrandCollViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carbrandView"];
            //[navCtlr pushViewController:brandViewCtlr animated:YES];
            [Util pushWithFadeEffectOnNavigationController:navCtlr ViewController:brandViewCtlr];
        } else if ([nextPage isEqualToString:@"other-offer"]) {
            UIStoryboard *storyboard                = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            OtherOfferListViewCtlr *otherOfferList  = (OtherOfferListViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"otherofferlist"];
            //[navCtlr pushViewController:otherOfferList animated:YES];
            [Util pushWithFadeEffectOnNavigationController:navCtlr ViewController:otherOfferList];
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Problem Opening main offer view";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        [Util showTechnicalError];
    }
}

+ (NSString *) convertDateFromString:(NSString *) strDate WithType:(NSString *) conversionType {
    
    NSDateFormatter *srcFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *destFormatter = [[NSDateFormatter alloc] init];
    
    if ([conversionType isEqualToString:MSSQL_TO_NSDATE_DATE_CONVERSION]) {
        
        [srcFormatter setDateFormat:MSSQL_DATE_FORMAT];
        [destFormatter setDateFormat:NSDATE_FORMAT];
    }
    
    return [destFormatter stringFromDate:[srcFormatter dateFromString:strDate]];
}

+ (NSString *) encodeSpecialCharacter:(NSString *) string {
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}

+ (BOOL) isNull:(NSObject *) object {
    
    return (object == nil || [object isEqual:[NSNull null]]);
}

+ (BOOL) isKey:(NSString *) key ExistForDictionary:(NSDictionary *) dictionary {
    
    return [[dictionary allKeys] containsObject:key];
}

+ (void) showTechnicalError {
    UIAlertView *techAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                        message:[Util getSettingUsingName:TECHNICAL_ERROR]
                                                       delegate:nil
                                              cancelButtonTitle:ALERT_OPT_OK
                                              otherButtonTitles:nil];
    [techAlert show];
}

- (void) showVerifyViewControllerOnController:(UIViewController *)controller Model:(NSObject *)model Index:(NSInteger)index {
    
    UIStoryboard *storybaord = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    VerifyMPinViewCtlr *verifyCtlr = (VerifyMPinViewCtlr *) [storybaord instantiateViewControllerWithIdentifier:@"verifympin"];
    verifyCtlr.index = index;
    verifyCtlr.model = model;
    
    if([controller isKindOfClass:[GridViewCtlr class]]) {
        verifyCtlr.controller = (GridViewCtlr *) controller;
    } else {
        UINavigationController *viewController = (UINavigationController *) controller;
        NSArray *viewControllers = viewController.viewControllers;
        for (UIViewController *ctlr in viewControllers) {
            if([ctlr isKindOfClass:[CarouselViewCtlr class]]) {
                verifyCtlr.controller = (CarouselViewCtlr *) ctlr;
            }
        }
    }
    
    // New Code
    //controller.definesPresentationContext = YES;
    verifyCtlr.view.backgroundColor = [UIColor clearColor];
    verifyCtlr.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [controller presentViewController:verifyCtlr animated:YES completion:nil];
    
    // Old Code
    /*UIView *backgroundView = [[UIView alloc] initWithFrame:controller.view.frame];
     backgroundView.backgroundColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:0.6f];
     //controller.backgroundView = backgroundView;
     
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideVerifyCtlr)];
     [tapRecognizer setNumberOfTapsRequired:1];
     [tapRecognizer setDelegate:self];
     [backgroundView addGestureRecognizer:tapRecognizer];
     
     CGFloat bviewHeight = backgroundView.frame.size.height;
     CGFloat viewHeight = 480;
     
     //    CGFloat viewY = (bviewHeight - viewHeight)/2;
     
     verifyCtlr.view.frame = CGRectMake(0, 0, 320, bviewHeight);
     CGRect viewFrame = verifyCtlr.view.frame;
     viewFrame.size.height = viewHeight;
     verifyCtlr.view.frame = viewFrame;
     
     CGRect frame = backgroundView.frame;
     frame.origin.y = 0;
     frame.size.height = viewHeight;
     backgroundView.frame = frame;
     NSLog(@"frame : %f %f",backgroundView.frame.size.height,verifyCtlr.view.frame.size.height);
     
     [controller addChildViewController:verifyCtlr];
     [backgroundView addSubview:verifyCtlr.view];
     //[controller.view addSubview:backgroundView]; // Faded background
     [controller.view addSubview:verifyCtlr.view];
     [verifyCtlr didMoveToParentViewController:controller];*/
}

- (void) hideVerifyCtlrWithView:(UIViewController *)controller {
    //    [AApp singletonInstance].createMPinVisible = @(NO)
    //[[AApp singletonInstance].backgroundView removeFromSuperview];
    
    [controller dismissViewControllerAnimated:YES completion:nil];

    /*
    [verifyCtlr willMoveToParentViewController:nil];  // 1
    [verifyCtlr.view removeFromSuperview];            // 2
    [verifyCtlr removeFromParentViewController];      // 3*/
    
}

+ (NSData *) doCipher:(NSString *)strData context:(CCOperation)encryptOrDecrypt error:(NSError **)error {
    
    NSData* dataIn = [strData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* strIV = @"wimak8730J39IUKs";
    NSData* iv = [strIV dataUsingEncoding:NSUTF8StringEncoding];
    NSString* strKey= @"9JWHKsks74Hsu43G";
    NSData* symmetricKey=[strKey dataUsingEncoding:NSUTF8StringEncoding];
    
    CCCryptorStatus ccStatus   = kCCSuccess;
    size_t          cryptBytes = 0;    // Number of bytes moved to buffer.
    NSMutableData  *dataOut    = [NSMutableData dataWithLength:dataIn.length + kCCBlockSizeAES128];
    
    ccStatus = CCCrypt( encryptOrDecrypt,
                       kCCAlgorithmAES128,
                       kCCOptionPKCS7Padding,
                       symmetricKey.bytes,
                       kCCKeySizeAES128,
                       iv.bytes,
                       dataIn.bytes,
                       dataIn.length,
                       dataOut.mutableBytes,
                       dataOut.length,
                       &cryptBytes);
    
    if (ccStatus == kCCSuccess) {
        dataOut.length = cryptBytes;
        
    }
    else {
        if (error) {
            *error = [NSError errorWithDomain:@"kEncryptionError"
                                         code:ccStatus
                                     userInfo:nil];
        }
        dataOut = nil;
    }
    
    return dataOut;
}

+ (BOOL) serverResponseCheckNullAndSession:(NSDictionary *) response
                                InFunction:(const char *) function
                                    Silent:(BOOL) silent {
    
    BOOL result = TRUE;
    
    if ([[self class] isNull:response]) {
        // response null
        [ErrorLog logNullResponseInFunciton:function
                                WithMessage:@""];
        if (!silent) {
            UIAlertView *technilcalAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:[Util getSettingUsingName:TECHNICAL_ERROR]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
            [technilcalAlert show];
        }
        
        result = FALSE;
    } else {
        if ([[self class] isKey:SERVER_RES_STATUS ExistForDictionary:response]) {
            if ([Util isNull:response[SERVER_RES_STATUS]]) {
                //TODO response parameter sts null
                [ErrorLog logNullResponseInFunciton:function
                                        WithMessage:@"sts parameter"];
                if (!silent) {
                    UIAlertView *technilcalAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                              message:[Util getSettingUsingName:TECHNICAL_ERROR]
                                                                             delegate:nil
                                                                    cancelButtonTitle:ALERT_OPT_OK
                                                                    otherButtonTitles:nil];
                    [technilcalAlert show];
                }
            } else {
                if ([response[SERVER_RES_STATUS] intValue] == 500) {
                    //TODO session expired
                    if (!silent) {
                        [[Holder sharedHolder].auth sessionExpired:response];
                    }
                    result = FALSE;
                }
            }
        }
    }
    
    return result;
}

+ (BOOL) serverResponseForField:(NSString *) field
                             In:(NSDictionary *) response
                     InFunction:(const char *) function
                        Message:(NSString *) msg
                         Silent:(BOOL) silent {
    
    BOOL result = TRUE;
    
    if ([[self class] isKey:field ExistForDictionary:response]) {
        
        if ([[self class] isNull:response[field]]) {
            //TODO null field parameter
            [ErrorLog logNullResponseInFunciton:function
                                    WithMessage:msg];
            result = FALSE;
            if (!silent) {
                [Util showTechnicalError];
            }
        }
    } else {
        //TODO field parameter does not exist
        [ErrorLog logNullResponseInFunciton:function
                                WithMessage:msg];
        result = FALSE;
        if (!silent) {
            [Util showTechnicalError];
        }
    }
    
    return result;
}

+ (void) sendErrorLog {
    
    ErrorLog *errorLog = [[ErrorLog alloc] init];
    [errorLog sendErrorLog];
}

+ (NSString *) handleNewLine:(NSString *) string {
    
    NSMutableString *strNewLine = [string mutableCopy];
    [strNewLine replaceOccurrencesOfString:@"/r/n" withString:@"\n" options:NSLiteralSearch range:NSMakeRange(0, [strNewLine length])];
    [strNewLine replaceOccurrencesOfString:@"\\r\\n" withString:@"\n" options:NSLiteralSearch range:NSMakeRange(0, [strNewLine length])];
    [strNewLine replaceOccurrencesOfString:@"<BR>" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [strNewLine length])];
    [strNewLine replaceOccurrencesOfString:@"<BR/>" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [strNewLine length])];
    
    return [strNewLine copy];
}

+ (CGSize) getLableHeightForText:(NSString *) title
                         forFont:(UIFont*) fontName
{
    CGSize size;
    if ([title respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
        
        
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentLeft;
        
        NSDictionary * attributes = @{
                                      NSFontAttributeName : fontName,
                                      NSParagraphStyleAttributeName : paragraphStyle
                                      };
        //CGFLOAT_MAX
        size = [title boundingRectWithSize:CGSizeMake(200.0f, CGFLOAT_MAX)
                                   options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin
                                attributes:attributes
                                   context:nil].size;
    } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [title sizeWithFont:fontName
                 constrainedToSize:CGSizeMake(200.0f, CGFLOAT_MAX)
                     lineBreakMode:NSLineBreakByWordWrapping];
#pragma clang diagnostic pop
    }
    return size;
}

@end
