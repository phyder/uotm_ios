//
//  ResourceCopy.h
//  Home Offer
//
//  Created by Suraj on 30/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Util.h"

@interface ResourceCheck : NSObject {
    
}

+ (void) checkResourceCopy;

+ (void) copyAssets;

+ (void) copyResourceWithSourceDirectory: (NSString *) sourceDirectory toDestinationDirectory: (NSString *) destinationDirectory;

+ (BOOL) isDirectoryExistAtAbsolutePath:(NSString*)filename;

@end
