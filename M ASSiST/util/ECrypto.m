//
//  ECrypto.m
//  RBS Banking
//
//  Created by Suraj on 27/02/14.
//  Copyright (c) 2014 Phyder. All rights reserved.
//

#import "ECrypto.h"

#import "NSData+Base64.h"
#import "NSData+Random.h"

#import "BDError.h"
#import "BDRSACryptor.h"
#import "BDAESCBC256Cryptor.h"
#import "BDSHA512Cryptor.h"

static ECrypto* singleton = nil;

@implementation ECrypto

+ (ECrypto *)sharedInstance {
    
	@synchronized([ECrypto class]) {
        static dispatch_once_t oncePredicate;
        
        dispatch_once(&oncePredicate, ^{
            if (!singleton) {
                singleton = [[self alloc] init];
            }
        });
        
		return singleton;
	}
	return nil;
}

#pragma mark - AES
- (void)generateAESKey {
    NSData *data = [NSData random:16];
    _mCryptoAESIV = [[[NSData random:16] base64EncodedString] substringWithRange:NSMakeRange(0, 16)];
    NSString *strKey = [data base64EncodedString];
    _mCryptoAESKey = [strKey substringWithRange:NSMakeRange(0, 16)];
#ifdef DEBUG
    NSLog(@"AES secret key: %@ with data: %@ IV: %@", [strKey substringWithRange:NSMakeRange(0, 16)], data, _mCryptoAESIV);
#endif
}

- (NSString *)encryptAES:(NSString *)plainText {
    BDError *error = [[BDError alloc] init];
    BDAESCryptor *AESCryptor = [[BDAESCBC256Cryptor alloc] init];
    
    _mCryptoAESIV = @"wimak8730J39IUKs";
    _mCryptoAESKey = @"9JWHKsks74Hsu43G";
    
    NSString *cipher = [AESCryptor encrypt:plainText
                                       key:_mCryptoAESKey
                                        iv:[_mCryptoAESIV dataUsingEncoding:NSUTF8StringEncoding]
                                     error:error];
    if (error.errors.count != 0) {
        // Error occoured during the generation
        return nil;
    }
    return cipher;
}

- (NSString *)decryptAES:(NSString *)cipher {
    BDError *error = [[BDError alloc] init];
    BDAESCryptor *AESCryptor = [[BDAESCBC256Cryptor alloc] init];
    
    _mCryptoAESIV = @"wimak8730J39IUKs";
    _mCryptoAESKey = @"9JWHKsks74Hsu43G";
    
    NSString *decryptedText = [AESCryptor decrypt:cipher
                                              key:_mCryptoAESKey
                                               iv:[_mCryptoAESIV dataUsingEncoding:NSUTF8StringEncoding]
                                            error:error];
    if (error.errors.count != 0) {
        return nil;
    }
    return decryptedText;
}

#pragma mark - RSA
- (NSString *)RSAPublicKey {
    if (_mCryptoRSAKey == nil) {
        NSError *error;
        _mCryptoRSAKey = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"rsa_public_key" ofType:@"pem"]
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];
#if DEBUG
        NSLog(@"\n\nRSA Public Key: %@", _mCryptoRSAKey);
#endif
        if (error) {
            return nil;
        }
    }
    return _mCryptoRSAKey;
}

- (NSString *)encryptRSA:(NSString *)plainText {
    BDError *error = [[BDError alloc] init];
    BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
    NSString *cipher = [RSACryptor encrypt:plainText key:self.RSAPublicKey error:error];
    if (error.errors.count != 0) {
        // Error occoured during the generation
        return nil;
    }
    return cipher;
}


// Currently NO Decryption required
- (NSString *)decryptRSA:(NSString *)cipher {
    BDError *error = [[BDError alloc] init];
    BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
    NSString *decryptedText = [RSACryptor decrypt:cipher key:self.RSAPublicKey error:error];
    if (error.errors.count != 0) {
        // Error occoured during the generation
        return nil;
    }
    return decryptedText;
}

#pragma mark - SHA512
- (NSString *)generateHashSHA512WithData:(NSData *)plainData {
    
    return [self generateHashSHA512WithString:[plainData base64EncodedString]];
}

- (NSString *)generateHashSHA512WithString:(NSString *)plain {
    BDError *error = [[BDError alloc] init];
    BDSHA512Cryptor *sha512 = [[BDSHA512Cryptor alloc] init];
    NSString *hashSHA512 = [sha512 SHA512Hash:plain error:error];
    return hashSHA512;
}

@end
