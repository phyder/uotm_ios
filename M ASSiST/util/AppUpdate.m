//
//  AppUpdate.m
//  Universe on the move
//
//  Created by Suraj on 08/07/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "AppUpdate.h"

#import "Constant.h"
#import "Reachability.h"
#import "Util.h"
#import "ZipArchive.h"

@implementation AppUpdate

- (BOOL) downloadUpdated {
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *myPath    = myPathList[0];
    NSString *strURL    = @"http://localhost/test/www.zip";
    currentDownload     = [myPath stringByAppendingPathComponent:@"www.zip"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strURL]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:1000];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0
                                                                diskCapacity:0
                                                                    diskPath:nil]];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (![self checkInternetConnectivity]) {
        
        NSLog(@"Internet connectivity unable");
    } else {
        
        [connection start];
    }
    
    return true;
}

#pragma mark -
#pragma mark NSURLConnectionDelegate methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)resData {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Attempt to open the file and write the downloaded data to it
    if (![fileManager fileExistsAtPath:currentDownload]) {
        [fileManager createFileAtPath:currentDownload contents:nil attributes:nil];
    }
    // Append data to end of file
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:currentDownload];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:resData];
    [fileHandle closeFile];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response {
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    NSError *error;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:[[Util getDocumentsDirectory] stringByAppendingPathComponent:@"test"]
                                   withIntermediateDirectories:YES
                                                    attributes:nil
                                                         error:&error]) {
        NSLog(@"%s Error: Directory could not be created.%@\n", __FUNCTION__, error);
    }
    //UnZip location for update download
    NSString *unZipPath = [NSString stringWithFormat:@"%@/%@", [Util getDocumentsDirectory], @"test"];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    if ([zipArchive UnzipOpenFile:currentDownload]) {
        if ([zipArchive UnzipFileTo:unZipPath  overWrite:YES]) {
            NSLog(@"Archive unzip success");
            [zipArchive UnzipCloseFile];
        } else {
            NSLog(@"Failure to unzip archive");
        }
    } else {
        NSLog(@"Failure to open archive");
    }
}

#pragma mark - Reachability
- (int) checkInternetStatus {
    
    int internetStatus = NO_INTERNET_CONNECTIVITY;
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    BOOL connectionRequired= [internetReach connectionRequired];
    
    if (netStatus==ReachableViaWiFi) {
        internetStatus = INTERNET_CONNECTIVITY_WIFI; // Wifi data network connection available
    } else {
        if(!connectionRequired) {
            internetStatus = INTERNET_CONNECTIVITY_CELLULAR; // Cellular data network connection available
        } else {
            internetStatus = NO_INTERNET_CONNECTIVITY; // No data network connection available
        }
    }
    
    return internetStatus;
}

- (Boolean) checkInternetConnectivity {
    
    Boolean connectivityStatus;
    if ([self checkInternetStatus]!=NO_INTERNET_CONNECTIVITY) {
        connectivityStatus = true;
    } else {
        connectivityStatus = false;
    }
    
    return connectivityStatus;
}

@end
