//
//  Navigation.m
//  Universe on the move
//
//  Created by Suraj on 18/06/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "Navigation.h"

#import "Authentication.h"
#import "AppConnect.h"
#import "Constant.h"
#import "Holder.h"
#import "Util.h"

#import "BrandCollViewCtlr.h"
#import "CarouselViewCtlr.h"
#import "CarOfferListViewCtlr.h"
#import "GridViewCtlr.h"
#import "HomeListViewCtlr.h"
#import "HomeMapViewCtlr.h"
#import "IIViewDeckController.h"
#import "NotificationList.h"
#import "ListViewCtlr.h"
#import "OtherOfferListViewCtlr.h"
#import "OtherDetailPageViewCtlr.h"
#import "LoginViewCtlr.h"
#import "WebViewController.h"
#import "VerifyMPinViewCtlr.h"

@interface Navigation()

@property (nonatomic) VerifyMPinViewCtlr *verifyCtlr;


@end

@implementation Navigation

- (id)initWithViewController:(UIViewController *) viewCtlr PageData:(NSDictionary *) data {
    
    self = [super init];
    if (self) {
        // Initialization code
        self.controller  = viewCtlr;
        pageData    = data;
        pageCSS     = [Util getPageCSS:pageCSS[PAGE_CSS]];
        
        [self initHeader];
        [self initFooter];
    }
    return self;
}

- (void) checkHeaderAndFooter {
    
    if ([pageData[PAGE_HEADER] isEqualToString:PAGE_FALSE]) {
        self.controller.navigationController.navigationBarHidden = NO;
    }
    if ([pageData[PAGE_FOOTER] isEqualToString:PAGE_FALSE]) {
        [self.controller.navigationController setToolbarHidden:YES];
    }
}

- (void) initHeader {
    
    //[[UIBarButtonItem appearance] setTintColor:[Util colorWithHexString:[pageData objectForKey:CSS_PAGE_TINTCOLOR] alpha:@"1.0f"]];
    //controller.navigationController.navigationBar.tintColor = [Util colorWithHexString:[pageCSS objectForKey:CSS_PAGE_TINTCOLOR] alpha:@"1.0f"];
    
    // Create image for navigation background - portrait
    UIImage *NavigationPortraitBackground = [[Util getImageAtDocDirWithName:@"nav-bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    // Create image for navigation background - landscape
    //UIImage *NavigationLandscapeBackground = [[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@", [Util getDocumentsDirectory], LOCAL_IMGAGE_DIR, @"nav-bar.png"]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    // Set the background image all UINavigationBars
    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    [self.controller.navigationController.navigationBar setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
    
    //[[UINavigationBar appearance] setBackgroundImage:NavigationLandscapeBackground forBarMetrics:UIBarMetricsLandscapePhone];
    
    if (![pageData[PAGE_HEADER] isEqualToString:PAGE_TRUE]) {
        self.controller.navigationController.navigationBarHidden = YES;
    } else {
        self.controller.navigationController.navigationBarHidden = NO;
        
        [self initNavTitle];
        [self initTopLeftButton];
        [self initTopRightButton];
    }
}

- (void) initTopLeftButton {
    
    NSMutableArray *topLeftButton = [[NSMutableArray alloc] init];
    
    if ([pageData[PAGE_TOP_LEFT_MENU_BUTTON] isEqualToString:PAGE_TRUE]
        && [[Holder sharedHolder].isLoggedIn isEqualToString:PAGE_TRUE]) {
        
        btnCSS = [Util getButtonCSS:pageData[PAGE_TOP_LEFT_MENU_BUTTON_CSS]];
        //NSLog(@"button css: %@", cssPage);
        
        UIImage *imgTopbarLeft = [Util getImageAtDocDirWithName:btnCSS[@"img"]];
        //create the button and assign the image
        UIButton *btnTopbarLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnTopbarLeft setImage:imgTopbarLeft forState:UIControlStateNormal];
        //sets the frame of the button to the size of the image
        btnTopbarLeft.frame = CGRectMake(0, 0, imgTopbarLeft.size.width, imgTopbarLeft.size.height);
        //defining the action
        [btnTopbarLeft addTarget:self action:@selector(btnTopBarLeftButtonOnePressed) forControlEvents:UIControlEventTouchUpInside];
        //creates a UIBarButtonItem with the button as a custom view
        UIBarButtonItem *btnHomeBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnTopbarLeft];
        [topLeftButton addObject:btnHomeBarButton];
    }
    
    if ([pageData[PAGE_TOP_LEFT_BUTTON] isEqualToString:PAGE_TRUE]) {
        
        btnCSS = [Util getButtonCSS:pageData[PAGE_TOP_LEFT_BUTTON_CSS]];
        
        UIImage *imgTopbarLeft      = [Util getImageAtDocDirWithName:btnCSS[@"img"]];
        //create the button and assign the image
        UIButton *btnTopbarLeft     = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnTopbarLeft setImage:imgTopbarLeft forState:UIControlStateNormal];
        //sets the frame of the button to the size of the image
        btnTopbarLeft.frame         = CGRectMake(0, 0, imgTopbarLeft.size.width, imgTopbarLeft.size.height);
        //defining the action
        [btnTopbarLeft addTarget:self action:@selector(btnTopBarLeftButtonTwoPressed) forControlEvents:UIControlEventTouchUpInside];
        //creates a UIBarButtonItem with the button as a custom view
        UIBarButtonItem *btnTopbarLeftBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnTopbarLeft];
        [topLeftButton addObject:btnTopbarLeftBarButton];
    }
    //controller.navigationItem.leftBarButtonItem = btnTopbarLeftBarButton;
    self.controller.navigationItem.leftBarButtonItems = topLeftButton;
    topLeftButton = nil;
}

- (void) btnTopBarLeftButtonOnePressed {
#ifdef DEBUGX
    NSLog(@"%s Top Left Button one pressed.", __FUNCTION__);
#endif
    if ([self.controller isKindOfClass:[LoginViewCtlr class]]) {
        // No action for Top Left Button on Login page
    } else {
        /**
         * All View Except Login page have Navigation Drarwe (slider)
         */
        [((IIViewDeckController *)[Holder sharedHolder].viewController) toggleLeftView];
    }
}

- (void) btnTopBarLeftButtonTwoPressed {
#ifdef DEBUGX
    NSLog(@"%s Top Left Button one pressed.", __FUNCTION__);
#endif
    if ([self.controller isKindOfClass:[LoginViewCtlr class]]) {
        // No action for Top Left Button on Login page
    } else if ([self.controller isKindOfClass:[CarouselViewCtlr class]]) {
        // Sign Out
        [[Holder sharedHolder].auth requestSignOutWithMenuToggleCheck:NO];
        //[((CarouselViewCtlr *) controller) signOut];
    } else if ([self.controller isKindOfClass:[WebViewController class]]) {
        [((WebViewController *) self.controller) webViewGoBack];
    } else {
        // Back icon
        [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:NO];
    }
}

- (void) initNavTitle {
    
    [self setHeaderTitle:pageData[PAGE_HEADER_TITLE]];
}

- (void) setHeaderTitle:(NSString *) title {
    
    int size = 17, titleLength = (int) [title length];
    
    if (titleLength > 24) {
        size = 10;
    } else if (titleLength > 21) {
        size = 11;
    } else if (titleLength > 17) {
        size = 13;
    } else if (titleLength > 15) {
        size = 15;
    }
    
//    NSLog(@"\n\nHeader Title: %@\n\n", title);
    
    UILabel *lblTopBarTitle = (UILabel *) self.controller.navigationItem.titleView;
    if (!lblTopBarTitle) {
        lblTopBarTitle = [[UILabel alloc] initWithFrame:CGRectZero];
        lblTopBarTitle.backgroundColor = [UIColor clearColor];
        lblTopBarTitle.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        lblTopBarTitle.textColor = [UIColor whiteColor];
        lblTopBarTitle.text = title;
        self.controller.navigationItem.titleView = lblTopBarTitle;
    } else {
        lblTopBarTitle.text = title;
    }
    lblTopBarTitle.font = [UIFont fontWithName:APP_FONT_BOLD size:size];
    [lblTopBarTitle setLineBreakMode:NSLineBreakByWordWrapping];
    [lblTopBarTitle setNumberOfLines:0];
    [lblTopBarTitle sizeToFit];
}

- (void) initTopRightButton {
    
    if ([pageData[PAGE_TOP_RIGHT_BUTTON] isEqual:PAGE_TRUE]) {
        UIBarButtonItem *btnTopbarRightBarButton = nil;
        btnCSS = [Util getButtonCSS:pageData[PAGE_TOP_RIGHT_BUTTON_CSS]];
        
        if ([self.controller isKindOfClass:[LoginViewCtlr class]]) {
            
            UIImage *imgTopbarRight = [Util getImageAtDocDirWithName:btnCSS[@"img"]];
            UIButton *btnTopbarRight = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnTopbarRight setImage:imgTopbarRight forState:UIControlStateNormal];
            btnTopbarRight.frame = CGRectMake(0, 0, imgTopbarRight.size.width, imgTopbarRight.size.height);
            [btnTopbarRight addTarget:self action:@selector(btnTopBarRightOneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            btnTopbarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnTopbarRight];
        } else if ([[Holder sharedHolder].isLoggedIn isEqualToString:PAGE_TRUE]) {
            
            NSArray *bounds = [btnCSS[@"bg-img-inset"] componentsSeparatedByString:@"|"];
            UIImage *chatImage = [[Util getImageAtDocDirWithName:btnCSS[@"bg-img"]] resizableImageWithCapInsets:UIEdgeInsetsMake([bounds[0] floatValue], [bounds[1] floatValue], [bounds[2] floatValue], [bounds[3] floatValue])];
            UIButton *btnNotification = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnNotification setBackgroundImage:chatImage forState:UIControlStateNormal];
            [btnNotification addTarget:self action:@selector(btnTopBarRightOneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            NSString *strNotiCount = @"0";
            [btnNotification setTitle:strNotiCount forState:UIControlStateNormal];
//            if ([Holder sharedHolder].notificationCount != 0) {
//                strNotiCount = [Holder sharedHolder].notificationCount;
//            }
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [Holder sharedHolder].pendingBadgeCount = [[defaults valueForKey:@"badge"] integerValue];
            
            if ([Holder sharedHolder].pendingBadgeCount != 0) {
                strNotiCount = [NSString stringWithFormat:@"%d",[Holder sharedHolder].pendingBadgeCount];
            }
            
           
            
            
            NSString *strNotification = [NSString stringWithFormat:@"    %@  ", strNotiCount];
//            if ([Holder sharedHolder].testingNoti == true) {
//                [btnNotification setTitle:@"5" forState:UIControlStateNormal];
//                NSLog(@"this is grid 5 called");
//                NSLog(@"this is controller %@",self.controller);
//            }else if ([Holder sharedHolder].testingNoti == false){
//                [btnNotification setTitle:strNotification forState:UIControlStateNormal];
//                NSLog(@"this is grid 3 called");
//                NSLog(@"this is controller %@",self.controller);
//            }
            [btnNotification setTitle:strNotification forState:UIControlStateNormal];
            [btnNotification setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 1.0f, 0.0f)];
            [btnNotification.titleLabel setFont:[Util fontWithName:@"Zurich BT" style:@"" size:@"11.0f"]];
            [btnNotification sizeToFit];
            
//            btnTopbarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:nil];
            btnTopbarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnNotification];
            [btnTopbarRightBarButton setStyle:UIBarButtonItemStyleBordered];
        } else {
            btnTopbarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:nil];
        }
        
        if(self.controller.navigationItem.rightBarButtonItems == nil) {
            self.controller.navigationItem.rightBarButtonItems = @[btnTopbarRightBarButton];
            btnTopbarRightBarButton = nil;
        } else {
            self.controller.navigationItem.rightBarButtonItems = nil;
            self.controller.navigationItem.rightBarButtonItems = @[btnTopbarRightBarButton];
            btnTopbarRightBarButton = nil;
        }
    }
    //if (![[btnCSS objectForKey:@"bg-img"] isEqualToString:@""]) {
    //
    //}
}

- (void) btnTopBarRightOneButtonPressed {
#ifdef DEBUGX
    NSLog(@"%s Top Right Button one pressed.", __FUNCTION__);
#endif
    // Notification is passed via repective controller,
    // since it need to show loader which is bind with respective controller,
    // the call will go via model if it exist.
    if ([self.controller isKindOfClass:[LoginViewCtlr class]]) {
        [((LoginViewCtlr *) self.controller) signIn];
    } else if ([self.controller isKindOfClass:[CarouselViewCtlr class]]) {
        [((CarouselViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[GridViewCtlr class]]) {
        [((GridViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[ListViewCtlr class]]) {
        [((ListViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[WebViewController class]]) {
        [((WebViewController *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[HomeMapViewCtlr class]]) {
        [((HomeMapViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[HomeListViewCtlr class]]) {
        [((HomeListViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[BrandCollViewCtlr class]]) {
        [((BrandCollViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[CarOfferListViewCtlr class]]) {
        [((CarOfferListViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[OtherOfferListViewCtlr class]]) {
        [((OtherOfferListViewCtlr *) self.controller) openNotification];
    } else if ([self.controller isKindOfClass:[OtherDetailPageViewCtlr class]]) {
        [((OtherDetailPageViewCtlr *) self.controller) openNotification];
    }
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist ForURL:(NSString *) notificationURL {
    
    if ([Util serverResponseCheckNullAndSession:resNotificationlist InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"json" In:resNotificationlist InFunction:__FUNCTION__ Message:@"json parameter" Silent:NO]) {
            
            NSLog(@"Response notification data: %@", resNotificationlist);
            NSArray *notificationData = (NSArray *) resNotificationlist[@"json"];
            
            //NSLog(@"Data: %lu", (unsigned long)[notificationData count]);
                if ([notificationData count] > 0) {
                NSString *notificationPage = @"notification";
                NSDictionary *nextContent = [Util getPage:notificationPage];
                //NotificationList *notification = [[NotificationList alloc] initWithStyle:UITableViewStylePlain];
                UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                NotificationList *notification  = (NotificationList *) [storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
                notification.pageData           = nextContent;
                notification.listData           = notificationData;
                notification.url                = notificationURL;
                //[self.navigationController pushViewController:notification animated:YES];
                [Util pushWithFadeEffectOnViewController:self.controller ViewController:notification];
            } else {
                UIAlertView *noData =[[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                message:[Util getSettingUsingName:NOTIFICATION_NODATA_MSG]
                                                               delegate:self
                                                      cancelButtonTitle:ALERT_OPT_CANCEL
                                                      otherButtonTitles:nil];
                [noData show];
            }
        }
    }
}

- (void) initFooter {
    
    //controller.navigationController.toolbar.tintColor = [Util colorWithHexString:[pageCSS objectForKey:CSS_PAGE_TINTCOLOR] alpha:@"1.0f"];
    
    UIImage *NavigationPortraitBackground = [[Util getImageAtDocDirWithName:@"tool-bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UIToolbar appearance] setBackgroundImage:NavigationPortraitBackground forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    [self.controller.navigationController.toolbar setBackgroundImage:NavigationPortraitBackground forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    
    if ([pageData[PAGE_FOOTER] isEqualToString:PAGE_FALSE]) {
        [self.controller.navigationController setToolbarHidden:YES];
    } else {
        [self.controller.navigationController setToolbarHidden:NO];
        
        [self initFooterControl];
    }
}

- (void) initFooterControl {
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if ([pageData[PAGE_BOTTOM_RIGHT_BUTTON] isEqualToString:PAGE_TRUE]) {
        
        //Load the image
        btnCSS = [Util getButtonCSS:pageData[PAGE_BOTTOM_RIGHT_BUTTON_CSS]];
        UIImage *imgBottombarRight = [Util getImageAtDocDirWithName:btnCSS[@"img"]];
        
        //create the button and assign the image
        UIButton *btnBottombarRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBottombarRight setImage:imgBottombarRight forState:UIControlStateNormal];
        btnBottombarRight.frame = CGRectMake(0, 0, imgBottombarRight.size.width, imgBottombarRight.size.height);
        
        //defining the action
        [btnBottombarRight addTarget:self action:@selector(btnBottomBarRightButtonOnePressed) forControlEvents:UIControlEventTouchUpInside];
        
        //creates a UIBarButtonItem with the button as a custom view
        UIBarButtonItem *btnBottombarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarRight];
        
        [items addObject:btnBottombarRightBarButton];
    }
    
    if ([self.controller isKindOfClass:[OtherDetailPageViewCtlr class]]) {
        
        OtherDetailPageViewCtlr *detail = (OtherDetailPageViewCtlr *) self.controller;
        if (detail.offerType == OFFER_HOME || detail.offerType == OFFER_CAR) {
            UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            [items addObject:flexible];
            UIImage *imgIMInterested = [Util getImageAtDocDirWithName:@"iminterested.png"];
            UIButton *btnBottombarIMInterested = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnBottombarIMInterested setImage:imgIMInterested forState:UIControlStateNormal];
            btnBottombarIMInterested.frame = CGRectMake(0, 0, imgIMInterested.size.width, imgIMInterested.size.height);
            [btnBottombarIMInterested addTarget:self action:@selector(btnIMInterestedPressed) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barBtnBottombarIMInterested = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarIMInterested];
            [items addObject:barBtnBottombarIMInterested];
        }
    }
    
    if ([self.controller isKindOfClass:[ListViewCtlr class]]) {
        
        ListViewCtlr *list = (ListViewCtlr *) self.controller;
        if ([list.content[LIST_TYPE] isEqualToString:@"dyna-list"]) {
            UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                      target:nil action:nil];
            
            if ([list.content[@"name"] isEqualToString:@"leave-approve"] || [list.content[@"name"] isEqualToString:@"muster-approve"]  || [list.content[@"name"] isEqualToString:@"late-punch-approve"]) {
                [items addObject:flexible];
                
                UIBarButtonItem *editButton = [list editButtonWithState:NO];
                [items addObject:editButton];
            }
            
            if ([list.content[@"name"] isEqualToString:@"holiday-calendar"]) {
                [items addObject:flexible];
                
                UIBarButtonItem *barBtnHolidayCalendar = [list holidayButton];
                [items addObject:barBtnHolidayCalendar];
            }
            
        }
    }
    
    if ([self.controller isKindOfClass:[WebViewController class]] && [pageData[PAGE_NAME] isEqualToString:@"help"]) {
        UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:flexible];
        
        NSString *majorVersion  = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
        NSString *strAppBuild = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.controller.view.frame.size.width, 21.0f)];//[[UILabel alloc] init];
        [label setFont:[UIFont fontWithName:APP_FONT_BOLD size:13]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor whiteColor]];
        [label setText:[NSString stringWithFormat:@"App Version: v%@c%@", majorVersion, strAppBuild]];
        NSLog(@"majorVersion  %@",majorVersion);
        
        NSLog(@"strAppBuild  %@",strAppBuild);
        [label setTextAlignment:NSTextAlignmentCenter];
        UIBarButtonItem *title = [[UIBarButtonItem alloc] initWithCustomView:label];
        [title setWidth:250.0f];
        [items addObject:title];
        
        flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:flexible];
    }
    
    if (![pageData[PAGE_FOOTER_TITLE] isEqualToString:EMPTY]) {
        UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:flexible];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.controller.view.frame.size.width, 21.0f)];//[[UILabel alloc] init];
        [label setFont:[UIFont fontWithName:APP_FONT_BOLD size:13]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor whiteColor]];
        [label setText:[NSString stringWithFormat:@"%@%@",pageData[PAGE_FOOTER_TITLE], ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_LAST_LOGIN_TIME]]];
        [label setTextAlignment:NSTextAlignmentCenter];
        UIBarButtonItem *title = [[UIBarButtonItem alloc] initWithCustomView:label];
        [title setWidth:250.0f];
        [items addObject:title];
    }
    
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    [items addObject:flexible];
    
    if ([pageData[PAGE_BOTTOM_LEFT_BUTTON2] isEqualToString:PAGE_TRUE]) {
        
        //Load the image
        btnCSS = [Util getButtonCSS:pageData[PAGE_BOTTOM_LEFT_BUTTON2_CSS]];
        UIImage *imgBottombarLeft = [Util getImageAtDocDirWithName:btnCSS[@"img"]];
        
        //create the button and assign the image
        UIButton *btnBottombarLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBottombarLeft setImage:imgBottombarLeft forState:UIControlStateNormal];
        
        //sets the frame of the button to the size of the image
        btnBottombarLeft.frame = CGRectMake(0, 0, imgBottombarLeft.size.width, imgBottombarLeft.size.height);
        
        //defining the action
        [btnBottombarLeft addTarget:self action:@selector(btnBottomBarLeftButtonTwoPressed) forControlEvents:UIControlEventTouchUpInside];
        
        //creates a UIBarButtonItem with the button as a custom view
        UIBarButtonItem *btnBottombarLeftBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarLeft];
        
        [items addObject:btnBottombarLeftBarButton];
    }
    
    if ([pageData[PAGE_BOTTOM_LEFT_BUTTON1] isEqualToString:PAGE_TRUE]) {
        
        //Load the image
        btnCSS = [Util getButtonCSS:pageData[PAGE_BOTTOM_LEFT_BUTTON1_CSS]];
        UIImage *imgBottombarLeft = [Util getImageAtDocDirWithName:btnCSS[@"img"]];
        
        //create the button and assign the image
        UIButton *btnBottombarLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBottombarLeft setImage:imgBottombarLeft forState:UIControlStateNormal];
        //sets the frame of the button to the size of the image
        btnBottombarLeft.frame = CGRectMake(0, 0, imgBottombarLeft.size.width, imgBottombarLeft.size.height);
        //defining the action
        [btnBottombarLeft addTarget:self action:@selector(btnBottomBarLeftButtonOnePressed) forControlEvents:UIControlEventTouchUpInside];
        
        //creates a UIBarButtonItem with the button as a custom view
        UIBarButtonItem *btnBottombarLeftBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarLeft];
        
        [items addObject:btnBottombarLeftBarButton];
    }
    
    self.controller.toolbarItems = items;
    items = nil;
}

- (void) btnIMInterestedPressed {
    
    NSString *offerId = nil;
    
    if ([self.controller isKindOfClass:[OtherDetailPageViewCtlr class]]) {
        //[((OtherDetailPageViewCtlr *) controller) btnIMInterestedPressed];
        NSLog(@"i'm interested ok");
        offerId = [((OtherDetailPageViewCtlr *) self.controller) getOfferId];
    }
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resBtnIMInterested:)
                                                          onObject:self.controller];
    [conn btnIMInterestedPressedForUserId:([Holder sharedHolder].loginInfo)[@"e_id"]
                                  offerId:offerId
                             WithNSObject:self];
    NSLog(@"%s", __FUNCTION__);
}

- (void) resBtnIMInterested:(NSDictionary *) responseDict {
    
    NSLog(@"Response I'm Interested: %@", responseDict);
    if ([Util serverResponseCheckNullAndSession:responseDict InFunction:__FUNCTION__ Silent:NO]) {
        
        NSString *msg = responseDict[SERVER_RES_MEG];
        
        UIAlertView *showAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:ALERT_OPT_OK
                                                  otherButtonTitles:nil];
        [showAlert show];
    }
}

- (void) btnBottomBarLeftButtonOnePressed {
#ifdef DEBUGX
    NSLog(@"%s Help button pressed.", __FUNCTION__);
#endif
    [Util openHelp:self.controller];
}

- (void) btnBottomBarLeftButtonTwoPressed {
#ifdef DEBUGX
    NSLog(@"%s Refresh button pressed.", __FUNCTION__);
#endif
    if ([self.controller isKindOfClass:[LoginViewCtlr class]] ||
        [self.controller isKindOfClass:[CarouselViewCtlr class]] ||
        [self.controller isKindOfClass:[GridViewCtlr class]] ) {
        //No action for referesh on Login Page
    } else if ([self.controller isKindOfClass:[ListViewCtlr class]]) {
        [((ListViewCtlr *) self.controller) refreshList];
    } else if ([self.controller isKindOfClass:[WebViewController class]]) {
        [((WebViewController *) self.controller) refreshWeb];
    } else if ([self.controller isKindOfClass:[NotificationList class]]) {
        [((NotificationList *) self.controller) refreshNotificationList];
    }
}

- (void) btnBottomBarRightButtonOnePressed {
#ifdef DEBUGX
    NSLog(@"%s Saath Aap ka image clicked.", __FUNCTION__);
#endif
    if ([self.controller isKindOfClass:[LoginViewCtlr class]]) {
        //No action for Saath App ka logo on Login Page
    }
}

+ (NSArray *) dynalistFooterWithController: (UIViewController *) controller AndSelector: (SEL []) selector {
    
    
    NSMutableArray *toolbar = [[NSMutableArray alloc] init];
    ListViewCtlr *list = (ListViewCtlr *) controller;
    
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    UIImage *imgSelectAll = [Util getImageAtDocDirWithName:@"tool_selectall.png"];
    UIImage *imgUnSelectAll = [Util getImageAtDocDirWithName:@"tool_unselectall.png"];
    UIButton *btnCheck = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCheck.frame = CGRectMake(0, 0, imgSelectAll.size.width, imgSelectAll.size.height);
    btnCheck.tag = 101;
    [btnCheck setBackgroundImage:imgSelectAll forState:UIControlStateNormal];
    [btnCheck setBackgroundImage:imgUnSelectAll forState:UIControlStateSelected];
    [btnCheck addTarget:list action:selector[0] forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBarCheck = [[UIBarButtonItem alloc] initWithCustomView:btnCheck];
    [toolbar addObject:btnBarCheck];
    [toolbar addObject:flexible];
    
    
    UIBarButtonItem *cancel = [list editButtonWithState:YES];
    
    [toolbar addObject:cancel];
    [toolbar addObject:flexible];
    
    UIImage *imgApprove = [Util getImageAtDocDirWithName:@"tool_approve.png"];
    UIButton *btnApprove = [UIButton buttonWithType:UIButtonTypeCustom];
    btnApprove.frame = CGRectMake(0, 0, imgApprove.size.width, imgApprove.size.height);
    btnApprove.tag = 102;
    [btnApprove setBackgroundImage:imgApprove forState:UIControlStateNormal];
    [btnApprove addTarget:list action:selector[0] forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBarApprove = [[UIBarButtonItem alloc] initWithCustomView:btnApprove];
    [toolbar addObject:btnBarApprove];
    [toolbar addObject:flexible];
    
    UIImage *imgReject = [Util getImageAtDocDirWithName:@"tool_reject.png"];
    UIButton *btnReject = [UIButton buttonWithType:UIButtonTypeCustom];
    btnReject.tag = 103;
    btnReject.frame = CGRectMake(0, 0, imgReject.size.width, imgReject.size.height);
    [btnReject setBackgroundImage:imgReject forState:UIControlStateNormal];
    [btnReject addTarget:list action:selector[0] forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBarReject = [[UIBarButtonItem alloc] initWithCustomView:btnReject];
    [toolbar addObject:btnBarReject];
    
    return toolbar;
}

#pragma mark - UIALertViewDelegate -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_OPT_OK]) {
        [Util popWithFadeEffectOnNavigationController:self.controller.navigationController ShouldPopToRoot:NO];
    }
}

@end
