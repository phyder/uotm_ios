//
//  Constant.h
//  engage
//
//  Created by Suraj on 11/29/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * App constants
 */
//#define DEBUGX        true
//#define DEBUG_LEVEL_1 @"Debug Level One"
//#define DEBUG_LEVEL_2 @"Debug Level Two"
//#define DEBUG_LEVEL_3 @"Debug Level Three"
#define DEBUG_ERROR_LOG                         true
#define SHOW_EXECUTION_TIME                     false
#define EMPTY                                   @""

#define DEVICE_TYPE                             @"I"
// Not used
#define J_SESSION_ID                            @"jsessionid"
#define EMP_ID                                  @"id"
#define LOGIN_SESSION_TIMEOUT                   15
#define TAG_IDENTIFY_LOGIN_VIEW                 15
#define TAG_CAROUSEL_APPROVAL                   1320
#define TAG_PAC_CAROUSEL                        1325
#define TAG_PENDING_LEAVE_APPROVAL_COUNT        1321
#define TAG_PENDING_MUSTER_APPROVAL_COUNT       1322
#define TAG_PENDING_CONVEYANCE_APPROVAL_COUNT   1323
#define TAG_PENDING_LATE_PUNCH_APPROVAL_COUNT   1325
#define TAG_PAC_INDICATOR                       1324

/**
 * Constant for MSSQL Server date format to iOS NSDate format
 */
#define MSSQL_TO_NSDATE_DATE_CONVERSION @"MSSQL_TO_NSDATE"
#define MSSQL_DATE_FORMAT @"yyyy-MM-dd HH:mm:ss.S"
#define NSDATE_FORMAT @"dd MMM yyyy"


#define SETTINGS_FILE       @"www/xml/setting.xml"
#define CSS_FILE            @"www/xml/css.xml"
#define MENU_FILE           @"www/xml/menu.xml"
#define FILE_VERSION_LIST   @"www/xml/file.xml"

//#define APP_NAME @"e-Ngage"
#define APP_FONT_ROMAN      @"ZurichBT-Roman"
#define APP_FONT_BOLD       @"ZurichBT-Bold"
#define APP_FONT_ITALIC     @"ZurichBT-Italic"
#define APP_FONT_BOLDITALIC @"ZurichBT-BoldItalic"

#define LOCAL_IMGAGE_DIR    @"www/images/"

// Error Code
#define ERR_CODE_500                500
#define ERR_CODE_NSERROR            550
#define ERR_CODE_NULL               1234
#define ERR_CODE_NULL_STS           1235
#define ERR_CODE_UPD_ERR            1236
#define RUN_EXC_SIGN_IN             1300
#define RUN_EXC_SIGN_OUT            1301
#define RUN_EXC_PEN_CNT             1302
#define RUN_EXC_WEB                 1303
#define ERR_CODE_URL_MAL_FUNC       1400

#define ERR_MSG_500                 @"e-Ngage Server returned status 500"
#define ERR_MSG_NSERROR             @"NSError returned by system"
#define ERR_MSG_NULL                @"Error: Server returned null response"
#define ERR_DESC_NULL_RES           @"Error: Server returned null response"
#define ERR_MSG_UNEXP_UPD           @"Server returned Unexpected update value"
#define ERR_MSG_NULL_URL            @"Server returned null URL parameter for download"
#define ERR_MSG_NULL_UPD            @"Server returned null Update parameter"
#define ERR_MSG_NULL_STS            @"Server returned null status parameter"
#define ERR_MSG_URL_MAL_FUNC        @"URL mal function"

//Error Dump Fields
#define ERR_POST_PARAM              @"error_log"
#define ERR_DUMP_OS                 @"os"
#define ERR_DUMP_DEVICE_ID          @"deviceId"
#define ERR_DUMP_DATA               @"data"

#define ERR_DUMP_CODE               @"code"
#define ERR_DUMP_MSG                @"msg"
#define ERR_DUMP_DESC               @"desc"
#define ERR_DUMP_DATE               @"date"
#define ERR_DUMP_EMP_ID             @"empId"



//#define HIDDEN_TAG_CAROUSEL 137

#define REQUEST_FOR_SIGNIN                  1   // Signifies that the request for Signing In
#define REQUEST_FOR_DYANLIST                2   // Signifies that the request for Dyna-list data
#define REQUEST_FOR_WEBPAGE_DATA            3   // Signifies that the request for Web-page data
#define REQUEST_FOR_REFRESH_WEBPAGE_DATA    4   // Signifies that the request for refersh Web-page data
#define REQUEST_FOR_SIGNOUT                 5   // Signifies that the request for Signing In
#define REQUEST_FOR_PENDING_COUNT           6   // Signifies that the request for Pending approvals count
#define REQUEST_FOR_LEAVE                   7   // Signifies that the request for Multiple Approval LEAVE
#define REQUEST_FOR_MUSTER                  8   // Signifies that the request for Multiple Approval MUSTER
#define REQUEST_FOR_LATE_PUNCH              12   // Signifies that the request for Multiple Approval LATE PUNCH

#define REQUEST_FOR_MULTIPLE_APPROVE        9   // Signifies that the request for Multiple Approval
#define REQUEST_FOR_MULTIPLE_REJECT         10  // Signifies that the request for Multiple Reject
#define REQUEST_FOR_OTHER_CALENDAR          11  // Signifies that the request for Other Calendar
#define REQUEST_FOR_REGISTER                12  // Signifies that the request for Registration
#define REQUEST_FOR_OTP                     13  // Signifies that the request for set OTP
#define REQUEST_FOR_MPIN                    14  // Signifies that the request for set MPIN
#define REQUEST_FOR_AUTO_SIGNIN             15  // Signifies that the request for Auto Login
#define REQUEST_FOR_DEREGISTER              16  // Signifies that the request for Deregister
#define REQUEST_FOR_RESET_MPIN              17  // Signifies that the request for reset MPIN
#define REQUEST_FOR_VERIFY_MPIN             18  // Signifies that the request for verify MPIN
#define REQUEST_FOR_GET_REGISTER_LIST       19  // Signifies that the request for Registration Device List
#define REQUEST_FOR_ESOP                    20  // Signifies that the request for ESOP


/**
 * Network connectivity status
 */
#define NO_INTERNET_CONNECTIVITY            0   // No internet connectivity
#define INTERNET_CONNECTIVITY_WIFI          1   // Internet connectivity via Wifi data network
#define INTERNET_CONNECTIVITY_CELLULAR      2   // Internet connectivity via Cellular data network

#define ALERT_OPT_NOW           @"NOW"
#define ALERT_OPT_LATER         @"Later"
#define ALERT_OPT_OK            @"Ok"
#define ALERT_OPT_CANCEL        @"Cancel"
#define ALERT_OPT_SIGN_OUT      @"Exit"

/**
 * Login Data
 */
#define LOGIN_PARAM_VIEW_TYPE       @"view_type"
#define LOGIN_PARAM_USER_NAME       @"user_name"
#define LOGIN_PARAM_USER_PASS       @"user_password"
#define LOGIN_PARAM_ENCRYPT_KEY     @"K"
#define LOGIN_PARAM_DEVICE_ID       @"device_id"
#define LOGIN_PARAM_NOTIFICATION_ID @"notification_id"
#define LOGIN_PARAM_DEVICE_TYPE     @"device_type"
#define LOGIN_PARAM_RES             @"res"
#define LOGIN_PARAM_APP_VER         @"app_ver"
#define LOGIN_PARAM_MODEL           @"model"
#define LOGIN_PARAM_OS_VER          @"os_ver"
#define LOGIN_PARAM_USER_BIO        @"user_bio"

#define LOGIN_VIEW_TYPE         @"json"
#define LOGIN_RES               @"IOS_ALL"

#define LOGIN_RES_PARAM_EMPLOYEE_ID         @"e_id"
#define LOGIN_RES_PARAM_EMPLOYEE_NAME       @"e_name"
#define LOGIN_RES_PARAM_LAST_LOGIN_TIME     @"llt"
#define LOGIN_RES_PARAM_MENU_ACCESS_CODE    @"menu_ar"
#define LOGIN_RES_PARAM_MPIN_MENU           @"mpin_menu"
#define LOGIN_RES_PARAM_SESSION_ID          @"s_id"
#define LOGIN_RES_PARAM_USER_ID             @"u_id"
#define LOGIN_RES_PARAM_UPADTE              @"update"
#define LOGIN_RES_PARAM_DOWNLOAD_URL        @"url"

#define OTP_PARAM_OTP               @"otp"

#define SET_MPIN_NEW_MPIN           @"mpin"
#define SET_MPIN_RETYPE_MPIN        @"re_mpin"
#define SET_MPIN_IS_REG             @"is_reg"

#define AUTO_LOGIN_MPIN             @"mpin"

#define SERVER_RES_STATUS                   @"sts"
#define SERVER_RES_MEG                      @"msg"

#define PENDING_STATUS_APPROVED             @"Approved"
#define PENDING_STATUS_PENDING              @"Pending"
#define PENDING_STATUS_MUSTER               @"Pending / System Tagged"
#define PENDING_STATUS_SAVED_NOT_SUBMITTED  @"Saved but not submitted"
#define PENDING_STATUS_DELETED              @"Deleted"
#define PENDING_STATUS_REJECTED             @"Rejected"
#define PENDING_STATUS_DONT_INCLUDE         @"noinclude"

//#define JS_WITH_URL @"www/js/variable.js"
#define JSPATH_WEBPAGE_DATA @"www/js/variable.js"

// Pending Approval Count
#define PEN_CNT_DATA            @"data"
#define PEN_LV_APP_CNT          @"LV_P_CON_RES_CON_TOT"
#define PEN_MS_APP_CNT          @"MS_P_CON_RES_CON_TOT"
#define PEN_EXP_APP_CNT         @"EXP_P_CON_RES_CON_TOT"
#define PEN_LP_APP_CNT         @"LP_P_CON_RES_CON_TOT"


/**
 * Page Constant
 **/
#define POWERED_BY_ENGAGE_URL   @"http://www.cmss.in/"
#define PAGE_TRUE               @"true"
#define PAGE_FALSE              @"false"

//CSS
//Name
#define PAGE_REGISTER           @"register"
#define PAGE_LOGIN              @"login"
#define PAGE_RESET_MPIN         @"reset-mpin"
#define PAGE_SETTINGS           @"setting"
#define PAGE_MAIN_MENU          @"main-menu"
#define PAGE_MAIN_MENU_LIST     @"main-menu-list"
#define PAGE_LEAVE_HISTORY      @"leave-history"
#define PAGE_EMERGENCY_CONTACTS @"emergency-number"
#define PAGE_TOOLS              @"tools"
#define PAGE_DEREGISTER         @"deregister"

//Type
#define PAGE_TYPE_LIST          @"list"
#define PAGE_TYPE_LISTS         @"lists"
#define PAGE_TYPE_DYNA_LIST     @"dyna-list"
#define PAGE_TYPE_WEB           @"web"
#define PAGE_TYPE_APP           @"app"
#define PAGE_TYPE_OFEFR         @"offer"
#define PAGE_TYPE_CALL          @"call"
#define PAGE_TYPE_SMS           @"sms"
#define PAGE_TYPE_MENULIST      @"menulist"
#define PAGE_TYPE_MENULIST_DB   @"menulist-db"

//SMS
#define EMERGENCY_SMS_NUMBER    @"5676766"
#define EMERGENCY_SMS_TEXT      @"MEET"

/**
 * CSS Constant
 **/
#define CSS_PAGE_NAME           @"name"
#define CSS_PAGE_TINT_COLOR     @"tint-color"
#define CSS_PAGE_BGCOLOR        @"bgcolor"
// TODO header-bgcolor footer-bgcolor not used anymore since header & footer uses background image

#define CSS_BUTTON_NAME             @"name"
#define CSS_BUTTON_TEXT             @"text"
#define CSS_BUTTON_IMAGE            @"img"
#define CSS_BUTTON_BG_IMAGE         @"bg-img"
#define CSS_BUTTON_BG_IMAGE_INSET   @"bg-img-inset"
#define CSS_BUTTON_BGCOLOR          @"bgcolor"

#define CSS_LABEL_NAME              @"name"
#define CSS_LABEL_TEXT_BOUNDS       @"text-bounds"
#define CSS_LABEL_FONT_NAME         @"font-name"
#define CSS_LABEL_FONT_STYLE        @"font-style"
#define CSS_LABEL_FONT_COLOR        @"font-color"
#define CSS_LABEL_FONT_SIZE         @"font-size"
#define CSS_LABEL_NOTIFICATION_HEADER_BGCOLOR   @"header-bgcolor"
#define CSS_LABEL_NOTIFICATION_ICONS_BOUNDS     @"icon-bounds"

//=======Replaced
#define CSS_NAME                    @"name"
#define CSS_PAGE_BGCOLOR            @"bgcolor"
#define CSS_PAGE_TINTCOLOR          @"tint-color"
#define CSS_PAGE_HEADER_BGCOLOR     @"header-bgcolor"
#define CSS_PAGE_FOOTER_BGCOLOR     @"footer-bgcolor"
//*******Replaced

/**
 * File Constant
 **/
#define FILE_NAME   @"NAME"
#define FILE_VER    @"ver"

/**
 * MENU Constant
 **/
#define PAGE_NAME                       @"name"
#define PAGE_TYPE                       @"type"
#define PAGE_CSS                        @"page-css"
#define PAGE_HEADER                     @"header"
#define PAGE_FOOTER                     @"footer"
#define PAGE_HEADER_TITLE               @"header-title"
#define PAGE_FOOTER_TITLE               @"footer-title"
#define PAGE_TOP_LEFT_BUTTON            @"back-icon"
#define PAGE_TOP_LEFT_BUTTON_CSS        @"back-icon-css"
#define PAGE_TOP_LEFT_MENU_BUTTON       @"menu"
#define PAGE_TOP_LEFT_MENU_BUTTON_CSS   @"menu-css"
#define PAGE_TOP_RIGHT_BUTTON           @"home-icon"
#define PAGE_TOP_RIGHT_BUTTON_CSS       @"home-icon-css"
#define PAGE_BOTTOM_LEFT_BUTTON1        @"help"
#define PAGE_BOTTOM_LEFT_BUTTON1_CSS    @"help-css"
#define PAGE_BOTTOM_LEFT_BUTTON2        @"refresh"
#define PAGE_BOTTOM_LEFT_BUTTON2_CSS    @"refresh-css"
#define PAGE_BOTTOM_RIGHT_BUTTON        @"button-br"
#define PAGE_BOTTOM_RIGHT_BUTTON_CSS    @"button-br-css"
#define PAGE_URL                        @"url"
#define PAGE_PARAMS                     @"params"
#define PAGE_CHECK_WEB_STACK            @"check-web-stack"

#define LIST_NAME                       @"name"
#define LIST_MAC                        @"mac"
#define LIST_TYPE                       @"type"
#define LIST_NUMBER                     @"number"
#define LIST_IMAGE_ICON                 @"img-icon"
#define LIST_IMAGE_ICON_BOUNDS          @"img-icon-bounds"
#define LIST_TITLE                      @"title"
#define LIST_TITLE_CSS                  @"title-css"
#define LIST_SUB_TITLE                  @"sub-title"
#define LIST_SUB_TITLE_CSS              @"sub-title-css"
#define LIST_DESC                       @"desc"
#define LIST_DESC_CSS                   @"desc-css"
//=======Replaced
#define LIST_R_TEXT                     @"r-text"
#define LIST_COUNT                      @"count"
#define LIST_CLICKABLE                  @"clickable"
#define LIST_LANDING_PAGE               @"landing-page"
//*******Replaced
#define LIST_URL                        @"url"
#define LIST_ACTION_DATE                @"action-date"
#define LIST_ACTION_DATE_CSS            @"action-date-css"
#define LIST_DETAILS                    @"details"

/**
 * Setting Constant
 **/
#define SETTING_NAME                    @"name"
#define SETTING_VALUE                   @"value"

#define APP_DEVICE_ID                           @"app_id"
#define APP_NOTIFICATION_ID                     @"notification_id"
#define APP_ID_LAST_UPDATED_ON                  @"app_id_last_updated_on"
#define APP_ID_UPDATE_CHECK_INTERVAL            @"app_id_update_check_interval"
#define APP_LOCAL_VERSION                       @"app_local_version"
#define BACKGROUND_IS_LOGGED_IN                 @"background-is-logged-in"
#define BACKGROUND_LOGGED_INFO                  @"background-logged-info"
#define BACKGROUND_ENTER_TIME                   @"background-enter-time"
#define DYNA_LIST_LOADER                        @"dyna-list-loader"
#define LIST_LOADER_SUBMIT                      @"loader-submit"
#define DYNA_LIST_NODATA_MSG                    @"dyna-list-nodata-msg"
#define ERROR_LOG_DUMP_URL                      @"error-log-dump-url"
#define FIRST_RUN                               @"first-run"
#define LAST_LOGIN_USER_ID                      @"last-login-user-id"
#define LIST_TOUCHUP_COLOR                      @"list-touchup-color"
#define LIST_TOUCHUP_SINGLE_COLOR               @"list-touchup-single-color"
#define REGISTER_INVALID_CREDENTIALS            @"register-invalid-credentials"
#define MOBILE4U_REGISTER_URL                   @"register-url"
#define MOBILE4U_DEREGISTER_URL                 @"deregister-url"
#define OTP_INVALID_CREDENTIALS                 @"otp-invalid-credentials"
#define MPIN_INVALID_CREDENTIALS                @"mpin-invalid-credentials"
#define RESET_MPIN_INVALID_CREDENTIALS          @"reset-mpin-invalid-credentials"
#define MOBILE4U_OTP_URL                        @"otp-url"
#define MOBILE4U_MPIN_URL                       @"mpin-url"
#define MOBILE4U_RESET_MPIN_URL                 @"reset-mpin-url"
#define MOBILE4U_AUTO_LOGIN_URL                 @"auto-login-url"
#define MOBILE4U_VERIFY_MPIN_URL                @"verify-mpin-url"
#define LOGIN_INVALID_CREDENTIALS               @"login-invalid-credentials"
#define MOBILE4U_LOGIN_URL                      @"login-url"
#define MOBILE4U_DEREGISTER_URL                 @"deregister-url"
#define MOBILE4U_GET_REGISTER_LIST_URL          @"get-register-list-url"
#define MOBILE4U_ESOP_URL                       @"esop"
#define MOBILE4U_LOGOUT_URL                     @"logout-url"
#define LOGOUT_MSG                              @"logout-msg"
#define MAIN_MENU_VIEW                          @"main-menu-view"
#define EMP_MIN_GRADE                           @"minimum-grade"
#define EMP_MIN_GRADE_MSG                       @"minimum-grade-msg"
#define NOTIFICATION_COUNT_URL                  @"notification-count-url"
#define NOTIFICATION_REFRESH_TIME               @"notification-refresh-time"
#define NOTIFICATION_NODATA_MSG                 @"notification-nodata-msg"
#define POPUP_TITLE                             @"popup-title"
#define RESOURCE_COPY_LOADER                    @"resource-copy-loader"
#define TECHNICAL_ERROR                         @"technical-error"
#define SERVER_NOT_RESPONDING                   @"server-not-res"
#define SERVER_REQUEST_TIMEOUT                  @"server-request-timeout"
#define SERVER_REQUEST_TIMEOUT_TEAMCALENDAR     @"server-request-timeout-teamcalender"
#define SERVER_REQUEST_TIMEOUT_EXPENSE_APPLY    @"server-request-timeout-expense-apply"
#define SERVER_SESSION_EXPIRED                  @"server-session-expired"
#define MOBILE4U_URL_TEXT                       @"setting-page-url-text"
#define MOBILE4U_URL                            @"setting-page-url"
#define SETTING_PAGE_SAVE_MSG                   @"setting-page-save-msg"
#define REGISTER_LOADER_MSG                     @"register-loader"
#define OTP_LOADER_MSG                          @"otp-loader"
#define MPIN_LOADER_MSG                         @"mpin-loader"
#define SIGN_IN_LOADER_MSG                      @"sign-in-loader"
#define DEREGISTER_LOADER_MSG                   @"deregister-loader"
#define SIGN_OUT_LOADER_MSG                     @"sign-out-loader"
#define IM_INTEREST_LOADER_MSG                  @"im-interested-loader-msg"
#define TEXT_WRAP                               @"text-wrap"
#define URL_CRASH_REPORT                        @"url-crash-report"
#define URL_LEAVE_MULTIPLE                      @"url-leave-multiple"
#define URL_LEAVE_PENDING_COUNT                 @"url-leave-pending-count"
#define URL_MUSTER_MULTIPLE                     @"url-muster-multiple"
#define URL_MUSTER_PENDING_COUNT                @"url-muster-pending-count"
#define URL_CONVEYANCE_PENDING_COUNT            @"url-conveyance-pending-count"
#define URL_LATE_PUNCH_PENDING_COUNT            @"url-late-punch-pending-count"
#define URL_LATE_PUNCH_MULTIPLE                 @"url-late-punch-multiple"

#define WEBVIEW_LOADER_MSG                      @"web-loader"

#define KEY_CHAIN_DEVICE_ID_KEY                 @"com.cmssdigihull.Universe-on-the-move.deviceid"
#define KEY_CHAIN_NOTIFICATION_ID_KEY           @"com.cmssdigihull.Universe-on-the-move.notificationid"
#define KEY_CHAIN_USER_NAME_KEY                 @"com.cmssdigihull.Universe-on-the-move.userName"
#define KEY_CHAIN_MPIN_KEY                      @"com.cmssdigihull.Universe-on-the-move.mPin"
#define KEY_CHAIN_ACCOUNT                       @"com.cmssdigihull.Universe-on-the-move"
#define LOCAL_DB_IS_FINGER_ACTIVE               @"isFingerActive"


/**
 * Error Message
 */
#define NSURLCONNECTION_TIMEOUT_ERR_MSG         @"The request timed out."


/**
 * Offer Constant
 */
#define OFFER_DETAILS_SUBSTRING_NUM_CHAR    50
#define NOT_AVAILABLE                       @"N/A"

/**
 * Home Offer
 */
#define HOME_OFFER_SERVER_URL @"https://www.cogmat.in/FbApplication/turtle/homeoffer/"

#define REQUEST_FOR_OFFER_HOME_CITY         111     // Signifies that the request for offer brand
#define REQUEST_FOR_OFFER_HOME_LIST         112     // Signifies that the request for offer car list
#define REQUEST_FOR_OFFER_HOME_DETAILS      113     // Signifies that the request for offer Details
#define REQUEST_FOR_OFFER_IMINTERESTED      114     // Signifies that the request for offer Details
//#define REQUEST_FOR_OFFER_CAR_IMAGE 124           // Signifies that the request for car brand image
//#define REQUEST_FOR_OFFER_CAR_LIST_IMAGE 125      // Signifies that the request for car list image

// Home Offer Map Request
#define MAP_RES_ID @"id"
#define MAP_RES_CITY @"city"
#define MAP_RES_STATE @"state"
#define MAP_RES_RECENT @"recent"
#define MAP_RES_LAT @"lat"
#define MAP_RES_LNG @"lng"

// Home Offer List Request
#define LIST_RES_ID @"id"
#define LIST_RES_TITLE @"title"
#define LIST_RES_SUB_TITLE @"sub-title"
#define LIST_RES_IMAGE @"image"
#define LIST_RES_OFFER @"offer"
#define LIST_RES_LOCATION @"location"
#define LIST_RES_RECENT @"recent"
#define LIST_RES_VALIDITY @"valid-till"

// Home Offer Detail Request
#define DETAIL_OFFER_ID @"id"
#define DETAIL_OFFER_TITLE @"title"
#define DETAIL_OFFER_SUB_TITLE @"sub-title"
#define DETAIL_OFFER_DETAIL @"detail"
#define DETAIL_OFFER_IMAGE @"image"
#define DETAIL_OFFER_OFFER @"offer"
#define DETAIL_OFFER_CONTACT_NO_1 @"contact-no-1"
#define DETAIL_OFFER_CONTACT_NO_2 @"contact-no-2"
#define DETAIL_OFFER_CONTACT_EMAIL @"contact-email"
#define DETAIL_OFFER_CONTACT_URL @"contact-url"
#define DETAIL_OFFER_CONTACT_NAME @"contact-name"
#define DETAIL_OFFER_CONTACT_LOCATION @"location"
#define DETAIL_OFFER_START_DATE @"starts-from"
#define DETAIL_OFFER_END_DATE @"valid-till"
#define DETAIL_OFFER_RECENT @"recent"


/**
 * Car Offer
 */
#define REQUEST_FOR_OFFER_BRAND_LIST 121        // Signifies that the request for offer brand
#define REQUEST_FOR_OFFER_CAR_LIST 122          // Signifies that the request for offer car list
#define REQUEST_FOR_OFFER_CAR_DETAIL 123        // Signifies that the request for offer Details
#define REQUEST_FOR_OFFER_CAR_IMAGE 124         // Signifies that the request for car brand image
#define REQUEST_FOR_OFFER_CAR_LIST_IMAGE 125    // Signifies that the request for car list image

/**
 * Car Offers - Offer List Response Parameter
 */
#define CAR_OFFER_BRAND_ID          @"id"
#define CAR_OFFER_BRAND_NAME        @"name"
#define CAR_OFFER_BRAND_IMAGE       @"image"
#define CAR_OFFER_BRAND_RECENT      @"recent"

/**
 * Car Offers - Offer List Response Parameter
 */
#define CAR_OFFER_LIST_ID         @"id"
#define CAR_OFFER_LIST_TITLE      @"title"
#define CAR_OFFER_LIST_SUBTITLE   @"sub-title"
#define CAR_OFFER_LIST_RECENT     @"recent"
#define CAR_OFFER_LIST_OFFER      @"offer"
#define CAR_OFFER_LIST_IMAGE      @"image"
#define CAR_OFFER_LIST_ADDRESS    @"location"
#define CAR_OFFER_LIST_VALIDITY   @"valid-till"


/**
 * Car Offers - Offer Details Response Parameter
 */
#define CAR_OFFER_DETAIL_OFFER_ID               @"id"
#define CAR_OFFER_DETAIL_OFFER_TITLE            @"title"
#define CAR_OFFER_DETAIL_OFFER_SUB_TITLE        @"sub-title"
#define CAR_OFFER_DETAIL_OFFER_DETAIL           @"detail"
#define CAR_OFFER_DETAIL_OFFER_IMAGE            @"image"
#define CAR_OFFER_DETAIL_OFFER_OFFER            @"offer"
#define CAR_OFFER_DETAIL_OFFER_CONTACT_NO_1     @"contact-no-1"
#define CAR_OFFER_DETAIL_OFFER_CONTACT_NO_2     @"contact-no-2"
#define CAR_OFFER_DETAIL_OFFER_CONTACT_EMAIL    @"contact-email"
#define CAR_OFFER_DETAIL_OFFER_CONTACT_URL      @"contact-url"
#define CAR_OFFER_DETAIL_OFFER_CONTACT_NAME     @"contact-name"
#define CAR_OFFER_DETAIL_OFFER_CONTACT_LOCATION @"location"
#define CAR_OFFER_DETAIL_OFFER_START_DATE       @"starts-from"
#define CAR_OFFER_DETAIL_OFFER_END_DATE         @"valid-till"
#define CAR_OFFER_DETAIL_OFFER_RECENT           @"recent"

/**
 * Other Offer
 */
#define REQUEST_FOR_OFFER_LIST 131      // Signifies that the request for offer list
#define REQUEST_FOR_OFFER_DETAIL 132    // Signifies that the request for offer Details

/**
 * Other Offers - Offer List Response Parameter
 */
#define OTHER_OFFER_LIST_ID         @"id"
#define OTHER_OFFER_LIST_TITLE      @"title"
#define OTHER_OFFER_LIST_SUBTITLE   @"sub-title"
#define OTHER_OFFER_LIST_RECENT     @"recent"
#define OTHER_OFFER_LIST_OFFER      @"offer"
#define OTHER_OFFER_LIST_IMAGE      @"image"
#define OTHER_OFFER_LIST_ADDRESS    @"address"
#define OTHER_OFFER_LIST_VALIDITY   @"valid-till"


/**
 * Other Offers - Offer Details Response Parameter
 */
#define OTHER_DETAIL_OFFER_ID @"id"
#define OTHER_DETAIL_OFFER_TITLE @"title"
#define OTHER_DETAIL_OFFER_SUB_TITLE @"sub-title"
#define OTHER_DETAIL_OFFER_DETAIL @"detail"
#define OTHER_DETAIL_OFFER_IMAGE @"image"
#define OTHER_DETAIL_OFFER_OFFER @"offer"
#define OTHER_DETAIL_OFFER_CONTACT_NO_1 @"contact-no-1"
#define OTHER_DETAIL_OFFER_CONTACT_NO_2 @"contact-no-2"
#define OTHER_DETAIL_OFFER_CONTACT_EMAIL @"contact-email"
#define OTHER_DETAIL_OFFER_CONTACT_URL @"contact-url"
#define OTHER_DETAIL_OFFER_CONTACT_NAME @"contact-name"
#define OTHER_DETAIL_OFFER_CONTACT_LOCATION @"location"
#define OTHER_DETAIL_OFFER_START_DATE @"starts-from"
#define OTHER_DETAIL_OFFER_END_DATE @"valid-till"
#define OTHER_DETAIL_OFFER_RECENT @"recent"


@interface Constant : NSObject {
    
}

@end
