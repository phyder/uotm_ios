//
//  NSData+Random.h
//  RBS Banking
//
//  Created by Suraj on 27/02/14.
//  Copyright (c) 2014 Phyder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Random)

+(NSData*)random:(NSUInteger)size;

@end
