//
//  Holder.h
//  Universe on the move
//
//  Description: This is the singleton implmention used to store the global
//               information about the app
//
//  Created by Suraj on 12/7/12.
//  Copyright (c) 2013 Suraj. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Authentication;
@class HomeMapViewCtlr;

@interface Holder : NSObject {
    
}

/**
 * Login information
 */
@property (strong, nonatomic) IBOutlet NSString *isLoggedIn;                            // Stores boolean wheater the user is logged in.
@property (strong, nonatomic) IBOutlet NSDictionary *loginInfo;                         // Stores users log-in information.

@property (strong, nonatomic) IBOutlet Authentication *auth;                            // Stores users Authentication reference.

/**
 * Local information inside app
 */
@property (nonatomic) IBOutlet NSDictionary *settings;                          // Stores setting information.
@property (nonatomic) IBOutlet NSDictionary *menu;                              // Stores menu navigation information.
@property (nonatomic) IBOutlet NSDictionary *css;                               // Stores ui information for customizable elements.
@property (nonatomic) IBOutlet NSDictionary *file;                              // Stores the file update information of app.

@property (nonatomic) IBOutlet NSString *currentPageName;                       // Stores the current page name

@property (nonatomic) IBOutlet NSString *title;                                 // Stores the title of gridview;



@property (nonatomic) IBOutlet NSString *notificationCount;                     // Stores the in-app notification count
@property (nonatomic) IBOutlet NSString *pendingCountLeave;                     // Stores the in-app notification count
@property (nonatomic) IBOutlet NSString *pendingCountMuster;                    // Stores the in-app notification count
@property (nonatomic) IBOutlet NSString *pendingCountConveyance;                // Stores the in-app notification count
@property (nonatomic) IBOutlet NSString *pendingCountLatePunch;                 // Stores the in-app notification count
@property (nonatomic) int pendingBadgeCount;                     // Stores the in-app notification count
@property (nonatomic) IBOutlet NSString *notificationId;                        //Global notificationId


@property (nonatomic)  BOOL testingNoti;
@property (nonatomic)  int  notiCount;



@property (nonatomic) IBOutlet NSString *nextWebPage;                           // Stores the next-page that will be opened in WebView

@property (nonatomic) IBOutlet UIViewController *viewController;                // Use to store the global reference of the
                                                                                // IIViewDeckController (Navigation Drawer)

@property (nonatomic) IBOutlet HomeMapViewCtlr *offerHome;                      // Holds the Home offer map refernce;

@property (strong, nonatomic) IBOutlet UINavigationController *mainViewController;

@property (strong, nonatomic) IBOutlet NSString *backFlag;
@property (strong, nonatomic) IBOutlet UIViewController *actionFlag;
@property (strong, nonatomic) IBOutlet NSString *refresh;
@property (strong, nonatomic) IBOutlet NSString *isConnectionAbort;

+ (Holder *) sharedHolder;
- (void) sayHello;

@end
