//
//  Holder.m
//  engage
//
//  Created by Suraj on 12/7/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "Holder.h"

@implementation Holder

static Holder* _sharedHolder = nil;

+ (Holder *) sharedHolder {
    
	@synchronized([Holder class]) {
		if (!_sharedHolder) {
			NSObject *obj = [[self alloc] init];
            NSLog(@"Singleton Object created %@.", [obj description]);
        }
        
		return _sharedHolder;
	}
	return nil;
}

+ (id) alloc {
	@synchronized([Holder class]) {
		NSAssert(_sharedHolder == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedHolder = [super alloc];
		return _sharedHolder;
	}
	return nil;
}

- (id) init {
	self = [super init];
	if (self != nil) {
		// initialize stuff here
	}
	return self;
}

- (void) sayHello {
	NSLog(@"Hello World!");
}

@end