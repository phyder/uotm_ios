//
//  ResourceCopy.m
//  Home Offer
//
//  Created by Suraj on 30/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "ResourceCheck.h"

#import "Constant.h"
#import "ErrorLog.h"


#import "Holder.h"
@implementation ResourceCheck


+ (void) checkResourceCopy {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    @try {
        NSString *wwwDir = [[Util getDocumentsDirectory] stringByAppendingPathComponent:@"www"];
        if (![ResourceCheck isDirectoryExistAtAbsolutePath: wwwDir]) {
            
            NSLog(@"Initaiting for first run.");
            [[self class] copyAssets];
            
            [[self class] initXML];
        } else {
            [[self class] initXML];
            
            NSString *appLocalVersion = [Util getSettingUsingName:APP_LOCAL_VERSION];
            NSLog(@"App local version: %@", appLocalVersion);
            BOOL isInitXML = NO;
            if (appLocalVersion == (id) [NSNull null] || [appLocalVersion length] == 0 || [appLocalVersion isEqualToString:@""]) {
                [Util clearCache];
                [ResourceCheck copyAssets];
                [[self class] initXML];
                isInitXML = YES;
            } else {
                float fltAppLocalVersion = [appLocalVersion floatValue];
                float fltAppVersion  = [[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] floatValue];
                if (fltAppVersion != fltAppLocalVersion) {
                    [Util clearCache];
                    [ResourceCheck copyAssets];
                    [[self class] initXML];
                    isInitXML = YES;
                }   else {
                    NSString *strFirstRun = [Util getSettingUsingName:FIRST_RUN];
                    NSLog(@"First run: %@", strFirstRun);
                    if ([strFirstRun isEqualToString:PAGE_TRUE]) {
                        [Util clearCache];
                        [ResourceCheck copyAssets];
                        [[self class] initXML];
                        isInitXML = YES;
                    }
                }
            }
            
            if (isInitXML) {
                //Initiates menu, css & setting
                [Util initSettingFromXML];
                [Util initXML];
            }
        }
        
//        else {
//            @try {
//                NSLog(@"%s TODO Update Check - on Login", __FUNCTION__);
//                @throw [NSException exceptionWithName:@"Tset" reason:@"test" userInfo:nil];
//            }
//            @catch (NSException *exception) {
//                [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"Setting XML NOT initialized"];
//                @throw exception;
//            }
//        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Resource cannot be copied";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
        @throw exception;
    } @finally {
        /*@try {
            //Initiates menu, css & setting
            [Util initSettingFromXML];
            [Util initXML];
        } @catch (NSException *exception) {
            NSString *strLogMessage = @"Setting XML NOT initialized";
#if DEBUG_ERROR_LOG
            NSLog(@"%@", strLogMessage);
#endif
            [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
            @throw exception;
        }*/
    }
    
    //NSLog(@"setting info: %@", [Holder sharedHolder].settings);
    //NSLog(@"css info: %@", [Holder sharedHolder].css);
    //NSLog(@"menu info: %@", [Holder sharedHolder].menu);
    //NSLog(@"File info: %@", [Holder sharedHolder].file);
}

+ (void) initXML {
    
    [Util initSettingFromXML];
    [Util initXML];
    [Util setSettingWithName:FIRST_RUN ByValue:PAGE_FALSE];
}

+ (void) copyAssets {
    // Copies the resourses
    [ResourceCheck copyResourceWithSourceDirectory:@"www" toDestinationDirectory:[Util getDocumentsDirectory]];
}

+ (void) copyResourceWithSourceDirectory: (NSString *) sourceDirectory toDestinationDirectory: (NSString *) destinationDirectory {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString *sourcePath = [resourcePath stringByAppendingPathComponent:sourceDirectory];
    NSError *error;
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sourcePath error:&error];
    //NSLog(@"Directory %@ File: %@", directory, directoryContents);
    if (![[NSFileManager defaultManager] createDirectoryAtPath:[destinationDirectory stringByAppendingPathComponent:@"www"]
                                   withIntermediateDirectories:YES
                                                    attributes:nil
                                                         error:&error]) {
        NSLog(@"%s Error: Directory could not be created.%@\n", __FUNCTION__, error);
    }
    for (NSString __strong *file in directoryContents) {
        //NSLog(@"file: %@", file);
        if ([self isDirectoryExistAtAbsolutePath:[sourcePath stringByAppendingPathComponent:file]]) {
            file = [sourceDirectory stringByAppendingPathComponent:file];
            //NSLog(@"Directory: %@", file);
            
            if (![[NSFileManager defaultManager] createDirectoryAtPath:[destinationDirectory stringByAppendingPathComponent:file]
                                           withIntermediateDirectories:YES
                                                            attributes:nil
                                                                 error:&error]) {
                NSLog(@"%s Error: Directory could not be created.%@\n", __FUNCTION__, error);
            } else {
                NSLog(@"Directory created: %@", file);
                [self copyResourceWithSourceDirectory:file toDestinationDirectory:destinationDirectory];
            }
        } else {
            file = [sourceDirectory stringByAppendingPathComponent:file];
            NSFileManager *localFileManager=[[NSFileManager alloc] init];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath: [destinationDirectory stringByAppendingPathComponent:file]]) {
#ifdef DEBUG_LEVEL_3
                NSLog(@"file does not exits, copying from resource. %@ - r:%@ - %@", file, resourcePath, destinationDirectory);
#endif
                [localFileManager copyItemAtPath:[resourcePath stringByAppendingPathComponent:file]
                                          toPath:[destinationDirectory stringByAppendingPathComponent:file]
                                           error:nil];
                //NSLog(@"File Copied.");
            } else {
#ifdef DEBUG_LEVEL_3
                NSLog(@"%s Error: In copying the file, file already exits.", __FUNCTION__);
#endif
            }
        }
    }
}

+ (BOOL) isDirectoryExistAtAbsolutePath:(NSString*)filename {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    
    BOOL isDirectory;
    BOOL fileExistsAtPath = [[NSFileManager defaultManager] fileExistsAtPath:filename isDirectory:&isDirectory];
    
    return fileExistsAtPath && isDirectory;
}

@end
