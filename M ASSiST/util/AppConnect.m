	//
//  AppConnect.m
//  M ASSiST
//
//  Created by Suraj on 22/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "AppConnect.h"
#import "Reachability.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Holder.h"
#import "Util.h"
#import <UIKit/UIKit.h>

@implementation AppConnect

- (id)initWithAfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view {
    
    self = [super init];
    if (self) {
        // Initialization code
        callAfterfinish                 = offerListFinished;
        callAfterOnObject               = view;
    }
    return self;
}

- (id)initWithAfterFinishCall:(SEL) offerListFinished onGridObject:(PSUICollectionViewController *) view {
    
    self = [super init];
    if (self) {
        // Initialization code
        callAfterfinish                 = offerListFinished;
        callAfterOnGridObject           = view;
    }
    return self;
}

- (id)initWithAfterFinishCall:(SEL) offerListFinished onNSObject:(NSObject *) obj {
    
    self = [super init];
    if (self) {
        // Initialization code
        callAfterfinish                 = offerListFinished;
        callAfterOnNSObject             = obj;
    }
    return self;
}



- (NSString *) fetchPmsToken{
    #ifdef DEBUG_LEVEL_1
        NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
    #endif
    
        if ([self checkInternetConnectivity]) {
        
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            NSString *url = [NSString stringWithFormat:@"%@/module/pms/pms.htm", [Util getSettingUsingName:MOBILE4U_URL]];
            NSError *pmserror;
            NSURL *pmsurl = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSString* newStr = [NSString stringWithContentsOfURL:pmsurl encoding: NSUTF8StringEncoding error:&pmserror];
            if(newStr == nil){
                return @"";
            }
            return newStr;
            //            NSLog(@"connection start");
        
    }
    return @"";
    
    //NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    /*
    NSString *url = [NSString stringWithFormat:@"%@/module/pms/pms.htm", [Util getSettingUsingName:MOBILE4U_URL]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
     */
    /*
    self.progressBar                     = [MBProgressHUD showHUDAddedTo:view.view animated:YES];
    self.progressBar.mode                = MBProgressHUDModeCustomView;
    self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    self.progressBar.delegate            = self;
    self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    self.progressBar.dimBackground       = YES;
    self.progressBar.detailsLabelText    = @"Switvhing to PMS/TMS.";
    */
    /*
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            return [[NSString alloc] initWithData:self.responseData encoding:NSASCIIStringEncoding];
            //            NSLog(@"connection start");
        } else {
            return @"";
            // Inform the user that the connection failed.
            //            NSLog(@"connection failed");
        }
    }
    */
}

- (void)requestRegisterWithURL:(NSString *)url username:(NSString *) username password:(NSString *)password key:(NSString *)key {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    
    // now lets make the connection to the web
    NSString *viewtype      = [LOGIN_VIEW_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   // NSString *appId         = [[Util getKeyChainDeviceId] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *notificationId = [Util getKeyChainNotificationId];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
//    password = @"password123";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
#endif
    
//    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:APP_DEVICE_ID]
//                                                      message: [Util getKeyChainNotificationId]
//                                                     delegate:nil
//                                            cancelButtonTitle:@"Device & Noti ID"
//                                            otherButtonTitles:nil];
//    [message show];

    
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_USER_PASS, password, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId,LOGIN_PARAM_DEVICE_TYPE, deviceType];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nRegister URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_REGISTER;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:REGISTER_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestOTPWithURL:(NSString *)url username:(NSString *) username otp:(NSString *)otp key:(NSString *)key {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *viewtype      = [LOGIN_VIEW_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *notificationId = [Util getKeyChainNotificationId];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
//    otp = @"123456";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
#endif
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId,LOGIN_PARAM_DEVICE_TYPE, deviceType, OTP_PARAM_OTP,otp];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nOTP URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_OTP;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:OTP_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestSetMPinWithURL:(NSString *)url username:(NSString *)username newMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin key:(NSString *)key {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *viewtype      = [LOGIN_VIEW_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *notificationId = [Util getKeyChainNotificationId];
    NSString *majorVersion  = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    NSString *loginRes      = [LOGIN_RES stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *model         = [Util platformRawString]; //[Util platformNiceString];//[[UIDevice currentDevice] model];
    NSString *os            = [[UIDevice currentDevice] systemVersion];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
//    newMPin = @"0001";
//    retypeMPin = @"0001";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
//    majorVersion = @"1.20";
//    model = @"test";
//    os = @"2.0";
#endif
    
//    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%d", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,SET_MPIN_NEW_MPIN, newMPin, SET_MPIN_RETYPE_MPIN,retypeMPin , LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES, loginRes, LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os, SET_MPIN_IS_REG, NO];
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,SET_MPIN_NEW_MPIN, newMPin, SET_MPIN_RETYPE_MPIN,retypeMPin , LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES, loginRes,LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nSetMPin URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_MPIN;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:MPIN_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestResetMPinWithURL:(NSString *)url username:(NSString *)username password:(NSString *)password newMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin key:(NSString *)key {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *notificationId = [Util getKeyChainNotificationId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
//    password = @"password123";
//    newMPin = @"7777";
//    retypeMPin = @"7777";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
#endif
    
    //    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%d", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,SET_MPIN_NEW_MPIN, newMPin, SET_MPIN_RETYPE_MPIN,retypeMPin , LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES, loginRes, LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os, SET_MPIN_IS_REG, NO];
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_USER_PASS,password, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,AUTO_LOGIN_MPIN, newMPin, SET_MPIN_RETYPE_MPIN,retypeMPin];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nSetMPin URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_RESET_MPIN;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:MPIN_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestAutoLoginWithURL:(NSString *)url username:(NSString *)username mPin:(NSString *)mPin key:(NSString *)key bio:(NSString *) bio{
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *viewtype      = [LOGIN_VIEW_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   // NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   //NSString *notificationId = [Util getKeyChainNotificationId];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification Id  Login"
//                                                    message:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"notification"]]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
    
    
    NSString *notificationId = [Holder sharedHolder].notificationId;
    
//    if (notificationId == nil) {
//        notificationId = [Util getKeyChainNotificationId];
//    }
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"notification id 222222"
//                                                    message:[NSString stringWithFormat:@"%@",[Holder sharedHolder].notificationId]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
//    
//    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"notification id 3333333"
//                                                    message:[NSString stringWithFormat:@"%@",notificationId]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert2 show];
    NSString *majorVersion  = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    NSString *loginRes      = [LOGIN_RES stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *model         = [Util platformRawString];//[[UIDevice currentDevice] model];
    NSString *os            = [[UIDevice currentDevice] systemVersion];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    //NSString *notificationId;
    
#ifndef NDEBUG
//    username = @"231797";
//    mPin = @"0001";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
//    majorVersion = @"1.20";
//    model = @"test";
//    os = @"2.0";
#endif
    
//    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,AUTO_LOGIN_MPIN, mPin, LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES, loginRes, LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os];
    
//    if (notificationId == nil) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notifictaionid"
//                                                        message:notificationId
//                                                       delegate:nil
//                                              cancelButtonTitle:@"testok"
//                                              otherButtonTitles:nil];
//        [alert show];
////        while (notificationId == nil) {
////             notificationId = [Util getKeyChainNotificationId];
////        }
//       
//    }else{
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notifictaionid"
//                                                        message:notificationId
//                                                       delegate:nil
//                                              cancelButtonTitle:@"testok2"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, [defaults valueForKey:@"notification"], LOGIN_PARAM_DEVICE_TYPE, deviceType,AUTO_LOGIN_MPIN, mPin, LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES,loginRes, LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os, LOGIN_PARAM_USER_BIO, bio];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nAutoLogin URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    //AppConnect *app = [[AppConnect alloc]init];
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
 
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            // NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_AUTO_SIGNIN;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            if ([bio isEqualToString:EMPTY]) {
                self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:SIGN_IN_LOADER_MSG]];
            }
            else{
                self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", @"Processing Finger Scan"];
            }
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestVerifyMPinWithURL:(NSString *)url username:(NSString *)username mPin:(NSString *)mPin key:(NSString *)key {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *notificationId = [Util getKeyChainNotificationId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
//    mPin = @"7777";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
#endif
    
    //    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%d", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,SET_MPIN_NEW_MPIN, newMPin, SET_MPIN_RETYPE_MPIN,retypeMPin , LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES, loginRes, LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os, SET_MPIN_IS_REG, NO];
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType,AUTO_LOGIN_MPIN, mPin];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nVerify MPin URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_VERIFY_MPIN;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:SIGN_IN_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestSignInWithURL:(NSString *)url username:(NSString *) username password:(NSString *)password key:(NSString *)key {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *viewtype      = [LOGIN_VIEW_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *appId         = [[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *majorVersion  = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    NSString *loginRes      = [LOGIN_RES stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *model         = [Util platformRawString];//[[UIDevice currentDevice] model];
    NSString *os            = [[UIDevice currentDevice] systemVersion];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, viewtype, LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_USER_PASS, password, LOGIN_PARAM_ENCRYPT_KEY, key, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_DEVICE_TYPE, deviceType, LOGIN_PARAM_APP_VER, majorVersion, LOGIN_PARAM_RES, loginRes, LOGIN_PARAM_MODEL, model, LOGIN_PARAM_OS_VER, os];
    
//    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
//    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nSign In URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_SIGNIN;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"sign-in-loader"]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestGetRegisterListWithURL:(NSString *)url username:(NSString *)username {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
#endif
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@", LOGIN_PARAM_USER_NAME, username];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nGet Register List URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_GET_REGISTER_LIST;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", @"Fetching..."];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestDeregisterWithURL:(NSString *)url username:(NSString *)username deviceId:(NSString *)deviceId {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSString *appId         = deviceId;//[[Util getSettingUsingName:APP_DEVICE_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *deviceType    = [DEVICE_TYPE stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSString *notificationId= [[Util getSettingUsingName:APP_NOTIFICATION_ID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *notificationId = [Util getKeyChainNotificationId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
//    username = @"231797";
//    appId = @"1234";
//    notificationId = @"4567";
//    deviceType = @"A";
#endif
    
    NSString *strPostData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_USER_NAME, username, LOGIN_PARAM_DEVICE_ID, appId, LOGIN_PARAM_NOTIFICATION_ID, notificationId, LOGIN_PARAM_DEVICE_TYPE, deviceType];
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nAutoLogin URL: %@\nparam: %@", url, strPostData);
    
    NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_DEREGISTER;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:DEREGISTER_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void)requestESOPWithURL:(NSString *)url {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
#ifndef NDEBUG
    //    username = @"231797";
#endif
    
    
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nESOP URL: %@\n", url);
    
    NSMutableData *body = [[NSMutableData alloc] init];
    
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_ESOP;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", @"Fetching..."];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void) requestSignOutWithURL:(NSString *) url {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    
//    NSLog(@"Sign Out URL: %@", url);
    // now lets make the connection to the web
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                    cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"SignOut connection start");
            
            intReqFor                       = REQUEST_FOR_SIGNOUT;
            
//            progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
//            progressBar.mode                = MBProgressHUDModeCustomView;
//            progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
//            progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
//            progressBar.delegate            = self;
//            progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
//            progressBar.dimBackground       = YES;
//            progressBar.detailsLabelText    = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"sign-in-loader"]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"SignOut connection did not start");
        }
    }
}

- (void) requestDynalistDataWithURL:(NSString *) url {
    
    // now lets make the connection to the web
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", url]] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"Dynalist connection start");
            
            intReqFor                       = REQUEST_FOR_DYANLIST;
            
            self.progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode                = MBProgressHUDModeCustomView;
            self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate            = self;
            self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground       = YES;
            self.progressBar.detailsLabelText    = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"dyna-list-loader"]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"Dynalist connection did not start");
        }
    }
}

- (void)multipleApproveRejectWithType:(int)type For:(int)requestType Param:(NSString *)param {
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *strURL = nil;
    
    if (requestType == REQUEST_FOR_LEAVE) {
        strURL = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_LEAVE_MULTIPLE]];
    } else if (requestType == REQUEST_FOR_MUSTER) {
        strURL = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_MUSTER_MULTIPLE]];
    } else if (requestType == REQUEST_FOR_LATE_PUNCH) {
        strURL = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:URL_LATE_PUNCH_MULTIPLE]];
    }
    
    
    if (type == REQUEST_FOR_MULTIPLE_APPROVE) {
        param = [NSString stringWithFormat:@"%@&APPR=Y&REJ=N", param];
    } else if (type == REQUEST_FOR_MULTIPLE_REJECT) {
        param = [NSString stringWithFormat:@"%@&APPR=N&REJ=Y", param];
        
    }
    NSString *url = [NSString stringWithFormat:@"%@;%@=%@?%@", strURL, J_SESSION_ID, strSessionID, param];
    NSLog(@"URL: %@", url);
    
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", url]]
                                                    cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            // NSLog(@"Dynalist connection start");
            
            intReqFor                           = type;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:LIST_LOADER_SUBMIT]];
        } else {
            
            // Inform the user that the connection failed.
            NSLog(@"Dynalist Multiple Approve/Reject connection did not start");
        }
    }
}

- (void) requestWebPageDataForURL:(NSString *) url {
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    url = [NSString stringWithFormat:@"%@/%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], url, J_SESSION_ID, strSessionID];
//    NSLog(@"Webpage Data: URL: %@", url);
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", url]] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"WebPageData connection start");
            
            intReqFor                       = REQUEST_FOR_WEBPAGE_DATA;
            
            self.progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode                = MBProgressHUDModeCustomView;
            self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate            = self;
            self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground       = YES;
            self.progressBar.detailsLabelText    = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"dyna-list-loader"]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void) refreshWebPageDataForURL:(NSString *) url {
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    url = [NSString stringWithFormat:@"%@/%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], url, J_SESSION_ID, strSessionID];
    //NSLog(@"Webpage Data: URL: %@", url);
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", url]] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"WebPageData connection start");
            
            intReqFor                       = REQUEST_FOR_REFRESH_WEBPAGE_DATA;
            
            self.progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode                = MBProgressHUDModeCustomView;
            self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate            = self;
            self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground       = YES;
            self.progressBar.detailsLabelText    = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"dyna-list-loader"]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"SignIn connection did not start");
        }
    }
}

- (void) requestPendingApprovalsCount:(NSString *) strURL {
    
//    NSLog(@"\n\nPending Approval Count: %@", strURL);
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", strURL]]
                                                    cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"Pending count connection start");
            
            intReqFor                       = REQUEST_FOR_PENDING_COUNT;
            
//            progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
//            progressBar.mode                = MBProgressHUDModeCustomView;
//            progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
//            progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
//            progressBar.delegate            = self;
//            progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
//            progressBar.dimBackground       = YES;
//            progressBar.detailsLabelText    = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"sign-in-loader"]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"Pending count connection did not start");
        }
    }
}

/**
 * Home offer
 */
- (void) getJson {
    
    // now lets make the connection to the web
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *serverAddress = [NSString stringWithFormat:@"%@/module/offer/home_offer_location.htm;%@=%@?OFR_TYP_REQ_ID=1", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID];
//    NSLog(@"Home offer URL: %@", serverAddress);
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", serverAddress]] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"Home location connection start");
            
            intReqFor                       = REQUEST_FOR_OFFER_HOME_CITY;
            
            self.progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode                = MBProgressHUDModeCustomView;
            self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate            = self;
            self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground       = YES;
            self.progressBar.detailsLabelText    = [NSString stringWithFormat:@"%@", @"Fetching location"];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"Home Location connection did not start");
        }
    }
}

- (void) getOfferListWithCityId: (NSString *) cityId AnsCityName:(NSString *) cityName OnNSObject:(NSObject *) obj {
    
    callAfterOnNSObject = obj;
    cityName = [cityName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *serverAddress = [NSString stringWithFormat:@"%@/module/offer/home_offer_list.htm;%@=%@?OFR_TYP_REQ_ID=1&OFR_HOME_LOC_REQ_ID=%@&OFR_HOME_LOC_REQ_CT=%@", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID, cityId, cityName];
    NSLog(@"offer list URL: %@", serverAddress);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverAddress]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:1000];
    [request setHTTPMethod: @"GET"];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"Home List connection start");
            
            intReqFor                       = REQUEST_FOR_OFFER_HOME_LIST;
            
            self.progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode                = MBProgressHUDModeCustomView;
            self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate            = self;
            self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground       = YES;
            self.progressBar.detailsLabelText    = @"Fetching offers.";
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"Home List connection did not start");
        }
    }
}

- (void) getOfferDetails: (NSString *) offerId {
    
    NSString *strSessionId = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *serverAddress = [NSString stringWithFormat:@"%@/module/offer/home_offer_details.htm;%@=%@?OFR_HOME_LST_REQ_ID=%@", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionId, offerId];
//    NSLog(@"details address: %@", serverAddress);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverAddress]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:1000];
    
    [request setHTTPMethod: @"GET"];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"Home Details connection start");
            
            intReqFor                       = REQUEST_FOR_OFFER_HOME_DETAILS;
            
            self.progressBar                     = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode                = MBProgressHUDModeCustomView;
            self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate            = self;
            self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground       = YES;
            self.progressBar.detailsLabelText    = @"Fetching offer Details.";
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"Home Details connection did not start");
        }
    }
}

- (void) btnIMInterestedPressedForUserId: (NSString *)userId offerId:(NSString *) offerId WithNSObject:(NSObject *) obj {
    
    callAfterOnNSObject = obj;
    NSString *strSessionId = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *serverAddress = [NSString stringWithFormat:@"%@/module/offer/interested.htm;%@=%@?OFR_INTR_REQ_OFR_ID=%@&OFR_INTR_REQ_EMP_OS=%@", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionId, offerId, @"I"];
//    NSString *serverAddress = [NSString stringWithFormat:@"%@iMInterested.php?offerId=%@&userId=%@", HOME_OFFER_SERVER_URL, offerId, userId];
    
//    NSLog(@"I'm Interested address: %@", serverAddress);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverAddress]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    [request setHTTPMethod: @"GET"];
    
    // now lets make the connection to the web
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"Dynalist connection start");
            
            intReqFor                           = REQUEST_FOR_OFFER_IMINTERESTED;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:IM_INTEREST_LOADER_MSG]];
        } else {
            
            // Inform the user that the connection failed.
//            NSLog(@"Dynalist connection did not start");
        }
    }
}

/**
 * Car Offer
 */

- (void) getCarOfferBrandAfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *url = [NSString stringWithFormat:@"%@/module/offer/car_offer_brand.htm;%@=%@?OFR_TYP_REQ_ID=2", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID];
    
//    NSLog(@"Car Offer Brand URL: %@", url);
    NSMutableURLRequest *request        = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                           = REQUEST_FOR_OFFER_CAR_LIST;
    callAfterfinish                     = offerListFinished;
    callAfterOnObject                   = view;
    
    self.progressBar                    = [MBProgressHUD showHUDAddedTo:view.navigationController.view animated:YES];
    self.progressBar.mode               = MBProgressHUDModeCustomView;
    self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
    self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    self.progressBar.delegate           = self;
    self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
    self.progressBar.dimBackground      = YES;
    self.progressBar.detailsLabelText   = @"Fetching Car Offer Brands.";
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

- (void) getCarOfferListForBrandId:(NSString *) brandId BrandName:(NSString *) brandName AfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *url = [NSString stringWithFormat:@"%@/module/offer/car_offer_list.htm;%@=%@?OFR_TYP_REQ_ID=2&OFR_CAR_BRND_REQ_ID=%@&OFR_CAR_BRND_REQ_NM=%@", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID, brandId, brandName];
//    NSLog(@"Car Offer List URL: %@", url);
    NSMutableURLRequest *request    = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                       = REQUEST_FOR_OFFER_CAR_LIST;
    callAfterfinish                 = offerListFinished;
    callAfterOnObject               = view;
    
    self.progressBar                     = [MBProgressHUD showHUDAddedTo:view.view animated:YES];
    self.progressBar.mode                = MBProgressHUDModeCustomView;
    self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    self.progressBar.delegate            = self;
    self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    self.progressBar.dimBackground       = YES;
    self.progressBar.detailsLabelText    = @"Fetching Car Offers.";
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

- (void) getOfferDetailsForOfferId:(NSString *) offerId AfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *url = [NSString stringWithFormat:@"%@/module/offer/car_offer_details.htm;%@=%@?OFR_CAR_DETS_REQ_ID=%@", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID, offerId];
//    NSLog(@"Car Offer Details URL: %@", url);
    NSMutableURLRequest *request    = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                       = REQUEST_FOR_OFFER_CAR_DETAIL;
    callAfterfinish                 = offerListFinished;
    callAfterOnObject               = view;
    
    self.progressBar                     = [MBProgressHUD showHUDAddedTo:view.view animated:YES];
    self.progressBar.mode                = MBProgressHUDModeCustomView;
    self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    self.progressBar.delegate            = self;
    self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    self.progressBar.dimBackground       = YES;
    self.progressBar.detailsLabelText    = @"Fetching Offer Detail.";
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

- (void) getBrandImageWithName: (NSString *) name AfterFinishCall:(SEL) offerListFinished onObject:(BrandCollViewCell *) view {
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@", [Util getSettingUsingName:MOBILE4U_URL], @"/static/offer_images/brand/", name];
    //NSLog(@"Car Offer Brand Image URL: %@", url);
    NSMutableURLRequest *request    = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                       = REQUEST_FOR_OFFER_CAR_IMAGE;
    callAfterfinish                 = offerListFinished;
    callAfterOnObject               = (UIViewController *) view;
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

- (void) getListImageWithName: (NSString *) name AfterFinishCall:(SEL) offerListFinished onObject:(UITableViewCell *) view {
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@", [Util getSettingUsingName:MOBILE4U_URL], @"/static/offer_images/", name];
    //NSLog(@"Offer List Image URL: %@", url);
    NSMutableURLRequest *request    = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                       = REQUEST_FOR_OFFER_CAR_LIST_IMAGE;
    callAfterfinish                 = offerListFinished;
    callAfterOnObject               = (UIViewController *) view;
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

/**
 * Other Offer
 */
- (void) getOtherOfferListAfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *url = [NSString stringWithFormat:@"%@/module/offer/other_offer_list.htm;%@=%@?OFR_TYP_REQ_ID=3", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID];
//    NSLog(@"Other Offer List URL: %@", url);
    NSMutableURLRequest *request    = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                       = REQUEST_FOR_OFFER_LIST;
    callAfterfinish                 = offerListFinished;
    callAfterOnObject               = view;
    
    self.progressBar                     = [MBProgressHUD showHUDAddedTo:view.navigationController.view animated:YES];
    self.progressBar.mode                = MBProgressHUDModeCustomView;
    self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    self.progressBar.delegate            = self;
    self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    self.progressBar.dimBackground       = YES;
    self.progressBar.detailsLabelText    = @"Fetching Offer List.";
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

- (void) getOtherOfferDetailsForOfferId:(NSString *) offerId AfterFinishCall:(SEL) offerListFinished onObject:(UIViewController *) view {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
    NSString *url = [NSString stringWithFormat:@"%@/module/offer/other_offer_details.htm;%@=%@?OFR_OTHR_LST_REQ_ID=%@", [Util getSettingUsingName:MOBILE4U_URL], J_SESSION_ID, strSessionID, offerId];
//    NSLog(@"Other Offer Details URL: %@", url);
    NSMutableURLRequest *request    = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    intReqFor                       = REQUEST_FOR_OFFER_LIST;
    callAfterfinish                 = offerListFinished;
    callAfterOnObject               = view;
    
    self.progressBar                     = [MBProgressHUD showHUDAddedTo:view.view animated:YES];
    self.progressBar.mode                = MBProgressHUDModeCustomView;
    self.progressBar.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
    self.progressBar.customView          = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
    self.progressBar.delegate            = self;
    self.progressBar.labelText           = [Util getSettingUsingName:POPUP_TITLE];
    self.progressBar.dimBackground       = YES;
    self.progressBar.detailsLabelText    = @"Fetching Offer Detail.";
    
    self.connection                      = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        [self.connection start];
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
//            NSLog(@"connection start");
        } else {
            // Inform the user that the connection failed.
//            NSLog(@"connection failed");
        }
    }
}

- (void)requestOtherCalendar:(NSString *)url Params:(NSString *)params {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@ with URL: %@", __FUNCTION__, DEBUG_LEVEL_2, url);
#endif
    // now lets make the connection to the web
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
    
    
    
    //    NSLog(@"IP address: %@", [[NSHost currentHost] address]);
    //    strPostData = [strPostData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"\nOther Calendar request URL: %@\nParam: %@", url, params);
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:postData];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil]];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ([self checkInternetConnectivity]) {
        
        startDate = [NSDate date];
        
        [self.connection start];
        
        if (self.connection) {
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
            self.responseData = [[NSMutableData alloc] init];
            //            NSLog(@"SignIn connection start");
            
            intReqFor                           = REQUEST_FOR_OTHER_CALENDAR;
            
            self.progressBar                    = [MBProgressHUD showHUDAddedTo:callAfterOnObject.navigationController.view animated:YES];
            self.progressBar.mode               = MBProgressHUDModeCustomView;
            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
            self.progressBar.delegate           = self;
            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
            self.progressBar.dimBackground      = YES;
            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@", [Util getSettingUsingName:@"sign-in-loader"]];
        } else {
            
            // Inform the user that the connection failed.
            //            NSLog(@"SignIn connection did not start");
        }
    }
}

#pragma mark - Reachability
- (int) checkInternetStatus {
    
    int internetStatus = NO_INTERNET_CONNECTIVITY;
    //Reachability *internetReach = [Reachability reachabilityWithHostName:[Util getSettingUsingName:MOBILE4U_URL]];
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    BOOL connectionRequired= [internetReach connectionRequired];
    
    if (netStatus==ReachableViaWiFi) {
        internetStatus = INTERNET_CONNECTIVITY_WIFI; // Wifi data network connection available
    } else {
        if(!connectionRequired) {
            internetStatus = INTERNET_CONNECTIVITY_CELLULAR; // Cellular data network connection available
        } else {
            internetStatus = NO_INTERNET_CONNECTIVITY; // No data network connection available
        }
    }
    return internetStatus;
}

- (Boolean) checkInternetConnectivity {
    
    Boolean connectivityStatus;
    if ([self checkInternetStatus] != NO_INTERNET_CONNECTIVITY) {
        connectivityStatus = true;
    } else {
        connectivityStatus = false;
        
//        NSLog(@"No Internet Connection available");
        UIAlertView *noInternet = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                             message:@"No Internet Connection available"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
        [noInternet show];
    }
    
    return connectivityStatus;
}

+ (int) checkInternetStatus {
    
    int internetStatus = NO_INTERNET_CONNECTIVITY;
    //Reachability *internetReach = [Reachability reachabilityWithHostName:[Util getSettingUsingName:MOBILE4U_URL]];
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    BOOL connectionRequired= [internetReach connectionRequired];
    
    if (netStatus==ReachableViaWiFi) {
        internetStatus = INTERNET_CONNECTIVITY_WIFI; // Wifi data network connection available
    } else {
        if(!connectionRequired) {
            internetStatus = INTERNET_CONNECTIVITY_CELLULAR; // Cellular data network connection available
        } else {
            internetStatus = NO_INTERNET_CONNECTIVITY; // No data network connection available
        }
    }
    return internetStatus;
}

+ (Boolean) checkInternetConnectivity {
    
    Boolean connectivityStatus;
    if ([self checkInternetStatus] != NO_INTERNET_CONNECTIVITY) {
        connectivityStatus = true;
    } else {
        connectivityStatus = false;
        
//        NSLog(@"No Internet Connection available");
        UIAlertView *noInternet = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                             message:@"No Internet Connection available"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
        [noInternet show];
    }
    
    return connectivityStatus;
}

#pragma mark -
#pragma mark MBProgessHUDDelegate methods
- (void) loaderCancelPressed {
#ifdef DEBUG
    NSLog(@"%s", __FUNCTION__);
#endif
    
    [self.progressBar hide:YES afterDelay:0.1];
    [self.connection cancel];
}

#pragma mark -
#pragma mark NSURLConnectionDelegate methods
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSURL *urlDomain = [NSURL URLWithString:[Util getSettingUsingName:MOBILE4U_URL]];
		if ([challenge.protectionSpace.host isEqualToString:[urlDomain host]]) {
			[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
		}
	}
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)resData {
   
    [self.responseData appendData:resData];
}

- (void)connection:(NSURLConnection *)conn didFailWithError:(NSError *)error {

    // inform the user
//    NSLog(@"%s Connection failed! Error - %@ %@", __FUNCTION__, [error localizedDescription], [error userInfo][NSURLErrorFailingURLStringErrorKey]);
    
    if ([[error localizedDescription] isEqualToString:NSURLCONNECTION_TIMEOUT_ERR_MSG]) {
        
        [self.progressBar hide:YES afterDelay:0.1];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                         delegate:nil
                                                cancelButtonTitle:ALERT_OPT_OK
                                                otherButtonTitles:nil];
        [message show];
    }
    if (conn != nil) {
        [conn cancel];
        conn = nil;
    }
    [ErrorLog logError:&error WithCode:ERR_CODE_NSERROR Message:ERR_MSG_NSERROR InFunction:__FUNCTION__];
    //[Util showTechnicalError];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response {
    
    int intResponseStatusCode = (int) [response statusCode];
    
    if (!(intResponseStatusCode % 200 == 0 || intResponseStatusCode % 300 == 0)) {
        
        NSLog(@"\n\n\nReponse %d\n\n", intResponseStatusCode);
        
        [self.connection cancel];
        [ErrorLog logAuthErrorWithCode:intResponseStatusCode
                           Description:[NSString stringWithFormat:@"Server response code: %d", intResponseStatusCode]
                               Message:@"Invalid server code"
                            InFunction:__FUNCTION__];
        [Util showTechnicalError];
        
        [self.progressBar hide:YES];
        if(self.progressBar!=nil) {
            [self.progressBar removeFromSuperview];
            self.progressBar = nil;
        }
        
        switch (intReqFor) {
                
            case REQUEST_FOR_OFFER_CAR_IMAGE:
            case REQUEST_FOR_OFFER_CAR_LIST_IMAGE:
                
              
                [callAfterOnObject performSelectorOnMainThread:callAfterfinish
                                                    withObject:self.responseData
                                                 waitUntilDone:YES];
                break;
            default:
                break;
        }
        
        if (self.connection != nil) {
            [self.connection cancel];
            self.connection = nil;
        }
    }
   
    [self.responseData setLength:0];
}

- (void)connectionDidFinishLoading:(NSURLConnection *) conn {
 
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    if (intReqFor==REQUEST_FOR_DYANLIST || intReqFor==REQUEST_FOR_SIGNIN) {
        
        endDate = [NSDate date];
        NSTimeInterval executionTime = [endDate timeIntervalSinceDate:startDate];
        NSString *msg =[NSString stringWithFormat:@"Request Download Time: %f", executionTime];
        NSLog(@"%@", msg);
        
        if (SHOW_EXECUTION_TIME) {
            UIAlertView *exeTimeAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:MOBILE4U_URL]
                                                                   message:msg
                                                                  delegate:nil
                                                         cancelButtonTitle:ALERT_OPT_OK
                                                         otherButtonTitles:nil, nil];
            [exeTimeAlert show];
        }
    }
    
    if (intReqFor != REQUEST_FOR_SIGNOUT && intReqFor != REQUEST_FOR_PENDING_COUNT) {
        
//        [self.progressBar hide:YES afterDelay:0.1];
        [self.progressBar hide:YES];
        if(self.progressBar!=nil) {
            [self.progressBar removeFromSuperview];
            self.progressBar = nil;
        }
    }
    
    if (self.responseData!=nil) {
        
        NSError* error = nil;
        NSMutableDictionary *resData = nil;
        
        // NSData to NSDictionary conversion
        resData = [NSJSONSerialization JSONObjectWithData:self.responseData
                                                  options:NSJSONReadingMutableLeaves
                                                    error:&error];
        if (error) {
            if (intReqFor != REQUEST_FOR_OFFER_CAR_IMAGE) {
                if (intReqFor != REQUEST_FOR_OFFER_CAR_LIST_IMAGE) {
                    
                    [ErrorLog logError:&error WithCode:ERR_CODE_NSERROR Message:ERR_MSG_NSERROR InFunction:__FUNCTION__];
                    [Util showTechnicalError];
                    return;
                }
            }
        }
        
        NSLog(@"Succeeded! Received %lu bytes of data", (unsigned long)[self.responseData length]);
        NSLog(@"Response Message %@", [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding]);
//        NSLog(@"Response: %@", resData);
        
        switch (intReqFor) {
            
            case REQUEST_FOR_REGISTER:
            case REQUEST_FOR_OTP:
            case REQUEST_FOR_MPIN:
            case REQUEST_FOR_RESET_MPIN:
            case REQUEST_FOR_VERIFY_MPIN:
            case REQUEST_FOR_AUTO_SIGNIN:
            case REQUEST_FOR_SIGNIN:
            case REQUEST_FOR_DEREGISTER:
            case REQUEST_FOR_GET_REGISTER_LIST:
            case REQUEST_FOR_ESOP:
            case REQUEST_FOR_DYANLIST:
            case REQUEST_FOR_MULTIPLE_APPROVE:
            case REQUEST_FOR_MULTIPLE_REJECT:
            
            // Webpage data request
            case REQUEST_FOR_WEBPAGE_DATA:
            // Webpage data refresh
            case REQUEST_FOR_REFRESH_WEBPAGE_DATA:
                
            // Home offer
            case REQUEST_FOR_OFFER_HOME_CITY:
            case REQUEST_FOR_OFFER_HOME_DETAILS:
                
            // Car Offer
            case REQUEST_FOR_OFFER_BRAND_LIST:
            case REQUEST_FOR_OFFER_CAR_LIST:
            case REQUEST_FOR_OFFER_CAR_DETAIL:
            
            // Other Offer
            case REQUEST_FOR_OFFER_LIST:
            case REQUEST_FOR_OFFER_DETAIL:
                
            // Other Calendar
            case REQUEST_FOR_OTHER_CALENDAR:
                [callAfterOnObject performSelectorOnMainThread:callAfterfinish
                                                    withObject:resData
                                                 waitUntilDone:YES];
                break;
            
            // Pending Approval Count
            case REQUEST_FOR_PENDING_COUNT:
                [callAfterOnGridObject performSelectorOnMainThread:callAfterfinish
                                                        withObject:resData
                                                     waitUntilDone:YES];
                break;
                
            case REQUEST_FOR_OFFER_CAR_IMAGE:
            case REQUEST_FOR_OFFER_CAR_LIST_IMAGE:
                [callAfterOnObject performSelectorOnMainThread:callAfterfinish
                                                    withObject:self.responseData
                                                 waitUntilDone:YES];
                break;
            case REQUEST_FOR_OFFER_IMINTERESTED:
                [callAfterOnNSObject performSelectorOnMainThread:callAfterfinish
                                                      withObject:resData
                                                   waitUntilDone:YES];
                 break;
            case REQUEST_FOR_SIGNOUT:
                [callAfterOnNSObject performSelectorOnMainThread:callAfterfinish
                                                      withObject:resData
                                                   waitUntilDone:YES];
                break;
            // Home offer
            case REQUEST_FOR_OFFER_HOME_LIST:
                [callAfterOnNSObject performSelectorOnMainThread:callAfterfinish
                                                      withObject:resData
                                                   waitUntilDone:YES];
                break;
            default:
                break;
        }
    }
}

@end
