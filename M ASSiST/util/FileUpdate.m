//
//  FileUpdate.m
//  engage
//
//  Created by Suraj on 19/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "FileUpdate.h"

#import "Constant.h"
#import "Util.h"

@implementation FileUpdate

@synthesize view=_view;

enum {
    ADD = 1,
    UPDATE = 2,
    DELETE = 3,
    DONOTHING = 4
};
typedef NSUInteger TODO;

- (void) check {
    [self performSelectorInBackground:@selector(fileUpdateCheck) withObject:nil];
}

- (void) fileUpdateCheck {
#if DEBUGX
    NSLog(@"%s", __FUNCTION__);
#endif
    NSLog(@"File update check Start.");
    @autoreleasepool {
        NSString *urlString=[NSString stringWithFormat:@"%@%@", @"http://localhost/engage/file.xml", @""];
        NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlString]]];
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSDictionary *fileHashmap = [Util parseXML:responseString];
        float filesVersion = [fileHashmap[@"files"][@"ver"] floatValue];
        NSLog(@"\n\nResponse version: %.2f", filesVersion);
        
        NSArray *serverList = fileHashmap[@"files"][@"file"];
        //NSLog(@"file server: %@", serverList);
        
        urlString = [NSString stringWithFormat:@"%@/%@", [Util getDocumentsDirectory], FILE_VERSION_LIST];
        NSString *xmlData = [NSString stringWithContentsOfFile:urlString encoding:NSUTF8StringEncoding error:nil];
        fileHashmap = [Util parseXML:xmlData];
        
        NSArray *localList = fileHashmap[@"files"][@"file"];
        //NSLog(@"file local: %@", localList);
        
        NSArray *list3 = [self compare:localList With:serverList];
        //NSLog(@"final list: %@", list3);
        
        [self completeUpdateAndDelete:list3];
        
        //[Util writeFileList:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%f", filesVersion], @"ver", list3, @"data", nil]];
        
        progressHUD = [[MBProgressHUD alloc] initWithView:_view];
        [_view addSubview:progressHUD];
        
        progressHUD.delegate            = self;
        progressHUD.color               = [UIColor colorWithWhite:0.75f alpha:1.0f];
        progressHUD.labelText           = [Util getSettingUsingName:POPUP_TITLE]; // Title in the Loader
        progressHUD.dimBackground       = YES;
        progressHUD.detailsLabelText    = @"Initiating app";
        [progressHUD showWhileExecuting:@selector(updateApp:) onTarget:self withObject:nil animated:YES];
        
        /*NSMutableArray *list = [[NSMutableArray alloc] initWithObjects:@"www/test1.html", @"www/test2.html", nil];
        
        if (filesVersion>1.0f) {
            [self updateApp:list];
        }*/
        
        [self performSelectorOnMainThread:@selector(fileCheckComplete) withObject:nil waitUntilDone:NO];
    }
}

- (NSArray *) compare: (NSArray *) list1 With:(NSArray *) list2 {
    
    NSMutableArray *list3 = [[NSMutableArray alloc] init];
    
    //NSLog(@"called list 1: %@\n list2: %@", list1, list2);
    
    int i=0, j=0;
    NSDictionary *listLocal = nil, *listServer = nil;
    NSMutableDictionary *listFinal = [[NSMutableDictionary alloc] init];
    NSString *localName = nil, *serverName = nil, *localVer = nil, *serverVer = nil;
    NSLog(@"list 1 count %lu - list 2 count %lu", (unsigned long)[list1 count], (unsigned long)[list2 count]);
    
    while (i<[list1 count] && j<[list2 count]) {
        
        listLocal = list1[i];
        listServer = list2[j];
        localName = listLocal[@"name"];
        serverName = listServer[@"name"];
        localVer = listLocal[@"ver"];
        serverVer = listServer[@"ver"];
        
        int compare = [localName compare:serverName];
        //NSLog(@"local name: %@ server name: %@ result: %d", localName, serverName, compare);
        
        if (compare==0) {
            if ([localVer floatValue] < [serverVer floatValue]) {
                listFinal[@"name"] = serverName;
                listFinal[@"ver"] = serverVer;
                listFinal[@"todo"] = @"update";
                //NSLog(@"i=%d j=%d update %@", i, j, listFinal);
                [list3 addObject:[listFinal copy]];
            } else if ([localVer floatValue] == [serverVer floatValue]) {
                listFinal[@"name"] = serverName;
                listFinal[@"ver"] = serverVer;
                listFinal[@"todo"] = @"nothing";
                //NSLog(@"i=%d j=%d nothing %@", i, j, listFinal);
                [list3 addObject:[listFinal copy]];
            }
            i++;
            j++;
        } else if (compare < 0) {
            listFinal[@"name"] = localName;
            listFinal[@"ver"] = localVer;
            listFinal[@"todo"] = @"delete";
            [list3 addObject:[listFinal copy]];
            i++;
            //NSLog(@"i=%d j=%d delete %@", i, j, listFinal);
        } else if (compare > 0) {
            listFinal[@"name"] = serverName;
            listFinal[@"ver"] = serverVer;
            listFinal[@"todo"] = @"add";
            [list3 addObject:[listFinal copy]];
            j++;
            //NSLog(@"i=%d j=%d add %@", i, j, listFinal);
        }
    }
    //NSLog(@"i=%d j=%d", --i, --j);
    while (j<[list2 count]) {
        listFinal[@"name"] = list2[j][@"name"];
        listFinal[@"ver"] = list2[j][@"ver"];
        listFinal[@"todo"] = @"add";
        [list3 addObject:[listFinal copy]];
        j++;
    }
    while (i<[list1 count]) {
        listFinal[@"name"] = list1[i][@"name"];
        listFinal[@"ver"] = list1[i][@"ver"];
        listFinal[@"todo"] = @"delete";
        [list3 addObject:[listFinal copy]];
        i++;
    }
    return list3;
}

- (void) completeUpdateAndDelete: (NSArray *) list {
    
    NSMutableDictionary *todoList = [[NSMutableDictionary alloc] init];
    NSMutableArray *delete = [[NSMutableArray alloc] init];
    NSMutableArray *deleteList = [[NSMutableArray alloc] init];
    NSMutableArray *update = [[NSMutableArray alloc] init];
    NSMutableArray *updateList = [[NSMutableArray alloc] init];
    NSDictionary *element = nil;
    NSString *todo = nil, *name = nil;
    
    for (int i=0; i<[list count]; ++i) {
        element = list[i];
        todo = element[@"todo"];
        name = element[@"name"];
        if ([todo isEqualToString:@"delete"]) {
            [delete addObject:element];
            [deleteList addObject:name];
        } else if ([todo isEqualToString:@"add"] || [todo isEqualToString:@"update"]) {
            [update addObject:element];
            [updateList addObject:name];
            if ([name rangeOfString:@".png"].location != NSNotFound) {
                NSLog(@"image png: %@", name);
                int location = (int) [name rangeOfString:@".png"].location;
                name = [NSString stringWithFormat:@"%@%@%@", [name substringToIndex:location], @"@2x", [name substringFromIndex:location]];
                //NSLog(@"loc %d data %@", location, name);
                [updateList addObject:name];
            }
        }
    }
    
    [todoList setValue:delete forKey:@"delete"];
    [todoList setValue:update forKey:@"update"];
    
    [self updateApp:updateList];
    [self deleteFiles:deleteList];
    
    NSLog(@"todo: %@", todoList);
}

- (void) deleteFiles: (NSArray *) list {
    
    NSFileManager *fileMgr = [[NSFileManager alloc] init];
    NSString *documentsDir = [Util getDocumentsDirectory];
    NSString *fullPath = nil;
    BOOL removeSuccess;
    NSError *error = nil;
    
    for (int i=0; i<[list count]; ++i) {
        fullPath = [documentsDir stringByAppendingPathComponent:list[i]];
        removeSuccess = [fileMgr removeItemAtPath:fullPath error:&error];
        if (!removeSuccess) {
            // Error handling
            NSLog(@"%s Error: %@\nFile at location %@ cannot be removed.", __FUNCTION__, error, list[i]);
        } else {
            NSLog(@"File at location %@ deleted.", list[i]);
        }
    }
}

- (void) updateApp: (NSArray *) list {
    
    NSString *urlString = nil;
    NSMutableURLRequest *request = nil;
    NSData *responseData = nil;
    
    NSString *documentsDir = [Util getDocumentsDirectory];
    
    for (int i=0; i<[list count]; i++) {
        urlString=[NSString stringWithFormat:@"%@%@", @"http://localhost/engage/", list[i]];
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlString]]];
        NSLog(@"file path: %@", urlString);
        responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        if (responseData!=nil) {
            NSLog(@"Data received for file: %@", list[i]);
            
            NSError* error;
            [responseData writeToFile:[documentsDir stringByAppendingPathComponent:list[i]] options:NSDataWritingFileProtectionNone error:&error];
            //NSLog(@"file: %@ data:%@ ", [list objectAtIndex:i], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
            if (error!=nil) {
                NSLog(@"%@ copied.", list[i]);
            }
            
            responseData=nil;
        }
    }
}

- (void) fileCheckComplete {
#if DEBUGX
    NSLog(@"%s", __FUNCTION__);
#endif
    NSLog(@"File update check End.");
}

@end
