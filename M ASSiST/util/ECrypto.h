//
//  ECrypto.h
//  RBS Banking
//
//  Created by Suraj on 27/02/14.
//  Copyright (c) 2014 Phyder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ECrypto : NSObject

@property (nonatomic, setter = setAESIV:, getter = AESIV) NSString * mCryptoAESIV;
@property (nonatomic, setter = setAESKey:, getter = AESKey) NSString * mCryptoAESKey;
@property (nonatomic, setter = setRSAPublicKey:, getter = RSAPublicKey) NSString * mCryptoRSAKey;

// Gives singleton instance
+ (ECrypto *)sharedInstance;

#pragma mark - AES
- (void)generateAESKey;
- (NSString *)encryptAES:(NSString *)plainText;
- (NSString *)decryptAES:(NSString *)cipher;

#pragma mark - RSA
- (NSString *)encryptRSA:(NSString *)plainText;
//- (NSString *)decryptRSA:(NSString *)cipher;

#pragma mark - SHA512
- (NSString *)generateHashSHA512WithData:(NSData *)plainData;
- (NSString *)generateHashSHA512WithString:(NSString *)plain;

@end
