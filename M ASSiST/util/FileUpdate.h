//
//  FileUpdate.h
//  engage
//
//  Created by Suraj on 19/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface FileUpdate : NSObject<MBProgressHUDDelegate> {
    UIView *_view;
    MBProgressHUD *progressHUD;
}

@property (strong, nonatomic) IBOutlet UIView *view;

- (void) check;

@end
