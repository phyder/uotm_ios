//
//  Navigation.h
//  Universe on the move
//
//  Created by Suraj on 18/06/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Navigation : NSObject<UIAlertViewDelegate,UIGestureRecognizerDelegate> {
    
    NSDictionary *pageData, *pageCSS, *btnCSS;
}

@property (weak, nonatomic) UIViewController *controller;

- (id)initWithViewController:(UIViewController *) viewCtlr PageData:(NSDictionary *) data;

- (void) checkHeaderAndFooter;

- (void) setHeaderTitle:(NSString *) title;

- (void) initTopRightButton;

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist ForURL:(NSString *) notificationURL;

+ (NSArray *) dynalistFooterWithController: (UIViewController *) controller AndSelector: (SEL []) selector;


@end
