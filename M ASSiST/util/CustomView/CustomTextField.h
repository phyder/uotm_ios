//
//  CustomTextField.h
//  engage
//
//  Created by Suraj on 09/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Util.h"

@interface CustomTextField : UITextField<UITextFieldDelegate> {
    
}

- (void) drawPlaceholderInRect:(CGRect)rect;

@end
