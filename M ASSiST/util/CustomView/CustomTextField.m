//
//  CustomTextField.m
//  engage
//
//  Created by Suraj on 09/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "CustomTextField.h"

#import "Constant.h"

@implementation CustomTextField

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    
    [super drawPlaceholderInRect:rect];
    
    [[Util colorWithHexString:@"E9B472" alpha:@"0.8"] setFill];
    [[self placeholder] drawInRect:rect withFont:[Util fontWithName:APP_FONT_ROMAN style:@"" size:@"17.0f"]];
}

/*// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    // Drawing code
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGRect viewBounds = self.bounds;
    
    CGContextTranslateCTM(theContext, 0, viewBounds.size.height);
    CGContextScaleCTM(theContext, 1, -1);
    
    // Draw the text using the MyDrawText function
    MyDrawText(theContext, viewBounds);
}

////Drawing text
void MyDrawText (CGContextRef myContext, CGRect contextRect) {
    
    float w, h;
    
    w = contextRect.size.width;
    h = contextRect.size.height;
    
    CGContextSelectFont (myContext, "Helvetica-Bold", h/10, kCGEncodingMacRoman);
    
    CGContextSetCharacterSpacing (myContext, 10); // 4
    
    CGContextSetTextDrawingMode (myContext, kCGTextFillStroke); // 5
    
    CGContextSetRGBFillColor (myContext, 0, 1, 0, .5); // 6
    
    CGContextSetRGBStrokeColor (myContext, 0, 0, 1, 1); // 7
    
    CGContextShowTextAtPoint (myContext, 40, 0, "Quartz 2D", 9); // 10
    
    UIImage *theImage=  UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
}*/

@end
