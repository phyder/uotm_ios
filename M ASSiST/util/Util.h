//
//  Util.h
//  engage
//
//  Created by Suraj on 12/3/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#import <UIKit/UIKit.h>

@interface Util : NSObject <UIGestureRecognizerDelegate> {
    
}

// View transition function
+ (void) pushWithFadeEffectOnViewController:(UIViewController *) sourceVC ViewController:(UIViewController *) destinationVC;
+ (void) pushWithFadeEffectOnNavigationController:(UINavigationController *) sourceNC ViewController:(UIViewController *) destinationVC;
+ (void) popWithFadeEffectOnViewController:(UIViewController *) sourceVC ShouldPopToRoot:(BOOL) option;
+ (void) popWithFadeEffectOnNavigationController:(UINavigationController *) sourceNC ShouldPopToRoot:(BOOL) option;

// Device platform
+ (NSString *)platformRawString;
+ (NSString *)platformNiceString;

+ (void) featureNotSupported;

+ (NSString *) getDocumentsDirectory;

+ (void) clearCacheWithViewController:(UIViewController *) viewController;
+ (void) clearCache;

// XML parse function
+ (void) initSettingFromXML;
+ (void) initXML;
+ (NSDictionary *) parseXML: (NSString *) xmlData;

+ (BOOL) setSettingWithName: (NSString *) settingName ByValue: (NSString *) value;
+ (NSString *) getSettingUsingName: (NSString *) settingName;

+ (BOOL) writeSettings;
+ (BOOL) writeFileList: (NSDictionary *) list;

+ (NSString *) setKeyChainWithDeviceId:(NSString *) deviceId;
+ (NSString *) getKeyChainDeviceId;

+ (BOOL) setKeyChainWithNotificationId:(NSString *) deviceId;
+ (NSString *) getKeyChainNotificationId;

+ (void) setKeyChainWithUserName:(NSString *) userName;
+ (NSString *) getKeyChainUserName;

+ (void) setKeyChainWithMPin:(NSString *) mPin;
+ (NSString *) getKeyChainMPin;

+ (BOOL) removeUserSettings;
+ (BOOL) removeAllKeyChain;

+ (void) writeDataInJS:(NSDictionary *) data;                                   //Write web page data in js file
+ (NSString *) encodingStringJavaScriptEscaped:(NSString *) unescapedString;    //Encode string javascript escaped

// XML Data access function
+ (NSDictionary *) getMenu;
+ (NSDictionary *) getPage: (NSString *) pageName;

+ (NSDictionary *) getCSS;
+ (NSDictionary *) getPageCSS: (NSString *) pageName;
+ (NSDictionary *) getButtonCSS: (NSString *) buttonName;
+ (NSDictionary *) getLabelCSS: (NSString *) labelName;

+ (UIFont *) fontWithName: (NSString *)name style: (NSString *)style size: (NSString *) size;

+ (UIColor*)colorWithHexString:(NSString*)hex alpha:(NSString *)alpha;

+ (void) initLabel: (UILabel *) label withTitle: (NSString *) title AndCSS: (NSDictionary *) css;

+ (UIImageView *) getLoaderImageWithTarget: (id)target Action:(SEL)method;
+ (UIImage *) getImageAtDocDirWithName:(NSString *) name;

+ (NSMutableArray *) menuAuthorization:(NSMutableArray *) menuList;

+ (void) openHelp: (UIViewController *) view;

+ (void) openMenuWithNextPage:(NSString *) nextPage OnView:(UIViewController *) view;
+ (void) openMenuAsGridWithNextPage:(NSString *) nextPage Data:(NSDictionary *) nextPageData OnView:(UIViewController *) view;
+ (NSString *) openDynaListWithNextPage:(NSString *) nextPage AfterFinishCall:(SEL) callAfterFinish OnView:(UIViewController *) view;
+ (void) refreshDynaListWithURL:(NSString *)url AfterFinishCall:(SEL)callAfterFinish OnView:(UIViewController *)view;
+ (NSString *) getNotificationURL;
+ (void) openNotificationWithURL:(NSString *)url AfterFinishCall:(SEL)callAfterFinish OnView:(UIViewController *)view;
+ (void) finishLoadingDynaListWithata: (NSDictionary *) resData NextPage:(NSString *) nextPage JSONURL:(NSString *) url OnView:(UIViewController *) view;

+ (void) getWebPageDataForWebPage:(NSString *) pageName AfterfinishCall:(SEL) callAfterFinish OnViewController:(UIViewController *) controller;
+ (void) OpenWebWithNextPage:(NSString *) nextPage AndParam:(NSString *) params OnView:(UIViewController *) view;
+ (void) OpenWebPageAfterDataLoadWithNextPage:(NSString *) nextPage AndParam:(NSString *) params OnView:(UIViewController *) view;
+ (void) openCarouselSelectionOnView:(UINavigationController *) navCtlr;

//Engagement list selection
+ (void) openEngagementSelection:(NSString *) nextPage OnView:(UINavigationController *) navCtlr;

//String MSSQL Server to String NSDate
+ (NSString *) convertDateFromString:(NSString *) strDate WithType:(NSString *) conversionType;

// encode the special charecter in string
+ (NSString *) encodeSpecialCharacter:(NSString *) string;
//
+ (BOOL) isNull:(NSObject *) obj;
//
+ (BOOL) isKey:(NSString *) key ExistForDictionary:(NSDictionary *) dictionary;
//
+ (void) showTechnicalError;
//
+ (BOOL) serverResponseCheckNullAndSession:(NSDictionary *) response InFunction:(const char *) function Silent:(BOOL) silent;
//
+ (BOOL) serverResponseForField:(NSString *) field In:(NSDictionary *) response InFunction:(const char *) function Message:(NSString *) msg Silent:(BOOL) silent;
//
+ (void) sendErrorLog;
//
+ (NSString *) handleNewLine:(NSString *) string;
+ (CGSize) getLableHeightForText:(NSString *) title forFont:(UIFont*) fontName;

- (void) showVerifyViewControllerOnController:(UIViewController *)controller Model:(NSObject *)model Index:(NSInteger)index;
- (void) hideVerifyCtlrWithView:(UIViewController *)controller;
+ (NSData *) doCipher:(NSString *)strData context:(CCOperation)encryptOrDecrypt error:(NSError **)error;

@end
