//
//  ViewController.m
//  M ASSiST
//
//  Created by Suraj on 19/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "LoginViewCtlr.h"

#import "LoginModel.h"

#import "Constant.h"
#import "Holder.h"
#import "Util.h"

#import "IIViewDeckController.h"
#import "MenuListViewCtlr.h"

#import "MBProgressHUD.h"
#import "ResourceCheck.h"

@interface LoginViewCtlr ()

@end

@implementation LoginViewCtlr

- (void) loadView {
    
    [super loadView];
    
    _imgLoginBackground.image   = [Util getImageAtDocDirWithName:@"loginbox.png"];
    _imgBackground.image        = [Util getImageAtDocDirWithName:@"login-view-bg.png"];
    
    loginPage                   = [[LoginModel alloc] init];
    [loginPage initDataWithView:self];
    
    NSArray *lists              = [loginPage getPageData][PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list      = lists[i];
        if ([list[@"type"] isEqualToString:@"image"] && [list[@"name"] isEqualToString:@"logo"]) {
            controlData         = list;
        }
    }
    
    _logo.image = [Util getImageAtDocDirWithName:controlData[@"img"]];
    
    [self initUsername];
    [self initPassword];
    
//    NSString *strDevice = [NSString stringWithFormat:@"The device raw: %@ custom: %@", [Util platformRawString], [Util platformNiceString]];
//    UIAlertView *device = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
//                                                     message:[[NSUUID UUID] UUIDString]
//                                                    delegate:nil
//                                           cancelButtonTitle:@"Ok"
//                                           otherButtonTitles:nil];
//    [device show];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //Custom Initialization
    [loginPage initNavControlWithUser:_txtUserName Password:_txtPassword Emergency:_btnEmergency SignIn:_btnSignIn];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"Scale: %f", [UIScreen mainScreen].scale);
    NSLog(@"Bounds: %@", NSStringFromCGRect([UIScreen mainScreen].bounds));
//    self.txtUserName.text = @"250282";
//    self.txtPassword.text = @"march@2015";
    
    /*NSString *appLocalVersion = [Util getSettingUsingName:APP_LOCAL_VERSION];
    NSLog(@"App local version: %@", appLocalVersion);
    
    if (appLocalVersion == (id) [NSNull null] || [appLocalVersion length] == 0 || [appLocalVersion isEqualToString:@""]) {
        [_txtUserName resignFirstResponder];
        [Util clearCacheWithViewController:self];
        [ResourceCheck copyAssets];
    } else {
        float fltAppLocalVersion = [appLocalVersion floatValue];
        float fltAppVersion  = [[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] floatValue];
        if (fltAppVersion != fltAppLocalVersion) {
            [_txtUserName resignFirstResponder];
            [Util clearCacheWithViewController:self];
            [ResourceCheck copyAssets];
        }
    }*/
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) clearLoginCredentials {
    [loginPage clearLoginCredentials];
}

- (void)clearPassword {
    [loginPage clearPassword];
}

- (void) initUsername {
    
    NSString *strLastLoginUserId = [Util getSettingUsingName:LAST_LOGIN_USER_ID];
    NSLog(@"Last Login User: %@", strLastLoginUserId);
    
    if (strLastLoginUserId != nil && ![strLastLoginUserId isEqualToString:@""]) {
        _txtUserName.text = strLastLoginUserId;
    }
    
    //_txtUserName.textColor = [Util colorWithHexString:@"E9B472"];
    [_txtUserName setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"user.png"];
    [paddingView addSubview:userimage];
    _txtUserName.leftView               = paddingView;
    _txtUserName.leftViewMode           = UITextFieldViewModeAlways;
    _txtUserName.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtUserName.borderStyle            = UITextBorderStyleNone;
    _txtUserName.layer.cornerRadius     = 4.0f;
    _txtUserName.layer.masksToBounds    = YES;
    _txtUserName.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtUserName.layer.borderWidth      = 1.0f;
    _txtUserName.delegate               = self;
    // Custom place holder color txtUserName.placeholder
    //[_txtUserName drawPlaceholderInRect:_txtUserName.frame];
    if ([_txtUserName.text isEqualToString:@""]) {
        [_txtUserName becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _txtUserName) {
        [_txtPassword becomeFirstResponder];
    } else if (textField == _txtPassword) {
        [_txtPassword resignFirstResponder];
        [self signIn];
    }
    return YES;
}

- (void) initPassword {
    
    //_txtPassword.textColor = [Util colorWithHexString:@"E9B472"];
    _txtPassword.delegate               = self;
    [_txtPassword setFont:[UIFont fontWithName:APP_FONT_ROMAN size:17]];
    UIView *leftPaddingView             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
    UIImageView *userimage              = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    userimage.image                     = [Util getImageAtDocDirWithName:@"pass.png"];
    [leftPaddingView addSubview:userimage];
    _txtPassword.leftView               = leftPaddingView;
    _txtPassword.leftViewMode           = UITextFieldViewModeAlways;
    _txtPassword.textColor              = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@""];
    _txtPassword.borderStyle            = UITextBorderStyleNone;
    _txtPassword.layer.cornerRadius     = 4.0f;
    _txtPassword.layer.masksToBounds    = YES;
    _txtPassword.layer.borderColor      = [Util colorWithHexString:@"a1321e" alpha:@"1.0f"].CGColor;
    _txtPassword.layer.borderWidth      = 1.0f;
    _txtPassword.returnKeyType = UIReturnKeyGo;
    // Custom place holder color txtPassword.placeholder
    //[_txtPassword drawPlaceholderInRect:_txtUserName.frame];
}

#pragma mark - UITextFieldDelegate
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    // Transform the views to the normal view
    [self maintainVisibityOfControl:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self resetViewToIdentityTransform];
}

#pragma mark - Keyboard Slider
- (void) maintainVisibityOfControl: (UITextField *) textField {
    
    CGRect bounds = [self.view bounds];
    CGRect toolbar = [self.navigationController.toolbar bounds];
    double visibleHeight = bounds.size.height+toolbar.size.height-216;
    //double visibleHeight = bounds.size.height-216;
    
    double fieldYPosition = textField.frame.origin.y+textField.frame.size.height;
    if (visibleHeight<fieldYPosition) {
        slide = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformMakeTranslation(0.0, (visibleHeight-fieldYPosition-10));
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
    //NSLog(@"%s visible height: %f y: %f", __FUNCTION__, visibleHeight, fieldYPosition);
}

- (void) resetViewToIdentityTransform {
    
    if (slide==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f]; //this is speed of keyboard
        CGAffineTransform slideTransform = CGAffineTransformIdentity;
        self.view.transform = slideTransform;
        [UIView commitAnimations];
    }
}

- (IBAction)btnClearPressed:(id)sender {
    [self hideKeyboard];
}

- (void) hideKeyboard {
    [_txtUserName resignFirstResponder];
    [_txtPassword resignFirstResponder];
}

#pragma mark - Navigation Slider
- (void) initPanMenuListController {
    
    NSLog(@"%s", __FUNCTION__);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MenuListViewCtlr *menuListViewCtlr = [storyboard instantiateViewControllerWithIdentifier:@"menuview"];
    
    IIViewDeckController *deckController = (IIViewDeckController *) [Holder sharedHolder].viewController;
    //deckController.panningMode = IIViewDeckFullViewPanning;
    deckController.leftController = menuListViewCtlr;
}

#pragma mark - Sign In
- (void) signIn {
    
    [loginPage signIn];
}

- (void) resSignIn:(NSMutableDictionary *) resSignIn {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"login"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    
    [alert show];
    [loginPage postAuthenticationValidationWithResData:resSignIn];
}

@end
