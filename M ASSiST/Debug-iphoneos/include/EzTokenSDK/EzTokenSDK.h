//
//  EzTokenSDK.h
//  Copyright © 2015 Ezmcom. All rights reserved.


#import <Foundation/Foundation.h>
#import "EzAccount.h"

#define SDK_VERSION "1.5"

@protocol EzTouchDelegate <NSObject>
@required
-(void) authResult:(int)result;
@end

@interface EzTokenSDK : NSObject

@property(nonatomic, readonly) EzAccount* account;

/*!
 @abstract Initializes the EzTokenSDK. This MUST be called before any other SDK methods.
 */
+(void)initSDK;

/*!
 @abstract Generates the unique Push ID for this application. This API should ONLY be called ONCE in the lifetime of the application
 
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return an NSString representing the PushID. This should be persisted on the device.
 */
+(NSString *)generatePushID: (NSError**) error;

/*!
 @abstract Initializes the Parse push API. This API MUST BE CALLED ON EVERY STARTUP OF THE APPLICATION.
 
 @param pushId The unique pushID generated for this application.
 @param appId The unique Parse application Id. This is obtained from Parse.
 @param clientKey The unique Parse client Key . This is obtained from Parse.
 @param error A reference to an NSError object. This will be filled with information if the method returns NO
 @return a BOOL that indicates if the parse registration was successful. YES if successful, NO otherwise.
 */
//+(BOOL) initializeParsePush: (NSString*) pushId appId:(NSString *)parseAppID clientKey:(NSString*)parseClientKey err:(NSError **) error;

/*!
 @abstract Initializes the AWS push API. This API MUST BE CALLED ON EVERY STARTUP OF THE APPLICATION.
 
 @param pushId The unique pushID generated for this application.
 @param appId The unique Parse application Id. This is obtained from Parse.
 @param clientKey The unique Parse client Key . This is obtained from Parse.
 @param error A reference to an NSError object. This will be filled with information if the method returns NO
 @return an NSString that indicates the push Token that will be used to identify the device when sending push notifications. This token will be passed to the back-end during activation.
 */
//+(NSString* ) initializeAWSPush:(NSString*)awsDeviceToken error:(NSError **) error;


/*!
 @abstract Process an activation for an EzAccount. This method should be typically called after scanning an activation QR-Code.
 
 @param pushId The unique pushID generated for this application.
 @param base64ActivationString A Base64 encoded string. This string is obtained from scanning the activation QR-Code.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil.
 @return An NSString that should be sent to the EZMCOM back end for activation processing or nil otherwise.
 */
+(NSString*)procesActivationScan: (NSString*)pushId base64ActivationString:(NSString*)base64ActivationString certPath:(NSString*) certPath keychainId:(CFDataRef) keychainId error:(NSError **)error;


/*!
 @abstract Process an activation for an EzAccount.
 @param pushId The unique pushID generated for this application.
 @param base64ActivationString A Base64 encoded string. This string is usually obtained by calling an EzIdentity backend service.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil.
 @return An NSString that should be sent to the back end for activation processing or nil otherwise.
 */
+(NSString*)procesActivationString: (NSString*)pushId base64ActivationString:(NSString*)base64ActivationString certPath:(NSString*) certPath keychainId:(CFDataRef) keychainId error:(NSError **)error;

/*!
 @abstract Gets the 16 digit Serial number for the newly activated EzAccount object.
 
 @param activationResultString The NSString returned by the EZMCOM back-end after sending the NSString returned by calling processActivation
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An 16-digit NSString representing the EzAccount serial number or nil otherwise.
 */
+(NSString*)getActivationTSN: (NSString*)activationResultString error:(NSError **)error;

/*!
 @abstract Gets the newly activated EzAccount. This account should be persisted on the device.
 
 @param tsn The 16-digit serial number returned by calling getActivationTSN.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An EzAccount object. This account should be persisted on the device, or nil otherwise
 */
+(EzAccount*)getEzTokenAccount: (NSString*)tsn error:(NSError **)error;

/*!
 @abstract Sets up the EzAccount to use a PIN to protect the EzAccount object.
 
 @param ezAccount The ezAccount object that is to be used setup for PIN usage.
 @param strPIN An NSString that represents the PIN to be used to protect the EzAccount object. Recommended to be between 4 to 8 characters.
 @param error A reference to an NSError object. This will be filled with information if the method returns NO
 @return bool to indicate the status of the operation. YES is successful, NO otherwise.
 */
+(bool)setEzAccountPIN:(EzAccount **)ezAccount strPIN:(NSString *)strPIN error:(NSError **)error;

/*!
 @abstract Changes the PIN used to protect the EzAccount object.
 
 @param ezAccount The ezAccount object whose PIN is to be changed.
 @param oldPIN An NSString that represents the PIN to that is currently used to protect the EzAccount object.
 @param newPIN An NSString that represents the new PIN to be used to protect the EzAccount object. Recommended to be between 4 to 8 characters.
 @param error A reference to an NSError object. This will be filled with information if the method returns NO
 @return bool to indicate the status of the operation. YES is successful, NO otherwise.
 */
+(bool)changeEzAccountPIN:(EzAccount **)ezAccount oldPIN:(NSString *)oldPIN newPIN:(NSString*)newPIN error:(NSError **)error;

/*!
 @abstract Generates a One Time Password for perfoming verifications/authorizations.
 
 @param ezAccount The ezAccount object that is to be used to generate the OTP.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the One Time Password or nil otherwise.
 */
+(NSString*)getOTP: (EzAccount **)ezAccount error:(NSError **)error;

/*!
 @abstract Generates a One Time Password for perfoming verifications/authorizations.
 
 @param ezAccount The ezAccount object that is to be used to generate the OTP.
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the One Time Password or nil otherwise.
 */
+(NSString*)getOTP: (EzAccount **)ezAccount strPIN:(NSString*) strPIN error:(NSError **)error;

/*!
 @abstract Generates a One Time Password for perfoming verifications/authorizations.
 
 @param ezAccount The ezAccount object that is to be used to generate the OTP.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the One Time Password or nil otherwise.
 */
+(NSString*)generateOTP: (EzAccount **)ezAccount error:(NSError **)error __attribute__((deprecated("This method has been deprecated in EzTokenSDK version 1.3. Use getOTP:ezAccount:error instead")));

/*!
 @abstract Generates a One Time Password for perfoming verifications/authorizations.
 
 @param ezAccount The ezAccount object that is to be used to generate the OTP.
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the One Time Password or nil otherwise.
 */
+(NSString*)generateOTP: (EzAccount **)ezAccount strPIN:(NSString*) strPIN error:(NSError **)error __attribute__((deprecated("This method has been deprecated in EzTokenSDK version 1.3. Use getOTP:ezAccount:strPIN:error instead")));


/*!
 @abstract Generates a Signature One Time Password for perfoming verifications/authorizations.
 
 @param ezAccount The ezAccount object that is to be used to generate the OTP.
 @param signatureData The data to use to generate the Signature One Time Password
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the Signature One Time Password or nil otherwise.
 */
+(NSString*)getSignatureOTP: (EzAccount **)ezAccount signatureData:(NSString*) signatureData error:(NSError **)error;

/*!
 @abstract Generates a Signature One Time Password for perfoming verifications/authorizations.
 
 @param ezAccount The ezAccount object that is to be used to generate the OTP.
 @param signatureData The data to use to generate the Signature One Time Password
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the Signature One Time Password or nil otherwise.
 */
+(NSString*)getSignatureOTP: (EzAccount **)ezAccount strPIN:(NSString*) strPIN signatureData:(NSString*) signatureData error:(NSError **)error;

/*!
 @abstract Retrieves the UID from the received pushMessage. This can be used together with the GID to obtain the EzAcccount to be used for performing further operations.
 
 @param pushMessage The push message received.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the UID or nil otherwise.
 */
+(NSString*)getTransactionPullUid: (NSString*)pushMessage error:(NSError **)error;

/*!
 @abstract Retrieves the GID from the received pushMessage. This can be used together with the UID to obtain the EzAcccount to be used for performing further operations.
 
 @param pushMessage The push message received.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the GID or nil otherwise.
 */
+(NSString*)getTransactionPullGid: (NSString*)pushMessage error:(NSError **)error;

/*!
 @abstract Retrieves the transaction reference number from the received pushMessage. This value is later used when pulling transaction details.
 
 @param pushMessage The push message received.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the transaction reference or nil otherwise.
 */
+(NSString*)getTransactionPullRef: (NSString*)pushMessage error:(NSError **)error;



/*!
 @abstract Generates a payload to be sent to the EZMCOM back-end for pulling the transaction details.
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param txRef The transaction reference, tpically obtained by calling getTransactionPullRef
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString payload that should be sent to the EZMCOM back-end to pull the transaction details or nil otherwise.
 */
+(NSString*)pullTransactionDetails: (EzAccount**)ezAccount txRef:(NSString*)txRef certPath:(NSString*)certPath keychainId:(CFDataRef) keychainID error:(NSError **)error;

/*!
 @abstract Generates a payload to be sent to the EZMCOM back-end for pulling the transaction details.
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param txRef The transaction reference, tpically obtained by calling getTransactionPullRef
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString payload that should be sent to the EZMCOM back-end to pull the transaction details or nil otherwise.
 */
+(NSString*)pullTransactionDetails: (EzAccount**)ezAccount strPIN:(NSString*)strPIN txRef:(NSString*)txRef certPath:(NSString*)certPath keychainId:(CFDataRef) keychainID error:(NSError **)error;

/*!
 @abstract Gets the Key/Pair values of the transaction details.
 
 @param jsonString The json string received from the EZMCOM back-end after calling pullTransactionDetails.
 @param txKeys A reference to an NSArray that will hold the sorted keys after the function returns. This array can be used to sort the returned NSDictionary object.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return an NSDictionary containing the Key/Value pairs of the transaction values or nil otherwise.
 */
+(NSDictionary*) getTransactionValues:(NSString*)jsonString txKeys:(NSArray**)keys error:(NSError**)error;


/*!
 @abstract Generates a payload to be sent to the EZMCOM back-end for sigining the transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param txRef The transaction reference, tpically obtained by calling getTransactionPullRef
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param txStatus The signing status MUST BE EITHER 1 for APPROVE OR 2 for REJECT.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString payload that should be sent to the EZMCOM back-end to sign the transaction or nil otherwise.
 */
+(NSString*) signTransaction: (EzAccount**) ezAccount txRef:(NSString*)ref txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys txStatus:(int) txStatus certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Generates a payload to be sent to the EZMCOM back-end for sigining the transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param txRef The transaction reference, tpically obtained by calling getTransactionPullRef
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param txStatus The signing status MUST BE EITHER 1 for APPROVE OR 2 for REJECT.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString payload that should be sent to the EZMCOM back-end to sign the transaction or nil otherwise.
 */
+(NSString*) signTransaction: (EzAccount**) ezAccount strPIN:(NSString*)strPIN txRef:(NSString*)ref txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys txStatus:(int) txStatus certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Generates a payload to be sent to the EZMCOM back-end for sigining the transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param txRef The transaction reference, tpically obtained by calling getTransactionPullRef
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param txStatus The signing status MUST BE EITHER 1 for APPROVE OR 2 for REJECT.
 @param signatureData The NSString data to be used to sign the transaction.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString payload that should be sent to the EZMCOM back-end to sign the transaction or nil otherwise.
 */
+(NSString*) signTransactionWithData: (EzAccount**) ezAccount txRef:(NSString*)ref txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys txStatus:(int) txStatus signatureData:(NSString*) signatureData certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Generates a payload to be sent to the EZMCOM back-end for sigining the transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param txRef The transaction reference, tpically obtained by calling getTransactionPullRef
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param txStatus The signing status MUST BE EITHER 1 for APPROVE OR 2 for REJECT.
 @param signatureData The NSString data to be used to sign the transaction.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainId A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString payload that should be sent to the EZMCOM back-end to sign the transaction or nil otherwise.
 */
+(NSString*) signTransactionWithData: (EzAccount**) ezAccount strPIN:(NSString*)strPIN txRef:(NSString*)ref txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys txStatus:(int) txStatus signatureData:(NSString*) signatureData certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Gets the Key/Pair values of the offline transaction details.
 
 @param base64QRString The base64 encoded string typically obtained by scanning a transaction QR-Code.
 @param txKeys A reference to an NSArray that will hold the sorted keys after the function returns. This array can be used to sort the returned NSDictionary object.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return an NSDictionary containing the Key/Value pairs of the offline transaction values or nil otherwise.
 */
+(NSDictionary*) getOfflineTransactionValues:(NSString*) base64QRString txKeys:(NSArray**)txKeys error:(NSError **)error;

/*!
 @abstract Retrieves the UID from the received offline transaction QR-Code string. This can be used together with the GID to obtain the EzAcccount to be used for performing further operations.
 
 @param offlineQRString The offline transaction QR-Code string.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the UID or nil otherwise.
 */
+(NSString*)getOfflineTransactionUid: (NSString*)offlineQRString error:(NSError **)error;

/*!
 @abstract Retrieves the GID from the received offline transaction QR-Code string. This can be used together with the UID to obtain the EzAcccount to be used for performing further operations.
 
 @param offlineQRString The offline transaction QR-Code string.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the GID or nil otherwise.
 */
+(NSString*)getOfflineTransactionGid: (NSString*)offlineQRString err:(NSError **)error;

/*!
 @abstract Retrieves the transaction reference number from the received offline transaction QR-Code string. This value is later used when pulling transaction details.
 
 @param offlineQRString The offline transaction QR-Code string.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the transaction reference or nil otherwise.
 */
+(NSString*)getOfflineTransactionRef: (NSString*)offlineQRString err:(NSError **)error;

/*!
 @abstract Generates a One Time Password that can be used to verify/authorize an offline transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainID A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the OTP that should be used to verify/authorize the offline transaction or nil otherwise.
 */
+(NSString*)signOfflineTransaction:(EzAccount**) ezAccount txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Generates a One Time Password that can be used to verify/authorize an offline transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainID A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the OTP that should be used to verify/authorize the offline transaction or nil otherwise.
 */
+(NSString*)signOfflineTransaction:(EzAccount**) ezAccount strPIN:(NSString*)strPIN txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Generates a One Time Password that can be used to verify/authorize an offline transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param signatureData An NSString that will be used to sign the transaction data.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainID A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the OTP that should be used to verify/authorize the offline transaction or nil otherwise.
 */
+(NSString*)signOfflineTransactionWithData:(EzAccount**) ezAccount txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys signatureData:(NSString*)signatureData certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Generates a One Time Password that can be used to verify/authorize an offline transaction.
 
 @param ezAccount A reference to the EzAccount object that will be used to process the pull transaction operation.
 @param strPIN An NSString that represents the PIN to be used to lock/unlock the EzAccount object during the operation.
 @param txValues The NSDictionary containing the Key/Value pairs of the transaction details. This is usually obtained by calling getTransactionValues.
 @param txKeys The NSArray containing the sorted transaction keys. This is usually obtained by calling getTransactionValues.
 @param signatureData An NSString that will be used to sign the transaction data.
 @param certPath A path to a public key .cer file. The file should be bundled with the calling application
 @param keychainID A CFDataRef object that points to a keychain.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return An NSString representing the OTP that should be used to verify/authorize the offline transaction or nil otherwise.
 */
+(NSString*)signOfflineTransactionWithData:(EzAccount**) ezAccount strPIN:(NSString*)strPIN txValues:(NSDictionary*)txValues txKeys:(NSArray*)txKeys signatureData:(NSString*)signatureData certPath:(NSString*)certPath keyChainID:(CFDataRef)keychainID error:(NSError**)error;

/*!
 @abstract Checks for TouchID authentication support on the device.
 
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 @return an int. 0= TouchID supported, -1= TouchID is not supported, 1=TouchID is supported but no fingerprints are enrolled
 */
+(int) isTouchAuthSupported: (NSError**)error;

/*!
 @abstract Displays an alertview for performing TouchId authentication
 
 @param dialogMsg The message to be displayed in the dialog.
 @param delegate An implementation of the EzTouchDelegate protocol. The authResult:(int) method of the delegate will be fired after authentication process happens. The int value can be one of the following. 0 = Authentication succeeded, -1 = Authentication Failed, 1 = No fingerprints enrolled on device, -2 = User cancelled the authentication dialog.
 @param error A reference to an NSError object. This will be filled with information if the method returns nil
 */
+(void) doTouchAuth:(NSString*)dialogMsg delegate:(id)delegate error:(NSError**)error;
@end
