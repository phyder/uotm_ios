//
//  EzAccount.h
//  Copyright (c) 2015 EZMCOM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EzAccount : NSObject<NSCoding>

@property (nonatomic, copy, readonly, getter=getServiceName) NSString *serviceName;
@property (nonatomic, copy, readonly, getter=getServiceGID) NSString *serviceGID;
@property (nonatomic, copy, readonly, getter=getAccountSN) NSString *accountSN;
@property (nonatomic, copy, readonly, getter=getUID) NSString *uID;
//@property (nonatomic, copy, readonly, getter=getSuiteString) NSString *suiteString;
@property (nonatomic, copy, readonly, getter=getVersion) NSString *version;

@property (nonatomic) unsigned char *accountDevice;
@property (nonatomic, readonly, getter=getTimeCreated) int timeCreated;
@property (nonatomic, readonly, getter=getIsEventBased) BOOL isEventBased;
@property (nonatomic, getter=getByteLength) int byteLength;

@property (nonatomic) NSData* accountData;

//@property (nonatomic, copy, readonly, getter=getFields) NSDictionary* fields;


-(id)initWithValues:(NSString*)newAccountSN uid:(NSString*)newUID sGID:(NSString *)newServiceGID svcName:(NSString *)newServiceName evtBased:(BOOL)newIsEventBased acctDevice: (unsigned char*) newAccountDevice byteLen:(int) newByteLength;
//-(id)initWithValues:(NSString*)newKey :(int)newOrder :(NSString *)newServiceGID :(NSDictionary *)newFields;

-(NSString *) getServiceName;
-(NSString *) getServiceGID;
-(NSString *) getAccountSN;
-(NSString *) getUID;
//-(NSString *) getSuiteString;
-(NSString *) getVersion;

-(unsigned char*) getAccountDevice;
-(void) setAccountDevice:(unsigned char*) bytes;

-(int) getTimeCreated;
-(BOOL) getIsEventBased;
-(int) getByteLength;
-(void) setByteLength:(int)_byteLength;


-(id)initWithNSData:(NSData *)data;
-(NSData *)toNSData;
@end
