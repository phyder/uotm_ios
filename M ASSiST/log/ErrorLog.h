//
//  EngageError.h
//  Universe on the move
//
//  Created by Suraj on 03/08/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorLog : NSObject {
    
    NSManagedObjectContext *context;
}

- (BOOL) addLogWithErrorCode:(NSNumber *)errorCode Data:(NSDate *) errorDate Description:(NSString *) errorDesc Messsage:(NSString *) errorMsg EmpID:(NSString *) empId;

- (void) sendErrorLog;
- (BOOL) getJSONLog:(NSString **) string;


+ (void) logNullResponseInFunciton:(const char *)function WithMessage:(NSString *) msg;

+ (void) log500ResponseInFunction:(const char *) function WithMessage:(NSString *) msg;

+ (void) logException:(NSException *) exception InFunction:(const char *) function AndMessage:(NSString *) msg;

+ (BOOL) logError:(NSError **) error WithCode:(int) code Message:(NSString *) msg InFunction:(const char *) function;

+ (void) logAuthException:(NSException *) exception WithCode:(int) code Message:(NSString *) msg InFunction:(const char *) function EmpId:(NSString *)empId;

+ (void) logAuthErrorWithCode:(int) code Description:(NSString *) desc Message:(NSString *) msg InFunction:(const char *) function;

@end
