//
//  EngageError.h
//  Universe on the move
//
//  Created by Suraj on 12/08/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EngageError : NSManagedObject

@property (nonatomic, strong) NSNumber * errorCode;
@property (nonatomic, strong) NSDate * errorDate;
@property (nonatomic, strong) NSString * errorDesc;
@property (nonatomic, strong) NSString * errorMsg;
@property (nonatomic, strong) NSString * errorEmpId;

@end
