//
//  EngageError.m
//  Universe on the move
//
//  Created by Suraj on 03/08/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "ErrorLog.h"

#import "AppConnect.h"
#import "EngageError.h"

#import "Constant.h"
#import "Holder.h"
#import "Util.h"

#import "AppDelegate.h"

static NSString *entityEngageError = @"EngageError";

@implementation ErrorLog

- (id) init {
    
    self = [super init];
    if (self) {
        // Custom initialization
        context = [((AppDelegate *)[[UIApplication sharedApplication] delegate]) managedObjectContext];
    }
    return self;
}

- (BOOL) addLogWithErrorCode:(NSNumber *)errorCode
                        Data:(NSDate *) errorDate
                 Description:(NSString *) errorDesc
                    Messsage:(NSString *) errorMsg
                       EmpID:(NSString *) empId  {
    BOOL result = true;
    if ([Util isNull:empId]) {
        empId = @"NotAvail";
    }
    //NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"ErrorLog" inManagedObjectContext:context];
    EngageError *newLog = [NSEntityDescription insertNewObjectForEntityForName:entityEngageError inManagedObjectContext:context];
    
    newLog.errorCode    = errorCode;
    newLog.errorDate    = errorDate;
    newLog.errorDesc    = errorDesc;
    newLog.errorMsg     = errorMsg;
    newLog.errorEmpId   = empId;
    
    NSError *error;
    
    [context save:&error];
    
    if (error) {
        NSLog(@"Error: %@", error);
        result = false;
    } else {
//        NSLog(@"Error Logged");
    }
    return result;
}

- (void) sendErrorLog {
    
    NSString *strErrorLog;
    BOOL isLogAvailable = [self getJSONLog:&strErrorLog];
    NSLog(@"Log availability check: %@", (isLogAvailable)?@"YES":@"NO");
    
    @try {
        if (isLogAvailable) {
            
            NSString *strErrorLogUrl = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:ERROR_LOG_DUMP_URL]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strErrorLogUrl]
                                                                        cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                                    timeoutInterval:[[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue]];
            
            NSString *strPostData = [NSString stringWithFormat:@"%@=%@", ERR_POST_PARAM, [strErrorLog stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
//            NSLog(@"Error dump URL: %@", strErrorLogUrl);
//            NSLog(@"Error dump data: %@", strErrorLog);
//            NSLog(@"Error Log encoded data: %@", strPostData);
            
            NSData *postData = [strPostData dataUsingEncoding:NSUTF8StringEncoding];
            NSString* postDataLengthString = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            NSMutableData *body = [[NSMutableData alloc] init];
            [body appendData:postData];
            [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request addValue:postDataLengthString forHTTPHeaderField:@"Content-Length"];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:body];
            
            if ([AppConnect checkInternetConnectivity]) {
                // now lets make the Synchronous Connection to the web
                NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
                
                NSError *error;
                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:returnData
                                                                             options:kNilOptions
                                                                               error:&error];
                if (!error) {
                    NSLog(@"Response: %@", jsonResponse);
                    if ([jsonResponse[@"sts"] intValue] == 200) {
                        // clears the EngageError log table
                        NSLog(@"Error log %@cleared", ([self clearLog]) ? @"" : @"NOT ");
                    }
                }
            } else {
                NSLog(@"Internet Connection NOT available.");
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%s Exception name: %@ \n%@", __FUNCTION__, exception.name, exception);
    }
    
}

- (BOOL) clearLog {
    
    BOOL result = true;
    
    @try {
        NSFetchRequest *allEngageErrors = [[NSFetchRequest alloc] init];
        [allEngageErrors setEntity:[NSEntityDescription entityForName:entityEngageError inManagedObjectContext:context]];
        [allEngageErrors setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        NSError *error = nil;
        NSArray *errors = [context executeFetchRequest:allEngageErrors error:&error];
        
        if (error == nil) {
            // If No Error
            for (EngageError *error in errors) {
                [context deleteObject:error];
            }
            NSError *saveError = nil;
            [context save:&saveError];
            if (saveError) {
                // If Error
                result = false;
            }
        } else {
            // else Error
            NSLog(@"Error: %@", error);
            result = false;
        }
    } @catch (NSException *exception) {
        NSLog(@"%s Exception name: %@\n%@", __FUNCTION__, exception.name, exception);
    }
    
    return result;
}

- (BOOL) getJSONLog:(NSString **) string {
    
    BOOL result = true;
    
    @try {
        NSMutableArray *dataLog = [[NSMutableArray alloc] init];
        NSMutableDictionary *data;
        
        NSMutableDictionary *errDump = [[NSMutableDictionary alloc] init];
        [errDump setValue:DEVICE_TYPE forKey:ERR_DUMP_OS];
        [errDump setValue:[Util getSettingUsingName:APP_DEVICE_ID] forKey:ERR_DUMP_DEVICE_ID];
        
        NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:entityEngageError];
        NSError *error = nil;
        
        NSArray *resultLog = [context executeFetchRequest:request error:&error];
        
        if (error != nil) {
            //Deal with failure
        } else {
            //Deal with success
            if (resultLog.count <= 0) {
                result = false;
            } else {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:MSSQL_DATE_FORMAT];
                
                for (EngageError *errObj in resultLog) {
                    data = [[NSMutableDictionary alloc] init];
                    [data setValue:errObj.errorCode forKey:ERR_DUMP_CODE];
                    [data setValue:errObj.errorMsg forKey:ERR_DUMP_MSG];
                    [data setValue:errObj.errorDesc forKey:ERR_DUMP_DESC];
                    [data setValue:[dateFormatter stringFromDate:errObj.errorDate] forKey:ERR_DUMP_DATE];
                    [data setValue:errObj.errorEmpId forKey:ERR_DUMP_EMP_ID];
                    [dataLog addObject:data];
                }
                [errDump setValue:dataLog forKey:ERR_DUMP_DATA];
                
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:errDump
                                                                   options:0 // Pass 0 if you don't care about the readability of the generated string
                                                                     error:&error];
                
                if (!jsonData) {
                    NSLog(@"Got an error: %@", error);
                } else {
                    *string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    //*string = [Util encodingStringJavaScriptEscaped:*string];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%s Exception: name - %@ description - %@", __FUNCTION__, exception.name, exception.debugDescription);
    }
    
    return result;
}

+ (void) logNullResponseInFunciton:(const char *) function
                       WithMessage:(NSString *) msg {
    
    ErrorLog *errorLog = [[ErrorLog alloc] init];
    [errorLog addLogWithErrorCode:@ERR_CODE_NULL
                             Data:[[NSDate alloc] init]
                      Description:ERR_MSG_NULL
                         Messsage:[NSString stringWithFormat:@"%s %@", function, msg]
                            EmpID:[Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_EMPLOYEE_ID]];
#if (DEBUG_ERROR_LOG)
    NSLog(@"%s %@ %@", function, ERR_MSG_NULL, msg);
#endif
}

+ (void) log500ResponseInFunction:(const char *) function
                      WithMessage:(NSString *) msg {
    
    ErrorLog *errorLog = [[ErrorLog alloc] init];
    [errorLog addLogWithErrorCode:@ERR_CODE_500
                             Data:[[NSDate alloc] init]
                      Description:ERR_MSG_500
                         Messsage:[NSString stringWithFormat:@"%s %@", function, msg]
                            EmpID:[Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_EMPLOYEE_ID]];
#if (DEBUG_ERROR_LOG)
    NSLog(@"%s %@", function, ERR_MSG_500);
#endif
}

+ (void) logException:(NSException *) exception
           InFunction:(const char *) function
           AndMessage:(NSString *) msg {
    
    ErrorLog *err_log = [[ErrorLog alloc] init];
    [err_log addLogWithErrorCode:@RUN_EXC_WEB
                            Data:[NSDate date]
                     Description:[NSString stringWithFormat:@"Exception name %@ reason %@", exception.name, exception.reason]
                        Messsage:[NSString stringWithFormat:@"%s %@", function, msg]
                           EmpID:[Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_EMPLOYEE_ID]];
#if (DEBUG_ERROR_LOG)
    NSLog(@"Exception msg %@ name %@ reason %@", msg, exception.name, exception.reason);
#endif
}

+ (void) logAuthException:(NSException *) exception
                 WithCode:(int) code
                  Message:(NSString *) msg
               InFunction:(const char *) function
                    EmpId:(NSString *)empId {

    ErrorLog *errorLog = [[ErrorLog alloc] init];
    [errorLog addLogWithErrorCode:@(code)
                             Data:[[NSDate alloc] init]
                      Description:[NSString stringWithFormat:@"Exception name %@ reason %@", exception.name, exception.reason]
                         Messsage:[NSString stringWithFormat:@"%s %@", function, msg]
                            EmpID:empId];
#if (DEBUG_ERROR_LOG)
    NSLog(@"%@ Exception name %@ reason %@ logged", msg, exception.name, exception.reason);
#endif
}

+ (BOOL) logError:(NSError **) error
         WithCode:(int) code
          Message:(NSString *) msg
       InFunction:(const char *) function {
    
    ErrorLog *errorLog = [[ErrorLog alloc] init];
    BOOL result = [errorLog addLogWithErrorCode:@(code)
                                           Data:[[NSDate alloc] init]
                                    Description:[NSString stringWithFormat:@"%s Debug Description %@ %@", function, [*error debugDescription], msg]
                                       Messsage:[NSString stringWithFormat:@"Error code %ld domain %@", (long)[*error code], [*error domain]]
                                          EmpID:[Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_EMPLOYEE_ID]];
#if (DEBUG_ERROR_LOG)
    NSLog(@"Error code %ld domain %@ Debug Description: %@", (long)[*error code], [*error domain], [*error debugDescription]);
#endif
    return result;
}

+ (void) logAuthErrorWithCode:(int) code
                  Description:(NSString *) desc
                      Message:(NSString *) msg
                   InFunction:(const char *) function {
    
    ErrorLog *errorLog = [[ErrorLog alloc] init];
    [errorLog addLogWithErrorCode:@(code)
                             Data:[[NSDate alloc] init]
                      Description:[NSString stringWithFormat:@"%s %@", function, msg]
                         Messsage:desc
                            EmpID:[Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_EMPLOYEE_ID]];
#if (DEBUG_ERROR_LOG)
    NSLog(@"%s Description: %@ message: %@", function, desc, msg);
#endif
}

@end
