//
//  ViewController.h
//  M ASSiST
//
//  Created by Suraj on 19/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class LoginModel;

@interface LoginViewCtlr : UIViewController<UITextFieldDelegate> {
    
    BOOL slide;
    
    LoginModel *loginPage;
}

@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgLoginBackground;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnEmergency;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIn;

- (IBAction)btnClearPressed:(id)sender;

- (void)clearLoginCredentials;
- (void)clearPassword;

- (void)signIn;

@end
