//
//  AppDelegate.h
//  M ASSiST
//
//  Created by Suraj on 19/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


/*!
 @category Universe on the move (AppDelegate)
 @class AppDelegate
 @superclass UIResponder
 @discussion This class instantiate the app.
 @encoding utf8
 @author Suraj Singh    
 */

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UIApplication *_application;                // Used by QunicyKit for crash logging
    
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;

- (NSString *)applicationDocumentsDirectory;
- (void) postAutoLoginAuthenticationWithData:(NSMutableDictionary *)responseData;

@end
