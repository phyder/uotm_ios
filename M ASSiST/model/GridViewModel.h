//
//  GridViewModel.h
//  Universe on the move
//
//  Created by Suraj on 29/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSTCollectionView.h"

@class GridViewCell;
@class Navigation;

@interface GridViewModel : NSObject <UIAlertViewDelegate> {
    
    NSMutableArray *data, *dataHeight;
    NSString *listType, *pageName, *pageTitle;
    NSMutableDictionary *content;
    
    NSDictionary *cssPage;
    
    NSString *jsonURL, *nextPage;
}

@property (nonatomic, weak) PSUICollectionViewController *controller;   // Controller view reference
@property (nonatomic) Navigation *navigation;

- (id) initDataForView:(PSUICollectionViewController *) view WithPage:(NSString *) page WithTitle:(NSString *) title AndWithData:(NSMutableDictionary *) pageContent;

- (void) openNotification;
- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist;

//- (void) countReceived:(NSDictionary *) resCount;
- (void) updatePendingApprovalCount;

// Request for Webpage Data Received
- (void) webPageDataReceived:(NSDictionary *) resData;

- (int) getRowCount;
- (UIView *) getFooterForExpense;

- (void) loadCell:(GridViewCell *) cell AtIndexPath:(NSIndexPath *) indexPath;

- (void) didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
- (void) didSelectItemAtIndex:(NSInteger) index;

- (void) dynalistDataLoad:(NSDictionary *) resData;
- (void)updateNotificationCount;
@end
