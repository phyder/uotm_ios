//
//  ESOPModel.m
//  Universe on the move
//
//  Created by Phyder on 28/10/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "ESOPModel.h"
#import "Util.h"
#import "Holder.h"
#import "Constant.h"
#import "Navigation.h"

@implementation ESOPModel

- (id) initDataForView:(UIViewController *) view
              WithPage:(NSString *) page WithTitle: (NSString *) title
           AndWithData:(NSMutableDictionary *) pageContent {
    
    self = [super init];
    if (self) {
        // Custom initialization
        pageName        = page;
        pageTitle       = title;
        _controller     = view;
        content         = pageContent;
        listType        = content[LIST_TYPE];
        [Holder sharedHolder].currentPageName = pageName;
        
        [Holder sharedHolder].title = pageTitle;
        
        NSLog(@"content type: %@ page name: %@", listType, pageName);
        
        if ([listType isEqual:PAGE_TYPE_MENULIST]) {
            
            data = [[NSMutableArray alloc] init];
            //data = [[content objectForKey:PAGE_TYPE_LISTS] objectForKey:PAGE_TYPE_LIST];
            [data addObjectsFromArray:content[PAGE_TYPE_LISTS][PAGE_TYPE_LIST]];
            data = [Util menuAuthorization:data];
            //NSLog(@"Data %@\nmenu code: %@", [Holder sharedHolder].loginInfo, menuAccessCode);
        }
        
        dataHeight = [[NSMutableArray alloc] init];
        //NSLog(@"%@\n\n\n%@\n\n%@", pageName, data, listType);
        
        // Initialize the navigation controller
        self.navigation  = [[Navigation alloc] initWithViewController:_controller PageData:content];
    }
    return self;
}

@end
