//
//  GridViewModel.m
//  Universe on the move
//  Created by Suraj on 29/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.

#import "GridViewModel.h"
#import "AppConnect.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Holder.h"
#import "Navigation.h"
#import "Util.h"
#import "GridViewCell.h"
#import "ResetMPinViewCtlr.h"
#import "IIViewDeckController.h"

@implementation GridViewModel

- (id) initDataForView:(PSUICollectionViewController *) view
              WithPage:(NSString *) page WithTitle: (NSString *) title
           AndWithData:(NSMutableDictionary *) pageContent {
    
    self = [super init];
    if (self) {
        // Custom initialization
        pageName        = page;
        pageTitle       = title;
        _controller     = view;
        content         = pageContent;
        listType        = content[LIST_TYPE];
        [Holder sharedHolder].currentPageName = pageName;
        
        [Holder sharedHolder].title = pageTitle;
        
        NSLog(@"content type: %@ page name: %@", listType, pageName);
        
        if ([listType isEqual:PAGE_TYPE_MENULIST]) {
            
            data = [[NSMutableArray alloc] init];
            //data = [[content objectForKey:PAGE_TYPE_LISTS] objectForKey:PAGE_TYPE_LIST];
            [data addObjectsFromArray:content[PAGE_TYPE_LISTS][PAGE_TYPE_LIST]];
            data = [Util menuAuthorization:data];
            //NSLog(@"Data %@\nmenu code: %@", [Holder sharedHolder].loginInfo, menuAccessCode);
        }
        
        dataHeight = [[NSMutableArray alloc] init];
        //NSLog(@"%@\n\n\n%@\n\n%@", pageName, data, listType);
        
        if ([pageName isEqualToString:@"expense-manager"]) {
            [self expenseCheck];
        }
        
        // Initialize the navigation controller
        self.navigation  = [[Navigation alloc] initWithViewController:_controller PageData:content];
    }
    return self;
}
- (void)updateNotificationCount {
    NSLog(@"This is grid view Notification method");
    
    [self.navigation initTopRightButton];
    
}
- (void) expenseCheck {
#ifdef DEBUG
    NSLog(@"%s", __FUNCTION__);
#endif
    
    NSString *grade = ([Holder sharedHolder].loginInfo)[@"grade"];
    NSString *empGrade = [Util getSettingUsingName:@"minimum-grade"];
    
    NSLog(@"this is %@ Data ",data);
    if ([grade intValue] > [empGrade intValue]) {
        
        NSMutableArray *newData = data.mutableCopy;
        
        for (NSDictionary *element in data) {
            if ([element[@"mac"] isEqualToString:@"EXP_APP"] || [element[@"mac"] isEqualToString:@"PTRL_CLM"]) {
                
            } else {
                [newData removeObject:element];
            }
        }
        
        data = newData;
        newData = nil;
        
//       // [data removeObjectAtIndex:0];
//        [data removeObjectAtIndex:0];
    }
     NSLog(@"%@ Data 2 ",data);
    
    if ([grade intValue] < [empGrade intValue]) {
        for (NSDictionary *element in data) {
            if ([element[@"mac"] isEqualToString:@"PTRL_CLM"]) {
                [data removeObject:element];
            }
        }
    }
}

- (void) updatePendingApprovalCount {
    
    NSString *strCount = nil;
    UIButton *btnPendingCount = nil;
    if ([pageName isEqualToString: @"leave-manager"]) {
        strCount = [Holder sharedHolder].pendingCountLeave;
        btnPendingCount = (UIButton *) [[self.controller view] viewWithTag:TAG_PENDING_LEAVE_APPROVAL_COUNT];
    } else if ([pageName isEqualToString:@"muster-manager"]) {
        
        if ([pageTitle isEqualToString: @"Muster Approvals"]) {
        strCount = [Holder sharedHolder].pendingCountMuster;
        btnPendingCount = (UIButton *) [[self.controller view] viewWithTag:TAG_PENDING_MUSTER_APPROVAL_COUNT];
        }
        else if ([pageTitle isEqualToString: @"Late Sign In Approvals"]) {
            strCount = [Holder sharedHolder].pendingCountLatePunch;
            btnPendingCount = (UIButton *) [[self.controller view] viewWithTag:TAG_PENDING_LATE_PUNCH_APPROVAL_COUNT];
        }
    } else if ([pageName isEqualToString:@"expense-manager"]) {
        strCount = [Holder sharedHolder].pendingCountConveyance;
        btnPendingCount = (UIButton *) [[self.controller view] viewWithTag:TAG_PENDING_CONVEYANCE_APPROVAL_COUNT];
    }
    
    
    //    NSLog(@"Data %@ count: %@", resCount, strCount);
    
    //This is new code
//    if ([strCount isEqualToString:@""]) {
//        strCount = [NSString stringWithFormat:@"%@",@"0"];;
//    }
    
//    btnPendingCount.titleLabel.textColor = [UIColor whiteColor];
//    [btnPendingCount setTitle:strCount forState:UIControlStateNormal];
    
//    int badgeCount = [[Holder sharedHolder].pendingCountLeave intValue] + [[Holder sharedHolder].pendingCountMuster intValue] + [[Holder sharedHolder].pendingCountLatePunch intValue] + [[Holder sharedHolder].pendingCountConveyance intValue];
//    
//    [Holder sharedHolder].pendingBadgeCount = badgeCount;
//    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
    //    NSLog(@"There is Grid view in stack at: %@", btnPendingCount);
}

- (void) openNotification {
    
    jsonURL = [Util getNotificationURL];
    [Util openNotificationWithURL:jsonURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self.controller];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:jsonURL];
}

- (int) getRowCount {
    
    // Return the number of rows for the list.
    int rowCount = 0;
    
    rowCount = (int) data.count;
    
    return rowCount;
}

- (UIView *) getFooterForExpense {
    
    UILabel *lblMsg = [[UILabel alloc] init];
    
    if ([pageName isEqualToString:@"expense-manager"]) {
        if ([([Holder sharedHolder].loginInfo)[@"grade"] intValue] > [[Util getSettingUsingName:@"minimum-grade"] intValue]) {
            lblMsg.frame = CGRectMake(10, 10, 300, 42);
            lblMsg.backgroundColor = [UIColor clearColor];
            lblMsg.text = [Util getSettingUsingName:@"minimum-grade-msg"];
            lblMsg.font = [UIFont fontWithName:APP_FONT_ROMAN size:13.0];
            lblMsg.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
            lblMsg.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            lblMsg.lineBreakMode = NSLineBreakByWordWrapping;
            lblMsg.numberOfLines = 0;
            [lblMsg sizeToFit];
        }
    } else {
        
        [lblMsg setFrame:CGRectMake(0, 0, 320, 0)];
    }
    
    return lblMsg;
}

- (void) loadCell:(GridViewCell *) cell AtIndexPath:(NSIndexPath *) indexPath {
    
   // NSLog(@"check load cell height: %@\n%@", [dataHeight objectAtIndex:indexPath.row], [data objectAtIndex:indexPath.row]);
   // NSLog(@"Data height: %@", [[data objectAtIndex:indexPath.row] objectForKey:@"title-css"]);
    if ([listType isEqualToString:PAGE_TYPE_MENULIST]) {
        [cell initCellWithData:data[indexPath.row]
                           CSS:[Util getLabelCSS:data[indexPath.row][@"title-css"]]
                           For:PAGE_TYPE_MENULIST];
    }
}

- (void) didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_MPIN_MENU];
    if([dict isEqual:[NSNull null]]) {
        [self didSelectItemAtIndex:indexPath.row];
    } else {
        NSString *mac = data[indexPath.row][@"mac"];
        if ([[dict allKeys] containsObject:mac]) {
            // contains key
            NSString *value = [NSString stringWithFormat:@"%@",[dict objectForKey:mac]];
            if([value isEqualToString:@"1"]) {
                Util *util = [[Util alloc] init];
                [util showVerifyViewControllerOnController:self.controller Model:self Index:indexPath.row];
            } else {
                [self didSelectItemAtIndex:indexPath.row];
            }
        } else {
            [self didSelectItemAtIndex:indexPath.row];
        }
    }
}

- (void) didSelectItemAtIndex:(NSInteger) index {
    nextPage = data[index][@"name"];
    NSDictionary *gridNextPageData = [Util getPage:nextPage];
    //NSLog(@"data: %@", gridNextPageData);
    
    if ([gridNextPageData[@"type"] isEqualToString:@"web"]) {
        [Util OpenWebWithNextPage:nextPage AndParam:gridNextPageData[@"params"] OnView:self.controller];
    }
    if ([gridNextPageData[@"type"] isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        // TODO: Open Reset controller
        if([nextPage isEqualToString:@"reset-mpin"]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            ResetMPinViewCtlr *resetMPinViewController = [storyboard instantiateViewControllerWithIdentifier:@"resetmpin"];
            IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            
            //[nav pushViewController:resetMPinViewController animated:YES];
            [Util pushWithFadeEffectOnNavigationController:nav ViewController:resetMPinViewController];
        } else if([nextPage isEqualToString:@"deregister"]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            ResetMPinViewCtlr *deregisterViewController = [storyboard instantiateViewControllerWithIdentifier:@"deregister"];
            IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            
            //[nav pushViewController:deregisterViewController animated:YES];
            [Util pushWithFadeEffectOnNavigationController:nav ViewController:deregisterViewController];
        } else {
            jsonURL = [Util openDynaListWithNextPage:nextPage AfterFinishCall:@selector(finishedLoadingDynalistData:) OnView:self.controller];
        }
    }
    if ([gridNextPageData[@"type"] isEqualToString:@"offer"]) {
        [Util openEngagementSelection:nextPage OnView:self.controller.navigationController];
    }
}

- (void) webPageDataReceived:(NSDictionary *) resData {
    
    //NSLog(@"Data received is: %@", resData);
    [Util writeDataInJS:resData[@"data"]];
    NSDictionary *gridNextPageData = [Util getPage:[Holder sharedHolder].nextWebPage];
    //NSLog(@"page data: %@", gridNextPageData);
    [Util OpenWebPageAfterDataLoadWithNextPage:nextPage AndParam:gridNextPageData[@"params"] OnView:self.controller];
}

- (void) dynalistDataLoad:(NSDictionary *) resData {
    
    if ([Util serverResponseCheckNullAndSession:resData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:PAGE_TYPE_LIST In:resData InFunction:__FUNCTION__ Message:[NSString stringWithFormat:@"%@ parameter", PAGE_TYPE_LIST] Silent:NO]) {
            int dataCount = (int) [resData[PAGE_TYPE_LIST] count];
//            NSLog(@"response count %d page: %@", dataCount, nextPage);
            
            //Checks the data count in list
            if (dataCount==0) {
                
                UIAlertView *noData =[[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                message:[Util getSettingUsingName:DYNA_LIST_NODATA_MSG]
                                                               delegate:nil
                                                      cancelButtonTitle:ALERT_OPT_OK
                                                      otherButtonTitles:nil];
                [noData show];
            } else {
                [Util finishLoadingDynaListWithata:resData NextPage:nextPage JSONURL:jsonURL OnView:self.controller];
            }
        }
    }
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:ALERT_OPT_OK]) {
//        [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:NO];
    }
}

@end
