//
//  OfferDetail.h
//  Universe on the move
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarOfferDetail : UITableViewCell {
    
    UIImage *imageMore, *imageContactPerson, *imageContactNo;
    NSString *strOfferId, *offer, *offervalidity, *location, *strSubTitle, *strDetail, *shortDetail, *strRecent;
    NSString *strContactPerson, *strContactNo1, *strContactNo2, *strContactURL;
    
    float heightCell1, heightCell2, heightCell3, heightCell4, heightCell5;
    float subTitleHeight, detailHeight, validityheight, locationheight, offerHeight, contactPersonHeight, contactNumberheight, contactURLHeight;
}

@property (nonatomic, weak) UIViewController *controller;
@property (weak, nonatomic) IBOutlet NSDictionary *data;

@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblValidity;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblContactPerson;
@property (weak, nonatomic) IBOutlet UILabel *lblContactNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblContactNumber2;
@property (weak, nonatomic) IBOutlet UILabel *lblContactURL;
@property (weak, nonatomic) IBOutlet UILabel *lblOffer;

@property (weak, nonatomic) IBOutlet UIButton *btnMore;

@property (weak, nonatomic) IBOutlet UIImageView *imgNewOffer;
@property (weak, nonatomic) IBOutlet UIImageView *imgContactPerson;
@property (weak, nonatomic) IBOutlet UIImageView *imageContactNumber;

- (id) initWithData:(NSMutableDictionary *) data UIViewController:(UIViewController *)view;
- (NSString *) getOfferId;

//- (void) btnIMInterestedPressed;
//- (void) resBtnIMInterested:(NSDictionary *) resData;

//- (UILabel *) setHeaderTitle:(UILabel *) titleView;
//- (UIBarButtonItem *) getIMInterestedButton;

- (void) getCellOneWithCell: (CarOfferDetail *) cell;
- (void) getCellTwoWithCell: (CarOfferDetail *) cell;
- (void) getCellThreeWithCell: (CarOfferDetail *) cell;
- (void) getCellFourWithCell: (CarOfferDetail *) cell;
- (void) getCellFiveWithCell: (CarOfferDetail *) cell;

- (float) getCellOneHeight;
- (float) getCellTwoHeight;
- (float) getCellThreeHeight;
- (float) getCellFourHeight;
- (float) getCellFiveHeight;

@end
