//
//  NotificationCell.m
//  engage
//
//  Created by Suraj on 22/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import "NotificationCell.h"

#import "Constant.h"
#import "Util.h"

@implementation NotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    UIView *bgColorView = [[UIView alloc] init];
    NSArray *color      = [[Util getSettingUsingName:LIST_TOUCHUP_COLOR] componentsSeparatedByString:@"|"];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame      = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, cellHeight);
    gradient.colors     = @[(id)[[Util colorWithHexString:color[0] alpha:@"1.0f"] CGColor], (id)[[Util colorWithHexString:color[1] alpha:@"1.0f"] CGColor]];
    [bgColorView.layer insertSublayer:gradient atIndex:0];
    self.selectedBackgroundView = bgColorView;
}

- (UITableViewCell *) cellForRowAtIndexPath:(NSIndexPath *)indexPath
                                   WithData:(NSDictionary *) data
                                        CSS:(NSDictionary *) css
                                     Height:(float) height {
    
    //NSLog(@"data: %@", data);
    NSString *title;
    NSDictionary *cssData;
    
    title = data[LIST_TITLE];
    if (![title isEqualToString:EMPTY]) {
        
        cssData = [Util getLabelCSS:css[LIST_TITLE_CSS]];
        //NSLog(@"title %@ css: %@", title, cssData);
        
        [Util initLabel:_lblTitle withTitle:title AndCSS:cssData];
        
        _lblTitle.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
     // _lblTitle.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [_lblTitle setLineBreakMode:NSLineBreakByWordWrapping];
        [_lblTitle setNumberOfLines:0];
        [_lblTitle sizeToFit];
    }
    
    title = data[LIST_SUB_TITLE];
    if (![title isEqualToString:EMPTY]) {
        
        cssData = [Util getLabelCSS:css[LIST_SUB_TITLE_CSS]];
        //NSLog(@"title %@ css: %@", title, cssData);
        
        [Util initLabel:_lblSubTitle withTitle:title AndCSS:cssData];
        NSArray *bound = [cssData[CSS_LABEL_TEXT_BOUNDS] componentsSeparatedByString:@"|"];
        _lblSubTitle.frame = CGRectMake([bound[0] floatValue], [bound[1] floatValue]+_lblTitle.frame.size.height, [bound[2] floatValue], [bound[3] floatValue]);
        
        [_lblSubTitle setLineBreakMode:NSLineBreakByWordWrapping];
        [_lblSubTitle setNumberOfLines:0];
        [_lblSubTitle sizeToFit];
    }
    
    title = data[LIST_DESC];
    if (![title isEqualToString:EMPTY]) {
        
        cssData = [Util getLabelCSS:css[LIST_DESC_CSS]];
        //NSLog(@"title %@ css: %@", title, cssData);
        
        [Util initLabel:_lblDesc withTitle:title AndCSS:cssData];
        NSArray *bound = [cssData[CSS_LABEL_TEXT_BOUNDS] componentsSeparatedByString:@"|"];
        _lblDesc.frame = CGRectMake([bound[0] floatValue], [bound[1] floatValue]+_lblSubTitle.frame.origin.y+_lblSubTitle.frame.size.height, [bound[2] floatValue], [bound[3] floatValue]);
        
        [_lblDesc setLineBreakMode:NSLineBreakByWordWrapping];
        [_lblDesc setNumberOfLines:0];
        [_lblDesc sizeToFit];
    }
    
    title = data[LIST_ACTION_DATE];
    if (![title isEqualToString:EMPTY]) {
        
        cssData = [Util getLabelCSS:css[LIST_ACTION_DATE_CSS]];
        //NSLog(@"title %@ css: %@", title, cssData);
        
        [Util initLabel:_lblActionDate withTitle:title AndCSS:cssData];
        
        [_lblActionDate setLineBreakMode:NSLineBreakByWordWrapping];
        [_lblActionDate setNumberOfLines:0];
        [_lblActionDate sizeToFit];
    }
    
    _details = data[LIST_DETAILS];
    
    if ([_details isEqualToString:PAGE_FALSE]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.userInteractionEnabled = NO;
    } else {
        // Set the cell background color, color defined in setting.xml
        cellHeight          = height;
        [self setSelected:YES animated:YES];
    }
    
    if ([_details isEqualToString:PAGE_TRUE]) {
        [_imgRightDetails setImage:[Util getImageAtDocDirWithName:@"right-arrow.png"]];
    }
    
    _pageName   = data[@"page-name"];
    _url        = data[LIST_URL];
    _params     = data[@"params"];
    
    return nil;
}

@end
