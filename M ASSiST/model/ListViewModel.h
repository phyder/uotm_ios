//
//  ListViewModel.h
//  M ASSiST
//
//  Created by Suraj on 24/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <QuartzCore/QuartzCore.h>

@class Navigation;
@class ListViewCell;

@interface ListViewModel : NSObject <UIAlertViewDelegate, UISearchBarDelegate, MFMessageComposeViewControllerDelegate> {
    
    NSArray *request, *requestEmpID;
    NSMutableArray *data, *dataHeight, *filteredData;
    NSString *listType, *pageName;
    NSMutableDictionary *content, *cssList;
    NSDictionary *item, *cssPage;
    
    NSString *jsonURL, *nextPage;
    UISearchBar *searchBar;
    CGFloat height;
    UILabel *pageTitle;
    NSString *holidayTitleText;
    BOOL isFiltered;
    
}

@property (nonatomic) UIAlertView *showResponse;

@property (nonatomic, weak) UIViewController *controller;   // Controller view reference
@property (nonatomic) Navigation *navigation;
@property (nonatomic) BOOL isSelected;

- (id) initDataForView:(UIViewController *) view WithPage:(NSString *) page AndWithData:(NSMutableDictionary *) pageContent;       //inittalize the data of the controller
- (void) initDynaListWithURL:(NSString *) url Data:(NSDictionary *) jsonData;

- (void) updatePendingApprovalCount;
//- (void) initControl;

- (void) openNotification;

- (int) getRowCount;        // Return the number of rows for the list.

- (int) heightForHeader;
- (UIView *) loadSearchBar;
- (int) heightForFooter;
- (UIView *) loadLegendBar;

- (int) getHeightOfCellAtIndexPath:(NSIndexPath *) indexPath; 
- (void) loadCell:(ListViewCell *) cell AtIndexPath:(NSIndexPath *) indexPath;

- (void) viewDidAppear;

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) dynalistDataLoad:(NSDictionary *) resData;
- (void) dynalistDataRefresh:(NSDictionary *) resDynalist;
- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist;

- (void) refreshDynalist;

- (void) selectAll:(id)sender;
- (void) approve:(id)sender;
- (void) reject:(id)sender;

- (void) responseMultipleApproveReject:(NSDictionary *)response;

- (void) updateNotificationCount;
@end
