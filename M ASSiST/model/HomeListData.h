//
//  ListData.h
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HomeListViewCell;

@interface HomeListData : NSObject {
    
    float heightCell1;
    float titleHeight, subTitleHeight, offerHeight, locationHeight, validityHeight;
}

@property (nonatomic, weak) UIViewController *controller;

@property (nonatomic) IBOutlet NSArray *data;
@property (nonatomic) IBOutlet NSMutableArray *validityDate;

@property (nonatomic) NSMutableArray *cellArray;

- (id)initWithData:(NSArray *)listData ViewController:(UIViewController *) controller;

- (void) initTable:(UITableView *) tableView WithSection:(NSInteger) sections WithNumberOfRows:(NSInteger) numsRows AndCSS:(NSDictionary *) css;

- (HomeListViewCell *) cellAtRow:(NSInteger) row;

- (void) getOfferDataWithIndexPath:(NSIndexPath *) indexPath;

- (void) openOfferDetailsWithData:(NSDictionary *) offerData AndNavigation: (UINavigationController *) navigation;

- (float) getHeightforindexpath:(NSIndexPath *) indexPath AndCSS:(NSDictionary *) css;

@end
