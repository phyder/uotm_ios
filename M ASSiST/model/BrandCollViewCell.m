//
//  BrandCollViewCell.m
//  Universe on the move
//
//  Created by Suraj on 16/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "BrandCollViewCell.h"

#import "AppConnect.h"
#import "Constant.h"
#import "Util.h"

@implementation BrandCollViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) initWithData: (NSDictionary *) data {
    
    [self.btnBrandRecentCount setBackgroundImage:[Util getImageAtDocDirWithName:@"star.png"] forState:UIControlStateNormal];
    self.btnBrandRecentCount.titleLabel.font    = [UIFont fontWithName:APP_FONT_ROMAN size:15.0f];
    [self.btnBrandRecentCount setTitle:data[CAR_OFFER_BRAND_RECENT] forState:UIControlStateNormal];
    
    if (self.imgBrandImage.image == nil) {
        
        self.imgBrandImage.animationImages = @[[Util getImageAtDocDirWithName:@"loader1.png"],
                                           [Util getImageAtDocDirWithName:@"loader2.png"],
                                           [Util getImageAtDocDirWithName:@"loader3.png"],
                                           [Util getImageAtDocDirWithName:@"loader4.png"]];
        self.imgBrandImage.animationDuration = 0.8;
        [self.imgBrandImage startAnimating];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getImageForImageName:data[CAR_OFFER_BRAND_IMAGE]];
        });
        
    }
}

- (void) getImageForImageName:(NSString *) imageName {
    
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getBrandImageWithName:imageName
                AfterFinishCall:@selector(getImageFinishedWithData:)
                       onObject:self];
}

- (void) getImageFinishedWithData:(NSData *) data {
    
    [self.imgBrandImage stopAnimating];
    self.imgBrandImage.image    = [UIImage imageWithData:data];//[[UIImage alloc] initWithData:data];
    //NSLog(@"image holder: %@", data);
}

@end
