//
//  ListData.m
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "HomeListData.h"

#import "HomeListViewCell.h"

#import "OtherDetailPageViewCtlr.h"

#import "AppConnect.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Util.h"

@implementation HomeListData

- (id)initWithData:(NSArray *)listData ViewController:(UIViewController *) viewController {
    
    self = [super init];
    if (self) {
        // Initialization code
        _controller = viewController;
        _data = listData;
        self.validityDate = [[NSMutableArray alloc] init];
        
        for (NSMutableDictionary *item in _data) {
            //NSLog(@"Date formatted: %@", [Util convertDateFromString:[item objectForKey:LIST_RES_VALIDITY] WithType:MSSQL_TO_NSDATE_DATE_CONVERSION]);
            [self.validityDate addObject:[Util convertDateFromString:item[LIST_RES_VALIDITY]
                                                            WithType:MSSQL_TO_NSDATE_DATE_CONVERSION]];
        }
    }
    return self;
}

- (void) initTable:(UITableView *) tableView WithSection:(NSInteger) sections WithNumberOfRows:(NSInteger) numsRows AndCSS:(NSDictionary *) css {
    
    self.cellArray = [[NSMutableArray alloc] initWithCapacity:3];
    static NSString *CellIdentifier = @"ListCell";
    
//    NSLog(@"data: %ld", (long)indexPath.row);
    for (int inc=0; inc<numsRows; ++inc) {
//        NSLog(@"Init section %d row: %d", sections, inc);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:inc inSection:sections];
//        HomeListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        HomeListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [cell initCellWithData:_data[indexPath.row] AndCSS:css];
        cell.height = @([self getHeightforindexpath:indexPath AndCSS:css]);
        [self.cellArray addObject:cell];
    }
    
}

- (HomeListViewCell *) cellAtRow:(NSInteger) row {
    
    return (self.cellArray)[row];
}

- (void) getOfferDataWithIndexPath:(NSIndexPath *) indexPath {
    
    //Connection *connection = [[Connection alloc] init];
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(responseReceivedHomeDetails:) onObject:self.controller];
    [conn getOfferDetails:self.data[indexPath.row][LIST_RES_ID]];
}

- (void) openOfferDetailsWithData:(NSDictionary *) offerData AndNavigation: (UINavigationController *) navigation {
    
    if ([Util serverResponseCheckNullAndSession:offerData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:offerData InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            
            @try {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                
                OtherDetailPageViewCtlr *detailViewCtlr = (OtherDetailPageViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"otherofferdetailpage"];
                detailViewCtlr.data = offerData[@"data"];
                detailViewCtlr.offerType = OFFER_HOME;
                [Util pushWithFadeEffectOnNavigationController:navigation ViewController:detailViewCtlr];
            } @catch (NSException *exception) {
                NSString *strLogMessage = @"Not able to open Home Detail View";
#if DEBUG_ERROR_LOG
                NSLog(@"%@", strLogMessage);
#endif
                [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
                [Util showTechnicalError];
            }
        }
    }
}

- (float) getHeightforindexpath:(NSIndexPath *) indexPath AndCSS:(NSDictionary *) css {
    
    NSString *text          = _data[indexPath.row][LIST_RES_TITLE];
    NSDictionary *lblCSS    = [Util getLabelCSS:css[@"title-css"]];
    titleHeight             = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                      style:lblCSS[@"font-style"]
                                                                       size:lblCSS[@"font-size"]]
                                       constrainedToSize: (CGSize){220.0f, CGFLOAT_MAX}
                                           lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                    = _data[indexPath.row][LIST_RES_SUB_TITLE];
    lblCSS                  = [Util getLabelCSS:css[@"sub-title-css"]];
    subTitleHeight          = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                      style:lblCSS[@"font-style"]
                                                                       size:lblCSS[@"font-size"]]
                                       constrainedToSize: (CGSize){220.0f, CGFLOAT_MAX}
                                           lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                    = _data[indexPath.row][LIST_RES_OFFER];
    lblCSS                  = [Util getLabelCSS:css[@"offer-css"]];
    offerHeight             = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                      style:lblCSS[@"font-style"]
                                                                       size:lblCSS[@"font-size"]]
                                       constrainedToSize: (CGSize){220.0f, CGFLOAT_MAX}
                                           lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                    = _data[indexPath.row][LIST_RES_LOCATION];
    lblCSS                  = [Util getLabelCSS:css[@"location-css"]];
    locationHeight          = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                      style:lblCSS[@"font-style"]
                                                                       size:lblCSS[@"font-size"]]
                                       constrainedToSize: (CGSize){125.0f, CGFLOAT_MAX}
                                           lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                    = _data[indexPath.row][LIST_RES_VALIDITY];
    lblCSS                  = [Util getLabelCSS:css[@"validity-css"]];
    validityHeight          = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                      style:lblCSS[@"font-style"]
                                                                       size:lblCSS[@"font-size"]]
                                       constrainedToSize: (CGSize){75.0f, CGFLOAT_MAX}
                                           lineBreakMode:NSLineBreakByWordWrapping].height;
    
    return heightCell1 = 10 + titleHeight + subTitleHeight + offerHeight + MAX(locationHeight, validityHeight) + 10;
}

@end
