//
//  AutoLoginModel.m
//  Universe on the move
//
//  Created by Phyder on 20/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "AutoLoginModel.h"
#import "Util.h"
#import "Constant.h"
#import "Navigation.h"
#import "Holder.h"
#import "Authentication.h"
#import "AutoLoginViewCtlr.h"
#import <LocalAuthentication/LocalAuthentication.h>
//#import "iphoneos/include/EzTokenSDK/EzTokenSDK.h"



@implementation AutoLoginModel
  id delegate;


- (void) initDataWithView:(UIViewController *) controllerView {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    _controller = controllerView;

    data        = [Util getPage:PAGE_LOGIN];
    css         = [Util getPageCSS:data[PAGE_CSS]];//[Holder sharedHolder].css;
    
    [[_controller view] setTag:TAG_IDENTIFY_LOGIN_VIEW];
}

- (NSDictionary *) getPageData {
    
    return data;
}

- (void) initNavControlWithUser:(UITextField *) txtUserName
                          MPin1:(UITextField *) txtMPin1
                          MPin2:(UITextField *) txtMPin2
                          MPin3:(UITextField *) txtMPin3
                          MPin4:(UITextField *) txtMPin4
                      Emergency:(UIButton *) emergency
                         SignIn:(UIButton *) signIn
                        ResetMPin:(UIButton *)resetMPin {
    
    username        = txtUserName;
    mPin1           = txtMPin1;
    mPin2           = txtMPin2;
    mPin3           = txtMPin3;
    mPin4           = txtMPin4;
    btnEmergency    = emergency;
    btnSignIn       = signIn;
    btnResetMPin    = resetMPin;
    
    
    [self initEmergencyAndHelpButton];

     [self fingerScan];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"notification id dsfadf"
//                                                    message:[NSString stringWithFormat:@"%@", [Holder sharedHolder].notificationId]
//                                                   delegate:nil cancelButtonTitle:@"No"
//                                          otherButtonTitles:@"Yes", nil];
//    [alert show];
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self.controller PageData:data];
    
    [self sendLogtoServer];
}

#pragma mark - Log Send -
/**
 * Background thread for error log dump forward to Server
 */
- (void) sendLogtoServer {
    
    [NSThread detachNewThreadSelector:@selector(backgroundLogSend) toTarget:self withObject:nil];
}

- (void) backgroundLogSend {
    
    // Dummps error log to the server
    [Util sendErrorLog];
}

- (void) initEmergencyAndHelpButton {
    int btnFingerScanYPos = 305;
    int btnEmergencyYPos = 315;
    int intSignInYPos = 220;
    int intResetYPos = 270;
    if ([UIScreen mainScreen].bounds.size.height==568) {
        NSLog(@"\n\n\n4 inch retina\n\n\n");
        btnEmergencyYPos    = 395;
        intSignInYPos       = 270;
        intResetYPos        = 320;
        btnFingerScanYPos   = 365;
    }
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    NSString * devicemodel = [self platformType:platform];
    NSLog(@"model name %@",devicemodel);
    
    
    btnEmergency = [[UIButton alloc] initWithFrame:CGRectMake(67, btnEmergencyYPos, 192, 44)];
    [btnEmergency setImage:[Util getImageAtDocDirWithName:@"emergency-number.png"] forState:UIControlStateNormal];
    [btnEmergency addTarget:self action:@selector(btnEmergencyCallPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.controller.view addSubview:btnEmergency];
    
    btnSignIn = [[UIButton alloc] initWithFrame:CGRectMake(115, intSignInYPos, 96, 32)];
    [btnSignIn setImage:[Util getImageAtDocDirWithName:@"sign-in.png"] forState:UIControlStateNormal];
    [btnSignIn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    // /[self.controller.view addSubview:btnSignIn];
    
    btnResetMPin = [[UIButton alloc] initWithFrame:CGRectMake(115, intResetYPos, 96, 32)];
    [btnResetMPin setImage:[Util getImageAtDocDirWithName:@"resetmpin.png"] forState:UIControlStateNormal];
    [btnResetMPin addTarget:self action:@selector(resetMPin) forControlEvents:UIControlEventTouchUpInside];
    [self.controller.view addSubview:btnResetMPin];
    
    btnFingerScan = [[UIButton alloc] initWithFrame:CGRectMake(115, btnFingerScanYPos, 96, 32)];
    [btnFingerScan setImage:[Util getImageAtDocDirWithName:@"fingerprint.png"] forState:UIControlStateNormal];
    //[btnFingerScan setBackgroundColor:[UIColor blueColor]];
    [btnFingerScan addTarget:self action:@selector(fingerScan) forControlEvents:UIControlEventTouchUpInside];
   // [self.controller.view addSubview:btnFingerScan];
    
    NSArray *lists = data[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list = lists[i];
        if ([list[@"type"] isEqualToString:@"button"] && [list[@"name"] isEqualToString:@"emergency-call"]) {
            controlData = list;
            strEmergencyNumber = controlData[@"number"];
            break;
        }
    }
    
    //    This code gives button fall effect during back transition
    //    UIImage *imgTopbarRight = [Util getImageAtDocDirWithName:@"sign-in.png"];
    //    btnSignIn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btnSignIn setImage:imgTopbarRight forState:UIControlStateNormal];
    //    btnSignIn.frame = CGRectMake(130, intSignInYPos, imgTopbarRight.size.width, imgTopbarRight.size.height);
    //    [btnSignIn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    //    [view.view addSubview:btnSignIn];
}
- (NSString *) platformType:(NSString *)platform
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

- (void) hideKeyboard {
    
    [username resignFirstResponder];
    [mPin1 resignFirstResponder];
    [mPin2 resignFirstResponder];
    [mPin3 resignFirstResponder];
    [mPin4 resignFirstResponder];
}

- (void) clearLoginCredentials {
    
    username.text = @"";
    mPin1.text = @"";
    mPin2.text = @"";
    mPin3.text = @"";
    mPin4.text = @"";
}
- (void) clearPassword{
    mPin1.text = @"";
    mPin2.text = @"";
    mPin3.text = @"";
    mPin4.text = @"";
}
- (NSString *) getEmergencyNumber {
    
    return strEmergencyNumber;
}

- (void) btnEmergencyCallPressed {
    
    NSURL *callNumber = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", strEmergencyNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:callNumber]) {
        [[UIApplication sharedApplication] openURL:callNumber];
    } else {
        [Util featureNotSupported];
    }
}

- (void) signIn {
    
   // [self hideKeyboard];
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self.controller AndModel:self];
    NSString *mPin = [NSString stringWithFormat:@"%@%@%@%@",mPin1.text,mPin2.text,mPin3.text,mPin4.text];
    [[Holder sharedHolder].auth requestAutoLoginWithUserName:username.text mPin:mPin bio:@""];
    [self clearPassword];
    
//    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
//    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    imagePicker.delegate = self;
//    [self.controller presentModalViewController:imagePicker animated:YES];
//    
//}
//
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//    [self.controller dismissModalViewControllerAnimated:YES];
//}
//
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    UIImage * image = [info objectForKey:UIImagePickerControllerEditedImage];
//    
//    // You have the image. You can use this to present the image in the next view like you require in `#3`.
//    
//    [self.controller dismissModalViewControllerAnimated:YES];
}

- (void) resetMPin {
    AutoLoginViewCtlr *ctlr = (AutoLoginViewCtlr *) _controller;
    [ctlr initResetViewController];
}

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData {
    NSLog(@"SignIn response: %@", responseData);
    [[Holder sharedHolder].auth postAutoLoginAuthenticationWithData:responseData];
}

- (void) dealloc {
    self.navigation = nil;
}
-(void) fingerScan{
    
        NSError *error;
        int result = [EzTokenSDK isTouchAuthSupported:&error];
        NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    
       if (result == 0 ) {
           
           if ( ![[defaul stringForKey:LOCAL_DB_IS_FINGER_ACTIVE]  isEqual: @"Yes"]) {
          
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                           message:@"Do you want to use Touch ID For Login"
                                                          delegate:self
                                                 cancelButtonTitle:@"No"
                                                 otherButtonTitles:@"Yes",nil];
               [alert show];
               alert.tag = 1;
           }
            else{
                NSLog(@"TouchID Authentication is supported on the device.");
                NSString *dialogueMsg = @"Scan your finger";
                NSError *error = nil;
                delegate = self;
                [EzTokenSDK doTouchAuth: dialogueMsg  delegate: delegate error: &error];
                
                
                
//                self.progressBar                    = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
//                self.progressBar.mode               = MBProgressHUDModeCustomView;
//                self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
//                self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
//                self.progressBar.delegate           = self;
//                self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
//                self.progressBar.dimBackground      = YES;
//                self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@",@"Please Wait"];

            }
           
        } else if (result == -1) {
            NSLog(@"TouchID Authentication is not supported on the device.");
            return;
        } else if (result == 1) {
            NSLog(@"TouchID Authentication is supported on the device but there are no fingerprints enrolled on the device.");
            NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
            [defaul setObject:@"" forKey:LOCAL_DB_IS_FINGER_ACTIVE];
            [defaul synchronize];

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                            message:@"No finger Prints Enrolled."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            alert.tag = 2;
        
            return;
        } else
        {
            NSLog(@"Error occured while initializing touchId");
    
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please try again"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    
}

-(void) authResult:(int)result;
{
    if (result == 0) {
        //Authentication was successful
        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.progressBar hide:YES];
//            [self.progressBar removeFromSuperview];
//            self.progressBar = nil;
//   
            [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self.controller AndModel:self];
            [[Holder sharedHolder].auth requestAutoLoginWithUserName:username.text mPin:EMPTY bio:[NSString stringWithFormat:@"%@|success", username.text]];
            
        });

    }
    
//    else if (result == -1) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed"
//                                                        message:@"Authentication failed."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        [alert show];
//        
//    }
    
//    else if (result == 1) {
    //TouchID Authentication is supported on the device but there are no fingerprints enrolled on the device

//        return;
//    } else if (result == -2) {
        //User cancelled the authentication dialog.
//        return;
//    }

//    if (self.progressBar != nil) {
//        [self.progressBar hide:YES];
//        [self.progressBar removeFromSuperview];
//        self.progressBar = nil;
//    }
   
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    NSLog(@"Alert title: %@", title);
    if (alertView.tag == 1) {
        
        if([title isEqualToString:@"Yes"]) {
            
            NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
            [defaul setObject:@"Yes" forKey:LOCAL_DB_IS_FINGER_ACTIVE];
            [defaul synchronize];
            
            NSLog(@"TouchID Authentication is supported on the device.");
            NSString *dialogueMsg = @"Scan your finger";
            NSError *error = nil;
            delegate = self;
            [EzTokenSDK doTouchAuth: dialogueMsg  delegate: delegate error: &error];
//            self.progressBar                    = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
//            self.progressBar.mode               = MBProgressHUDModeCustomView;
//            self.progressBar.color              = [UIColor colorWithWhite:0.75f alpha:1.0f];
//            self.progressBar.customView         = [Util getLoaderImageWithTarget:self Action:@selector(loaderCancelPressed)];
//            self.progressBar.delegate           = self;
//            self.progressBar.labelText          = [Util getSettingUsingName:POPUP_TITLE];
//            self.progressBar.dimBackground      = YES;
//            self.progressBar.detailsLabelText   = [NSString stringWithFormat:@"%@",@"Please Wait"];
        }
    }else if(alertView.tag == 2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

@end
