//
//  OfferDetail.m
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "OtherOfferDetail.h"

#import "OtherDetailPageViewCtlr.h"
#import "Constant.h"
#import "Util.h"

@implementation OtherOfferDetail

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithData:(NSMutableDictionary *) data UIViewController:(UIViewController *)view {
    
    self = [super init];
    if (self) {
        // Initialization code
        _data = data;
        _controller = view;
        //NSLog(@"data: %@", _data);
        
        [self initData];
    }
    return self;
}

- (void) initData {
    
    NSString *stringFromDate = [Util convertDateFromString:_data[OTHER_DETAIL_OFFER_END_DATE] WithType:MSSQL_TO_NSDATE_DATE_CONVERSION];
    //NSLog(@"date: %@, %@", startDate, stringFromDate);
    
    imageMore           = [Util getImageAtDocDirWithName:@"more.png"];
    imageContactPerson  = [Util getImageAtDocDirWithName:@"contact.png"];
    imageContactNo      = [Util getImageAtDocDirWithName:@"phone.png"];
    
    strOfferId          = self.data[OTHER_DETAIL_OFFER_ID];
    strSubtitle         = self.data[DETAIL_OFFER_SUB_TITLE];
    //strips the characters '\r\n', '/r/n' & <BR> in more content
    strDetail       = [Util handleNewLine:self.data[DETAIL_OFFER_DETAIL]];
    
    if ([strDetail length] > 40) {
        shortDetail     = [NSString stringWithFormat:@"%@ ...", [strDetail substringToIndex:40]];
    } else {
        shortDetail     = [NSString stringWithFormat:@"%@", strDetail];
    }
    
    offervalidity       = [NSString stringWithFormat:@"Valid till: %@", stringFromDate];
    offer               = [NSString stringWithFormat:@"ICICI employee %@", _data[OTHER_DETAIL_OFFER_OFFER]];
    offer               = [Util handleNewLine:offer];
    location            = [NSString stringWithFormat:@"Location: %@", _data[OTHER_DETAIL_OFFER_CONTACT_LOCATION]];
    strContactPerson    = self.data[OTHER_DETAIL_OFFER_CONTACT_NAME];
    if ([strContactPerson isEqualToString:@"-"] || [strContactPerson isEqualToString:EMPTY]) {
        strContactPerson = NOT_AVAILABLE;
    }
    strContactNo1       = self.data[OTHER_DETAIL_OFFER_CONTACT_NO_1];
    if ([strContactNo1 isEqualToString:@"-"] || [strContactNo1 isEqualToString:EMPTY]) {
        strContactNo1 = NOT_AVAILABLE;
    }
    strContactNo2       = self.data[OTHER_DETAIL_OFFER_CONTACT_NO_2];
    if ([strContactNo2 isEqualToString:@"-"] || [strContactNo2 isEqualToString:EMPTY]) {
        strContactNo2 = NOT_AVAILABLE;
    }
    strContactURL       = self.data[OTHER_DETAIL_OFFER_CONTACT_URL];
    if ([strContactURL isEqualToString:@"-"] || [strContactURL isEqualToString:EMPTY]) {
        strContactURL = NOT_AVAILABLE;
    }
    
    subTitleHeight      = (float) [_data[OTHER_DETAIL_OFFER_SUB_TITLE]
                                   sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17.0]
                                   constrainedToSize: (CGSize){280.0f, CGFLOAT_MAX}
                                   lineBreakMode:NSLineBreakByWordWrapping].height;
    
    detailHeight        = (float) [shortDetail sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                          constrainedToSize: (CGSize){180.0f, CGFLOAT_MAX}
                                              lineBreakMode:NSLineBreakByWordWrapping].height;
    
    validityheight      = (float) [offervalidity sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                            constrainedToSize: (CGSize){260, CGFLOAT_MAX}
                                                lineBreakMode:NSLineBreakByWordWrapping].height;
    
    locationheight      = (float) [location sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                       constrainedToSize: (CGSize){260, CGFLOAT_MAX}
                                           lineBreakMode:NSLineBreakByWordWrapping].height;
    
    offerHeight         = (float) [offer sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                    constrainedToSize: (CGSize){280, CGFLOAT_MAX}
                                        lineBreakMode:NSLineBreakByWordWrapping].height;
    
    contactPersonHeight = (float) [strContactPerson sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                               constrainedToSize: (CGSize){225, CGFLOAT_MAX}
                                                   lineBreakMode:NSLineBreakByWordWrapping].height;
    
    contactNumberheight = (float) [strContactNo1 sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                            constrainedToSize: (CGSize){225, CGFLOAT_MAX}
                                                lineBreakMode:NSLineBreakByWordWrapping].height;
    
    contactURLHeight    = (float) [strContactURL sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                            constrainedToSize: (CGSize){260, CGFLOAT_MAX}
                                                lineBreakMode:NSLineBreakByWordWrapping].height;
    
    heightCell1         = 15 + subTitleHeight + 15 + detailHeight+10;
    //NSLog(@"cell 1 title: %f subtitle: %f", subTitleHeight, detailHeight);
    heightCell2         = 15 + validityheight + 10;
    heightCell3         = 15 + locationheight + 10;
    heightCell4         = 15 + offerHeight + 10;
    heightCell5         = 15 + contactPersonHeight + 15 + contactNumberheight + 15 + contactURLHeight + 10;
    //NSLog(@"cell 4 person: %f number: %f url: %f", contactPersonHeight, contactNumberheight, contactURLHeight);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (UILabel *) setHeaderTitle:(UILabel *) titleView {
    
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor   = [UIColor clearColor];
        titleView.font              = [UIFont fontWithName:APP_FONT_BOLD size:20];
        titleView.shadowColor       = [UIColor colorWithWhite:0.0 alpha:0.5];
        titleView.textColor         = [UIColor whiteColor];
        titleView.text              = _data[OTHER_DETAIL_OFFER_TITLE];
        [titleView sizeToFit];
    }
    return titleView;
}

- (NSString *) getOfferId {
    return strOfferId;
}

//- (UIBarButtonItem *) getIMInterestedButton {
//    
//    UIImage *imgIMInterested = [Util getImageAtDocDirWithName:@"iminterested.png"];
//    UIButton *btnBottombarRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnBottombarRight setImage:imgIMInterested forState:UIControlStateNormal];
//    btnBottombarRight.frame = CGRectMake(0, 0, imgIMInterested.size.width, imgIMInterested.size.height);
//    [btnBottombarRight addTarget:self action:@selector(btnIMInterestedPressed) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *btnBottombarRightBarButton = [[UIBarButtonItem alloc] initWithCustomView:btnBottombarRight];
//    
//    return btnBottombarRightBarButton;
//}


- (void) btnMorePressed {
    
    NSLog(@"%s", __FUNCTION__);
    
    NSString *title = nil;
    
    OtherDetailPageViewCtlr *detail = (OtherDetailPageViewCtlr *) self.controller;
    
    if (detail.offerType == OFFER_HOME) {
        title = @"Home Offer - More Details";
    } else if (detail.offerType == OFFER_CAR) {
        title = @"Car Offer - More Details";
    } else if (detail.offerType == OFFER_OTHER) {
        title = @"Other Offer - More Details";
    }
    
    UIAlertView *detailData = [[UIAlertView alloc] initWithTitle:title
                                                         message:strDetail
                                                        delegate:nil
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:nil, nil];
    [detailData show];
}

- (void) getCellOneWithCell: (OtherOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblSubTitle.text       = strSubtitle;
    cell.lblSubTitle.textColor  = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblSubTitle setNumberOfLines:0];
    [cell.lblSubTitle sizeToFit];
    
    cell.lblDetail.text         = shortDetail;
    cell.lblDetail.textColor    = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblDetail setNumberOfLines:0];
    [cell.lblDetail sizeToFit];
    
    cell.lblDetail.frame    = CGRectMake(cell.lblDetail.frame.origin.x, cell.lblSubTitle.frame.origin.y + cell.lblSubTitle.frame.size.height+15, cell.lblDetail.frame.size.width, cell.lblDetail.frame.size.height);
    
    [cell.btnMore setBackgroundImage:imageMore forState:UIControlStateNormal];
    [cell.btnMore addTarget:self action:@selector(btnMorePressed) forControlEvents:UIControlEventTouchUpInside];
    cell.btnMore.frame      = CGRectMake(cell.btnMore.frame.origin.x, cell.lblSubTitle.frame.origin.y + cell.lblSubTitle.frame.size.height+15, cell.btnMore.frame.size.width, cell.btnMore.frame.size.height);
    
    
    if ([_data[OTHER_DETAIL_OFFER_RECENT] intValue]==1) {
        cell.imgNewOffer.hidden = NO;
    } else if ([_data[OTHER_DETAIL_OFFER_RECENT] intValue]==0) {
        cell.imgNewOffer.hidden = YES;
        cell.imgNewOffer.image  = [Util getImageAtDocDirWithName:@"star.png"];
    }
}

- (void) getCellTwoWithCell: (OtherOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblValidity.text       = offervalidity;
    cell.lblValidity.textColor  = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR]
                                                     alpha:@"1.0f"];
}

- (void) getCellThreeWithCell: (OtherOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblLocation.text       = location;
    cell.lblLocation.textColor  = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR]
                                                     alpha:@"1.0f"];
    [cell.lblLocation setNumberOfLines:0];
    [cell.lblLocation sizeToFit];
}

- (void) getCellFourWithCell:(OtherOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblOffer.text          = offer;
    cell.lblOffer.textColor     = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblOffer setNumberOfLines:0];
    [cell.lblOffer sizeToFit];
}

- (void) getCellFiveWithCell: (OtherOfferDetail *) cell {
    
    // Configure the cell...
    if ([strContactPerson isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactPerson.text      = EMPTY;
    } else {
        cell.lblContactPerson.text      = strContactPerson;
        cell.lblContactPerson.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        cell.imgContactPerson.image     = imageContactPerson;
    }
    
    if ([strContactNo1 isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactNumber.text = EMPTY;
        cell.lblContactNumber.userInteractionEnabled = NO;
    } else {
        cell.lblContactNumber.text      = strContactNo1;
        cell.lblContactNumber.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        
        UITapGestureRecognizer *tapGestureRecognizer    = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(contactNumberTapped)];
        tapGestureRecognizer.numberOfTapsRequired       = 1;
        [cell.lblContactNumber addGestureRecognizer:tapGestureRecognizer];
        cell.lblContactNumber.userInteractionEnabled    = YES;
        
        cell.lblContactNumber.frame     = CGRectMake(cell.lblContactNumber.frame.origin.x, cell.lblContactPerson.frame.origin.y + cell.lblContactPerson.frame.size.height+10, cell.lblContactNumber.frame.size.width, cell.lblContactNumber.frame.size.height);
    }
    
    if ([strContactNo2 isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactNumber2.text = EMPTY;
        cell.lblContactNumber2.userInteractionEnabled = NO;
    } else {
        cell.lblContactNumber2.text      = strContactNo2;
        cell.lblContactNumber2.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        
        UITapGestureRecognizer *tapGestureRecognizer2   = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(contactNumber2Tapped)];
        tapGestureRecognizer2.numberOfTapsRequired       = 1;
        [cell.lblContactNumber2 addGestureRecognizer:tapGestureRecognizer2];
        cell.lblContactNumber2.userInteractionEnabled    = YES;
        
        cell.lblContactNumber2.frame     = CGRectMake(cell.lblContactNumber2.frame.origin.x, cell.lblContactPerson.frame.origin.y + cell.lblContactPerson.frame.size.height+10, cell.lblContactNumber2.frame.size.width, cell.lblContactNumber2.frame.size.height);
    }
    
    if (!([strContactNo1 isEqualToString:NOT_AVAILABLE] && [strContactNo2 isEqualToString:NOT_AVAILABLE])) {
        cell.imageContactNumber.image   = imageContactNo;
    }
    
    
    if ([strContactURL isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactURL.text = EMPTY;
        cell.lblContactURL.userInteractionEnabled = NO;
    } else {
        
        cell.lblContactURL.text         = strContactURL;
        cell.lblContactURL.textColor    = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        cell.lblContactURL.frame        = CGRectMake(cell.lblContactURL.frame.origin.x, cell.lblContactNumber.frame.origin.y + cell.lblContactNumber.frame.size.height+10, cell.lblContactURL.frame.size.width, cell.lblContactURL.frame.size.height);
        [cell.lblContactURL setNumberOfLines:0];
        [cell.lblContactURL sizeToFit];
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openLink)];
        // if labelView is not set userInteractionEnabled, you must do so
        [cell.lblContactURL setUserInteractionEnabled:YES];
        [cell.lblContactURL addGestureRecognizer:gesture];
    }
}

- (void) openLink {
    
    NSString *strWebURL = strContactURL;
    if ([strWebURL rangeOfString:@"http"].location==NSNotFound) {
        strWebURL = [NSString stringWithFormat:@"http://%@", strWebURL];
    }
    strWebURL = [strWebURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strWebURL]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strWebURL]];
    } else {
        NSLog(@"Unable to open %@", strWebURL);
    }
}   

- (float) getCellOneHeight {
    return heightCell1;
}

- (float) getCellTwoHeight {
    return heightCell2;
}

- (float) getCellThreeHeight {
    return heightCell3;
}

- (float) getCellFourHeight {
    return heightCell4;
}

- (float) getCellFiveHeight {
    return heightCell5;
}

- (void) contactNumberTapped {
    [self callWithNumber: strContactNo1];
}

- (void) contactNumber2Tapped {
    [self callWithNumber: strContactNo2];
}

- (void) callWithNumber:(NSString *) number {
    NSLog(@"number tapped: %@", number);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", number]]];
}

@end
