//
//  GridViewCell.m
//  Universe on the move
//
//  Created by Suraj on 29/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//  

#import "GridViewCell.h"

#import "Constant.h"
#import "Holder.h"
#import "Util.h"

@implementation GridViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void) initCellWithData:(NSDictionary *) data CSS:(NSDictionary *) cssList For:(NSString *) viewController {
    
//    [_spinner setHidden:YES];
    _imgIcon.image = [Util getImageAtDocDirWithName:data[@"img-icon"]];
    
    // Initialize the pending count in for Leave, Muster & Conveyance
    [self initPendingCount:data[@"name"]];
    
    if (![_spinner isAnimating]) {
        [_spinner removeFromSuperview];
    }
    
    [_lblName setText:data[@"title"]];
    //NSLog(@"css for %@ is %@" ,[data objectForKey:@"title"], cssList);
    [_lblName setFont:[Util fontWithName:cssList[@"font-name"] style:cssList[@"font-style"] size:cssList[@"font-size"]]];
    [_lblName setNumberOfLines:0];
    [_lblName sizeToFit];
    [_lblName setFrame:CGRectMake(10, 75, 130, 42)];
    //NSLog(@"css: %@", cssList);
    [_lblName setTextColor:[Util colorWithHexString:cssList[@"font-color"] alpha:@"1.0f"]];
}

- (void) initPendingCount:(NSString *) strName {
    NSLog(@"cell name: %@", strName);
    
    if ([strName isEqualToString:@"leave-approve"] || [strName isEqualToString:@"muster-approve"] || [strName isEqualToString:@"expense-approve"] || [strName isEqualToString:@"late-punch-approve"])
    {
        
          //  NSLog(@"cell name inner: %@", strName);
            
                NSString *strCount = @"    ";
                if ([strName isEqualToString:@"leave-approve"]) {
                    [self.btnPendingCount setTag:TAG_PENDING_LEAVE_APPROVAL_COUNT];
                    strCount = [Holder sharedHolder].pendingCountLeave;
                    //strCount = [NSString stringWithFormat:@"%@",@"0"];
        //            NSLog(@"Leave count: %@", strCount);
                }
                if ([strName isEqualToString:@"muster-approve"]) {
                    [self.btnPendingCount setTag:TAG_PENDING_MUSTER_APPROVAL_COUNT];
                    strCount = [Holder sharedHolder].pendingCountMuster;
        //            NSLog(@"Muster count: %@", strCount);
                }
                if ([strName isEqualToString:@"expense-approve"]) {
                    [self.btnPendingCount setTag:TAG_PENDING_CONVEYANCE_APPROVAL_COUNT];
                    strCount = [Holder sharedHolder].pendingCountConveyance;
        //            NSLog(@"Conveyance count: %@", strCount);
                }
                
                if ([strName isEqualToString:@"late-punch-approve"]) {
                    [self.btnPendingCount setTag:TAG_PENDING_LATE_PUNCH_APPROVAL_COUNT];
                    strCount = [Holder sharedHolder].pendingCountLatePunch;
                    NSLog(@"Late Punch count: %@", strCount);
                }
        
                //This is old code
                if ([strCount isEqualToString:@""]) {
                    strCount = @"    ";
                    [self.spinner setHidden:NO];
                    [self.spinner startAnimating];
                    [_spinner setTag:TAG_PAC_INDICATOR];
                }
                
                //This is new code
        //        if ([strCount isEqualToString:@""]) {
        //            strCount = [NSString stringWithFormat:@"%@",@"0"];;
        //            [self.spinner setHidden:YES];
        //            [self.spinner stopAnimating];
        //            [_spinner setTag:TAG_PAC_INDICATOR];
        //        }
        
       /// if ([Holder sharedHolder].testingNoti == true) {
            
            NSLog(@"cell name inner: %@", strName);
        
            UIImage *imgPending = [[Util getImageAtDocDirWithName:@"pending-count.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 15.0f, 0.0f, 15.0f)];
            [self.btnPendingCount setBackgroundImage:imgPending forState:UIControlStateNormal];
            [self.btnPendingCount setUserInteractionEnabled:NO];
            [self.btnPendingCount.titleLabel setFont:[UIFont fontWithName:APP_FONT_ROMAN size:13.0f]];
            [self.btnPendingCount.titleLabel setTextColor:[UIColor whiteColor]];
            [self.btnPendingCount setTitle:strCount forState:UIControlStateNormal];
            [self.btnPendingCount sizeToFit];
            //[Holder sharedHolder].testingNoti = false;
     ///   }
        
               // [self.btnPendingCount setTitleEdgeInsets:UIEdgeInsetsMake(3, 1, 0, 0)];
    }
}

- (void) didHighlightItemForGridViewCell:(GridViewCell *) cell {
    
    UIView *bgColorView = [[UIView alloc] init];
    NSArray *color = [[Util getSettingUsingName:LIST_TOUCHUP_COLOR] componentsSeparatedByString:@"|"];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    gradient.colors = @[(id)[[Util colorWithHexString:color[0] alpha:@"0.75f"] CGColor], (id)[[Util colorWithHexString:color[1] alpha:@"0.75f"] CGColor]];
    [bgColorView.layer insertSublayer:gradient atIndex:0];
    
    self.backgroundView = bgColorView;
    
    _lblName.textColor = [UIColor whiteColor];
}

- (void) didUnHighlightItemForGridViewCell:(GridViewCell *) cell {
    
    self.backgroundView = nil;
    
    _lblName.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR]
                                            alpha:@"1.0f"];
}

@end
