//
//  LoginViewMod.h
//  M ASSiST
//
//  Created by Suraj on 19/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Navigation;
@class LoginViewCtlr;

@interface LoginModel : NSObject<UIAlertViewDelegate> {
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button
    UIButton *btnEmergency, *btnSignIn;
    NSString *strEmergencyNumber;   // Stores Emergency number;
    
    UITextField *username, *password;
    
    NSDictionary *responseJSONData;
    NSString *strDownlaodURL;
}

@property (nonatomic, weak) UIViewController *controller;        // Controller view reference
@property (nonatomic) Navigation *navigation;

- (void) clearLoginCredentials;
- (void) clearPassword;

- (NSString *) getEmergencyNumber;

- (void) initDataWithView:(UIViewController *) controllerView;  // Init model for the controller

- (NSDictionary *) getPageData;

- (void) initNavControlWithUser:(UITextField *) txtUserName
                       Password:(UITextField *) txtPassword
                      Emergency: (UIButton *) emergency
                         SignIn:(UIButton *) signIn;

- (void) signIn;

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData;

@end
