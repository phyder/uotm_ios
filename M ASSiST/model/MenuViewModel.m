//
//  MenuViewModel.m
//  M ASSiST
//
//  Created by Suraj on 23/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "MenuViewModel.h"

#import "Authentication.h"
#import "Constant.h"
#import "Holder.h"
#import "Util.h"
#import "ToolsViewCtlr.h"
#import "IIViewDeckController.h"
#import "ListViewCell.h"
#import "ESOPViewController.h"
#import "AppConnect.h"
#import "CarouselViewCtlr.h"

@interface MenuViewModel() {
    ListViewCell *cell;
    IIViewDeckController *viewCtlr;
}

@end

@implementation MenuViewModel

- (void) initDataWithView:(UIViewController *) view {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
    
    _controller    = view;
    [_controller.view setBackgroundColor:[Util colorWithHexString:@"888888" alpha:@"0.6f"]];
    NSDictionary *content  = [Util getPage:PAGE_MAIN_MENU][PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    
    
    
    data = [content mutableCopy];
//    NSDictionary *dict = nil;
//    NSString *mac = nil;
//    for (int i=0; i<[data count]; i++) {
//        mac = data[i][@"mac"];
//        if([mac isEqualToString:@"SETT"]) {
//            dict = [data objectAtIndex:i];
//        }
//    }
    data = [Util menuAuthorization:data];
    //[data addObject:dict];
    // Modern Objective-C Syntax
    NSDictionary *home = @{
                           @"clickable":@"true",
                           @"count":@"",
                           @"desc":@"",
                           @"img-icon":@"home-mgr.png",
                           @"img-icon-bounds":@"10|10|40|40",
                           @"img-icon-sel":@"sel-home-mgr.png",
                           @"landing-page":@"home-menu",
                           @"name":@"home-menu",
                           @"sub-title":@"",
                           @"sub-type":@"home-menu",
                           @"title":@"Home",
                           @"title-css":@"title-image-one-field",
                           @"type":@"home"};
    
    [data insertObject:home atIndex:0];
    //NSLog(@"Menu Data: %@\n home data: %@", data, home);
    //css     = [Util getPageCSS:[data objectForKey:PAGE_CSS]];//[Holder sharedHolder].css;
}

- (UIView *) getSignOutButtonInMenuHeaderWithHeight:(int) height {
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, height)];
    UIImage *imageBg = [[Util getImageAtDocDirWithName:@"nav-bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    //header.backgroundColor = [UIColor colorWithPatternImage:headerBg];
    UIImageView *image = [[UIImageView alloc] initWithImage:imageBg];
    image.frame = CGRectMake(0, 0, 320, 44);
    [header addSubview:image];
    [header setBackgroundColor:[Util colorWithHexString:@"222222" alpha:@"0.6f"]];
    UIImage *imgTopbarLeft = [Util getImageAtDocDirWithName:@"sign-out.png"];
    UIButton *btnSignOut = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSignOut setImage:imgTopbarLeft forState:UIControlStateNormal];
    btnSignOut.frame = CGRectMake(10, 8, imgTopbarLeft.size.width, imgTopbarLeft.size.height);
    [btnSignOut addTarget:self action:@selector(btnSignoutPressed) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:btnSignOut];
    
    return header;
}

- (void) btnSignoutPressed {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@ Sign Out pressed.", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    [[Holder sharedHolder].auth requestSignOutWithMenuToggleCheck:YES];
}

- (int) getMenuItemCount {
    
    return (int) [data count];
}

- (NSDictionary *) getDataForIndexPath:(int) position {
    
    return data[position];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell = (ListViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
    
    NSDictionary *dict = [Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_MPIN_MENU];
    if([dict isEqual:[NSNull null]]) {
        [self didSelectItemAtIndex:indexPath.row];
    } else {
        NSString *mac = data[indexPath.row][@"mac"];
        if ([[dict allKeys] containsObject:mac]) {
            // contains key
            NSString *value = [NSString stringWithFormat:@"%@",[dict objectForKey:mac]];
            if([value isEqualToString:@"1"]) {
                [viewCtlr toggleLeftView];
                Util *util = [[Util alloc] init];
                [util showVerifyViewControllerOnController:viewCtlr.centerController Model:self Index:indexPath.row - 1];
            } else {
                [self didSelectItemAtIndex:indexPath.row];
            }
        } else {
            [self didSelectItemAtIndex:indexPath.row];
        }
    }
}

- (void)didSelectItemAtIndex:(NSInteger)index {
    NSString *type = data[index][@"type"];
    NSString *nextPage = [cell getNextPage];
    NSString *currentPage = [Holder sharedHolder].currentPageName;
    //NSLog(@"Current: %@ next: %@", currentPage, nextPage);
    
    //NSLog(@"index: %d and type: %@", indexPath.row, type);
    if ([type isEqualToString:@"home"]) {
        [viewCtlr toggleLeftView];
        if (![currentPage isEqualToString:@"main-menu"]) {
            viewCtlr.panningMode = IIViewDeckNoPanning;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            [Util openCarouselSelectionOnView:nav];
        }
    } else if ([type isEqualToString:PAGE_TYPE_MENULIST]) {
        
        if (![currentPage isEqualToString:nextPage]) {
            [Util openMenuAsGridWithNextPage:nextPage Data:[Util getPage:nextPage] OnView:self.controller];
        } else {
            [viewCtlr toggleLeftView];
        }
    } else if ([type isEqualToString:PAGE_TYPE_MENULIST_DB]) {
        
        if (![currentPage isEqualToString:nextPage]) {
            [Util openMenuWithNextPage:[cell getNextPage] OnView:self.controller];
        } else {
            [viewCtlr toggleLeftView];
        }
    } else if ([type isEqualToString:@"offer"]) {
        //NSLog(@"offer Check Next page: %@", nextPage);
        
        if (![currentPage isEqualToString:nextPage]) {
            viewCtlr.panningMode = IIViewDeckNoPanning;
            UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
            
            [Util openEngagementSelection:nextPage OnView:nav];
        } else {
            [viewCtlr toggleLeftView];
        }
    } else if ([type isEqualToString:PAGE_TYPE_WEB]) {
        if (![currentPage isEqualToString:nextPage]) {
            [Util OpenWebWithNextPage:[cell getNextPage] AndParam:[cell getCellData][@"params"] OnView:self.controller];
        } else {
            [viewCtlr toggleLeftView];
        }
        //[self openWebView:indexPath];
    } else if ([type isEqualToString:@"calendar"]) {
        NSLog(@"Calendar found");
    } else if ([type isEqualToString:@"esop"]) {
        [viewCtlr toggleLeftView];
        UINavigationController *nav = (UINavigationController *) viewCtlr.centerController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        ESOPViewController *esop = (ESOPViewController *) [storyboard instantiateViewControllerWithIdentifier:@"esop"];
        esop.nextPage       = nextPage;
        esop.content        = [Util getPage:nextPage].mutableCopy;
        
        [Util pushWithFadeEffectOnNavigationController:nav ViewController:esop];
        //[nav pushViewController:esop animated:YES];
    } else if([type isEqualToString:PAGE_TYPE_APP]){
        if ([nextPage isEqualToString:@"pms"]){
            [self openpmstms:0];
        }else if ([nextPage isEqualToString:@"tms"])
        {
            [self openpmstms:1];
        }
    }else {
        NSLog(@"Type not found");
    }
}
-(void)openpmstms:(NSInteger)apptype{
    
    CarouselViewCtlr *carousel = [[CarouselViewCtlr alloc]init];
    [carousel openpmstms:apptype];
//    AppConnect *conn = [[AppConnect alloc] init];
//    NSString *token = [conn fetchPmsToken];
//    //token = @"test";
//    //NSString *dummytoken = @"eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIxMTY2ODgiLCJpYXQiOjE0ODMxNzAwMjcsImlzcyI6IlBNU19UTVNfU0VSVkVSIiwiZXhwIjoxNDgzMTcwMzI3LCJ1c2VyIjoiMTE2Njg4In0.c72ZjZ7YEihEWFXmoSjBvuSq2PCZ2aw26tUHThCnYrtPubNi5IxCCr_qvU1gFNJqoYsnd4OH8pYzVes2vsGoLZumIaDqlOBb7viRUn-9vacnuEnp_g7CUUSl6StVhunT7_PeUjCBzCEpMT1J33HZw9cnKCW9PRzJ4yKsZ2dEv5YORgs47W5TIERn6cJrZdLvsD2qwX-miDLctmtz3KMxjuPvbrQ61ej8slkLWTgvTdPomlQizxCzwhxfNERwdTcr7jSmeksPqwSiaKBvkO_DnS2SPptYeAJHeePUBrK1EIktQh1Y18qQo5_fMC9-bdkaq9x10j4P69xDgbFG34Jbmg";
//    
//    if(![token isEqualToString:@""]){
//        NSData *jsonData = [token dataUsingEncoding:NSUTF8StringEncoding];
//        NSError * error=nil;
//        NSDictionary * parsedJson = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
//        NSDictionary * parsedData = [parsedJson objectForKey:@"data"];
//        NSString *tkn = [parsedData objectForKey:@"tkn"];
//        NSString *appToken = tkn;
//        //NSString *appToken = dummytoken;
//        NSString *appTypeVal = @"pms";
//        if(apptype==1){
//            appTypeVal = @"tms";
//        }
//        NSString *customURL = @"icicipms://?token=";
//        NSString *finalURL = [NSString stringWithFormat:@"%@%@%@%@", customURL, appToken, @"&type=",appTypeVal];
//        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
//        NSLog(@"This is %f ",currSysVer.floatValue);
//        if (currSysVer.floatValue >= 10.0) {
//            NSLog(@"This is ios 10 block");
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL] options:@{} completionHandler:^(BOOL success) {
//                if (success) {
//                    //NSLog(@"Opened %@",scheme);
//                }else{
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App not installed"
//                                                                    message:[NSString stringWithFormat:
//                                                                             @"Please install PMS/TMS app."]
//                                                                   delegate:self cancelButtonTitle:@"No"
//                                                          otherButtonTitles:@"Yes", nil];
//                    [alert show];
//                    
//                }
//            }];
//        }else {
//            if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL]]) {
//                NSLog(@"yes this is version lower than 10");
//                NSLog(@"This is responce app not installed %hhd",[[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]]);
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App not installed"
//                                                                message:[NSString stringWithFormat:
//                                                                         @"Please install UOTM app."]
//                                                               delegate:self cancelButtonTitle:@"No"
//                                                      otherButtonTitles:@"Yes",nil];
//                
//                [alert show];
//            }
//        }
//        
//    }else{
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Technical Error Occurred"
//                                                        message:[NSString stringWithFormat:
//                                                                 @"Unable to connect to PMS/TMS. Please try again later."]
//                                                       delegate:nil cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
}
#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"OK"]) {
        [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:YES];
    }
    if([title isEqualToString:@"Back"]) {
        [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:NO];
    }
    if ([title isEqualToString:@"Yes"]) {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"https://universeonthemove.icicibank.com/engage/downloadptms.htm"]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://universeonthemove.icicibank.com/engage/downloadptms.htm"]];
            }
       
    }
}

@end
