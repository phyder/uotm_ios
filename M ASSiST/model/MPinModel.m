//
//  MPinModel.m
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "MPinModel.h"
#import "Util.h"
#import "Constant.h"
#import "Navigation.h"
#import "Holder.h"
#import "Authentication.h"
#import "MPinViewCtlr.h"

@implementation MPinModel

- (void) initDataWithView:(UIViewController *) controllerView {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    _controller = controllerView;
    data        = [Util getPage:PAGE_REGISTER];
    css         = [Util getPageCSS:data[PAGE_CSS]];//[Holder sharedHolder].css;
    
//    [[_controller view] setTag:TAG_IDENTIFY_LOGIN_VIEW];
}

- (NSDictionary *) getPageData {
    return data;
}

- (void) initNavControlWithNewMPin1:(UITextField *) txtNewMPin1
                           newMPin2:(UITextField *) txtNewMPin2
                           newMPin3:(UITextField *) txtNewMPin3
                           newMPin4:(UITextField *) txtNewMPin4
                        retypeMPin1:(UITextField *) txtRetypeMPin1
                        retypeMPin2:(UITextField *) txtRetypeMPin2
                        retypeMPin3:(UITextField *) txtRetypeMPin3
                        retypeMPin4:(UITextField *) txtRetypeMPin4
                             Submit:(UIButton *) submit {
    
    newMPin1           = txtNewMPin1;
    newMPin2           = txtNewMPin2;
    newMPin3           = txtNewMPin3;
    newMPin4           = txtNewMPin4;
    retypeMPin1        = txtRetypeMPin1;
    retypeMPin2        = txtRetypeMPin2;
    retypeMPin3        = txtRetypeMPin3;
    retypeMPin4        = txtRetypeMPin4;
    btnSubmit          = submit;
    
    [self initEmergencyAndHelpButton];
    
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self.controller PageData:data];
    
    [self sendLogtoServer];
}

#pragma mark - Log Send -
/**
 * Background thread for error log dump forward to Server
 */
- (void) sendLogtoServer {
    
    [NSThread detachNewThreadSelector:@selector(backgroundLogSend) toTarget:self withObject:nil];
}

- (void) backgroundLogSend {
    
    // Dummps error log to the server
    [Util sendErrorLog];
}

- (void) initEmergencyAndHelpButton {
    
    int intSubmitYPos = 330;
    if ([UIScreen mainScreen].bounds.size.height==568) {
        NSLog(@"\n\n\n4 inch retina\n\n\n");
        intSubmitYPos       = 330;
    }
    else if (isiPhone4s) {
        NSLog(@"this is iphone 4s");
        intSubmitYPos       = 290;
    }
    
    btnSubmit = [[UIButton alloc] initWithFrame:CGRectMake(115, intSubmitYPos, 96, 32)];
    [btnSubmit setImage:[Util getImageAtDocDirWithName:@"submit.png"] forState:UIControlStateNormal];
    [btnSubmit addTarget:self action:@selector(setMPin) forControlEvents:UIControlEventTouchUpInside];
    [self.controller.view addSubview:btnSubmit];
    
    NSArray *lists = data[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list = lists[i];
        if ([list[@"type"] isEqualToString:@"button"] && [list[@"name"] isEqualToString:@"emergency-call"]) {
            controlData = list;
            strEmergencyNumber = controlData[@"number"];
            break;
        }
    }
    
    //    This code gives button fall effect during back transition
    //    UIImage *imgTopbarRight = [Util getImageAtDocDirWithName:@"sign-in.png"];
    //    btnSignIn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btnSignIn setImage:imgTopbarRight forState:UIControlStateNormal];
    //    btnSignIn.frame = CGRectMake(130, intSignInYPos, imgTopbarRight.size.width, imgTopbarRight.size.height);
    //    [btnSignIn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    //    [view.view addSubview:btnSignIn];
}

- (void) hideKeyboard {
    [newMPin1 resignFirstResponder];
    [newMPin2 resignFirstResponder];
    [newMPin3 resignFirstResponder];
    [newMPin4 resignFirstResponder];
    [retypeMPin1 resignFirstResponder];
    [retypeMPin2 resignFirstResponder];
    [retypeMPin3 resignFirstResponder];
    [retypeMPin4 resignFirstResponder];
}

- (void) clearCredentials {
    
    newMPin1.text = @"";
    newMPin2.text = @"";
    newMPin3.text = @"";
    newMPin4.text = @"";
    retypeMPin1.text = @"";
    retypeMPin2.text = @"";
    retypeMPin3.text = @"";
    retypeMPin4.text = @"";
}

- (NSString *) getEmergencyNumber {
    
    return strEmergencyNumber;
}

- (void) setMPin {
    
    [self hideKeyboard];
    
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self.controller AndModel:self];
    NSString *newMpin = [NSString stringWithFormat:@"%@%@%@%@",newMPin1.text,newMPin2.text,newMPin3.text,newMPin4.text];
    NSString *retypeMpin = [NSString stringWithFormat:@"%@%@%@%@",retypeMPin1.text,retypeMPin2.text,retypeMPin3.text,retypeMPin4.text];
    [[Holder sharedHolder].auth requestSetMPinWithNewMPin:newMpin retypeMPin:retypeMpin];
    [self clearCredentials];
}

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData {
    
    NSLog(@"MPin response: %@", responseData);
    [[Holder sharedHolder].auth postMPinAuthenticationWithData:responseData];
}

- (void) dealloc {
    
    self.navigation = nil;
}

@end
