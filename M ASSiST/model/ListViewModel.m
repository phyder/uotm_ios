//
//  ListViewModel.m
//  M ASSiST
//
//  Created by Suraj on 24/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "ListViewModel.h"

#import "Constant.h"
#import "Holder.h"
#import "Navigation.h"
#import "Util.h"

#import "AppConnect.h"

#import "ListViewCell.h"
#import "ListViewCtlr.h"

@implementation ListViewModel

- (id) initDataForView:(UIViewController *) view
              WithPage:(NSString *) page
           AndWithData:(NSMutableDictionary *) pageContent {
    
    self = [super init];
    if (self) {
        // Custom initialization
        pageName    = page;
        _controller = view;
        content     = pageContent;
        listType    = content[LIST_TYPE];
        _isSelected = NO;
        [Holder sharedHolder].currentPageName = pageName;
        NSLog(@"content type: %@", listType);
        
        if ([listType isEqual:PAGE_TYPE_MENULIST]) {
            
            data = content[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
            data = [Util menuAuthorization:data];
            //NSLog(@"data: %@", data);
            //NSLog(@"Data %@\nmenu code: %@", [Holder sharedHolder].loginInfo, menuAccessCode);
            
        } else if ([listType isEqual:PAGE_TYPE_MENULIST_DB]) {
            
            data = content[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
            //NSLog(@"data: %@", data);
        }
        dataHeight = [[NSMutableArray alloc] init];
        //NSLog(@"%@\n\n\n%@\n\n%@", pageName, data, listType);
        
        if ([pageName isEqualToString:@"expense"]) {
            [self expenseCheck];
        }
        
        if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
            //NSLog(@"css check: %@", [Util getPage:page]);
            //contentHolder = [content objectForKey:PAGE_TYPE_LIST];
            //[content setValue:@"Title" forKey:PAGE_HEADER_TITLE];
            cssList = content[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
        }
        //NSLog(@"page data: %@", content);
        
        //Initialize the navigation controller
        self.navigation                  = [[Navigation alloc] initWithViewController:_controller PageData:content];
        
        if ([pageName isEqualToString:@"holiday-calendar"] || [pageName isEqualToString:@"other-holiday"]) {
            searchBar                   = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 23.0, [UIScreen mainScreen].bounds.size.width, 40)];
        }
        else {
            searchBar                   = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
        }
        
        
        //  searchBar                   = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
        searchBar.tintColor         = [UIColor colorWithPatternImage:[Util getImageAtDocDirWithName:@"login-view-bg.png"]];
        
        // Get the instance of the UITextField of the search bar
        UITextField *searchField    = [searchBar valueForKey:@"_searchField"];
        
        // Change search bar text color
        searchField.textColor       = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        
        // Change the search bar placeholder text color
        [searchField setValue:[Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"]
                   forKeyPath:@"_placeholderLabel.textColor"];
        
        //searchBar.tintColor = [UIColor clearColor];
        searchBar.delegate          = self;
        [searchBar setPlaceholder:@"Filter"];
    }
    return self;
}


- (void) updateNotificationCount {
    NSLog(@"This is list view Notification method");
    [self.navigation initTopRightButton];
    
}
- (void) updatePendingApprovalCount {
    
    NSString *strCount = [NSString stringWithFormat:@"%lu", (unsigned long)[data count]];
    
    //This is comment for testing
//    if ([pageName isEqualToString:@"leave-approve"]) {
//        [Holder sharedHolder].pendingCountLeave = strCount;
//    } else if ([pageName isEqualToString:@"muster-approve"]) {
//        [Holder sharedHolder].pendingCountMuster = strCount;
//    } else if ([pageName isEqualToString:@"expense-approve"]) {
//        [Holder sharedHolder].pendingCountConveyance = strCount;
//    } else if ([pageName isEqualToString:@"late-punch-approve"]) {
//        [Holder sharedHolder].pendingCountLatePunch = strCount;
//    }
    
//    int badgeCount = [[Holder sharedHolder].pendingCountLeave intValue] + [[Holder sharedHolder].pendingCountMuster intValue] + [[Holder sharedHolder].pendingCountLatePunch intValue] + [[Holder sharedHolder].pendingCountConveyance intValue];
//    
//    [Holder sharedHolder].pendingBadgeCount = badgeCount;
//    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
}

- (void) expenseCheck {
#ifdef DEBUG
    NSLog(@"%s", __FUNCTION__);
#endif
    NSString *grade = ([Holder sharedHolder].loginInfo)[@"grade"];
    if ([grade intValue] > [[Util getSettingUsingName:@"minimum-grade"] intValue]) {
        [data removeObjectAtIndex:0];
        [data removeObjectAtIndex:0];
    }
}

- (NSDictionary *) getParamsFromURL: (NSString *) url {
    url = [url substringFromIndex:1];
    NSArray *components = [url componentsSeparatedByString:@"&"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (int i=0; i<[components count]; i++) {
        NSArray *tmp = [components[i] componentsSeparatedByString:@"="];
        [params setValue:tmp[1] forKey:tmp[0]];
    }
    
    return params;
}

- (NSArray *)setCellIDWithData:(NSArray *) idData {
    NSMutableArray *arrParams = [[NSMutableArray alloc] init];
    for (NSDictionary *cellData in idData) {
        NSString *params = cellData[@"params"];
        NSDictionary *dicParams = [self getParamsFromURL:params];
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        
        if (params != nil) {
            BOOL isExist = [params rangeOfString:@"LV_P_AP_RSP_TRXN_ID"].location != NSNotFound && [params rangeOfString:@"LV_P_AP_RSP_EMP_ID"].location != NSNotFound;
            if (isExist) {
                [arr addObject:dicParams[@"LV_P_AP_RSP_TRXN_ID"]];
                [arr addObject:dicParams[@"LV_P_AP_RSP_EMP_ID"]];
                [arrParams addObject:arr];
            }
            isExist = [params rangeOfString:@"MS_P_AP_RSP_DT"].location != NSNotFound && [params rangeOfString:@"MS_P_AP_RSP_EMP_ID"].location != NSNotFound;
            if (isExist) {
                [arr addObject:dicParams[@"MS_P_AP_RSP_DT"]];
                [arr addObject:dicParams[@"MS_P_AP_RSP_EMP_ID"]];
                [arrParams addObject:arr];
            }
            isExist = [params rangeOfString:@"LATE_PNCH_RES_DUR"].location != NSNotFound && [params rangeOfString:@"LATE_PNCH_RES_EMP_ID"].location != NSNotFound;
            if (isExist) {
                [arr addObject:dicParams[@"LATE_PNCH_RES_DUR"]];
                [arr addObject:dicParams[@"LATE_PNCH_RES_EMP_ID"]];
                [arrParams addObject:arr];
            }
            
        } else {
            // No 'params' key found
            NSLog(@"Hello Late Punch");
        }
    }
    return arrParams;
}

- (void) initDynaListWithURL:(NSString *) url Data:(NSDictionary *) jsonData {
    
    jsonURL = url;
    data    = [jsonData[PAGE_TYPE_LIST] mutableCopy];
    if ([pageName isEqualToString:@"holiday-calendar"] || [pageName isEqualToString:@"other-holiday"]) {
    holidayTitleText = jsonData[@"msg"];
    }
    
    
    if ([pageName isEqualToString:@"leave-approve"] || [pageName isEqualToString:@"muster-approve"] || [pageName isEqualToString:@"late-punch-approve"]) {
        request = [self setCellIDWithData:data];
    }
    
    for (int i=0; i<[data count]; ++i) {
        //NSLog(@"image: %@", [[data objectAtIndex:i] objectForKey:@"icon"]);
        if ([data[i] valueForKey:@"icon"] != nil) {
            NSMutableDictionary * tempData = [[NSMutableDictionary alloc] init];
            if ([data[i][@"icon"] isEqualToString:@"g.png"]) {
                tempData = [data[i] mutableCopy];
                [tempData setValue:PENDING_STATUS_APPROVED forKey:@"status"];
            } else if ([data[i][@"icon"] isEqualToString:@"b.png"]) {
                tempData = [data[i] mutableCopy];
//                if([pageName isEqualToString:@"muster-approve"]) {
//                    [tempData setValue:PENDING_STATUS_MUSTER forKey:@"status"];
//                } else {
                    [tempData setValue:PENDING_STATUS_PENDING forKey:@"status"];
//                }
            } else if ([data[i][@"icon"] isEqualToString:@"r.png"]) {
                tempData = [data[i] mutableCopy];
                [tempData setValue:PENDING_STATUS_REJECTED forKey:@"status"];
            } else if ([data[i][@"icon"] isEqualToString:@"f.png"]) {
                tempData = [data[i] mutableCopy];
                [tempData setValue:PENDING_STATUS_DONT_INCLUDE forKey:@"status"];
            }
            data[i] = tempData;
        }
        //        NSLog(@"data check %@", [data objectAtIndex:i]);
    }
    //    NSLog(@"Data dyna list: %@", data);
}

- (void) openNotification {
    
    jsonURL = [Util getNotificationURL];
    [Util openNotificationWithURL:jsonURL AfterFinishCall:@selector(finishedLoadingNotificationData:) OnView:self.controller];
}

- (void) finishedLoadingNotificationData: (NSDictionary *) resNotificationlist {
    
    [self.navigation finishedLoadingNotificationData:resNotificationlist ForURL:jsonURL];
}

- (void) viewDidAppear {
    NSLog(@"back from web: %@", listType);
    if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        [self refreshDynalist];
        NSLog(@"Dyna-list while coming back from web-view.");
    }
}

- (void) refreshDynalist {
    
    NSString *url = [NSString stringWithFormat:@"%@?refresh=true", jsonURL];
    //NSLog(@"refresh url: %@", url);
    [Util refreshDynaListWithURL:url AfterFinishCall:@selector(refreshedDynalistData:) OnView:self.controller];
}

- (void) dynalistDataRefresh:(NSDictionary *) resDynalist {
    
    //NSLog(@"refreshed dynalist data data: %@", resDynalist);
    if ([Util serverResponseCheckNullAndSession:resDynalist InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:PAGE_TYPE_LIST In:resDynalist InFunction:__FUNCTION__ Message:[NSString stringWithFormat:@"%@ parameter", PAGE_TYPE_LIST] Silent:NO]) {
          //  NSLog(@"Res DynaList %@", resDynalist);
            data = resDynalist[@"list"];
            
            //NSLog(@"data DynaList %@", data);
            //holidayTitleText = resDynalist[@"msg"];
           //  NSLog(@"holidayTitleText DynaList %@", holidayTitleText);
            [((ListViewCtlr *) self.controller).tableView reloadData];
        }
    }
}

- (void) btnBottomOneRightPressed {
#ifdef DEBUG_LEVEL_2
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_2);
#endif
}

- (int) getRowCount {
    
    // Return the number of rows for the list.
    int rowCount = 0;
    
    if(isFiltered) {
        rowCount = (int) filteredData.count;
    } else
        rowCount = (int) data.count;
    
    return rowCount;
}

- (int) getHeightOfCellAtIndexPath:(NSIndexPath *) indexPath {
    
    CGSize constraint;
    NSString *titleCSS, *subTitleCSS, *descCSS;
    if (isFiltered) {
        item = filteredData[indexPath.row];
    } else {
        item = data[indexPath.row];
    }
    if ([listType isEqual:PAGE_TYPE_MENULIST] || [listType isEqual:PAGE_TYPE_MENULIST_DB]) {
        titleCSS = item[@"title-css"];
        subTitleCSS = item[@"sub-title-css"];
        descCSS = item[@"desc-css"];
    } else {
        titleCSS = cssList[@"title-css"];
        subTitleCSS = cssList[@"sub-title-css"];
        descCSS = cssList[@"desc-css"];
    }
    NSString *text = item[@"title"];
    NSDictionary *css = [Util getLabelCSS:titleCSS];
    NSString *bounds = css[CSS_LABEL_TEXT_BOUNDS];
    NSArray *bound = [bounds componentsSeparatedByString:@"|"];
    if ([bound[0] intValue] < 50) {
        constraint = CGSizeMake(250, 20000.0f);
    } else {
        constraint = CGSizeMake(200, 20000.0f);
    }
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:[css[@"font-size"] intValue]] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat temp = size.height;
    //NSLog(@"title cal height: %.0f", temp);
    CGFloat titleHeight, subTitleHeight, descHeight, rowHeight;
    titleHeight = MAX(size.height+22, 60.0f);
    rowHeight = titleHeight;
    
    NSString *subText = item[LIST_SUB_TITLE];
    if (![subText isEqualToString:@""]) {
        css = [Util getLabelCSS:subTitleCSS];
        NSString *bounds = css[CSS_LABEL_TEXT_BOUNDS];
        NSArray *bound = [bounds componentsSeparatedByString:@"|"];
        if ([bound[0] intValue] < 50) {
            constraint = CGSizeMake(250, 20000.0f);
        } else {
            constraint = CGSizeMake(200, 20000.0f);
        }
        size = [subText sizeWithFont:[UIFont systemFontOfSize:[css[@"font-size"] intValue]] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        subTitleHeight = MAX(size.height, 15.0f);
        rowHeight += subTitleHeight;
        if (temp<30) {
            rowHeight = rowHeight - 20;
        }
        //NSLog(@"temp %.0f title height: %.0f sub-title height %.0f", temp, titleHeight, subTitleHeight);
    }
    //NSLog(@"row height: %.2f sub-title height: %.2f section: %d row: %d", rowHeight, subTitleHeight, indexPath.section, indexPath.row);
    
    NSString *descText = item[@"desc"];
    if (![descText isEqualToString:@""]) {
        css = [Util getLabelCSS:descCSS];
        size = [descText sizeWithFont:[UIFont systemFontOfSize:[css[@"font-size"] intValue]] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        descHeight = MAX(size.height, 10.0f);
        //NSLog(@"title height: %.0f sub-title height %.0f desc height: %.0f", titleHeight, subTitleHeight, size.height);
        rowHeight += descHeight;
    } else {
        rowHeight += 10.0f;
    }
    //NSLog(@"row height: %.2f section: %d row: %d", rowHeight, indexPath.section, indexPath.row);
    [dataHeight insertObject:[NSString stringWithFormat:@"%.0f", rowHeight] atIndex:indexPath.row];
    
    return rowHeight;// [self.height floatValue];
}

- (int) heightForHeader {
    if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        height = 40;
        if ([pageName isEqualToString:@"holiday-calendar"] || [pageName isEqualToString:@"other-holiday"]) {
            height += 31;
        }
    }
    return height;
}

- (UIView *) loadSearchBar {
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 0.0)];
    //NSLog(@"page: %@ list type: %@", page, listType);
    if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        header = [[UIView alloc] init];
        header.frame = CGRectMake(0.0, 0.0, 320.0, height);
        header.backgroundColor      = [UIColor whiteColor];
        if ([pageName isEqualToString:@"holiday-calendar"] || [pageName isEqualToString:@"other-holiday"]) {
            pageTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 2.0, 320.0f, 21.0)];
            NSString *fontColor = @"053C6D";
            [pageTitle setFont:[Util fontWithName:@"ZurichBT" style:@"B" size:@"15"]];
            pageTitle.textColor = [Util colorWithHexString:fontColor alpha:@"1.0f"];
            pageTitle.text = holidayTitleText;
            pageTitle.textAlignment = NSTextAlignmentCenter;
            [header addSubview:pageTitle];
            [header addSubview:searchBar];
        }
        else {
            [header addSubview:searchBar];
        }
    }
    return header;
}

- (int) heightForFooter {
    
    int footerHeight = 0;
    if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST] && ([pageName isEqualToString:@"leave-history"] ||
                                                           [pageName isEqualToString:@"muster-history"] ||
                                                           [pageName isEqualToString:@"expense-history"])) {
        if ([pageName isEqualToString:@"expense-history"]) {
            footerHeight = 60;
        } else if ([pageName isEqualToString:@"muster-history"]) {
            footerHeight = 50;
        } else {
            footerHeight = 30;
        }
    } else if ([pageName isEqualToString:@"expense"] && [([Holder sharedHolder].loginInfo)[@"grade"] intValue] > [[Util getSettingUsingName:@"minimum-grade"] intValue]) {
        footerHeight = 30;
    } else {
        footerHeight = 0;
    }
    return footerHeight;
}

- (UIView *) loadLegendBar {
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 44.0)];
    footer.backgroundColor = [UIColor colorWithPatternImage:[Util getImageAtDocDirWithName:@"login-view-bg.png"]];
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0f, 0.0f, 320.0f, 1.0f);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f].CGColor;
    [footer.layer addSublayer:topBorder];
    
    if ([pageName isEqualToString:@"expense"]) {
        
        UILabel *strMsgExpense = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, 300.0, 44.0)];
        strMsgExpense.text      = [Util getSettingUsingName:@"minimum-grade-msg"];
        strMsgExpense.font      = [UIFont fontWithName:APP_FONT_ROMAN size:11.0];
        strMsgExpense.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        strMsgExpense.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [strMsgExpense setLineBreakMode:NSLineBreakByWordWrapping];
        [strMsgExpense setNumberOfLines:0];
        [strMsgExpense sizeToFit];
        [footer addSubview:strMsgExpense];
    }
    if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        
        UIImage *icon           = [[Util getImageAtDocDirWithName:@"g.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        UIImageView *iconHolder = [[UIImageView alloc] initWithFrame:CGRectMake(27, 10, 6, 6)];
        iconHolder.image        = icon;
        iconHolder.contentMode  = UIViewContentModeCenter;
        footer.contentMode      = UIViewContentModeScaleAspectFit;
        UILabel *text           = [[UILabel alloc] initWithFrame:CGRectMake(44, 8, 60, 12)];
        text.backgroundColor    = [UIColor clearColor];
        text.text               = PENDING_STATUS_APPROVED;
        text.font               = [UIFont fontWithName:APP_FONT_ROMAN size:10.0];
        text.textColor          = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        [footer addSubview:iconHolder];
        [footer addSubview:text];
        
        icon                    = [[Util getImageAtDocDirWithName:@"b.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
//        if([pageName isEqualToString:@"muster-history"]) {
//            iconHolder              = [[UIImageView alloc] initWithFrame:CGRectMake(105, 10, 6, 6)];
//            text                    = [[UILabel alloc] initWithFrame:CGRectMake(120, 8, 60, 12)];
//        } else {
//            iconHolder              = [[UIImageView alloc] initWithFrame:CGRectMake(135, 10, 6, 6)];
//            text                    = [[UILabel alloc] initWithFrame:CGRectMake(150, 8, 60, 12)];
//        }
        iconHolder              = [[UIImageView alloc] initWithFrame:CGRectMake(135, 10, 6, 6)];
        text                    = [[UILabel alloc] initWithFrame:CGRectMake(150, 8, 60, 12)];
        
        
        text.backgroundColor    = [UIColor clearColor];
        [footer addSubview:iconHolder];
        [footer addSubview:text];
        iconHolder.image        = icon;
        iconHolder.contentMode  = UIViewContentModeCenter;
        footer.contentMode = UIViewContentModeScaleAspectFit;
        if ([pageName isEqualToString:@"expense-history"]) {
            text.text           = [NSString stringWithFormat:@"%@ / %@", PENDING_STATUS_PENDING, PENDING_STATUS_SAVED_NOT_SUBMITTED];
        } else if([pageName isEqualToString:@"muster-history"]) {
            text.text           = PENDING_STATUS_MUSTER;
        } else {
            text.text           = PENDING_STATUS_PENDING;
        }
        text.font               = [UIFont fontWithName:APP_FONT_ROMAN size:10.0];
        text.textColor          = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        
//        if([pageName isEqualToString:@"muster-history"]) {
//        [text setLineBreakMode:NSLineBreakByWordWrapping];
//        [text setNumberOfLines:1];
//        [text sizeToFit];
//        } else {
//            [text setLineBreakMode:NSLineBreakByWordWrapping];
//            [text setNumberOfLines:0];
//            [text sizeToFit];
//
//        }
        
        [text setLineBreakMode:NSLineBreakByWordWrapping];
                    [text setNumberOfLines:0];
                    [text sizeToFit];
        icon                    = [[Util getImageAtDocDirWithName:@"r.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        if([pageName isEqualToString:@"muster-history"]) {
            iconHolder              = [[UIImageView alloc] initWithFrame:CGRectMake(255, 10, 6, 6)];
            text                    = [[UILabel alloc] initWithFrame:CGRectMake(270, 8, 60, 12)];
        } else {
            iconHolder              = [[UIImageView alloc] initWithFrame:CGRectMake(235, 10, 6, 6)];
            text                    = [[UILabel alloc] initWithFrame:CGRectMake(250, 8, 60, 12)];
        }
        text.backgroundColor    = [UIColor clearColor];
        [footer addSubview:iconHolder];
        [footer addSubview:text];
        iconHolder.image        = icon;
        iconHolder.contentMode  = UIViewContentModeCenter;
        footer.contentMode = UIViewContentModeScaleAspectFit;
        if ([pageName isEqualToString:@"expense-history"]) {
            text.text           = [NSString stringWithFormat:@"%@ / %@", PENDING_STATUS_REJECTED, PENDING_STATUS_DELETED];
        } else {
            text.text           = PENDING_STATUS_REJECTED;
        }
        text.font               = [UIFont fontWithName:APP_FONT_ROMAN size:10.0];
        text.textColor          = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        [text setLineBreakMode:NSLineBreakByWordWrapping];
        [text setNumberOfLines:0];
        [text sizeToFit];
    }
    return footer;
}

- (void) loadCell:(ListViewCell *) cell AtIndexPath:(NSIndexPath *) indexPath {
    
    //NSLog(@"check load cell height: %@\n%@", [dataHeight objectAtIndex:indexPath.row], [data objectAtIndex:indexPath.row]);
    if ([listType isEqualToString:PAGE_TYPE_MENULIST] || [listType isEqualToString:PAGE_TYPE_MENULIST_DB]) {
        [cell initCellWithData:data[indexPath.row]
                      OfHeight:[dataHeight[indexPath.row] floatValue]
                           CSS:nil
                           For:PAGE_MAIN_MENU
                      PageName:pageName];
        //[cell initCellWithData:[data objectAtIndex:indexPath.row] OfHeight:[[dataHeight objectAtIndex:indexPath.row] floatValue] For:PAGE_MAIN_MENU];
    } else if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        //NSLog(@"data: %@", [data objectAtIndex:indexPath.row]);
        if (isFiltered) {
            item = filteredData[indexPath.row];
        } else {
            item = data[indexPath.row];
        }
        //NSLog(@"css list name:%@ and data %@", [cssList objectForKey:@"name"], cssList);
        [cell initCellWithData:item
                      OfHeight:[dataHeight[indexPath.row] floatValue]
                           CSS:cssList
                           For:PAGE_TYPE_DYNA_LIST
                      PageName:pageName];
        
        //[cell initCellWithData:[data objectAtIndex:indexPath.row] OfHeight:[[dataHeight objectAtIndex:indexPath.row] floatValue] For:PAGE_TYPE_DYNA_LIST];
    }
    //[cell setBackgroundColor:[UIColor colorWithPatternImage:[Util getImageAtDocDirWithName:@"login-view-bg.png"]]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListViewCell *cell = (ListViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    NSString *type = data[indexPath.row][@"type"];
    if ([listType isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        type = cssList[@"type"];
    }
    //NSLog(@"index: %d and list type: %@, type: %@", indexPath.row, listType, type);
    if ([type isEqualToString:PAGE_TYPE_MENULIST] || [type isEqualToString:PAGE_TYPE_MENULIST_DB]) {
        [Util openMenuWithNextPage:[cell getNextPage] OnView:self.controller];
    } else if ([type isEqualToString:PAGE_TYPE_CALL] || [type isEqualToString:PAGE_TYPE_SMS]) {
        [self dailForType:[cell getCellData][@"type"]
               WithNumber:[cell getCellData][@"number"]
                 WithText:[cell getCellData][@"sms-text"]
                   OnView:self.controller];
        //[self CALLNSMS:indexPath];
    } else if ([type isEqualToString:PAGE_TYPE_DYNA_LIST]) {
        nextPage = [cell getNextPage];
        jsonURL = [Util openDynaListWithNextPage:nextPage AfterFinishCall:@selector(finishedLoadingDynalistData:) OnView:self.controller];
        //[self openDynaList:indexPath];
    } else if ([type isEqualToString:PAGE_TYPE_WEB]) {
        //NSString *params = [[data objectAtIndex:indexPath.row] objectForKey:@"params"];
        nextPage = [cell getNextPage];
        //NSLog(@"next page: %@", nextPage);
        [Util OpenWebWithNextPage:nextPage AndParam:[cell getCellData][@"params"] OnView:self.controller];
    } else if ([type isEqualToString:@"calendar"]) {
        NSLog(@"Calendar found");
    } else {
        NSLog(@"Type not found");
    }
}

- (void) dailForType: (NSString *) type WithNumber: (NSString *) number WithText: (NSString *) text OnView: (UIViewController *) view {
    
    //NSLog(@"type: %@, number: %@", type, number);
    UIDevice *device = [UIDevice currentDevice];
    NSLog(@"Device model: %@", [device model]);
    if ([[device model] isEqualToString:@"iPhone"]) {
        if ([type isEqual:@"call"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", number]]];
        } else {
            text = [NSString stringWithFormat:@"%@ %@", text, ([Holder sharedHolder].loginInfo)[@"e_id"]];
            NSLog(@"text: %@", text);
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText]) {
                controller.body = text;
                controller.recipients = @[number];
                controller.messageComposeDelegate = self;
                [Holder sharedHolder].viewController = view;
                [view presentViewController:controller animated:YES completion:nil];
            }
        }
    } else if ([[device model] isEqualToString:@"iPad"]) {
        if ([type isEqual:@"sms"]) {
            
            text = [NSString stringWithFormat:@"%@ %@", text, ([Holder sharedHolder].loginInfo)[@"e_id"]];
            //NSLog(@"text: %@", text);
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText]) {
                controller.body = text;
                controller.recipients = @[number];
                controller.messageComposeDelegate = self;
                [Holder sharedHolder].viewController = view;
                [view presentViewController:controller animated:YES completion:nil];
            }
        }
    } else {
        [Util featureNotSupported];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
            NSLog(@"Result: failed");
			break;
		case MessageComposeResultSent:
            NSLog(@"Result: sent");
			break;
		default:
            NSLog(@"Result: not sent");
            break;
	}
    
	[[Holder sharedHolder].viewController.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) dynalistDataLoad:(NSDictionary *) resData {
    
    if ([Util serverResponseCheckNullAndSession:resData InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:PAGE_TYPE_LIST In:resData InFunction:__FUNCTION__ Message:[NSString stringWithFormat:@"%@ parameter", PAGE_TYPE_LIST] Silent:NO]) {
            
            int dataCount = (int) [resData[PAGE_TYPE_LIST] count];
            //NSLog(@"response count %d page: %@", dataCount, nextPage);
            if (dataCount==0) {
                NSLog(@"called");
                UIAlertView *noData =[[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                message:[Util getSettingUsingName:DYNA_LIST_NODATA_MSG]
                                                               delegate:self
                                                      cancelButtonTitle:@"Back"
                                                      otherButtonTitles:nil];
                [noData show];
            } else {
                [Util finishLoadingDynaListWithata:resData NextPage:nextPage JSONURL:jsonURL OnView:self.controller];
            }
        }
    }
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([self.showResponse isEqual:alertView]) {
        if ([title isEqualToString:ALERT_OPT_OK]) {
            if ([data count] == 0) {
                [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:NO];
            }
        }
    }
    
    if([title isEqualToString:@"OK"]) {
        [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:YES];
    }
    
    if([title isEqualToString:@"Back"]) {
        [Util popWithFadeEffectOnViewController:self.controller ShouldPopToRoot:NO];
    }
}

#pragma mark - UISearchBarDelegate methods

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)filterSearchBar {
    
    [filterSearchBar setShowsScopeBar:YES];
    [filterSearchBar sizeToFit];
    [filterSearchBar setShowsCancelButton:YES animated:YES];
    
    for (UIView *view in filterSearchBar.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            //Change its properties
            [(UIButton *)view setTintColor:[Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"]];
            [(UIButton *)view setEnabled:YES];
            [(UIButton *)view setTitle:@"Search" forState:UIControlStateNormal];
        }
    }
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)filterSearchBar {
    
    [filterSearchBar setShowsScopeBar:NO];
    [filterSearchBar sizeToFit];
    [filterSearchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)filterSearchBar {
    [filterSearchBar resignFirstResponder];
    [self searchBarSearchButtonClicked:filterSearchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarWithText {
    
    NSString *text = searchBarWithText.text;
    NSLog(@"search bar text:%@ ", text);
    
    if(text.length == 0) {
        isFiltered = false;
    } else {
        isFiltered = true;
        filteredData = [[NSMutableArray alloc] init];
        //NSLog(@"data: %@", data);
        for (NSDictionary* dataItem in data) {
            
            NSRange titleRange, subTitleRange, descRange, actionDateRange, statusRange;
            BOOL titleCon = false, subTitleCon = false, descCon = false, actionDateCon = false, statusCon = false;
            
            if ([dataItem valueForKey:@"title"] != nil) {
                titleRange = [dataItem[@"title"] rangeOfString:text options:NSCaseInsensitiveSearch];
                titleCon = titleRange.location!=NSNotFound;
            }
            if ([dataItem valueForKey:@"sub-title"] != nil) {
                subTitleRange = [dataItem[@"sub-title"] rangeOfString:text options:NSCaseInsensitiveSearch];
                subTitleCon = subTitleRange.location!=NSNotFound;
            }
            if ([dataItem valueForKey:@"desc"] != nil) {
                descRange = [dataItem[@"desc"] rangeOfString:text options:NSCaseInsensitiveSearch];
                descCon = descRange.location!=NSNotFound;
            }
            if ([dataItem valueForKey:@"action-date"] != nil) {
                actionDateRange = [dataItem[@"action-date"] rangeOfString:text options:NSCaseInsensitiveSearch];
                actionDateCon = actionDateRange.location!=NSNotFound;
            }
            if ([dataItem valueForKey:@"status"] != nil) {
                statusRange = [dataItem[@"status"] rangeOfString:text options:NSCaseInsensitiveSearch];
                statusCon = statusRange.location!=NSNotFound;
            }
            
            if(titleCon || subTitleCon || descCon || actionDateCon || statusCon) {
                [filteredData addObject:dataItem];
            }
        }
        //NSLog(@"filtered data: %@", filteredData);
    }
    
    request = [self setCellIDWithData:filteredData];
    
    [((ListViewCtlr *) self.controller) reloadTable];
}

- (void) selectAll:(id)sender {
    
    UIButton *check = (UIButton *) sender;
    UITableViewController *controller = (UITableViewController *) self.controller;
    int section = 0;
    if (check.selected) {
        for (int inc=0; inc<[data count]; ++inc) {
            [controller.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:inc inSection:section]
                                                animated:NO];
        }
        self.isSelected = NO;
        check.selected = NO;
    } else {
        for (int inc=0; inc<[data count]; ++inc) {
            [controller.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:inc inSection:section]
                                              animated:NO
                                        scrollPosition:UITableViewScrollPositionNone];
        }
        self.isSelected = YES;
        check.selected = YES;
    }
}

- (void) approve:(id) sender {
    
    NSString *strParam = [self multipleApproveOrReject];
    int intRequestType;
    if ([pageName isEqualToString:@"leave-approve"]) {
        intRequestType = REQUEST_FOR_LEAVE;
    } else if([pageName isEqualToString:@"muster-approve"]){ //if ([pageName isEqualToString:@"muster-approve"])
        intRequestType = REQUEST_FOR_MUSTER;
    } else { //if ([pageName isEqualToString:@"muster-approve"])
        intRequestType = REQUEST_FOR_LATE_PUNCH;
    }
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(responseMultipleApproveReject:) onObject:self.controller];
    
    [conn multipleApproveRejectWithType:REQUEST_FOR_MULTIPLE_APPROVE For:intRequestType Param:strParam];
}

- (void) reject:(id) sender {
    
    NSString *strParam = [self multipleApproveOrReject];
    int intRequestType;
    if ([pageName isEqualToString:@"leave-approve"]) {
        intRequestType = intRequestType = REQUEST_FOR_LATE_PUNCH;
    } else if([pageName isEqualToString:@"muster-approve"]) { //if ([pageName isEqualToString:@"muster-approve"])
        intRequestType = REQUEST_FOR_MUSTER;
    } else { //if ([pageName isEqualToString:@"muster-approve"])
        intRequestType = REQUEST_FOR_LATE_PUNCH;
    }
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(responseMultipleApproveReject:) onObject:self.controller];
    [conn multipleApproveRejectWithType:REQUEST_FOR_MULTIPLE_REJECT For:intRequestType Param:strParam];
}

- (NSString *) multipleApproveOrReject {
    
    UITableViewController *controller = (UITableViewController *) self.controller;
    NSArray *arrSelectedIndexs = [controller.tableView indexPathsForSelectedRows];
    
    
    int intSelectedCount = (int) [arrSelectedIndexs count];
    
    NSLog(@"Select Count %d",intSelectedCount);
    
    NSMutableString *param1 = [[NSMutableString alloc] init];
    for (int inc=0; inc<intSelectedCount; ++inc) {
        int row = (int) ((NSIndexPath *)arrSelectedIndexs[inc]).row;
        
        NSLog(@"12 %@",request[row][0]);
        NSLog(@"123 %@",request[row][1]);
        
        NSString *chkData = [NSString stringWithFormat:@"%@|%@", request[row][0], request[row][1]];
        NSLog(@"2 %d",intSelectedCount);
        chkData = [Util encodeSpecialCharacter:chkData];
        NSLog(@"3 %d",intSelectedCount);
        [param1 appendString:[NSString stringWithFormat:@"chk_%d=%@", inc, chkData]];
        NSLog(@"4 %d",intSelectedCount);
        if (inc != intSelectedCount-1) {
            [param1 appendString:@"&"];
            NSLog(@"5 %d",intSelectedCount);
        }
    }
    NSLog(@"Query string: %@", param1);
    return [NSString stringWithFormat:@"listSize=%lu&%@", (unsigned long)[request count], param1];
}

- (void) responseMultipleApproveReject:(NSDictionary *)response {
    
    if ([response[SERVER_RES_STATUS] isEqualToString:@"200"]) {
        
        UITableViewController *controller = (UITableViewController *) self.controller;
        NSArray *arrSelectedIndexs = [controller.tableView indexPathsForSelectedRows];
        int intSelectedCount = (int) [arrSelectedIndexs count];
        
        NSMutableArray *dataCheck = data;
        //        Not required
        //        if (isFiltered) {
        //            dataCheck = filteredData;
        //        } else {
        //            dataCheck = data;
        //        }
        
        //        NSLog(@"Request: %@", request);
        //        NSLog(@"Selected Count: %d", intSelectedCount);
        //        NSLog(@"Count Before: %d", [dataCheck count]);
        for (int inc=0; inc < intSelectedCount; ++inc) {
            int row = (int) ((NSIndexPath *)arrSelectedIndexs[inc]).row;
            for (int inc2 = 0; inc2 < [dataCheck count]; ++ inc2) {
                
                BOOL isParamMatch = [dataCheck[inc2][@"params"] rangeOfString:request[row][0]].location != NSNotFound &&
                [dataCheck[inc2][@"params"] rangeOfString:request[row][1]].location != NSNotFound;
                if (isParamMatch) {
                    //                    NSLog(@"Request: %d loop: %d", inc, inc2);
                    [dataCheck removeObjectAtIndex:inc2];
                    continue;
                }
            }
        }
        //        NSLog(@"Count After: %d", [dataCheck count]);
        
        // Assign new new data
        data = dataCheck;
        [self updatePendingApprovalCount];
        
        int count = [UIApplication sharedApplication].applicationIconBadgeNumber;
        
        count = count - [dataCheck count];
        
        NSLog(@"Update Badge %i",count);
//        [Holder sharedHolder].pendingBadgeCount = count;
        
        //Update Badge Count
//        [UIApplication sharedApplication].applicationIconBadgeNumber = count;
        
        if ([dataCheck count] != 0) {
            ListViewCtlr *listController = (ListViewCtlr *) self.controller;
            isFiltered = NO;
            [listController editButtonSelected:nil];
            [listController.tableView reloadData];
            
            // Reset Request Param value
            request = [self setCellIDWithData:data];
        }
    }
    
    self.showResponse = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                   message:response[SERVER_RES_MEG]
                                                  delegate:self
                                         cancelButtonTitle:ALERT_OPT_OK
                                         otherButtonTitles:nil];
    [self.showResponse show];
}

@end
