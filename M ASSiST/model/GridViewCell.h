//
//  GridViewCell.h
//  Universe on the move
//
//  Created by Suraj on 29/05/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTCollectionView.h"

@interface GridViewCell : PSUICollectionViewCell {
    
}

@property (nonatomic) IBOutlet UIImageView *imgIcon;
@property (nonatomic) IBOutlet UILabel *lblName;
@property (nonatomic) IBOutlet UIButton *btnPendingCount;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (void) initCellWithData:(NSDictionary *) data CSS:(NSDictionary *) cssList For:(NSString *) viewController;

- (void) didHighlightItemForGridViewCell:(GridViewCell *) cell;
- (void) didUnHighlightItemForGridViewCell:(GridViewCell *) cell;

@end
