//
//  ESOPModel.h
//  Universe on the move
//
//  Created by Phyder on 28/10/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Navigation;

@interface ESOPModel : NSObject {
    
    NSMutableArray *data, *dataHeight;
    NSString *listType, *pageName, *pageTitle;
    NSMutableDictionary *content;
    
    NSDictionary *cssPage;
    
    NSString *jsonURL, *nextPage;
}

@property (nonatomic, weak) UIViewController *controller;   // Controller view reference
@property (nonatomic) Navigation *navigation;

- (id) initDataForView:(UIViewController *) view WithPage:(NSString *) page WithTitle:(NSString *) title AndWithData:(NSMutableDictionary *) pageContent;

@end
