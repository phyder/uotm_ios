//
//  Annotation.m
//  Home Offer
//
//  Created by Suraj on 28/02/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "HomeAnnotation.h"

#import "HomeListViewCtlr.h"

#import "AppConnect.h"
#import "ErrorLog.h"
#import "Util.h"

@implementation HomeAnnotation

- (id)initWithViewController:(UIViewController *) view
                          ID:(NSString *)ID
                       Title:(NSString *)title
                    subtitle:(NSString *)subtitle
                 recentCount:(NSString *) count
                  coordinate:(CLLocationCoordinate2D)coordinate {
    
    if ((self = [super init])) {
        _controller = view;
        _ID = ID;
        _title = title;
        if ([count intValue] > 0) {
            _subtitle = [NSString stringWithFormat:@"%@ - Recent offer: %@", subtitle, count];
        } else {
//            _subtitle = [NSString stringWithFormat:@"%@ - No recent offers", subtitle];
        }
        _recentOffer = count;
        _coordinate = coordinate;
        
        _detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [_detailButton addTarget:self
                         action:@selector(detailDisclosurePressed)
               forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void) detailDisclosurePressed {
    
    //NSLog(@"%@ pressed, ID: %@", [_title substringToIndex:[_title rangeOfString:@" - "].location], _ID);
    NSString *cityName = [self.title substringToIndex:[self.title rangeOfString:@" - "].location];
    
    AppConnect *conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resHomeList:) onObject:_controller];
    [conn getOfferListWithCityId:self.ID AnsCityName:cityName OnNSObject:self];
}

- (void) resHomeList:(NSDictionary *) responseDict {
    
//    NSLog(@"Home list: %@", responseDict);
    responseJSON = responseDict;
    [self validateHomeListResponse];
}

- (void) validateHomeListResponse {
    
    if ([Util serverResponseCheckNullAndSession:responseJSON InFunction:__FUNCTION__ Silent:NO]) {
        
        if ([Util serverResponseForField:@"data" In:responseJSON InFunction:__FUNCTION__ Message:@"data parameter" Silent:NO]) {
            
            @try {
                //NSLog(@"response data: %@", responseJSON);
                UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                HomeListViewCtlr *listViewCtlr  = (HomeListViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"HomeListView"];
                listViewCtlr.title          = responseJSON[@"city"];
                listViewCtlr.listData       = responseJSON[@"data"];
                //[_navigation pushViewController:listViewCtlr animated:YES];
                [Util pushWithFadeEffectOnViewController:_controller ViewController:listViewCtlr];
            } @catch (NSException *exception) {
                NSString *strLogMessage = @"Not able to open Home List View";
#if DEBUG_ERROR_LOG
                NSLog(@"%@", strLogMessage);
#endif
                [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
                [Util showTechnicalError];
            }
        }
    }
}

- (int) getRecentOfferCount {
    
    return [_recentOffer intValue];
}

@end
