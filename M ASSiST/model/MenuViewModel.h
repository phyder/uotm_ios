//
//  MenuViewModel.h
//  M ASSiST
//
//  Created by Suraj on 23/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuViewModel : NSObject <UIAlertViewDelegate> {
    
    NSMutableArray *data;           // Current page data
    NSDictionary *css;              // Current page css data
}

@property (nonatomic, weak) UIViewController *controller;   // Controller view reference

- (void) initDataWithView:(UIViewController *) controllerView;
- (UIView *) getSignOutButtonInMenuHeaderWithHeight:(int) height;

- (int) getMenuItemCount;           // Number of Item in the list

- (NSDictionary *) getDataForIndexPath:(int) position;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) didSelectItemAtIndex:(NSInteger) index;

@end
