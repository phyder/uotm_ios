//
//  Authentication.m
//  Universe on the move
//
//  Created by Suraj on 03/08/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "Authentication.h"

#import <CommonCrypto/CommonCrypto.h>
#import "AppConnect.h"
#import "Constant.h"
#import "ErrorLog.h"
#import "Holder.h"
#import "Util.h"

#import "CarouselViewCtlr.h"
#import "GridViewCtlr.h"
#import "IIViewDeckController.h"
#import "LoginModel.h"
#import "ListViewCtlr.h"
#import "RegisterModel.h"
#import "OTPModel.h"
#import "MPinModel.h"
#import "AutoLoginModel.h"
#import "ResetMPINModel.h"
#import "VerifyMPinViewCtlr.h"

#import "ECrypto.h"

@implementation Authentication

- (id) initWithViewController:(UIViewController *) viewController AndModel:(NSObject *) controllerModel {
    
    self = [super init];
    if (self) {
        // Custom initialization
        controller  = viewController;
        model       = controllerModel;
        //
        // Check model is of type LoginModel
        //
        if ([model isKindOfClass:[LoginModel class]]) {
            loginModel = (LoginModel *) model;
        } else if ([model isKindOfClass:[RegisterModel class]]) {
            registerModel = (RegisterModel *) model;
        } else if ([model isKindOfClass:[OTPModel class]]) {
            otpModel = (OTPModel *) model;
        } else if ([model isKindOfClass:[MPinModel class]]) {
            mPinModel = (MPinModel *) model;
        } else if ([model isKindOfClass:[AutoLoginModel class]]) {
            autoLoginModel = (AutoLoginModel *) model;
        } else if ([model isKindOfClass:[ResetMPINModel class]]) {
            resetMPinModel = (ResetMPINModel *) model;
        }
    }
    return self;
}

- (BOOL)requestRegisterWithUsername:(NSString *)username AndPassword:(NSString *)password {
    
    BOOL result = TRUE;
    
    strUsername = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strPassword = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
    // Encryption
    //    NSLog(@"Password: %@", strPassword);
    //strPassword         = [crypto encryptAES:strPassword];
    //    NSLog(@"Encrypted Password: %@", strPassword);
    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
    //    NSLog(@"Key: %@", encAesKey);
    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
    //    NSLog(@"Key: %@", encAesKey);
    
    if ([strUsername isEqualToString:EMPTY] || [strPassword isEqualToString:EMPTY]) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:REGISTER_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_REGISTER_URL]];
        
        strUsername = [Util encodeSpecialCharacter:strUsername];
        strPassword = [Util encodeSpecialCharacter:strPassword];
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
        
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resRegister:) onObject:controller];
        [self.conn requestRegisterWithURL:url username:strUsername password:strPassword key:encAesKey];
        //        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    
    return result;
}

- (BOOL)postRegisterAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    [[Holder sharedHolder] setLoginInfo:responseData];
                    [self registerSuccesfull];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Register request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) registerSuccesfull {
    
    NSLog(@"register successfull & Authorise");
    
    //
    // Check model is of type LoginModel
    //
    if ([model isKindOfClass:[RegisterModel class]]) {
        
        // Clears UITextField Username and Password.
        [registerModel clearCredentials];
    }
    NSLog(@"Register User Id: %@", strUsername);
    [Util setKeyChainWithUserName:strUsername];
//    [Util setSettingWithName:LAST_LOGIN_USER_ID ByValue:strUsername];
    
    Holder *holder              = [Holder sharedHolder];
    holder.refresh              = PAGE_FALSE;
    holder.isLoggedIn           = PAGE_FALSE;
    holder.notificationCount    = 0;
    
    //NSLog(@"content: %@", content);
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
    [controller performSelectorOnMainThread:@selector(initOtpViewController) withObject:nil waitUntilDone:YES];
  //  [self startBackgroundNotificationCheck];
}

- (BOOL)requestOTPWithOtp:(NSString *)otp {
    
    BOOL result = TRUE;
    
    otp = [otp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
    // Encryption
    //    NSLog(@"Password: %@", strPassword);
    //otp         = [crypto encryptAES:otp];
    //    NSLog(@"Encrypted Password: %@", strPassword);
    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
    //    NSLog(@"Key: %@", encAesKey);
    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
    //    NSLog(@"Key: %@", encAesKey);
    
    if ([otp isEqualToString:EMPTY]) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:OTP_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_OTP_URL]];
        strUsername = [Util getKeyChainUserName];
        strUsername = [Util encodeSpecialCharacter:strUsername];
        otp = [Util encodeSpecialCharacter:otp];
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
        
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resOtp:) onObject:controller];
        [self.conn requestOTPWithURL:url username:strUsername otp:otp key:encAesKey];
        //        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    
    return result;
}

- (BOOL)postOTPAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    [self otpSuccesfull];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"OTP request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) otpSuccesfull {
    
    NSLog(@"otp successfull & Authorise");
    //
    // Check model is of type LoginModel
    //
    if ([model isKindOfClass:[OTPModel class]]) {
        // Clears UITextField Username and Password.
        [otpModel clearCredentials];
    }
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
    [controller performSelectorOnMainThread:@selector(initMPinViewController) withObject:nil waitUntilDone:YES];
    
   // [self startBackgroundNotificationCheck];
}

- (BOOL)requestSetMPinWithNewMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin {
    
    BOOL result = TRUE;
    
    newMPin = [newMPin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    retypeMPin = [retypeMPin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    strMPin = newMPin;
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
    // Encryption
    //    NSLog(@"Password: %@", strPassword);
    //newMPin         = [crypto encryptAES:newMPin];
    
    //retypeMPin      = [crypto encryptAES:retypeMPin];
    //    NSLog(@"Encrypted Password: %@", strPassword);
    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
    //    NSLog(@"Key: %@", encAesKey);
    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
    //    NSLog(@"Key: %@", encAesKey);
    
    if ([newMPin isEqualToString:EMPTY] || [retypeMPin isEqualToString:EMPTY]) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:MPIN_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *strSessionID = ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID];
        NSString *url = [NSString stringWithFormat:@"%@/%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_MPIN_URL], J_SESSION_ID, strSessionID];
//        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_MPIN_URL]];
        
        strUsername = [Util getKeyChainUserName];
        strUsername = [Util encodeSpecialCharacter:strUsername];
//        newMPin = [Util encodeSpecialCharacter:newMPin];
//        retypeMPin = [Util encodeSpecialCharacter:retypeMPin];
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
        
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resSetMPin:) onObject:controller];
        [self.conn requestSetMPinWithURL:url username:strUsername newMPin:newMPin retypeMPin:retypeMPin key:encAesKey];
        //        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    
    return result;
}

- (BOOL)postMPinAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    strUsername = serverResponseData[@"e_id"];
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    
                    setMPinAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:@"m-pin submitted successfully"
                                                                     delegate:self
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [setMPinAlert show];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Set MPin request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) setMPinSuccesfull {
    
    NSLog(@"set MPin successfull & Authorise");
    
    //
    // Check model is of type LoginModel
    //
    if ([model isKindOfClass:[MPinModel class]]) {
        
        // Clears UITextField Username and Password.
        [mPinModel clearCredentials];
    }
    NSLog(@"Login User Id: %@", strUsername);
    [Util setSettingWithName:LAST_LOGIN_USER_ID ByValue:strUsername];
    
    Holder *holder              = [Holder sharedHolder];
    holder.refresh              = PAGE_FALSE;
    holder.isLoggedIn           = PAGE_TRUE;
    holder.loginInfo            = serverResponseData;
    holder.notificationCount    = 0;
    
    NSMutableDictionary *content = (NSMutableDictionary *) [Util getPage:PAGE_MAIN_MENU];
    //NSLog(@"content: %@", content);
    //serverResponseData
    
    NSString *str = serverResponseData[@"mpin"];
    
    [Util setKeyChainWithMPin:str];
    
    NSString *badge = serverResponseData[@"count"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:badge forKey:@"badge"];
    [defaults synchronize];
    
    [Holder sharedHolder].pendingBadgeCount = [badge intValue];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
    
    
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
    [controller performSelectorOnMainThread:@selector(initPanMenuListController) withObject:nil waitUntilDone:YES];
    
    /*Holder *singletonRef = [Holder sharedHolder];
    singletonRef.pendingCountLeave      = @"";
    singletonRef.pendingCountMuster     = @"";
    singletonRef.pendingCountConveyance = @"";
    singletonRef.pendingCountLatePunch = @"";
    
    @try {
        if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"carousel"]) {
            
            NSLog(@"%s Main menu is Carorsel", __FUNCTION__);
            
            //
            // Check model is of type LoginViewMod
            //
            NSString *strEmergencyNumber = nil;
            if ([model isKindOfClass:[MPinModel class]]) {
                
                // Gets Emergency call number from login model which, required on CarouselViewCtlr
                strEmergencyNumber = [mPinModel getEmergencyNumber];
            }
            
            UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            CarouselViewCtlr *carouselCtlr  = (CarouselViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carouselview"];
            carouselCtlr.page               = PAGE_MAIN_MENU;
            carouselCtlr.content            = content;
            carouselCtlr.strEmergencyNumber = strEmergencyNumber;
            
            [Util pushWithFadeEffectOnViewController:controller ViewController:carouselCtlr];
        } else if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"grid"]) {
            
            NSLog(@"%s grid not implement yet", __FUNCTION__);
        } else if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"list"]) {
            
            NSLog(@"%s list", __FUNCTION__);
            UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            ListViewCtlr *listViewCtlr  = (ListViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"listview"];
            listViewCtlr.page           = PAGE_MAIN_MENU;
            listViewCtlr.content        = content;
            
            [Util pushWithFadeEffectOnViewController:controller ViewController:listViewCtlr];
        } else {
            NSLog(@"%s main main view not found", __FUNCTION__);
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Cannot transition to Main menu";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }*/
    
   // [self startBackgroundNotificationCheck];
}

- (BOOL)requestAutoLoginWithUserName:(NSString *)userName mPin:(NSString *)mPin bio:(NSString *)bio {
    
    BOOL result = TRUE;
    
    strUsername = [userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //mPin = [mPin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //mPin = [Util getKeyChainMPin];
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
    NSError *error;
    if (![bio isEqualToString:EMPTY]) {
        NSData *cipherEncryp = [Util doCipher:bio context:kCCEncrypt error:&error];
        bio = [cipherEncryp base64EncodedStringWithOptions:0];
    }else if (![mPin isEqualToString:EMPTY]){
        NSData *cipherEncryp = [Util doCipher:mPin context:kCCEncrypt error:&error];
        mPin = [cipherEncryp base64EncodedStringWithOptions:0];
    }
    
    
    //NSLog(@"This is Encrypted key %@",newStr1);
    
//    NSData *cipherDecrypt = [Util doCipher:mPin context:kCCDecrypt error:&error];
//    NSString *string = [[NSString alloc] initWithData:cipherDecrypt encoding:NSUTF8StringEncoding];
//    NSString* newStr2 = [cipherDecrypt base64EncodedStringWithOptions:0];
//    NSLog(@"This is Decrypted key %@",string);
    
    // Encryption
    //    NSLog(@"Password: %@", strPassword);
    //mPin         = [crypto encryptAES:mPin];
    
    //    NSLog(@"Encrypted Password: %@", strPassword);
    
    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
    //    NSLog(@"Key: %@", encAesKey);
    
    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
    //    NSLog(@"Key: %@", encAesKey);
    
    if ([userName isEqualToString:EMPTY] || ([mPin isEqualToString:EMPTY] && [bio isEqualToString:EMPTY])) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:LOGIN_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_AUTO_LOGIN_URL]];
        
        //strUsername = [Util encodeSpecialCharacter:strUsername];
        
        if ([bio isEqualToString:@""]) {
            mPin = [Util encodeSpecialCharacter:mPin];
            mPin = [Util encodeSpecialCharacter:mPin];//Testing purpose
        }else {
            bio = [Util encodeSpecialCharacter:bio];
            bio = [Util encodeSpecialCharacter:bio];//Testing purpose
        }
        
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
     
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resSignIn:) onObject:controller];
        [self.conn requestAutoLoginWithURL:url username:strUsername mPin:mPin key:encAesKey bio:bio];
        //        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    return result;
}

- (BOOL)postAutoLoginAuthenticationWithData:(NSMutableDictionary *)responseData {
 
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    [self updateCheck];
                   // [self autoLoginSuccesfull];
                } else {
                    
                    registerAlert.tag = 0;
                    if ([status isEqualToString:@"501"]) {
                        
                        registerAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                   message:@"Your device not register, Please register your device"
                                                                  delegate:self
                                                         cancelButtonTitle:ALERT_OPT_OK
                                                         otherButtonTitles:nil];
                        
                        registerAlert.tag = 1;
                    } else {
                        registerAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                   message:serverResponseData[SERVER_RES_MEG]
                                                                  delegate:self
                                                         cancelButtonTitle:ALERT_OPT_OK
                                                         otherButtonTitles:nil];
                        registerAlert.tag = 0;
                    }
                    
                    [registerAlert show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Auto Login request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) autoLoginSuccesfull {
    
    NSLog(@"Auto Login successfull & Authorise");
    
    //
    // Check model is of type LoginModel
    //
//    if ([model isKindOfClass:[MPinModel class]]) {
//        
//        // Clears UITextField Username and Password.
//        [mPinModel clearCredentials];
//    }
    NSLog(@"Login User Id: %@", strUsername);
    [Util setSettingWithName:LAST_LOGIN_USER_ID ByValue:strUsername];
    
    NSLog(@"iSRegistered %@",serverResponseData[@"is_reg"]);
    
    NSString *isReg = serverResponseData[@"is_reg"];
    
//    NSString *badge = serverResponseData[@"count"];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:badge forKey:@"badge"];
//    [defaults synchronize];
//    
//    [Holder sharedHolder].pendingBadgeCount = [badge intValue];
//    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
    
    Holder *holder              = [Holder sharedHolder];
    holder.refresh              = PAGE_FALSE;
    holder.isLoggedIn           = PAGE_TRUE;
    holder.loginInfo            = serverResponseData;
    holder.notificationCount    = 0;
    
    NSMutableDictionary *content = (NSMutableDictionary *) [Util getPage:PAGE_MAIN_MENU];
    //NSLog(@"content: %@", content);
    
    [controller performSelectorOnMainThread:@selector(initPanMenuListController) withObject:nil waitUntilDone:YES];
    
    Holder *singletonRef = [Holder sharedHolder];
    singletonRef.pendingCountLeave      = @"";
    singletonRef.pendingCountMuster     = @"";
    singletonRef.pendingCountConveyance = @"";
    singletonRef.pendingCountLatePunch = @"";
    
    @try {
        if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"carousel"]) {
            
            NSLog(@"%s Main menu is Carorsel", __FUNCTION__);
            
            //
            // Check model is of type LoginViewMod
            //
            NSString *strEmergencyNumber = nil;
            if ([model isKindOfClass:[AutoLoginModel class]]) {
                
                // Gets Emergency call number from login model which, required on CarouselViewCtlr
                strEmergencyNumber = [autoLoginModel getEmergencyNumber];
            }
            
            UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            CarouselViewCtlr *carouselCtlr  = (CarouselViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carouselview"];
            carouselCtlr.page               = PAGE_MAIN_MENU;
            carouselCtlr.content            = content;
            carouselCtlr.strEmergencyNumber = strEmergencyNumber;
            
            [Util pushWithFadeEffectOnViewController:controller ViewController:carouselCtlr];
        } else if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"grid"]) {
            
            NSLog(@"%s grid not implement yet", __FUNCTION__);
        } else if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"list"]) {
            
            NSLog(@"%s list", __FUNCTION__);
            UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            ListViewCtlr *listViewCtlr  = (ListViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"listview"];
            listViewCtlr.page           = PAGE_MAIN_MENU;
            listViewCtlr.content        = content;
            
            [Util pushWithFadeEffectOnViewController:controller ViewController:listViewCtlr];
        } else {
            NSLog(@"%s main main view not found", __FUNCTION__);
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Cannot transition to Main menu";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }
    [self startBackgroundNotificationCheck];
}

- (BOOL)requestResetMPinWithUserName:(NSString *)userName password:(NSString *)password NewMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin {
    
    BOOL result = TRUE;
    
    userName = [Util getKeyChainUserName];
    
    userName = [userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    password = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    newMPin = [newMPin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    retypeMPin = [retypeMPin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    strUsername = userName;
    strPassword = password;
    strMPin = newMPin;
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
    // Encryption
    //    NSLog(@"Password: %@", strPassword);
    //newMPin         = [crypto encryptAES:newMPin];
    
    //retypeMPin      = [crypto encryptAES:retypeMPin];
    //    NSLog(@"Encrypted Password: %@", strPassword);
    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
    //    NSLog(@"Key: %@", encAesKey);
    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
    //    NSLog(@"Key: %@", encAesKey);
    
    if ([userName isEqualToString:EMPTY] || [password isEqualToString:EMPTY] || [newMPin isEqualToString:EMPTY] || [retypeMPin isEqualToString:EMPTY]) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:RESET_MPIN_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_RESET_MPIN_URL]];
        
        strUsername = [Util encodeSpecialCharacter:strUsername];
        strPassword = [Util encodeSpecialCharacter:strPassword];
//        newMPin = [Util encodeSpecialCharacter:newMPin];
//        retypeMPin = [Util encodeSpecialCharacter:retypeMPin];
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
        
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resResetMPin:) onObject:controller];
        [self.conn requestResetMPinWithURL:url username:userName password:password newMPin:newMPin retypeMPin:retypeMPin key:encAesKey];
        //        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    
    return result;
}

- (BOOL)postResetMPinAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if(serverResponseData[@"data"] == nil) {
                
            } else {
                serverResponseData = serverResponseData[@"data"];
            }
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    
                    [self setResetMPinSuccesfull];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Reset MPin request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) setResetMPinSuccesfull {
    
    NSLog(@"reset MPin successfull & Authorise");
    
    //
    // Check model is of type LoginModel
    //
    if ([model isKindOfClass:[ResetMPINModel class]]) {
        
        // Clears UITextField Username and Password.
        [resetMPinModel clearCredentials];
    }
    NSLog(@"Login User Id: %@", strUsername);
    [Util setSettingWithName:LAST_LOGIN_USER_ID ByValue:strUsername];
    
    [Util setKeyChainWithUserName:strUsername];
    
    NSString *str = serverResponseData[@"mpin"];
    NSLog(@"mPin: %@", str);
    
    [Util setKeyChainWithMPin:str];
    
    resetAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                      message:serverResponseData[SERVER_RES_MEG]
                                                     delegate:self
                                            cancelButtonTitle:ALERT_OPT_OK
                                            otherButtonTitles:nil];
    [resetAlert show];
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
//    [controller performSelectorOnMainThread:@selector(initPanMenuListController) withObject:nil waitUntilDone:YES];
//    
//    Holder *singletonRef = [Holder sharedHolder];
//    singletonRef.pendingCountLeave      = @"";
//    singletonRef.pendingCountMuster     = @"";
//    singletonRef.pendingCountConveyance = @"";
//    singletonRef.pendingCountLatePunch = @"";
    
  //  [self startBackgroundNotificationCheck];
}

- (BOOL)requestSignInWithUsername:(NSString *)username AndPassword:(NSString *)password {
    
    BOOL result = TRUE;
    
    strUsername = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strPassword = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
    // Encryption
//    NSLog(@"Password: %@", strPassword);
    //strPassword         = [crypto encryptAES:strPassword];
//    NSLog(@"Encrypted Password: %@", strPassword);
    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
//    NSLog(@"Key: %@", encAesKey);
    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
//    NSLog(@"Key: %@", encAesKey);
    
    if ([strUsername isEqualToString:EMPTY] || [strPassword isEqualToString:EMPTY]) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:LOGIN_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_LOGIN_URL]];
        
        strUsername = [Util encodeSpecialCharacter:strUsername];
        strPassword = [Util encodeSpecialCharacter:strPassword];
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
        
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resSignIn:) onObject:controller];
        [self.conn requestSignInWithURL:url username:strUsername password:strPassword key:encAesKey];
//        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    
    return result;
}

#pragma mark - Post Login Authentication

- (BOOL)postLoginAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
//                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    
                    [self updateCheck];
//                    [self loginSuccesfull];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Sign In request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) updateCheck {
    
    if ([Util serverResponseForField:LOGIN_RES_PARAM_UPADTE In:serverResponseData InFunction:__FUNCTION__ Message:@"update parameter" Silent:NO]) {
        
        if ([Util serverResponseForField:LOGIN_RES_PARAM_DOWNLOAD_URL In:serverResponseData InFunction:__FUNCTION__ Message:@"url parameter" Silent:NO]) {
            
            if ([Util serverResponseForField:@"msg" In:serverResponseData InFunction:__FUNCTION__ Message:@"msg parameter" Silent:NO]) {
                
                NSString *strUpdate = serverResponseData[LOGIN_RES_PARAM_UPADTE];
                
                int intUpdate = 0;
                intUpdate = [strUpdate intValue];
                if (intUpdate == 0) {
                   // [self loginSuccesfull]; This is old change
                    [self autoLoginSuccesfull];//This is new change
                } else {
                    strDownlaodURL = serverResponseData[LOGIN_RES_PARAM_DOWNLOAD_URL];
                    if (![Util isNull:strDownlaodURL]) {
                        if (intUpdate == 1) {
                            updateAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                     message:serverResponseData[SERVER_RES_MEG]
                                                                    delegate:self
                                                           cancelButtonTitle:ALERT_OPT_NOW
                                                           otherButtonTitles:ALERT_OPT_LATER, nil];
                            [updateAlert show];
                        } else if (intUpdate == 2) {
                            updateAlert = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                     message:serverResponseData[SERVER_RES_MEG]
                                                                    delegate:self
                                                           cancelButtonTitle:ALERT_OPT_OK
                                                           otherButtonTitles:nil];
                            [updateAlert show];
                        } else {
                            [ErrorLog logAuthErrorWithCode:ERR_CODE_UPD_ERR
                                               Description:ERR_MSG_UNEXP_UPD
                                                   Message:[NSString stringWithFormat:@"update value returned by server: %d", intUpdate]
                                                InFunction:__FUNCTION__];
                            [Util showTechnicalError];
                        }
                        
                    } else {
                        [ErrorLog logAuthErrorWithCode:ERR_CODE_UPD_ERR
                                           Description:ERR_MSG_NULL_URL
                                               Message:@"download url"
                                            InFunction:__FUNCTION__];
                        [Util showTechnicalError];
                    }
                    
                }
            }
        }
    }
}

- (void) loginSuccesfull {
    
    NSLog(@"login successfull & Authorise");
    
    //
    // Check model is of type LoginModel
    //
    if ([model isKindOfClass:[LoginModel class]]) {
        
        // Clears UITextField Username and Password.
        [loginModel clearPassword];
    }
    NSLog(@"Login User Id: %@", strUsername);
    [Util setSettingWithName:LAST_LOGIN_USER_ID ByValue:strUsername];
    
    Holder *holder              = [Holder sharedHolder];
    holder.refresh              = PAGE_FALSE;
    holder.isLoggedIn           = PAGE_TRUE;
    holder.loginInfo            = serverResponseData;
    holder.notificationCount    = 0;
    
    NSMutableDictionary *content = (NSMutableDictionary *) [Util getPage:PAGE_MAIN_MENU];
    //NSLog(@"content: %@", content);
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
    [controller performSelectorOnMainThread:@selector(initPanMenuListController) withObject:nil waitUntilDone:YES];
    
    Holder *singletonRef = [Holder sharedHolder];
    singletonRef.pendingCountLeave      = @"";
    singletonRef.pendingCountMuster     = @"";
    singletonRef.pendingCountConveyance = @"";
    singletonRef.pendingCountLatePunch  = @"";
    
    @try {
        if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"carousel"]) {
            
            NSLog(@"%s Main menu is Carorsel", __FUNCTION__);
            
            //
            // Check model is of type LoginViewMod
            //
            NSString *strEmergencyNumber = nil;
            if ([model isKindOfClass:[LoginModel class]]) {
                
                // Gets Emergency call number from login model which, required on CarouselViewCtlr
                strEmergencyNumber = [loginModel getEmergencyNumber];
            }
            
            UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            CarouselViewCtlr *carouselCtlr  = (CarouselViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"carouselview"];
            carouselCtlr.page               = PAGE_MAIN_MENU;
            carouselCtlr.content            = content;
            carouselCtlr.strEmergencyNumber = strEmergencyNumber;
            
            [Util pushWithFadeEffectOnViewController:controller ViewController:carouselCtlr];
        } else if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"grid"]) {
            
            NSLog(@"%s grid not implement yet", __FUNCTION__);
        } else if ([[Util getSettingUsingName:MAIN_MENU_VIEW] isEqualToString:@"list"]) {
            
            NSLog(@"%s list", __FUNCTION__);
            UIStoryboard *storyboard    = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            ListViewCtlr *listViewCtlr  = (ListViewCtlr *)[storyboard instantiateViewControllerWithIdentifier:@"listview"];
            listViewCtlr.page           = PAGE_MAIN_MENU;
            listViewCtlr.content        = content;
            
            [Util pushWithFadeEffectOnViewController:controller ViewController:listViewCtlr];
        } else {
            NSLog(@"%s main main view not found", __FUNCTION__);
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Cannot transition to Main menu";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:strLogMessage];
    }
    
   // [self startBackgroundNotificationCheck];
}

- (BOOL)requestGetRegisterListWithUsername:(NSString *)username {
    
    BOOL result = TRUE;
    
    strUsername = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_GET_REGISTER_LIST_URL]];
    
    //strUsername = [Util encodeSpecialCharacter:strUsername];
    
    self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resGetRegisterList:) onObject:controller];
    [self.conn requestGetRegisterListWithURL:url username:username];
    
    return result;
}

- (BOOL)postGetRegisterListAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    [self getRegisterListSuccesfull];
    
    return result;
}

- (void) getRegisterListSuccesfull {
    
    NSLog(@"Get Register List successfull");
    
    [controller performSelectorOnMainThread:@selector(showRegisterDeviceList) withObject:nil waitUntilDone:YES];
    
   // [self startBackgroundNotificationCheck];
}

- (BOOL)requestDeregisterWithUsername:(NSString *)username deviceId:(NSString *)deviceId {
    
    BOOL result = TRUE;
    
    strUsername = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strDeregDeviceId = deviceId;
    
    NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_DEREGISTER_URL]];
    
    //strUsername = [Util encodeSpecialCharacter:strUsername];
    
    self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resDeregister:) onObject:controller];
    [self.conn requestDeregisterWithURL:url username:username deviceId:deviceId];
    
    return result;
}

- (BOOL)postDeregisterAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            if(serverResponseData[@"data"] == nil) {
                
            } else {
                serverResponseData = serverResponseData[@"data"];
            }
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    
                    [self deregisterSuccesfull];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Deregister request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) deregisterSuccesfull {
    
    NSLog(@"deregister successfull");
    if([strDeregDeviceId isEqualToString:[Util getSettingUsingName:APP_DEVICE_ID]]) {
        [Util removeUserSettings];
        
        [controller performSelectorOnMainThread:@selector(initRegisterViewController) withObject:nil waitUntilDone:YES];
    } else {
        [controller performSelectorOnMainThread:@selector(getDeviceList) withObject:nil waitUntilDone:YES];
    
    }
    
   // [self startBackgroundNotificationCheck];
}

- (BOOL)requestVerifyMPinWithUserName:(NSString *)userName MPin:(NSString *)mPin {
    
    BOOL result = TRUE;
    
    userName = [userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mPin = [mPin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    strUsername = userName;
    
    ECrypto *crypto = [ECrypto sharedInstance];
    [crypto generateAESKey];
    
//    // Encryption
//    //    NSLog(@"Password: %@", strPassword);
//    newMPin         = [crypto encryptAES:newMPin];
//    
//    retypeMPin      = [crypto encryptAES:retypeMPin];
//    //    NSLog(@"Encrypted Password: %@", strPassword);
//    NSString *encAesKey = [crypto encryptRSA:[NSString stringWithFormat:@"%@|%@", crypto.AESKey, crypto.AESIV]];
//    //    NSLog(@"Key: %@", encAesKey);
//    encAesKey           = [Util encodeSpecialCharacter:encAesKey];
//    //    NSLog(@"Key: %@", encAesKey);
    
    if ([mPin isEqualToString:EMPTY]) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                          message:[Util getSettingUsingName:MPIN_INVALID_CREDENTIALS]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        result = FALSE;
    } else {
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_VERIFY_MPIN_URL]];
        
        strUsername = [Util encodeSpecialCharacter:strUsername];
//        mPin = [Util encodeSpecialCharacter:mPin];
        
        //    NSString *params = [NSString stringWithFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", LOGIN_PARAM_VIEW_TYPE, @"json", LOGIN_PARAM_USER_NAME, strUsername, LOGIN_PARAM_USER_PASS, strPassword, LOGIN_PARAM_DEVICE_ID, [Util getSettingUsingName:APP_ID], LOGIN_PARAM_DEVICE_TYPE, DEVICE_TYPE];
        
        //    NSString *params = @"";
        
        //    urlString = [NSString stringWithFormat:@"%@%@", urlString, params];
        //    NSLog(@"login url: %@", urlString);
        
        self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resVerifyMPin:) onObject:controller];
        [self.conn requestVerifyMPinWithURL:url username:userName mPin:mPin key:@""];
        //        [self.conn requestSignInWithURL:urlString Username:strUsername AndPassword:strPassword];
    }
    
    return result;
}

- (BOOL)postVerifyMPinAuthenticationWithData:(NSMutableDictionary *)responseData {
    
    self.conn = nil;
    BOOL result = true;
    serverResponseData = responseData;
    
    @try {
        if ([Util isNull:serverResponseData]) {
            [ErrorLog logNullResponseInFunciton:__FUNCTION__ WithMessage:@""];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                              message:[Util getSettingUsingName:SERVER_NOT_RESPONDING]
                                                             delegate:nil
                                                    cancelButtonTitle:ALERT_OPT_OK
                                                    otherButtonTitles:nil];
            [message show];
            result = FALSE;
        } else {
            NSString *key = SERVER_RES_STATUS;
            NSString *status = nil;
            
            //serverResponseData = serverResponseData[@"data"];
            
            if(serverResponseData[@"data"] == nil) {
                
            } else {
                serverResponseData = serverResponseData[@"data"];
            }
            
            if ([Util isKey:key ExistForDictionary:serverResponseData]) {
                status = serverResponseData[key];
                //                NSLog(@"status %@", status);
                if ([status isEqualToString:@"200"]) {
                    
                    [self setVerifyMPinSuccesfull];
                } else {
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                                                      message:serverResponseData[SERVER_RES_MEG]
                                                                     delegate:nil
                                                            cancelButtonTitle:ALERT_OPT_OK
                                                            otherButtonTitles:nil];
                    [message show];
                    
                    [ErrorLog log500ResponseInFunction:__FUNCTION__
                                           WithMessage:serverResponseData[SERVER_RES_MEG]];
                }
            }
        }
    } @catch (NSException *exception) {
        NSString *strLogMessage = @"Verify MPin request";
#if DEBUG_ERROR_LOG
        NSLog(@"%@", strLogMessage);
#endif
        [ErrorLog logAuthException:exception WithCode:RUN_EXC_SIGN_IN Message:strLogMessage InFunction:__FUNCTION__ EmpId:strUsername];
    }
    
    return result;
}

- (void) setVerifyMPinSuccesfull {
    
    NSLog(@"Verify MPin successfull & Authorise");
    
    //
    // Check model is of type LoginModel
    //
    
    
    Util *util = [[Util alloc] init];
    //VerifyMPinViewCtlr *verifyController = (VerifyMPinViewCtlr *) controller;
    
    //NSLog(@"Login User Id: %@", verifyController.nextPage);
    
    [util hideVerifyCtlrWithView:controller];
    //[Util OpenWebWithNextPage:verifyController.nextPage AndParam:verifyController.param OnView:controller];
    
    [controller performSelectorOnMainThread:@selector(initSalarySlip) withObject:nil waitUntilDone:YES];
    
//    UIAlertView *message = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
//                                                      message:serverResponseData[SERVER_RES_MEG]
//                                                     delegate:nil
//                                            cancelButtonTitle:ALERT_OPT_OK
//                                            otherButtonTitles:nil];
//    [message show];
    
    //
    // Initialize the Sliding menu (Navigation Drawer)
    //
    //    [controller performSelectorOnMainThread:@selector(initPanMenuListController) withObject:nil waitUntilDone:YES];
    //
    //    Holder *singletonRef = [Holder sharedHolder];
    //    singletonRef.pendingCountLeave      = @"";
    //    singletonRef.pendingCountMuster     = @"";
    //    singletonRef.pendingCountConveyance = @"";
    //    singletonRef.pendingCountLatePunch = @"";
    
    [self startBackgroundNotificationCheck];
}

- (BOOL)requestGetESOP {
    
    BOOL result = TRUE;
    
    NSString *url = [NSString stringWithFormat:@"%@%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_ESOP_URL]];
    
    //strUsername = [Util encodeSpecialCharacter:strUsername];
    
    self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(resESOP:) onObject:controller];
    [self.conn requestESOPWithURL:url];
    
    return result;
}

//- (BOOL)postESOPAuthenticationWithData:(NSMutableDictionary *)responseData {
//    
//    self.conn = nil;
//    BOOL result = true;
//    serverResponseData = responseData;
//    [self getESOPSuccesfull];
//    
//    return result;
//}
//
//- (void) getESOPSuccesfull {
//    
//    NSLog(@"Get ESOP successfull");
//    
//    //[controller performSelectorOnMainThread:@selector(showRegisterDeviceList) withObject:nil waitUntilDone:YES];
//    
//    [self startBackgroundNotificationCheck];
//}

#pragma mark - "Universe on the move" download implementation
- (void) openDownloadURL {
    
    [Util clearCacheWithViewController:controller];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strDownlaodURL]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strDownlaodURL]];
    } else {
        //TODO URL malfunction LoginViewMod function openDownloadURL
        [ErrorLog logAuthErrorWithCode:ERR_CODE_URL_MAL_FUNC
                           Description:ERR_MSG_URL_MAL_FUNC
                               Message:@"downlaod url"
                            InFunction:__FUNCTION__];
        [Util showTechnicalError];
    }
}

#pragma mark - Background Notification Check
/**
 * Background thread for notification count
 */
- (void) startBackgroundNotificationCheck {
    
    [NSThread detachNewThreadSelector:@selector(engageNotificationCheck) toTarget:self withObject:nil];
}

- (void) engageNotificationCheck {
    
    @autoreleasepool {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
        dispatch_group_t group = dispatch_group_create();
        
        Holder *holder = [Holder sharedHolder];
        NSString *strBaseUrl = [Util getSettingUsingName:MOBILE4U_URL];
        NSString *strSessionId = holder.loginInfo[LOGIN_RES_PARAM_SESSION_ID];
        NSString *strEmpId = holder.loginInfo[LOGIN_RES_PARAM_EMPLOYEE_ID];
        int intServerRequestTimeout = [[Util getSettingUsingName:SERVER_REQUEST_TIMEOUT] intValue];
        
        //NSString *URL = [NSString stringWithFormat:@"%@%@;%@=%@", strBaseUrl, [Util getSettingUsingName:NOTIFICATION_COUNT_URL], J_SESSION_ID, strSessionId];
        
        NSString *URL = [NSString stringWithFormat:@"%@%@;%@=%@?%@=%@", strBaseUrl, [Util getSettingUsingName:NOTIFICATION_COUNT_URL], J_SESSION_ID, strSessionId, EMP_ID, strEmpId];
        
         NSLog(@"Notification Count URL: %@", URL);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:intServerRequestTimeout];
        
        //NSString *jData;
        int count = 1, refreshtime = [[Util getSettingUsingName:NOTIFICATION_REFRESH_TIME] intValue];
      
        
        // Pending Approval count
        BOOL isPendingCountRequired = FALSE;
        // Below iteration check wheater Grid contains the pending approvals and accrodingly server is request
        NSDictionary *menuCodeMAC = [Holder sharedHolder].loginInfo[LOGIN_RES_PARAM_MENU_ACCESS_CODE];
        if ([Util isKey:@"LV_APP" ExistForDictionary:menuCodeMAC] ||
            [Util isKey:@"MT_APP" ExistForDictionary:menuCodeMAC] ||
            [Util isKey:@"EXP_APP" ExistForDictionary:menuCodeMAC] ||
            [Util isKey:@"LP_APP" ExistForDictionary:menuCodeMAC]) {
            isPendingCountRequired = TRUE;
            NSLog(@"Pending approval Count to be checked.");
        }
        NSString *strLeaveURL, *strMusterURL, *strConvenyanceURL, *strLatePunchURL;
        
        static NSMutableURLRequest *requestLeave, *requestMuster, *requestConvenyance, *requestLatePunch;
        
        if (isPendingCountRequired) {
            
            strLeaveURL         = [NSString stringWithFormat:@"%@%@;%@=%@", strBaseUrl, [Util getSettingUsingName:URL_LEAVE_PENDING_COUNT], J_SESSION_ID, strSessionId];
            NSLog(@"Leave URL %@", strLeaveURL);
            requestLeave        = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strLeaveURL]
                                                          cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                      timeoutInterval:intServerRequestTimeout];
            
            strMusterURL        = [NSString stringWithFormat:@"%@%@;%@=%@", strBaseUrl, [Util getSettingUsingName:URL_MUSTER_PENDING_COUNT], J_SESSION_ID, strSessionId];
            
            NSLog(@"Muster Url %@", strMusterURL);
            
            requestMuster       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strMusterURL]
                                                          cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                      timeoutInterval:intServerRequestTimeout];
            
            strConvenyanceURL   = [NSString stringWithFormat:@"%@%@;%@=%@", strBaseUrl, [Util getSettingUsingName:URL_CONVEYANCE_PENDING_COUNT], J_SESSION_ID, strSessionId];
            
            NSLog(@"Convenyance URL %@", strConvenyanceURL);
            requestConvenyance  = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strConvenyanceURL]
                                                          cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                      timeoutInterval:intServerRequestTimeout];
            
            strLatePunchURL   = [NSString stringWithFormat:@"%@%@;%@=%@", strBaseUrl, [Util getSettingUsingName:URL_LATE_PUNCH_PENDING_COUNT], J_SESSION_ID, strSessionId];
            
            NSLog(@"Late Punch Url %@", strLatePunchURL);
            
            requestLatePunch  = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strLatePunchURL]
                                                        cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                    timeoutInterval:intServerRequestTimeout];
        }
        
        while (true) {
            
            if ([holder.isLoggedIn isEqualToString:PAGE_FALSE]) {
                // Release the group when it is no longer needed.
//                dispatch_group_leave(group);
//                dispatch_release(group);
                break;
            }
            
            // Add a task to the group
            dispatch_group_async(group, queue, ^{
                
                // Some asynchronous work
                if (nil == controller.navigationController.visibleViewController.navigationItem.rightBarButtonItem || NSNull.null == (id)controller.navigationController.visibleViewController.navigationItem.rightBarButtonItem) {
                    //NSLog(@"Notification button doesn't exits.");
                } else {
                    
//                    [self requestNotificationWithRequest:request InQueue:queue WithCount:count];
                }
                
                //Pending Approval Count
                if (isPendingCountRequired) {
                    
                    //[self requestNotificationWithRequest:request InQueue:queue WithCount:count];
                    [self requestLeavePACWithRequest:requestLeave InQueue:queue WithCount:count];
                    [self requestMusterPACWithRequest:requestMuster InQueue:queue WithCount:count];
                    [self requestConveyancePACWithRequest:requestConvenyance InQueue:queue WithCount:count];
                    [self requestLatePunchPACWithRequest:requestLatePunch InQueue:queue WithCount:count];
                    
                }
            });
            
            // When you cannot make any more forward progress,
            // wait on the group to block the current thread.
            dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
//            NSLog(@"\n\n");
            
            count++;
            sleep(refreshtime);
        }
    }
}

- (void) requestNotificationWithRequest:(NSURLRequest *) request InQueue:(dispatch_queue_t) queue WithCount:(int) count {
    
    @try {
        dispatch_async(queue, ^{
            NSDictionary *serverResponse = nil;
            NSURLResponse  *returnResponse = nil;
            NSError *error = nil;
            NSData *returnData = nil;
            
            NSDate *start = [NSDate date];
            returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&returnResponse error:&error];
//            NSString *response = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//            NSLog(@"response %@", response);
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
            if (returnData != nil) {
                
                serverResponse = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:&error];
                
                NSLog(@"Notification Response %@",serverResponse);
                
                if ([Util serverResponseCheckNullAndSession:serverResponse InFunction:__FUNCTION__ Silent:YES]) {
                    
                    if ([Util serverResponseForField:@"count" In:serverResponse InFunction:__FUNCTION__ Message:@"count parameter" Silent:YES]) {
                    //    NSLog(@"Notification count %d Time %f value: %@", count, executionTime, serverResponse[@"count"]);
                        
                        NSString *badge = serverResponse[@"count"];
                        
                        NSLog(@"Notification Count %@",badge);
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:badge forKey:@"badge"];
                        [defaults synchronize];
                        
                        [Holder sharedHolder].pendingBadgeCount = [badge intValue];
                        
                        [UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
                        
                        [Holder sharedHolder].notificationCount = serverResponse[@"count"];
                        [self performSelectorOnMainThread:@selector(updateNotification) withObject:nil waitUntilDone:YES];
                    }
                }
            }
        });
    } @catch (NSException *exception) {
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"Notification"];
    }
}

- (void) requestLeavePACWithRequest:(NSURLRequest *) requestLeave InQueue:(dispatch_queue_t) queue WithCount:(int) count {
    
    @try {
        dispatch_async(queue, ^{
            NSDictionary *serverResponseLeave;
            NSURLResponse *returnResponseLeave;
            NSError *errorLeave;
            NSData *returnDataLeave;
            NSString *strCountLeave;
            
            NSDate *start = [NSDate date];
            returnDataLeave = [NSURLConnection sendSynchronousRequest:requestLeave returningResponse:&returnResponseLeave error:&errorLeave];
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
//            NSLog(@"Leave %d Time %f", count, executionTime);
            
            if (returnDataLeave != nil) {
                serverResponseLeave = [NSJSONSerialization JSONObjectWithData:returnDataLeave options:NSJSONReadingMutableLeaves error:&errorLeave];
                
                NSLog(@"Leave Count Response: %@", serverResponseLeave);
                if ([Util serverResponseCheckNullAndSession:serverResponseLeave InFunction:__FUNCTION__ Silent:YES]) {
                    
                    if (![Util isNull:serverResponseLeave]) {
                        
                        if ([Util serverResponseForField:PEN_CNT_DATA In:serverResponseLeave InFunction:__FUNCTION__ Message:@"data parameter" Silent:YES]) {
                            
                            if ([Util serverResponseForField:PEN_LV_APP_CNT In:serverResponseLeave[PEN_CNT_DATA] InFunction:__FUNCTION__ Message:@"LEAVE Pending Approval count" Silent:YES]) {
                                strCountLeave = [NSString stringWithFormat:@"  %@  ", serverResponseLeave[PEN_CNT_DATA][PEN_LV_APP_CNT]];
                           //     NSLog(@"Leave PAC count %d Time %f value: %@", count, executionTime, strCountLeave);
                                if (![strCountLeave isEqual:[Holder sharedHolder].pendingCountLeave]) {
                                    // updates Leave Pending Count
                                   // if([Holder sharedHolder].pendingCountLeave < strCountLeave) {
                                        [Holder sharedHolder].pendingCountLeave = strCountLeave;
                                    //}
                                }
                                [self performSelectorOnMainThread:@selector(updatePendingApprovalCountLeave) withObject:nil waitUntilDone:YES];
                            }
                        }
                    }
                }
            }
        });
    } @catch (NSException *exception) {
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"Leave PAC"];
    }
}

- (void) requestMusterPACWithRequest:(NSURLRequest *) requestMuster InQueue:(dispatch_queue_t) queue WithCount:(int) count {
    
    @try {
        dispatch_async(queue, ^{
            NSDictionary *serverResponseMuster;
            NSURLResponse *returnResponseMuster;
            NSError *errorMuster;
            NSData *returnDataMuster;
            NSString *strCountMuster;
            NSDate *start = [NSDate date];
            returnDataMuster = [NSURLConnection sendSynchronousRequest:requestMuster returningResponse:&returnResponseMuster error:&errorMuster];
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
            
            if (returnDataMuster != nil) {
                serverResponseMuster = [NSJSONSerialization JSONObjectWithData:returnDataMuster options:NSJSONReadingMutableLeaves error:&errorMuster];
                
                NSLog(@"Muster Count Response: %@", serverResponseMuster);
                if ([Util serverResponseCheckNullAndSession:serverResponseMuster InFunction:__FUNCTION__ Silent:YES]) {
                    
                    if (![Util isNull:serverResponseMuster]) {
                        
                        if ([Util serverResponseForField:PEN_CNT_DATA In:serverResponseMuster InFunction:__FUNCTION__ Message:@"data parameter" Silent:YES]) {
                            
                            if ([Util serverResponseForField:PEN_MS_APP_CNT In:serverResponseMuster[PEN_CNT_DATA] InFunction:__FUNCTION__ Message:@"MUSTER Pending Approval count" Silent:YES]) {
                                strCountMuster = [NSString stringWithFormat:@"  %@  ", serverResponseMuster[PEN_CNT_DATA][PEN_MS_APP_CNT]];
                              //  NSLog(@"Muster PAC count %d Time %f value: %@", count, executionTime, strCountMuster);
                                if (![strCountMuster isEqual:[Holder sharedHolder].pendingCountMuster]) {
                                    // updates Muster Pending Count
                                   // if([Holder sharedHolder].pendingCountMuster < strCountMuster) {
                                        [Holder sharedHolder].pendingCountMuster = strCountMuster;
                                   // }
                                }
                                [self performSelectorOnMainThread:@selector(updatePendingApprovalCountMuster) withObject:nil waitUntilDone:YES];
                            }
                        }
                    }
                }
            }
        });
    } @catch (NSException *exception) {
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"Muster"];
    }
}
    
- (void) requestLatePunchPACWithRequest:(NSURLRequest *) requestLatePunch InQueue:(dispatch_queue_t) queue WithCount:(int) count {
    
    @try {
        dispatch_async(queue, ^{
            NSDictionary *serverResponseLatePunch;
            NSURLResponse *returnResponseLatePunch;
            NSError *errorLatePunch;
            NSData *returnDataLatePunch;
            NSString *strCountLatePunch;
            NSDate *start = [NSDate date];
            returnDataLatePunch = [NSURLConnection sendSynchronousRequest:requestLatePunch returningResponse:&returnResponseLatePunch error:&errorLatePunch];
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
            
            if (returnDataLatePunch != nil) {
                serverResponseLatePunch = [NSJSONSerialization JSONObjectWithData:returnDataLatePunch options:NSJSONReadingMutableLeaves error:&errorLatePunch];
                
                NSLog(@"Late Punch Count Response: %@", serverResponseLatePunch);
                if ([Util serverResponseCheckNullAndSession:serverResponseLatePunch InFunction:__FUNCTION__ Silent:YES]) {
                    
                    if (![Util isNull:serverResponseLatePunch]) {
                        
                        if ([Util serverResponseForField:PEN_CNT_DATA In:serverResponseLatePunch InFunction:__FUNCTION__ Message:@"data parameter" Silent:YES]) {
                            
                            if ([Util serverResponseForField:PEN_LP_APP_CNT In:serverResponseLatePunch[PEN_CNT_DATA] InFunction:__FUNCTION__ Message:@"Late Punch Pending Approval count" Silent:YES]) {
                                strCountLatePunch = [NSString stringWithFormat:@"  %@  ", serverResponseLatePunch[PEN_CNT_DATA][PEN_LP_APP_CNT]];
                                //  NSLog(@"Late Punch PAC count %d Time %f value: %@", count, executionTime, strCountLatePunch);
                                if (![strCountLatePunch isEqual:[Holder sharedHolder].pendingCountLatePunch]) {
                                    // updates LatePunch Pending Count
                                   // if([Holder sharedHolder].pendingCountLatePunch < strCountLatePunch) {
                                        [Holder sharedHolder].pendingCountLatePunch = strCountLatePunch;
                                   // }
                                }
                                [self performSelectorOnMainThread:@selector(updatePendingApprovalCountLatePunch) withObject:nil waitUntilDone:YES];
                            }
                        }
                    }
                }
            }
        });
    } @catch (NSException *exception) {
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"LatePunch"];
    }
}

- (void) requestConveyancePACWithRequest:(NSURLRequest *) requestConvenyance InQueue:(dispatch_queue_t) queue WithCount:(int) count {
    
    @try {
        dispatch_async(queue, ^{
            NSDictionary *serverResponseConvenyance;
            NSURLResponse *returnResponseConvenyance;
            NSError *errorConvenyance;
            NSData *returnDataConvenyance;
            NSString *strCountConvenyance;
            
            NSDate *start = [NSDate date];
            returnDataConvenyance = [NSURLConnection sendSynchronousRequest:requestConvenyance returningResponse:&returnResponseConvenyance error:&errorConvenyance];
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
            
            if (returnDataConvenyance != nil) {
                serverResponseConvenyance = [NSJSONSerialization JSONObjectWithData:returnDataConvenyance options:NSJSONReadingMutableLeaves error:&errorConvenyance];
                
                NSLog(@"Convenyance Count response: %@", serverResponseConvenyance);
                if ([Util serverResponseCheckNullAndSession:serverResponseConvenyance InFunction:__FUNCTION__ Silent:YES]) {
                    
                    if (![Util isNull:serverResponseConvenyance]) {
                        
                        if ([Util serverResponseForField:PEN_CNT_DATA In:serverResponseConvenyance InFunction:__FUNCTION__ Message:@"data parameter" Silent:YES]) {
                            
                            if ([Util serverResponseForField:PEN_EXP_APP_CNT In:serverResponseConvenyance[PEN_CNT_DATA] InFunction:__FUNCTION__ Message:@"CONVEYANCE Pending Approval count" Silent:YES]) {
                                strCountConvenyance = [NSString stringWithFormat:@"  %@  ", serverResponseConvenyance[PEN_CNT_DATA][PEN_EXP_APP_CNT]];
                          //      NSLog(@"Conveyance PAC count %d Time %f value: %@", count, executionTime, strCountConvenyance);
                                if (![strCountConvenyance isEqual:[Holder sharedHolder].pendingCountConveyance]) {
                                    // updates Conveyance Pending Count
                                   // if([Holder sharedHolder].pendingCountConveyance < strCountConvenyance) {
                                        [Holder sharedHolder].pendingCountConveyance = strCountConvenyance;
                                   // }
                                    
                                }
                                [self performSelectorOnMainThread:@selector(updatePendingApprovalCountMuster) withObject:nil waitUntilDone:YES];
                            }
                        }
                    }
                }
            }
        });
    } @catch (NSException *exception) {
        [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"Conveyance"];
    }
}
    
    
//    - (void) requestLatePunchPACWithRequest:(NSURLRequest *) requestLatePunch InQueue:(dispatch_queue_t) queue WithCount:(int) count {
//        
//        @try {
//            dispatch_async(queue, ^{
//                NSDictionary *serverResponseLatePunch;
//                NSURLResponse *returnResponseLatePunch;
//                NSError *errorLatePunch;
//                NSData *returnDataLatePunch;
//                NSString *strCountLatePunch;
//                NSDate *start = [NSDate date];
//                returnDataLatePunch = [NSURLConnection sendSynchronousRequest:requestLatePunch returningResponse:&returnResponseLatePunch error:&errorLatePunch];
//                NSDate *methodFinish = [NSDate date];
//                NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
//                
//                if (returnDataLatePunch != nil) {
//                    serverResponseLatePunch = [NSJSONSerialization JSONObjectWithData:returnDataLatePunch options:NSJSONReadingMutableLeaves error:&errorLatePunch];
//                    
//                    NSLog(@"Late Punch response Hello : %@", returnResponseLatePunch);
//                    if ([Util serverResponseCheckNullAndSession:serverResponseLatePunch InFunction:__FUNCTION__ Silent:YES]) {
//                        
//                        if (![Util isNull:serverResponseLatePunch]) {
//                            
//                            if ([Util serverResponseForField:PEN_CNT_DATA In:serverResponseLatePunch InFunction:__FUNCTION__ Message:@"data parameter" Silent:YES]) {
//                                
//                                if ([Util serverResponseForField:PEN_LP_APP_CNT In:serverResponseLatePunch[PEN_CNT_DATA] InFunction:__FUNCTION__ Message:@"Late Punch Pending Approval count" Silent:YES]) {
//                                    strCountLatePunch = [NSString stringWithFormat:@"  %@  ", serverResponseLatePunch[PEN_CNT_DATA][PEN_LP_APP_CNT]];
//                                    NSLog(@"Late Punch PAC count %d Time %f value: %@", count, executionTime, strCountLatePunch);
//                                    if (![strCountLatePunch isEqual:[Holder sharedHolder].pendingCountLatePunch]) {
//                                        // updates Muster Pending Count
//                                        [Holder sharedHolder].pendingCountLatePunch = strCountLatePunch;
//                                        [self performSelectorOnMainThread:@selector(updatePendingApprovalCountMuster) withObject:nil waitUntilDone:YES];
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            });
//        } @catch (NSException *exception) {
//            [ErrorLog logException:exception InFunction:__FUNCTION__ AndMessage:@"Late Punch"];
//        }
//    }

/**
 * Updates the notification count for each view
 */
- (void) updateNotification {
    
    NSArray *viewControllers = controller.navigationController.viewControllers;
    UIButton *notificationButton;
    NSString *strNotification = [NSString stringWithFormat:@"    %@  ", [Holder sharedHolder].notificationCount];
    
    for (int i=0; i<[viewControllers count]; ++i) {
        
        // Other than Login & Setting View each view must update the notification count
        // Tag 1001 is of Login View, constant TAG_IDENTIFY_LOGIN_VIEW
        if ([viewControllers[i] view].tag != TAG_IDENTIFY_LOGIN_VIEW) {
            notificationButton = (UIButton *) [[[viewControllers[i] navigationItem] rightBarButtonItem] customView];
            [notificationButton setTitle:strNotification forState:UIControlStateNormal];
            [notificationButton sizeToFit];
        }
    }
}

- (void) countPAC:(NSString *) strCount WithViewController: (UIViewController *) viewController ForTag: (int) tag {
    
    if ([[viewController view] viewWithTag:tag] != nil) {
        
        UIButton *btnPendingCount = (UIButton *) [[viewController view] viewWithTag:tag];
        btnPendingCount.titleLabel.textColor = [UIColor whiteColor];
        [btnPendingCount setTitle:strCount forState:UIControlStateNormal];
    }
}

- (void) removeSpinnerPACWithViewController: (UIViewController *) viewController {
    
    if ([[viewController view] viewWithTag:TAG_PAC_INDICATOR] != nil) {
        
        UIActivityIndicatorView *spinner = (UIActivityIndicatorView *) [[viewController view] viewWithTag:TAG_PAC_INDICATOR];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
    }
}

- (void) updatePendingApprovalCount:(NSString *) strCount ForTag: (int) intTag ForPage:(NSString *) page {
    
    NSArray *viewControllers = controller.navigationController.viewControllers;
    
    for (int i=0; i<[viewControllers count]; ++i) {
        
        if ([viewControllers[i] isKindOfClass:[GridViewCtlr class]]) {
            
            GridViewCtlr *grid = (GridViewCtlr *) viewControllers[i];
            if ([grid.nextPage isEqualToString:page]) {
                
                // Update the PAC dynalically & close loader indicator if any
                [self countPAC:strCount WithViewController:viewControllers[i] ForTag:intTag];
                [self removeSpinnerPACWithViewController:viewControllers[i]];
            }
        }
        if ([viewControllers[i] isKindOfClass:[CarouselViewCtlr class]]) {
            //[self updatePendingApprovalCountForCarouselWithController:viewControllers[i]];
        }
    }
}

- (void) updatePendingApprovalCountForCarouselWithController:(UIViewController *) viewController {
    
    Holder *holder = [Holder sharedHolder];
    if (![holder.pendingCountLeave isEqualToString:@""] && ![holder.pendingCountMuster isEqualToString:@""] && ![holder.pendingCountConveyance isEqualToString:@""]) {
        CarouselViewCtlr *carousel = (CarouselViewCtlr *) viewController;
        UIButton *button = (UIButton *) [[carousel view] viewWithTag:TAG_CAROUSEL_APPROVAL];
        
        int intPendingCount = [holder.pendingCountLeave floatValue] + [holder.pendingCountMuster floatValue] + [holder.pendingCountConveyance floatValue];
        [button setTitle:[NSString stringWithFormat:@"%d", intPendingCount] forState:UIControlStateNormal];
        
        UIActivityIndicatorView *spinner = (UIActivityIndicatorView *) [[carousel view] viewWithTag:TAG_PAC_CAROUSEL];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
    }
}

/**
 * Updates the LEAVE Pending Approval Count for each view
 */
- (void) updatePendingApprovalCountLeave {
    
    NSLog(@"Leave Update Count %@ ",[Holder sharedHolder].pendingCountLeave);

    [self updatePendingApprovalCount];
    
    [self updatePendingApprovalCount:[Holder sharedHolder].pendingCountLeave
                              ForTag:TAG_PENDING_LEAVE_APPROVAL_COUNT
                             ForPage:@"leave-manager"];
}

/**
 * Updates the MUSTER Pending Approval Count for each view
 */
- (void) updatePendingApprovalCountMuster {
    
     NSLog(@"Muster Update Count %@ ",[Holder sharedHolder].pendingCountMuster);
    
    [self updatePendingApprovalCount];
    
    [self updatePendingApprovalCount:[Holder sharedHolder].pendingCountMuster
                              ForTag:TAG_PENDING_MUSTER_APPROVAL_COUNT
                             ForPage:@"muster-manager"];
}

/**
 * Updates the CONVEYANCE Pending Approval Count for each view
 */
- (void) updatePendingApprovalCountConveyance {
    
     NSLog(@"Conveyence Update Count %@ ",[Holder sharedHolder].pendingCountConveyance);
    
    [self updatePendingApprovalCount];
    
    [self updatePendingApprovalCount:[Holder sharedHolder].pendingCountConveyance
                              ForTag:TAG_PENDING_CONVEYANCE_APPROVAL_COUNT
                             ForPage:@"expense-manager"];
}

- (void) updatePendingApprovalCountLatePunch {
    
     NSLog(@"Late Punch Update Count %@ ",[Holder sharedHolder].pendingCountLatePunch);
    
    [self updatePendingApprovalCount];
    
    [self updatePendingApprovalCount:[Holder sharedHolder].pendingCountLatePunch
                              ForTag:TAG_PENDING_LATE_PUNCH_APPROVAL_COUNT
                             ForPage:@"muster-manager"];
}
//This  is use for bell count and badge count
- (void) updatePendingApprovalCount {
    
    int badgeCount = [[Holder sharedHolder].pendingCountLeave intValue] + [[Holder sharedHolder].pendingCountMuster intValue] + [[Holder sharedHolder].pendingCountLatePunch intValue] + [[Holder sharedHolder].pendingCountConveyance intValue] ;
    
    [Holder sharedHolder].pendingBadgeCount = badgeCount;
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%d",badgeCount] forKey:@"badge"];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCount" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCountGrid" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNotificationCountList" object:self userInfo:nil];
    NSLog(@"There is sum of all counts %d", badgeCount);
}

#pragma mark - Sign Out Implementation
- (void) sessionExpired:(NSDictionary *) responseData {
    
    signout = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                         message:responseData[SERVER_RES_MEG]
                                        delegate:self
                               cancelButtonTitle:ALERT_OPT_OK
                               otherButtonTitles:nil];
    [signout show];
}

- (void) requestSignOutWithMenuToggleCheck:(BOOL) isMenuCall {
    
    isToggle = isMenuCall;
    signout = [[UIAlertView alloc] initWithTitle:[Util getSettingUsingName:POPUP_TITLE]
                                         message:[Util getSettingUsingName:LOGOUT_MSG]
                                        delegate:self
                               cancelButtonTitle:ALERT_OPT_SIGN_OUT
                               otherButtonTitles:ALERT_OPT_CANCEL, nil];
    [signout show];
}

- (void) signOut {
    
//    NSString *strSignOutURL = [NSString stringWithFormat:@"%@%@;%@=%@", [Util getSettingUsingName:MOBILE4U_URL], [Util getSettingUsingName:MOBILE4U_LOGOUT_URL], J_SESSION_ID, ([Holder sharedHolder].loginInfo)[LOGIN_RES_PARAM_SESSION_ID]];
//    //NSLog(@"Sign Out URL: %@", strSignOutURL);
//    self.conn = [[AppConnect alloc] initWithAfterFinishCall:@selector(responseSignOut:) onNSObject:self];
//    [self.conn requestSignOutWithURL:strSignOutURL];
    
    [Holder sharedHolder].isLoggedIn    = @"false";
    [Holder sharedHolder].loginInfo     = nil;
    [Util setSettingWithName:BACKGROUND_IS_LOGGED_IN ByValue:PAGE_FALSE];
    [Util setSettingWithName:BACKGROUND_LOGGED_INFO ByValue:EMPTY];
    
    if (isToggle) {
        IIViewDeckController *viewCtlr = (IIViewDeckController *) [Holder sharedHolder].viewController;
        viewCtlr.panningMode = IIViewDeckNoPanning;
        
        [viewCtlr toggleLeftView];
    }
    
    NSString *userName = [Util getKeyChainUserName];
    NSString *mpin = [Util getKeyChainMPin];
    
    [self requestAutoLoginWithUserName:userName mPin:mpin bio:@""];
    
    //[Util popWithFadeEffectOnViewController:controller ShouldPopToRoot:YES];
}

- (void) responseSignOut:(NSDictionary *) responseData {
    
    self.conn = nil;
    NSLog(@"Sign out response: %@", responseData);
}

#pragma mark - UIALertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    NSLog(@"Alert title: %@", title);
    if (alertView == updateAlert) {
        if ([title isEqualToString:ALERT_OPT_LATER]) {
            //[self loginSuccesfull];This is old change
            [self autoLoginSuccesfull];//This is new change
        } else if ([title isEqualToString:ALERT_OPT_NOW] || [title isEqualToString:ALERT_OPT_OK]) {
            [self openDownloadURL];
        }
    }
    if (alertView == signout) {
        if ([title isEqualToString:ALERT_OPT_OK] || [title isEqualToString:ALERT_OPT_SIGN_OUT]) {
            exit(0);
            //[self signOut];
        }
    }
    if (alertView == resetAlert) {
        [controller performSelectorOnMainThread:@selector(initPanMenuListController) withObject:nil waitUntilDone:YES];
    }
    if (alertView == registerAlert) {
        if(registerAlert.tag == 1) {
            
            NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
            [defaul setObject:@"" forKey:LOCAL_DB_IS_FINGER_ACTIVE];
            [defaul synchronize];
            [controller performSelectorOnMainThread:@selector(initRegisterViewController) withObject:nil waitUntilDone:YES];
        }
    }
    if (alertView == setMPinAlert) {
        [self setMPinSuccesfull];
    }
    
}

@end
