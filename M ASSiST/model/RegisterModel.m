//
//  RegisterModel.m
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import "RegisterModel.h"
#import "Util.h"
#import "Constant.h"
#import "Navigation.h"
#import "Holder.h"
#import "Authentication.h"

@implementation RegisterModel

- (void) initDataWithView:(UIViewController *) controllerView {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    _controller = controllerView;
    data        = [Util getPage:PAGE_REGISTER];
    css         = [Util getPageCSS:data[PAGE_CSS]];//[Holder sharedHolder].css;
    
//    [[_controller view] setTag:TAG_IDENTIFY_LOGIN_VIEW];
}

- (NSDictionary *) getPageData {
    
    return data;
}

- (void) initNavControlWithEmployeeId:(UITextField *) txtEmployeeId
                       Password:(UITextField *) txtPassword
                         Next:(UIButton *) next {
    
    employeeId        = txtEmployeeId;
    password        = txtPassword;
    btnNext         = next;
    
    [self initEmergencyAndHelpButton];
    
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self.controller PageData:data];
    
    [self sendLogtoServer];
}

#pragma mark - Log Send -
/**
 * Background thread for error log dump forward to Server
 */
- (void) sendLogtoServer {
    
    [NSThread detachNewThreadSelector:@selector(backgroundLogSend) toTarget:self withObject:nil];
}

- (void) backgroundLogSend {
    
    // Dummps error log to the server
    [Util sendErrorLog];
}

- (void) initEmergencyAndHelpButton {
    
    int intNextYPos = 270;
    if ([UIScreen mainScreen].bounds.size.height==568) {
        NSLog(@"\n\n\n4 inch retina\n\n\n");
        intNextYPos       = 320;
    }
    
    btnNext = [[UIButton alloc] initWithFrame:CGRectMake(115, intNextYPos, 96, 32)];
    [btnNext setImage:[Util getImageAtDocDirWithName:@"next.png"] forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    [self.controller.view addSubview:btnNext];
    
//    NSArray *lists = data[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
//    NSDictionary *controlData = nil;
//    for (int i=0; i<[lists count]; i++) {
//        NSDictionary *list = lists[i];
//        if ([list[@"type"] isEqualToString:@"button"] && [list[@"name"] isEqualToString:@"emergency-call"]) {
//            controlData = list;
//            strEmergencyNumber = controlData[@"number"];
//            break;
//        }
//    }
    
    //    This code gives button fall effect during back transition
    //    UIImage *imgTopbarRight = [Util getImageAtDocDirWithName:@"sign-in.png"];
    //    btnSignIn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btnSignIn setImage:imgTopbarRight forState:UIControlStateNormal];
    //    btnSignIn.frame = CGRectMake(130, intSignInYPos, imgTopbarRight.size.width, imgTopbarRight.size.height);
    //    [btnSignIn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    //    [view.view addSubview:btnSignIn];
}

- (void) hideKeyboard {
    
    [employeeId resignFirstResponder];
    [password resignFirstResponder];
}

- (void) clearCredentials {
    
    employeeId.text = @"";
    password.text = @"";
}

- (void) next {
    
    [self hideKeyboard];
    
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self.controller AndModel:self];
    [[Holder sharedHolder].auth requestRegisterWithUsername:employeeId.text AndPassword:password.text];
}

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData {
    
    NSLog(@"Register response: %@", responseData);
    [[Holder sharedHolder].auth postRegisterAuthenticationWithData:responseData];
}

- (void) dealloc {
    
    self.navigation = nil;
}

@end
