//
//  OTPModel.h
//  Universe on the move
//
//  Created by Phyder on 13/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Navigation;

@interface OTPModel : NSObject<UIAlertViewDelegate> {
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button
    UIButton *btnConfirm;
    
    UITextField *otp;
    
    NSDictionary *responseJSONData;
    NSString *strDownlaodURL;
}

@property (nonatomic, weak) UIViewController *controller;        // Controller view reference
@property (nonatomic) Navigation *navigation;

- (void) clearCredentials;

- (void) initDataWithView:(UIViewController *) controllerView;  // Init model for the controller

- (NSDictionary *) getPageData;

- (void) initNavControlWithOtp:(UITextField *) txtOtp
                                 Confirm:(UIButton *) confirm;

- (void) confirmOtp;

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData;

@end
