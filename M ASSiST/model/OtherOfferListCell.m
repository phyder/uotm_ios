//
//  OfferListCell.m
//  Other Offer
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "OtherOfferListCell.h"

#import "Constant.h"
#import "Util.h"

@implementation OtherOfferListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    UIView *bgColorView = [[UIView alloc] init];
    NSArray *color = [[Util getSettingUsingName:LIST_TOUCHUP_COLOR] componentsSeparatedByString:@"|"];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    gradient.colors = @[(id)[[Util colorWithHexString:color[0] alpha:@"0.75f"] CGColor], (id)[[Util colorWithHexString:color[1] alpha:@"0.75f"] CGColor]];
    [bgColorView.layer insertSublayer:gradient atIndex:0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void) loadWithData:(NSDictionary *) data AndCSS:(NSDictionary *) css {
    
    float verticalPadding;
    NSDictionary *lblCSS            = [Util getLabelCSS:css[@"title-css"]];
    _lblOfferTitle.text             = data[OTHER_OFFER_LIST_TITLE];
    _lblOfferTitle.textColor        = [Util colorWithHexString:lblCSS[@"font-color"]
                                                     alpha:@"1.0f"];
    _lblOfferTitle.font             = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferTitle setNumberOfLines:0];
    [_lblOfferTitle sizeToFit];
    
    verticalPadding                 = _lblOfferTitle.frame.origin.y+_lblOfferTitle.frame.size.height;
    lblCSS                          = [Util getLabelCSS:css[@"sub-title-css"]];
    _lblOfferSubTitle.text          = data[OTHER_OFFER_LIST_SUBTITLE];
    _lblOfferSubTitle.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                     alpha:@"1.0f"];
    _lblOfferSubTitle.font          = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferSubTitle setNumberOfLines:0];
    [_lblOfferSubTitle sizeToFit];
    _lblOfferSubTitle.frame     = CGRectMake(_lblOfferSubTitle.frame.origin.x, verticalPadding, _lblOfferSubTitle.frame.size.width, _lblOfferSubTitle.frame.size.height);
    
    verticalPadding                 = _lblOfferSubTitle.frame.origin.y+_lblOfferSubTitle.frame.size.height;
    lblCSS                          = [Util getLabelCSS:css[@"location-css"]];
    _lblOfferLocation.text          = data[OTHER_OFFER_LIST_ADDRESS];
    _lblOfferLocation.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    _lblOfferLocation.font          = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferLocation setNumberOfLines:0];
    [_lblOfferLocation sizeToFit];
    _lblOfferLocation.frame         = CGRectMake(_lblOfferLocation.frame.origin.x, verticalPadding, _lblOfferLocation.frame.size.width, _lblOfferLocation.frame.size.height);
    
    lblCSS                          = [Util getLabelCSS:css[@"validity-css"]];
    _lblOfferValidity.text          = [Util convertDateFromString:data[OTHER_OFFER_LIST_VALIDITY]
                                                         WithType:MSSQL_TO_NSDATE_DATE_CONVERSION];
    _lblOfferValidity.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    _lblOfferValidity.font          = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferValidity setNumberOfLines:0];
    [_lblOfferValidity sizeToFit];
    _lblOfferValidity.frame         = CGRectMake(_lblOfferValidity.frame.origin.x, verticalPadding, _lblOfferValidity.frame.size.width, _lblOfferValidity.frame.size.height);
    
    if ([data[OTHER_OFFER_LIST_RECENT] intValue]==0) {
        self.imgRecentImage.hidden  = YES;
    } else if ([data[OTHER_OFFER_LIST_RECENT] intValue]==1) {
        self.imgRecentImage.hidden  = NO;
        self.imgRecentImage.image   = [Util getImageAtDocDirWithName:@"star.png"];
    }
}

+ (float) getHeightForCellAt:(NSIndexPath *) indexPath withData:(NSDictionary *) data AndCSS:(NSDictionary *) css {
    
    //NSLog(@"CSS %@", css);
    NSString *text                  = data[OTHER_OFFER_LIST_TITLE];
    NSDictionary *lblCSS            = [Util getLabelCSS:css[@"title-css"]];
    float titleHeight               = (float) [text sizeWithFont: [Util fontWithName:lblCSS[@"font-name"]
                                                                               style:lblCSS[@"font-style"]
                                                                                size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){260.0f, CGFLOAT_MAX}
                                                   lineBreakMode: NSLineBreakByWordWrapping].height;
    
    text                            = data[OTHER_OFFER_LIST_SUBTITLE];
    lblCSS                          = [Util getLabelCSS:css[@"sub-title-css"]];
    float subTitleHeight            = (float) [text sizeWithFont: [Util fontWithName:lblCSS[@"font-name"]
                                                                               style:lblCSS[@"font-style"]
                                                                                size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){260.0f, CGFLOAT_MAX}
                                                   lineBreakMode: NSLineBreakByWordWrapping].height;
    
    text                            = data[OTHER_OFFER_LIST_ADDRESS];
    lblCSS                          = [Util getLabelCSS:css[@"location-css"]];
    float locationHeight            = (float) [text sizeWithFont: [Util fontWithName:lblCSS[@"font-name"]
                                                                               style:lblCSS[@"font-style"]
                                                                                size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){150.0f, CGFLOAT_MAX}
                                                   lineBreakMode: NSLineBreakByWordWrapping].height;
    
    text                            = [Util convertDateFromString:data[OTHER_OFFER_LIST_VALIDITY]
                                                         WithType:MSSQL_TO_NSDATE_DATE_CONVERSION];
    lblCSS                          = [Util getLabelCSS:css[@"validity-css"]];
    float validityHeight            = (float) [text sizeWithFont: [Util fontWithName:lblCSS[@"font-name"]
                                                                               style:lblCSS[@"font-style"]
                                                                                size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){100.0f, CGFLOAT_MAX}
                                                   lineBreakMode: NSLineBreakByWordWrapping].height;
    
    //NSLog(@"Height: title %f, sub-title: %f, %f %f", titleHeight, subTitleHeight, locationHeight, validityHeight);
    return (10 + titleHeight + subTitleHeight + MAX(locationHeight, validityHeight)+10);
}

@end
