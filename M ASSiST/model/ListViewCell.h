//
//  menuViewCell.h
//  M ASSiST
//
//  Created by Suraj on 22/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ListViewCell : UITableViewCell {
    
    NSDictionary *cellData;
    NSString *nextPage;
    float cellHeight;
}

@property(strong, nonatomic) IBOutlet UIImageView *imgLeftIcon;
@property(strong, nonatomic) IBOutlet UILabel *lblTitle;
@property(strong, nonatomic) IBOutlet UILabel *lblSubTitle;
@property(strong, nonatomic) IBOutlet UILabel *lblDesc;
@property(strong, nonatomic) IBOutlet UILabel *lblTopRightTitle;
@property(strong, nonatomic) IBOutlet UIImageView *imgRightDetailsIcon;

- (void) initCellWithData:(NSDictionary *) data OfHeight:(float) height CSS:(NSDictionary *) cssList For:(NSString *) viewController PageName:pageName;

- (NSDictionary *) getCellData;
- (NSString *) getNextPage;

@end
