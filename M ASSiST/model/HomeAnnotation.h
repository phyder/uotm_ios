//
//  Annotation.h
//  Home Offer
//
//  Created by Suraj on 28/02/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface HomeAnnotation : NSObject <MKAnnotation> {
    
    NSDictionary *responseJSON;
}

@property (nonatomic, weak) UIViewController *controller;

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, strong) UIButton *detailButton;
@property (nonatomic, strong) NSString *recentOffer;

- (id)initWithViewController:(UIViewController *)view
                          ID:(NSString*)ID
                       Title:(NSString*)title
                    subtitle:(NSString*)subtitle
                 recentCount:(NSString *) count
                  coordinate:(CLLocationCoordinate2D)coordinate;

- (int) getRecentOfferCount;

@end
