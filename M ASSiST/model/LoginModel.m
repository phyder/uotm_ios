//
//  LoginViewMod.m
//  M ASSiST
//
//  Created by Suraj on 19/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "LoginModel.h"

#import "Authentication.h"

#import "Constant.h"
#import "Holder.h"
#import "Navigation.h"
#import "Holder.h"
#import "Util.h"

@implementation LoginModel

- (void) initDataWithView:(UIViewController *) controllerView {
#ifdef DEBUG_LEVEL_1
    NSLog(@"%s %@", __FUNCTION__, DEBUG_LEVEL_1);
#endif
    
    _controller = controllerView;
    data        = [Util getPage:PAGE_LOGIN];
    css         = [Util getPageCSS:data[PAGE_CSS]];//[Holder sharedHolder].css;
    
    [[_controller view] setTag:TAG_IDENTIFY_LOGIN_VIEW];
}

- (NSDictionary *) getPageData {
    
    return data;
}

- (void) initNavControlWithUser:(UITextField *) txtUserName
                       Password:(UITextField *) txtPassword
                      Emergency: (UIButton *) emergency
                         SignIn:(UIButton *) signIn {
    
    username        = txtUserName;
    password        = txtPassword;
    btnEmergency    = emergency;
    btnSignIn         = signIn;
    
    [self initEmergencyAndHelpButton];
    
    /**
     * Initialize the navigation controller
     */
    self.navigation  = [[Navigation alloc] initWithViewController:self.controller PageData:data];
    
    [self sendLogtoServer];
}

#pragma mark - Log Send -
/**
 * Background thread for error log dump forward to Server
 */
- (void) sendLogtoServer {
    
    [NSThread detachNewThreadSelector:@selector(backgroundLogSend) toTarget:self withObject:nil];
}

- (void) backgroundLogSend {
    
    // Dummps error log to the server
    [Util sendErrorLog];
}

- (void) initEmergencyAndHelpButton {
    
    int btnEmergencyYPos = 315;
    int intSignInYPos = 270;
    if ([UIScreen mainScreen].bounds.size.height==568) {
        NSLog(@"\n\n\n4 inch retina\n\n\n");
        btnEmergencyYPos    = 395;
        intSignInYPos       = 320;
    }
    
    btnEmergency = [[UIButton alloc] initWithFrame:CGRectMake(67, btnEmergencyYPos, 192, 44)];
    [btnEmergency setImage:[Util getImageAtDocDirWithName:@"emergency-number.png"] forState:UIControlStateNormal];
    [btnEmergency addTarget:self action:@selector(btnEmergencyCallPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.controller.view addSubview:btnEmergency];
    
    btnSignIn = [[UIButton alloc] initWithFrame:CGRectMake(115, intSignInYPos, 96, 32)];
    [btnSignIn setImage:[Util getImageAtDocDirWithName:@"sign-in.png"] forState:UIControlStateNormal];
    [btnSignIn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    [self.controller.view addSubview:btnSignIn];
    
    NSArray *lists = data[PAGE_TYPE_LISTS][PAGE_TYPE_LIST];
    NSDictionary *controlData = nil;
    for (int i=0; i<[lists count]; i++) {
        NSDictionary *list = lists[i];
        if ([list[@"type"] isEqualToString:@"button"] && [list[@"name"] isEqualToString:@"emergency-call"]) {
            controlData = list;
            strEmergencyNumber = controlData[@"number"];
            break;
        }
    }
    
//    This code gives button fall effect during back transition
//    UIImage *imgTopbarRight = [Util getImageAtDocDirWithName:@"sign-in.png"];
//    btnSignIn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnSignIn setImage:imgTopbarRight forState:UIControlStateNormal];
//    btnSignIn.frame = CGRectMake(130, intSignInYPos, imgTopbarRight.size.width, imgTopbarRight.size.height);
//    [btnSignIn addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
//    [view.view addSubview:btnSignIn];
}

- (void) hideKeyboard {
    
    [username resignFirstResponder];
    [password resignFirstResponder];
}

- (void) clearLoginCredentials {
    
    username.text = @"";
    password.text = @"";
}

- (void) clearPassword {
    password.text = @"";
}

- (NSString *) getEmergencyNumber {
    
    return strEmergencyNumber;
}

- (void) btnEmergencyCallPressed {
    
    NSURL *callNumber = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", strEmergencyNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:callNumber]) {
        [[UIApplication sharedApplication] openURL:callNumber];
    } else {
        [Util featureNotSupported];
    }
}

- (void) signIn {
    
    [self hideKeyboard];
    
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self.controller AndModel:self];
    [[Holder sharedHolder].auth requestSignInWithUsername:username.text AndPassword:password.text];
}

- (void) resetMPin {
    
    [self hideKeyboard];
    
    [Holder sharedHolder].auth = [[Authentication alloc] initWithViewController:self.controller AndModel:self];
    [[Holder sharedHolder].auth requestSignInWithUsername:username.text AndPassword:password.text];
}

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData {
    
    NSLog(@"SignIn response: %@", responseData);
    [[Holder sharedHolder].auth postLoginAuthenticationWithData:responseData];
}

- (void) dealloc {
    
    self.navigation = nil;
}

@end
