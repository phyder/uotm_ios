//
//  OfferListCell.m
//  Universe on the move
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "CarOfferListCell.h"

#import "AppConnect.h"
#import "Constant.h"
#import "Util.h"

@implementation CarOfferListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    UIView *bgColorView = [[UIView alloc] init];
    NSArray *color = [[Util getSettingUsingName:LIST_TOUCHUP_COLOR] componentsSeparatedByString:@"|"];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    gradient.colors = @[(id)[[Util colorWithHexString:color[0] alpha:@"0.75f"] CGColor], (id)[[Util colorWithHexString:color[1] alpha:@"0.75f"] CGColor]];
    [bgColorView.layer insertSublayer:gradient atIndex:0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void) loadCellWithData:(NSDictionary *) data AndCSS:(NSDictionary *) css {
    
    float verticalPadding;
    NSDictionary *lblCSS            = [Util getLabelCSS:css[@"title-css"]];
    _lblOfferTitle.text             = data[CAR_OFFER_LIST_TITLE];
    _lblOfferTitle.textColor        = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    _lblOfferTitle.font             = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferTitle setNumberOfLines:0];
    [_lblOfferTitle sizeToFit];
    
    verticalPadding                 = _lblOfferTitle.frame.origin.y+_lblOfferTitle.frame.size.height;
    lblCSS                          = [Util getLabelCSS:css[@"sub-title-css"]];
    _lblOfferSubTitle.text          = data[CAR_OFFER_LIST_SUBTITLE];
    _lblOfferSubTitle.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    _lblOfferSubTitle.font          = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferSubTitle setNumberOfLines:0];
    [_lblOfferSubTitle sizeToFit];
    _lblOfferSubTitle.frame         = CGRectMake(_lblOfferSubTitle.frame.origin.x, verticalPadding, _lblOfferSubTitle.frame.size.width, _lblOfferSubTitle.frame.size.height);
    
    verticalPadding                 = _lblOfferSubTitle.frame.origin.y+_lblOfferSubTitle.frame.size.height;
    lblCSS                          = [Util getLabelCSS:css[@"location-css"]];
    _lblOfferLocation.text          = data[CAR_OFFER_LIST_ADDRESS];
    _lblOfferLocation.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    _lblOfferLocation.font          = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferLocation setNumberOfLines:0];
    [_lblOfferLocation sizeToFit];
    _lblOfferLocation.frame         = CGRectMake(_lblOfferLocation.frame.origin.x, verticalPadding, _lblOfferLocation.frame.size.width, _lblOfferLocation.frame.size.height);
    
    lblCSS                          = [Util getLabelCSS:css[@"validity-css"]];
    _lblOfferValidity.text          = [Util convertDateFromString:data[CAR_OFFER_LIST_VALIDITY]
                                                         WithType:MSSQL_TO_NSDATE_DATE_CONVERSION];
    _lblOfferValidity.textColor     = [Util colorWithHexString:lblCSS[@"font-color"]
                                                         alpha:@"1.0f"];
    _lblOfferValidity.font          = [Util fontWithName:lblCSS[@"font-name"]
                                                   style:lblCSS[@"font-style"]
                                                    size:lblCSS[@"font-size"]];
    [_lblOfferValidity setNumberOfLines:0];
    [_lblOfferValidity sizeToFit];
    _lblOfferValidity.frame     = CGRectMake(_lblOfferValidity.frame.origin.x, verticalPadding, _lblOfferValidity.frame.size.width, _lblOfferValidity.frame.size.height);
    
    if ([data[CAR_OFFER_LIST_RECENT] intValue]==0) {
        self.imgRecentImage.hidden  = YES;
    } else if ([data[CAR_OFFER_LIST_RECENT] intValue]==1) {
        self.imgRecentImage.hidden  = NO;
        self.imgRecentImage.image   = [Util getImageAtDocDirWithName:@"star.png"];
    }
    
    if (self.imgLeftImage.image == nil) {
        
        self.imgLeftImage.animationImages = @[[Util getImageAtDocDirWithName:@"loader1.png"],
                                               [Util getImageAtDocDirWithName:@"loader2.png"],
                                               [Util getImageAtDocDirWithName:@"loader3.png"],
                                               [Util getImageAtDocDirWithName:@"loader4.png"]];
        self.imgLeftImage.animationDuration = 0.8;
        [self.imgLeftImage startAnimating];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getImageForImageName:data[CAR_OFFER_LIST_IMAGE]];
        });   
    }
}

- (void) getImageForImageName:(NSString *) imageName {
    
    AppConnect *conn = [[AppConnect alloc] init];
    [conn getListImageWithName:imageName AfterFinishCall:@selector(getImageFinishedWithData:) onObject:self];
}

- (void) getImageFinishedWithData:(NSData *) data {
    
    [_imgLeftImage stopAnimating];
    _imgLeftImage.image         = [UIImage imageWithData:data];//[[UIImage alloc] initWithData:data];
    //NSLog(@"image holder: %@", data);
}

+ (float) getHeightForCellAt:(NSIndexPath *) indexPath withData:(NSDictionary *) data AndCSS:(NSDictionary *)css {
    
    NSString *text                  = data[CAR_OFFER_LIST_TITLE];
    NSDictionary *lblCSS            = [Util getLabelCSS:css[@"title-css"]];
    float titleHeight               = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                              style:lblCSS[@"font-style"]
                                                                               size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){220.0f, CGFLOAT_MAX}
                                                   lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                            = data[CAR_OFFER_LIST_SUBTITLE];
    lblCSS                          = [Util getLabelCSS:css[@"sub-title-css"]];
    float subTitleHeight            = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                              style:lblCSS[@"font-style"]
                                                                               size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){220.0f, CGFLOAT_MAX}
                                                   lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                            = data[CAR_OFFER_LIST_ADDRESS];
    lblCSS                          = [Util getLabelCSS:css[@"location-css"]];
    float locationHeight            = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                              style:lblCSS[@"font-style"]
                                                                               size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){110.0f, CGFLOAT_MAX}
                                                   lineBreakMode:NSLineBreakByWordWrapping].height;
    
    text                            = [Util convertDateFromString:data[CAR_OFFER_LIST_VALIDITY]
                                                         WithType:MSSQL_TO_NSDATE_DATE_CONVERSION];
    lblCSS                          = [Util getLabelCSS:css[@"validity-css"]];
    float validityHeight            = (float) [text sizeWithFont:[Util fontWithName:lblCSS[@"font-name"]
                                                                              style:lblCSS[@"font-style"]
                                                                               size:lblCSS[@"font-size"]]
                                               constrainedToSize: (CGSize){100.0f, CGFLOAT_MAX}
                                                   lineBreakMode:NSLineBreakByWordWrapping].height;
    
    //NSLog(@"Height: title %f, sub-title: %f, %f %f", titleHeight, subTitleHeight, locationHeight, validityHeight);
    return (10 + titleHeight + subTitleHeight + MAX(locationHeight, validityHeight)+10);
}

@end
