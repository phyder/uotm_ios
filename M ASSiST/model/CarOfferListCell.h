//
//  OfferListCell.h
//  Universe on the move
//
//  Created by Suraj on 15/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CarOfferListCell : UITableViewCell {
    
    float cellHeight;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgLeftImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgRecentImage;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferSubTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferValidity;

- (void) loadCellWithData:(NSDictionary *) data AndCSS:(NSDictionary *) css;

+ (float) getHeightForCellAt:(NSIndexPath *) indexPath withData:(NSDictionary *) data AndCSS:(NSDictionary *)css;

@end
