//
//  OfferDetail.m
//  Home Offer
//
//  Created by Suraj on 25/03/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "HomeOfferDetail.h"

#import "Constant.h"
#import "Util.h"

@implementation HomeOfferDetail

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithData:(NSMutableDictionary *) data UIViewController:(UIViewController *) view {
    
    self = [super init];
    if (self) {
        
        // Initialization code
        _data = data;
        _controller = view;
        //NSLog(@"data: %@", _data);
        
        NSString *stringFromDate = [Util convertDateFromString:_data[DETAIL_OFFER_END_DATE]
                                                      WithType:MSSQL_TO_NSDATE_DATE_CONVERSION];
        //NSLog(@"date: %@, %@", startDate, stringFromDate);
        
        imageMore       = [Util getImageAtDocDirWithName:@"more.png"];
        imageContactPerson = [Util getImageAtDocDirWithName:@"contact.png"];
        imageContactNo  = [Util getImageAtDocDirWithName:@"phone.png"];
        
        strOfferID      = self.data[DETAIL_OFFER_ID];
        strSubtitle     = self.data[DETAIL_OFFER_SUB_TITLE];
        strRecent       = self.data[DETAIL_OFFER_RECENT];
        
        //strips the characters '\r\n' & <BR> in more content
        strDetail       = [Util handleNewLine:self.data[DETAIL_OFFER_DETAIL]];
        
        shortDetail     = [NSString stringWithFormat:@"%@ ...", [strDetail substringToIndex:40]];
        offervalidity   = [NSString stringWithFormat:@"Valid till: %@", stringFromDate];
        offer           = [NSString stringWithFormat:@"ICICI employee %@", self.data[DETAIL_OFFER_OFFER]];
        location        = [NSString stringWithFormat:@"Location: %@", self.data[DETAIL_OFFER_CONTACT_LOCATION]];
        
        strContactPerson= self.data[DETAIL_OFFER_CONTACT_NAME];
        if ([strContactPerson isEqualToString:@"-"] || [strContactPerson isEqualToString:EMPTY]) {
            strContactPerson = NOT_AVAILABLE;
        }
        strContactNo1   = self.data[DETAIL_OFFER_CONTACT_NO_1];
        if ([strContactNo1 isEqualToString:@"-"] || [strContactNo1 isEqualToString:EMPTY]) {
            strContactNo1 = NOT_AVAILABLE;
        }
        strContactNo2   = self.data[DETAIL_OFFER_CONTACT_NO_2];
        if ([strContactNo2 isEqualToString:@"-"] || [strContactNo2 isEqualToString:EMPTY]) {
            strContactNo2 = NOT_AVAILABLE;
        }
        strContactURL   = self.data[DETAIL_OFFER_CONTACT_URL];
        if ([strContactURL isEqualToString:@"-"] || [strContactURL isEqualToString:EMPTY]) {
            strContactURL = NOT_AVAILABLE;
        }
        
        subTitleHeight      = (float) [_data[DETAIL_OFFER_SUB_TITLE] sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17.0] constrainedToSize: (CGSize){240.0f, CGFLOAT_MAX} lineBreakMode:NSLineBreakByWordWrapping].height;
        
        detailHeight        = (float) [shortDetail sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0] constrainedToSize: (CGSize){180.0f, CGFLOAT_MAX} lineBreakMode:NSLineBreakByWordWrapping].height;
        
        validityheight      = (float) [offervalidity sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0] constrainedToSize: (CGSize){260, CGFLOAT_MAX} lineBreakMode:NSLineBreakByWordWrapping].height;
        
        locationheight      = (float) [location sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0] constrainedToSize: (CGSize){260, CGFLOAT_MAX} lineBreakMode:NSLineBreakByWordWrapping].height;
        //NSLog(@"location height: %f", locationheight);
        
        offerHeight         = (float) [offer sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0] constrainedToSize: (CGSize){260, CGFLOAT_MAX} lineBreakMode:NSLineBreakByWordWrapping].height;
        
        contactPersonHeight = (float) [strContactPerson sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                                   constrainedToSize: (CGSize){225, CGFLOAT_MAX}
                                                       lineBreakMode:NSLineBreakByWordWrapping].height;
        contactNumberheight = (float) [strContactNo1 sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                                constrainedToSize: (CGSize){225, CGFLOAT_MAX}
                                                    lineBreakMode:NSLineBreakByWordWrapping].height;
        contactURLHeight    = (float) [strContactURL sizeWithFont:[UIFont fontWithName:@"Helvetica" size:13.0]
                                                constrainedToSize: (CGSize){260, CGFLOAT_MAX}
                                                    lineBreakMode:NSLineBreakByWordWrapping].height;
        
        heightCell1         = 15 + subTitleHeight + 15 + detailHeight+10;
        //NSLog(@"cell 1 title: %f subtitle: %f", subTitleHeight, detailHeight);
        heightCell2         = 15 + validityheight + 10;
        heightCell3         = 15 + locationheight + 10;
        heightCell4         = 15 + offerHeight + 10;
        heightCell5         = 15 + contactPersonHeight + 15 + contactNumberheight + 15 + contactURLHeight + 10;
        //NSLog(@"cell 4 person: %f number: %f url: %f", contactPersonHeight, contactNumberheight, contactURLHeight);
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) btnMorePressed {
    
    NSLog(@"%s", __FUNCTION__);
    
    UIAlertView *detailData = [[UIAlertView alloc] initWithTitle:@"Home Offer - More Details"
                                                         message:strDetail
                                                        delegate:nil
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:nil, nil];
    [detailData show];
}

- (NSString *) getOfferId {
    return strOfferID;
}

- (void) getCellOneWithCell: (HomeOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblSubTitle.text       = strSubtitle;
    cell.lblSubTitle.textColor  = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblSubTitle setNumberOfLines:0];
    [cell.lblSubTitle sizeToFit];
    
    cell.lblDetail.text         = shortDetail;
    cell.lblDetail.textColor    = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblDetail setNumberOfLines:0];
    [cell.lblDetail sizeToFit];
    
    cell.lblDetail.frame    = CGRectMake(cell.lblDetail.frame.origin.x, cell.lblSubTitle.frame.origin.y + cell.lblSubTitle.frame.size.height+15, cell.lblDetail.frame.size.width, cell.lblDetail.frame.size.height);
    
    [cell.btnMore setBackgroundImage:imageMore forState:UIControlStateNormal];
    [cell.btnMore addTarget:self action:@selector(btnMorePressed) forControlEvents:UIControlEventTouchUpInside];
    cell.btnMore.frame      = CGRectMake(cell.btnMore.frame.origin.x, cell.lblSubTitle.frame.origin.y + cell.lblSubTitle.frame.size.height+15, cell.btnMore.frame.size.width, cell.btnMore.frame.size.height);
    
    if ([strRecent intValue]==1) {
        self.imgNewOffer.image  = [Util getImageAtDocDirWithName:@"star.png"];
        cell.imgNewOffer.hidden = NO;
    } else if ([strRecent intValue]==0) {
        cell.imgNewOffer.hidden = YES;
    }
}

- (void) getCellTwoWithCell: (HomeOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblValidity.text       = offervalidity;
    cell.lblValidity.textColor  = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
}

- (void) getCellThreeWithCell: (HomeOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblLocation.text       = location;
    cell.lblLocation.textColor  = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblLocation setNumberOfLines:0];
    [cell.lblLocation sizeToFit];
}

- (void) getCellFourWithCell:(HomeOfferDetail *) cell {
    
    // Configure the cell...
    cell.lblOffer.text          = offer;
    cell.lblOffer.textColor     = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
    [cell.lblOffer setNumberOfLines:0];
    [cell.lblOffer sizeToFit];
}

- (void) getCellFiveWithCell: (HomeOfferDetail *) cell {
    
    if ([strContactPerson isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactPerson.text      = EMPTY;
    } else {
        cell.lblContactPerson.text      = strContactPerson;
        cell.lblContactPerson.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        cell.imgContactPerson.image     = imageContactPerson;
    }
    
    if ([strContactNo1 isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactNumber.text = EMPTY;
        cell.lblContactNumber.userInteractionEnabled = NO;
    } else {
        cell.lblContactNumber.text      = strContactNo1;
        cell.lblContactNumber.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        
        UITapGestureRecognizer *tapGestureRecognizer    = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(contactNumberTapped)];
        tapGestureRecognizer.numberOfTapsRequired       = 1;
        [cell.lblContactNumber addGestureRecognizer:tapGestureRecognizer];
        cell.lblContactNumber.userInteractionEnabled    = YES;
        
        cell.lblContactNumber.frame     = CGRectMake(cell.lblContactNumber.frame.origin.x, cell.lblContactPerson.frame.origin.y + cell.lblContactPerson.frame.size.height+10, cell.lblContactNumber.frame.size.width, cell.lblContactNumber.frame.size.height);
    }
    
    if ([strContactNo2 isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactNumber2.text = EMPTY;
        cell.lblContactNumber2.userInteractionEnabled = NO;
    } else {
        cell.lblContactNumber2.text      = strContactNo2;
        cell.lblContactNumber2.textColor = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        
        UITapGestureRecognizer *tapGestureRecognizer2   = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(contactNumber2Tapped)];
        tapGestureRecognizer2.numberOfTapsRequired       = 1;
        [cell.lblContactNumber2 addGestureRecognizer:tapGestureRecognizer2];
        cell.lblContactNumber2.userInteractionEnabled    = YES;
        
        cell.lblContactNumber2.frame     = CGRectMake(cell.lblContactNumber2.frame.origin.x, cell.lblContactPerson.frame.origin.y + cell.lblContactPerson.frame.size.height+10, cell.lblContactNumber2.frame.size.width, cell.lblContactNumber2.frame.size.height);
    }
    
    if (!([strContactNo1 isEqualToString:NOT_AVAILABLE] && [strContactNo2 isEqualToString:NOT_AVAILABLE])) {
        cell.imageContactNumber.image   = imageContactNo;
    }
    
    if ([strContactURL isEqualToString:NOT_AVAILABLE]) {
        cell.lblContactURL.text = EMPTY;
        cell.lblContactURL.userInteractionEnabled = NO;
    } else {
        
        cell.lblContactURL.text         = strContactURL;
        cell.lblContactURL.textColor    = [Util colorWithHexString:[Util getSettingUsingName:LIST_TOUCHUP_SINGLE_COLOR] alpha:@"1.0f"];
        cell.lblContactURL.frame        = CGRectMake(cell.lblContactURL.frame.origin.x, cell.lblContactNumber.frame.origin.y + cell.lblContactNumber.frame.size.height+10, cell.lblContactURL.frame.size.width, cell.lblContactURL.frame.size.height);
        [cell.lblContactURL setNumberOfLines:0];
        [cell.lblContactURL sizeToFit];
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openLink)];
        // if labelView is not set userInteractionEnabled, you must do so
        [cell.lblContactURL setUserInteractionEnabled:YES];
        [cell.lblContactURL addGestureRecognizer:gesture];
    }
}

- (void) openLink {
    
    NSString *strWebURL = strContactURL;
    if ([strWebURL rangeOfString:@"http"].location==NSNotFound) {
        strWebURL = [NSString stringWithFormat:@"http://%@", strContactURL];
    }
    strWebURL = [strWebURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strWebURL]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strWebURL]];
    } else {
        NSLog(@"Unable to open %@", strWebURL);
    }
}

- (float) getCellOneHeight {
    return heightCell1;
}

- (float) getCellTwoHeight {
    return heightCell2;
}

- (float) getCellThreeHeight {
    return heightCell3;
}

- (float) getCellFourHeight {
    return heightCell4;
}

- (float) getCellFiveHeight {
    return heightCell5;
}

- (void) contactNumberTapped {
    [self callWithNumber:strContactNo1];
}

- (void) contactNumber2Tapped {
    [self callWithNumber: strContactNo2];
}

- (void) callWithNumber:(NSString *) number {
    
    NSLog(@"number tapped: %@", number);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", number]]];
}

@end
