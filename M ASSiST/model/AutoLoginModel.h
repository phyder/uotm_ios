//
//  AutoLoginModel.h
//  Universe on the move
//
//  Created by Phyder on 20/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EzTokenSDK.h"
#import "MBProgressHUD.h"


@class Navigation;

@interface AutoLoginModel : NSObject<UIAlertViewDelegate,EzTouchDelegate,MBProgressHUDDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate> {
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button
    UIButton *btnEmergency, *btnSignIn, *btnResetMPin, *btnFingerScan;
    NSString *strEmergencyNumber;   // Stores Emergency number;
    
    UITextField *username, *mPin1, *mPin2, *mPin3, *mPin4;
    
    NSDictionary *responseJSONData;
    NSString *strDownlaodURL;
}

@property (nonatomic, weak) MBProgressHUD *progressBar;
@property (nonatomic, weak) UIViewController *controller;        // Controller view reference
@property (nonatomic) Navigation *navigation;

- (void) clearLoginCredentials;

- (NSString *) getEmergencyNumber;

- (void) initDataWithView:(UIViewController *) controllerView;  // Init model for the controller

- (NSDictionary *) getPageData;

- (void) initNavControlWithUser:(UITextField *) txtUserName
                          MPin1:(UITextField *) txtMPin1
                          MPin2:(UITextField *) txtMPin2
                          MPin3:(UITextField *) txtMPin3
                          MPin4:(UITextField *) txtMPin4
                      Emergency: (UIButton *) emergency
                         SignIn:(UIButton *) signIn
                      ResetMPin:(UIButton *) resetMPin;

- (void) signIn;

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData;
-(void) call;
@end
