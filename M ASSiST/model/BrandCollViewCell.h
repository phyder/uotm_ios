//
//  BrandCollViewCell.h
//  Universe on the move
//
//  Created by Suraj on 16/04/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import "PSTCollectionView.h"

@interface BrandCollViewCell : PSUICollectionViewCell {
    
}

@property (strong, nonatomic) IBOutlet UIImageView *imgBrandImage;
@property (strong, nonatomic) IBOutlet UIButton *btnBrandRecentCount;

- (void) initWithData: (NSDictionary *) data;

@end
