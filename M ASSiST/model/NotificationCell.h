//
//  NotificationCell.h
//  engage
//
//  Created by Suraj on 22/12/12.
//  Copyright (c) 2012 Suraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface NotificationCell : UITableViewCell {
    
    CGFloat cellHeight;
    NSDictionary *cellData, *cellCSS;
}

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblSubTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDesc;
@property (nonatomic, weak) IBOutlet UILabel *lblActionDate;
@property (nonatomic, weak) IBOutlet UIImageView *imgRightDetails;

@property (nonatomic) IBOutlet NSString *details;
@property (nonatomic) IBOutlet NSString *pageName;
@property (nonatomic) IBOutlet NSString *url;
@property (nonatomic) IBOutlet NSString *params;

- (UITableViewCell *) cellForRowAtIndexPath:(NSIndexPath *)indexPath
                                   WithData:(NSDictionary *) data
                                        CSS:(NSDictionary *) css
                                     Height:(float) height;

@end
