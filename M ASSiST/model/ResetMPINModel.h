//
//  ResetMPINModel.h
//  Universe on the move
//
//  Created by Phyder on 21/07/15.
//  Copyright (c) 2015 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Navigation;

@interface ResetMPINModel : NSObject <UIAlertViewDelegate> {
    
    NSDictionary *data;         // Current page data
    NSDictionary *css;          // Current page css data
    NSDictionary *buttonCss;    // css data of navigation button
    UIButton *btnSubmit;
    
    UITextField *userName,*password,*newMPin1,*newMPin2,*newMPin3,*newMPin4, *retypeMPin1,*retypeMPin2,*retypeMPin3,*retypeMPin4;
    
    NSDictionary *responseJSONData;
    NSString *strDownlaodURL;
}

@property (nonatomic, weak) UIViewController *controller;        // Controller view reference
@property (nonatomic) Navigation *navigation;

- (void) clearCredentials;

- (void) initDataWithView:(UIViewController *) controllerView;  // Init model for the controller

- (NSDictionary *) getPageData;

- (void) initNavControlWithUserName:(UITextField *) txtUserName
                           password:(UITextField *) txtPassword
                           newMPin1:(UITextField *) txtNewMPin1
                           newMPin2:(UITextField *) txtNewMPin2
                           newMPin3:(UITextField *) txtNewMPin3
                           newMPin4:(UITextField *) txtNewMPin4
                        retypeMPin1:(UITextField *) txtRetypeMPin1
                        retypeMPin2:(UITextField *) txtRetypeMPin2
                        retypeMPin3:(UITextField *) txtRetypeMPin3
                        retypeMPin4:(UITextField *) txtRetypeMPin4
                             Submit:(UIButton *) submit;

- (void) resetMPin;

- (void) postAuthenticationValidationWithResData:(NSMutableDictionary *) responseData;

@end
