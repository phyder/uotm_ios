//
//  Authentication.h
//  Universe on the move
//
//  Created by Suraj on 03/08/13.
//  Copyright (c) 2013 CMSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppConnect;
@class LoginModel;
@class RegisterModel;
@class OTPModel;
@class MPinModel;
@class ResetMPINModel;
@class AutoLoginModel;

@interface Authentication : NSObject<UIAlertViewDelegate> {
    
    UIViewController *controller;               // Reference of the controller on which it is instanctiated
    NSObject *model;                            // Reference of the model on which it is instanctiated
    LoginModel *loginModel;                   // Reference of the model to implement reverse method call
    RegisterModel *registerModel;                   // Reference of the model to implement reverse method call
    OTPModel *otpModel;                   // Reference of the model to implement reverse method call
    MPinModel *mPinModel;                   // Reference of the model to implement reverse method call
    AutoLoginModel *autoLoginModel;                   // Reference of the model to implement reverse method call
    ResetMPINModel *resetMPinModel;                   // Reference of the model to implement reverse method call
    
    NSMutableDictionary *serverResponseData;    // Server response data reference
    NSString *strDownlaodURL;                   // New app download location used newer version download available
    
    UIAlertView *signout, *updateAlert, *resetAlert, *registerAlert, *setMPinAlert;
    NSString *strUsername, *strPassword, *strMPin, *strDeregDeviceId;
    
    BOOL isToggle;
}

@property (nonatomic) AppConnect *conn;

// Initializes the Authorzation instance with its respective controller & model
- (id) initWithViewController:(UIViewController *) viewController AndModel:(NSObject *) controllerModel;

// Request for Register with username & password
- (BOOL)requestRegisterWithUsername:(NSString *)username AndPassword:(NSString *)password;

// Request for OTP with otp
- (BOOL)requestOTPWithOtp:(NSString *)otp;

// Request for setting MPIN with mpin
- (BOOL)requestSetMPinWithNewMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin;

// Request for Auto Login with username and mpin
- (BOOL)requestAutoLoginWithUserName:(NSString *)userName mPin:(NSString *)mPin bio:(NSString *)bio;

// Request for Reset Mpin with username , password mpin and retypempin
- (BOOL)requestResetMPinWithUserName:(NSString *)userName password:(NSString *)password NewMPin:(NSString *)newMPin retypeMPin:(NSString *)retypeMPin;

// Request for Sign In with username & password
- (BOOL) requestSignInWithUsername:(NSString *) username AndPassword:(NSString *) password;

// Request for Sign Out
- (void) requestSignOutWithMenuToggleCheck:(BOOL) isMenuCall;

// Request for Register Device List
- (BOOL)requestGetRegisterListWithUsername:(NSString *)username;

// Request for Deregister
- (BOOL)requestDeregisterWithUsername:(NSString *)username deviceId:(NSString *)deviceId;

// Request for Verify MPin
- (BOOL)requestVerifyMPinWithUserName:(NSString *)userName MPin:(NSString *)mPin;

// Request for ESOP
- (BOOL)requestGetESOP;

- (void) sessionExpired:(NSDictionary *) responseData;

// implements post registration Authentication with data received from the server
- (BOOL)postRegisterAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post otp confirmation Authentication with data received from the server
- (BOOL)postOTPAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post mpin Authentication with data received from the server
- (BOOL)postMPinAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post auto login Authentication with data received from the server
- (BOOL)postAutoLoginAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post reset mpin Authentication with data received from the server
- (BOOL)postResetMPinAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post login Authentication with data received from the server
- (BOOL) postLoginAuthenticationWithData:(NSMutableDictionary *) responseData;

// implements post register device list with data received from the server
- (BOOL)postGetRegisterListAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post Deregister with data received from the server
- (BOOL)postDeregisterAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post Verify with data received from the server
- (BOOL)postVerifyMPinAuthenticationWithData:(NSMutableDictionary *)responseData;

// implements post ESOP with data received from the server
- (BOOL)postESOPAuthenticationWithData:(NSMutableDictionary *)responseData;


- (void) startBackgroundNotificationCheck;
@end
