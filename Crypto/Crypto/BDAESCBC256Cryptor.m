//
//  Created by Patrick Hogan on 10/12/12.
//


////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public Interface
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#import "BDAESCBC256Cryptor.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Utilities
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>

#import "NSData+Base64.h"
#import "NSString+Base64.h"

#import "BDCryptorError.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Implementation
////////////////////////////////////////////////////////////////////////////////////////////////////////////
@implementation BDAESCBC256Cryptor


- (NSString *)encrypt:(NSString *)plainText
                  key:(NSString *)key
                   iv:(NSData *)iv
                error:(BDError *)error
{
    if (!plainText)
    {
        [error addErrorWithType:BDCryptoErrorEncrypt
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    if (plainText.length > [self maximumBlockSize])
    {
        [error addErrorWithType:BDCryptoErrorAESPlainTextSize
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    NSMutableString *pad = [NSMutableString string];
    int padLength = 16 - plainText.length % 16;
    for (int i = 0; i<padLength; ++i) {
        [pad appendString:@" "];
    }
    
    plainText = [NSString stringWithFormat:@"%@%@", plainText, pad];
    
//    NSLog(@"\n\nPlain Text: %@, Key: %@, iv: %@\n\n", plainText, key, iv);
    NSData *endataCheck = [[self class] doCipher:[plainText dataUsingEncoding:NSUTF8StringEncoding] iv:iv key:[key dataUsingEncoding:NSUTF8StringEncoding] context:kCCEncrypt];
    NSString *result = [endataCheck base64EncodedString];
    
    return result;
}


- (NSString *)decrypt:(NSString *)cipherText
                  key:(NSString *)key
                   iv:(NSData *)iv
                error:(BDError *)error
{
    if (!cipherText)
    {
        [error addErrorWithType:BDCryptoErrorDecrypt
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
//    NSLog(@"\n\nCipher Text: %@ decoded: %@ Key: %@, iv: %@\n\n", cipherText, [cipherText base64DecodedData], key, iv);
    NSData *dedataCheck = [[self class] doCipher:[cipherText base64DecodedData]
                                              iv:iv
                                             key:[key dataUsingEncoding:NSUTF8StringEncoding]
                                         context:kCCDecrypt];
    
//    NSLog(@"Decrypted data: %@", dedataCheck);
    
    NSString *result = [[NSString alloc] initWithData:dedataCheck encoding:NSUTF8StringEncoding];
//    NSLog(@"decrypted message: %@", result);
//    NSLog(@"length: %d", [result length]);
    
    return result;
}

+ (NSData *)doCipher:(NSData *)dataIn
                  iv:(NSData *)iv
                 key:(NSData *)symmetricKey
             context:(CCOperation)encryptOrDecrypt
{
    CCCryptorStatus ccStatus   = kCCSuccess;
    size_t          cryptBytes = 0;    // Number of bytes moved to buffer.
    NSMutableData  *dataOut    = [NSMutableData dataWithLength:dataIn.length + kCCBlockSizeAES128];
    
    ccStatus = CCCrypt( encryptOrDecrypt,
                       kCCAlgorithmAES128,
                       kCCOptionPKCS7Padding,
                       symmetricKey.bytes,
                       kCCKeySizeAES128,
                       iv.bytes,
                       dataIn.bytes,
                       dataIn.length,
                       dataOut.mutableBytes,
                       dataOut.length,
                       &cryptBytes);
    
    if (ccStatus != kCCSuccess) {
        NSLog(@"CCCrypt status: %d", ccStatus);
    }
    
    //dataOut.length = cryptBytes;
    
    return dataOut;
}


- (NSData *)performOperation:(CCOperation)operation
                        data:(NSData *)inputData
                         key:(NSString *)key
                          iv:(NSData *)ivData
                       error:(BDError *)error
{
    NSData *keyData = [[self paddedKey:key] dataUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"\n\n\nKey Data: %@ IV: %@\n\n\n", [[NSString alloc] initWithData:keyData encoding:NSUTF8StringEncoding], [[NSString alloc] initWithData:ivData encoding:NSUTF8StringEncoding]);
    
    CCCryptorRef cryptor = NULL;
    CCCryptorStatus status = kCCSuccess;
    
    uint8_t iv[kCCBlockSizeAES128];
    memset((void *)iv, 0x0, (size_t) sizeof(iv));
    *iv = *(uint8_t*)[ivData bytes];
    
    status = CCCryptorCreate(operation,
                             kCCAlgorithmAES128,
                             kCCOptionPKCS7Padding,
                             [keyData bytes],
                             kCCKeySizeAES128,
                             iv,
                             &cryptor);
    if (status != kCCSuccess)
    {
        [error addErrorWithType:BDCryptoErrorAESCreation
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    size_t bufsize = CCCryptorGetOutputLength(cryptor, (size_t)[inputData length], YES);
    
    void *buf = malloc(bufsize * sizeof(uint8_t));
    memset(buf, 0x0, bufsize);
    
    size_t bufused = 0;
    size_t bytesTotal = 0;
    
    status = CCCryptorUpdate(cryptor,
                             [inputData bytes],
                             (size_t)[inputData length],
                             buf,
                             bufsize,
                             &bufused);
    
    if (status != kCCSuccess)
    {
        free(buf);
        CCCryptorRelease(cryptor);
        [error addErrorWithType:BDCryptoErrorAESUpdate
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    bytesTotal += bufused;
    
    status = CCCryptorFinal(cryptor, buf + bufused, bufsize - bufused, &bufused);
    if (status != kCCSuccess)
    {
        free(buf);
        CCCryptorRelease(cryptor);
        
        [error addErrorWithType:BDCryptoErrorAESFinal
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    bytesTotal += bufused;
    
    CCCryptorRelease(cryptor);
    
    return [NSData dataWithBytesNoCopy:buf
                                length:bytesTotal];
}

- (NSString *)encrypt:(NSString *)plainText
                  key:(NSString *)key
                error:(BDError *)error
{
    if (!plainText)
    {
        [error addErrorWithType:BDCryptoErrorEncrypt
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    if (plainText.length > [self maximumBlockSize])
    {
        [error addErrorWithType:BDCryptoErrorAESPlainTextSize
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    NSString *result = [[self performOperation:kCCEncrypt
                                          data:[plainText dataUsingEncoding:NSUTF8StringEncoding]
                                           key:key
                                         error:error] base64EncodedString];
    
    return result;
}


- (NSString *)decrypt:(NSString *)cipherText
                  key:(NSString *)key
                error:(BDError *)error
{
    if (!cipherText)
    {
        [error addErrorWithType:BDCryptoErrorDecrypt
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    NSString *result = [[NSString alloc] initWithData:[self performOperation:kCCDecrypt
                                                                        data:[cipherText base64DecodedData]
                                                                         key:key
                                                                       error:error]
                                             encoding:NSUTF8StringEncoding];
    
    return result;
}


- (NSData *)performOperation:(CCOperation)operation
                        data:(NSData *)inputData
                         key:(NSString *)key
                       error:(BDError *)error
{
    NSData *keyData = [[self paddedKey:key] dataUsingEncoding:NSUTF8StringEncoding];
    
    CCCryptorRef cryptor = NULL;
    CCCryptorStatus status = kCCSuccess;
    
    uint8_t iv[kCCBlockSizeAES128];
    memset((void *)iv, 0x0, (size_t) sizeof(iv));
    
    status = CCCryptorCreate(operation,
                             kCCAlgorithmAES128,
                             kCCOptionPKCS7Padding,
                             [keyData bytes],
                             kCCKeySizeAES128,
                             iv,
                             &cryptor);
    
    if (status != kCCSuccess)
    {
        [error addErrorWithType:BDCryptoErrorAESCreation
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    size_t bufsize = CCCryptorGetOutputLength(cryptor, (size_t)[inputData length], YES);
    
    void *buf = malloc(bufsize * sizeof(uint8_t));
    memset(buf, 0x0, bufsize);
    
    size_t bufused = 0;
    size_t bytesTotal = 0;
    
    status = CCCryptorUpdate(cryptor,
                             [inputData bytes],
                             (size_t)[inputData length],
                             buf,
                             bufsize,
                             &bufused);
    
    if (status != kCCSuccess)
    {
        free(buf);
        CCCryptorRelease(cryptor);
        [error addErrorWithType:BDCryptoErrorAESUpdate
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    bytesTotal += bufused;
    
    status = CCCryptorFinal(cryptor, buf + bufused, bufsize - bufused, &bufused);
    
    if (status != kCCSuccess)
    {
        free(buf);
        CCCryptorRelease(cryptor);
        
        [error addErrorWithType:BDCryptoErrorAESFinal
                     errorClass:[BDCryptorError class]];
        
        return nil;
    }
    
    bytesTotal += bufused;
    
    CCCryptorRelease(cryptor);
    
    return [NSData dataWithBytesNoCopy:buf
                                length:bytesTotal];
}


- (NSString *)paddedKey:(NSString *)key
{
    NSString *result = key;
    while (result.length < [self keySize])
    {
        result = [result stringByAppendingString:@" "];
    }
    
    result = [result substringWithRange:NSMakeRange(0, [self keySize])];
    
    return result;
}


- (NSInteger)keySize
{
    return 16;
}


@end
